## Facha - The Facial Nerve Surgical Monitoring Device ##


### Introduction ###

Facha - The Facial Nerve Surgical Monitoring Device is designed to
prevent damage to the patient's facial nerve during Ear, Nose,
and Throat (ENT) surgeries. The device is particularly useful
when the nerve is located close to the operating area,
or in cases where its morphology makes it challenging
to assess the risk prior to surgery.

### The solution ###

This solution enables the surgeon to monitor the proximity of the
surgical cutting tool to the facial nerve by measuring
myoelectric response.

The surgeon stimulates with a controlled electrical current the
operating area, where a myoelectric response is
simultaneously measured. The surgeon then uses heuristics based
on the shape of the signal to estimate the distance between the
surgical cutting tool and the facial nerve.

### The device ###

The solution is comprised of:
(a) an application on a laptop running Windows or MacOS X,
(b) an external unit that amplifies the myoelectric signals and sends
the signal samples to the laptop,
(c) an electric current pulse stimulator.

The pulse stimulator produces an electrical current with
controlled width and intensity, which is injected into the
patient's operating area using the surgical cutting tool.

The signal is measured from the muscle's contracting response of the
orbicularis oris and orbicularis oculi muscles.
A metric is obtained heuristically from the shape and size
of the response. Several responses can be selected and
captured during surgery for later analysis.

### The application ###

The application has capabilities similar to those provided
by digital or analog oscilloscopes.
A few control knobs enable the surgeon assistant to control
auto sweep, triggered sweep, variable trigger threshold,
variable sweep velocity, variable trigger hold-off time,
variable sampling rate,
and a persistent signal storage.

The application is written entirely in the Java language.
It uses the JavaFX GUI library for displaying purposes,
and the Java Sound Library to acquire the signal
coming from two separate analog channels.
Those analog channels come through a USB connection between the
laptop and the input amplifiers/samplers.

The application uses real time linear and nonlinear digital filtering
on the signal. The signal processing library is included in
this repository.
The signal conditioning process features the Widrow's adaptive
normalized LMS filter in order to
remove the power line noise from the useful signal, without damaging
its temporal shape.
This task would be impossible using linear filtering,
as the spectrum of the signal and the noise overlap.

### Hardware units ###

The specifications for the biological signal amplifier and
the pulse stimulator are located below:

    ./notes/manuals/Manual*.doc

### Building and deploying the application ###

The repository contains the production source code,
the unit test code and the gradle/bash building scripts.
The build output artifacts consist of both a distribution .zip file
for MacOS X, and an installation executable for Windows.

For building and unit testing, please refer to the documentation below:

    ./notes/software/how-to-build-and-test.md

The source code repository, the continuous integration, and the
download of installation executables are hosted below:

    https://bitbucket.org/claudio_ortega/monitoreo

### License ###

A copy of the license for this project is included in:

    ./LICENSE.txt

### Credits to the original team ###

This project was sponsored by Fundación Arauz - http://www.farauzorl.org.ar,
and driven entirely by an interdisciplinary team, based in Argentina and USA.

    Project medical direction - Santiago Luis Arauz, MD, ENT Surgeon
    Product management and QA - Ing. Marcelo Ortega
    Hardware implementation - Ing. Esteban Castro
    Software construction - Ing. Claudio Ortega

### Contact ###

For contributing to this project, or to use it as is,
please contact me at:

    claudio.alberto.ortega@gmail.com

### Screenshots ###

Monitor screen, complete

![Alt text](notes/software/monitor-1.png?raw=true "Facha screen shot")

Settings screen, upper portion

![Alt text](notes/software/settings-1.png?raw=true "Facha screen shot")

Settings screen, lower portion

![Alt text](notes/software/settings-2.png?raw=true "Facha screen shot")



