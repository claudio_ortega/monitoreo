@echo off

java -version

set version_result=%errorlevel%

if %version_result% == 0 (
    java -Xmx1000m -Xms1000m -cp ..\lib\monitoreo-uber-all-1.0-SNAPSHOT.jar com.poplar.monitor.imp.javafx.MonitorApp
    if not %errorlevel% == 0 pause
)

if not %version_result% == 0 (
    echo *** java is not properly installed ***
    echo *** java no esta instalado correctamente ***
    pause
)


