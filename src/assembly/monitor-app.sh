# for Spanish execute this as
#    ./monitor-app.sh --locale=es

SCRIPT_DIR_NAME="$(dirname $0)"
cd ${SCRIPT_DIR_NAME}

java -Xmx1000m -Xms1000m -cp ../lib/monitoreo-uber-all-1.0-SNAPSHOT.jar com.poplar.monitor.imp.javafx.MonitorApp --locale=us $*
