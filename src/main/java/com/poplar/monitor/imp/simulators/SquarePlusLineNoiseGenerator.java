/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.poplar.monitor.api.SignalSourceBuilder;

public class SquarePlusLineNoiseGenerator extends MultiChannelSignalSourceSimulator
{
    public static SquarePlusLineNoiseGenerator create( SignalSourceBuilder aInBuilder )
    {
        return new SquarePlusLineNoiseGenerator ( aInBuilder );
    }

    private SquarePlusLineNoiseGenerator ( SignalSourceBuilder aInBuilder )
    {
        super(
            aInBuilder,
            ( aInChannel, aInSampleCount, aInSignalSampleSpaceMin, aInSignalSampleSpaceMax, aInSamplingFreqHz ) ->
            {
                final double f0Hz = 3.33;
                final double f1Hz = 50;
                final double a0 = 0.6;
                final double a1 = 0.2;
                final double delta = 0.33;

                final double lRange = aInSignalSampleSpaceMax - aInSignalSampleSpaceMin;
                final double lHalfWay = ( ( aInSignalSampleSpaceMax + aInSignalSampleSpaceMin ) / 2 );

                final double lSquareMin = lHalfWay - lRange * a0 / 2;
                final double lSquareMax = lHalfWay + lRange * a0 / 2;
                final double lSquarePeriodInSamples = aInSamplingFreqHz / f0Hz;
                final double lDeltaInSamples = delta * lSquarePeriodInSamples;

                final double lSquareSignal = squareWave ( aInSampleCount, lSquareMax, lSquareMin, lDeltaInSamples, lSquarePeriodInSamples );

                final double lSinePeriodInSamples = aInSamplingFreqHz / f1Hz;

                final double lSineSignal = ( a1 / 2 ) * lRange * Math.sin( aInSampleCount * 2.0 * Math.PI / lSinePeriodInSamples );

                return (float) ( lSquareSignal + lSineSignal);
            }
        );
    }

    @Override
    public String getDescription ( )
    {
        return "square 3Hz plus 50Hz simulator";
    }

    private static float squareWave( double iSampleIx, double max, double min, double deltaInSamples, double periodInSamples )
    {
        final long sampleIx = ( (long) iSampleIx ) % ( (long) periodInSamples );
        return (float) ( sampleIx < deltaInSamples ? max : min );
    }

    @Override
    public boolean isAvailable ( )
    {
        return true;
    }
}
