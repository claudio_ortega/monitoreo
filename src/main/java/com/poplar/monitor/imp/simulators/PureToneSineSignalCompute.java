/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

public class PureToneSineSignalCompute implements ICompute
{
    private double f1Hz;

    public static PureToneSineSignalCompute create()
    {
        return new PureToneSineSignalCompute ();
    }

    private PureToneSineSignalCompute ( )
    {
        f1Hz = 0;
    }

    public PureToneSineSignalCompute setFreqInHz( double aInF1InHz )
    {
        f1Hz = aInF1InHz;
        return this;
    }

    @Override
    public float compute(
        int aInChannel,
        long aInSampleCount,
        float aInSignalSampleSpaceMin,
        float aInSignalSampleSpaceMax,
        float aInSamplingFreqHz )
    {
        final double lSampleSpaceRange = aInSignalSampleSpaceMax - aInSignalSampleSpaceMin;

        final double lSignal = ( 0.5 * lSampleSpaceRange ) * Math.sin ( 2.0 * Math.PI * f1Hz * ( (double) aInSampleCount / aInSamplingFreqHz ) );

        final int cyclePeriodInSecs = 20;

        final double envelope = ( ( aInSampleCount / aInSamplingFreqHz ) % cyclePeriodInSecs ) < cyclePeriodInSecs/2 ? 1.0 : 1.0;

        final double lRetValue;

        if ( aInChannel == 0 )
        {
            lRetValue = lSignal * envelope;
        }

        else if ( aInChannel == 1 )
        {

            lRetValue = lSignal * envelope;
        }

        else
        {
            throw new IllegalStateException( );
        }

        return (float) lRetValue;
    }
}

