/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

public class CalibrationSignalCompute implements ICompute
{
    private static final double pulsePeriodInSecs = .030;
    private static final double pulseWidthInSecs = .010;

    public static CalibrationSignalCompute create()
    {
        return new CalibrationSignalCompute ();
    }

    private CalibrationSignalCompute()
    {
    }

    @Override
    public float compute(
        int aInChannel,
        long aInSampleCount,
        float aInSignalSampleSpaceMin,
        float aInSignalSampleSpaceMax,
        float aInSamplingFreqHz )
    {
        final double lRetValue;

        if ( aInChannel == 0 )
        {
            lRetValue = ( aInSampleCount % ( pulsePeriodInSecs * aInSamplingFreqHz ) < pulseWidthInSecs * aInSamplingFreqHz ) ? aInSignalSampleSpaceMax : aInSignalSampleSpaceMin;
        }

        else if ( aInChannel == 1 )
        {
            // aiming for ten times per second, as it is expensive to compute System.currentTimeMillis()
            lRetValue = aInSampleCount % 10 == 0 ? 1.0 : 0.0;
        }

        else
        {
            throw new IllegalStateException( );
        }

        return (float) lRetValue;
    }
}

