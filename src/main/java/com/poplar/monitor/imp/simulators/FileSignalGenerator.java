/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.poplar.monitor.api.SignalSourceBuilder;

import java.io.File;

public class FileSignalGenerator extends MultiChannelSignalSourceSimulator
{
    private int nChannels;
    private long samplingFrequency;

    public static FileSignalGenerator create( SignalSourceBuilder aInBuilder )
    {
        final FileSignalCompute fileSignalCompute = FileSignalCompute.create ( aInBuilder );
        return new FileSignalGenerator ( aInBuilder, fileSignalCompute );
    }

    private FileSignalGenerator ( SignalSourceBuilder aInBuilder, FileSignalCompute fileSignalCompute )
    {
        super( aInBuilder, fileSignalCompute );

        nChannels = fileSignalCompute.getNumberOfChannels();
        samplingFrequency = fileSignalCompute.getSamplingFrequencyInHz();
    }

    @Override
    public String getDescription ( )
    {
        final String lRet;

        if ( signalSourceBuilder.getSignalFileOriginType () == SignalSourceBuilder.SignalFileOriginType.internal_1 )
        {
            lRet = "internal file";
        }
        else
        {
            lRet = signalSourceBuilder.getSignalFilePathName () == null ? "signal file n/a" : new File ( signalSourceBuilder.getSignalFilePathName () ).getName ();
        }

        return lRet;
    }

    @Override
    public boolean isAvailable ( )
    {
        return true;
    }

    @Override
    public int getNumberOfChannels()
    {
       return nChannels;
    }

    @Override
    public long getSamplingFrequencyInHz ( )
    {
        return samplingFrequency;
    }
}
