/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.poplar.monitor.api.SignalSourceBuilder;

public class SweepGenerator extends MultiChannelSignalSourceSimulator
{
    public static SweepGenerator create ( SignalSourceBuilder aInBuilder )
    {
        return new SweepGenerator ( aInBuilder );
    }

    private SweepGenerator ( SignalSourceBuilder aInBuilder )
    {
        super(
            aInBuilder,
            ( aInChannel, aInSampleCount, aInSignalSampleSpaceMin, aInSignalSampleSpaceMax, aInSamplingFreqHz ) ->
            {
                final double a1 = 1.0;

                final double lSinePeriodInSamples = aInSamplingFreqHz / getSweepFreqHz ( aInSampleCount, aInSamplingFreqHz );
                final double lSineSignal = ( a1 / 2 ) * ( aInSignalSampleSpaceMax - aInSignalSampleSpaceMin ) * Math.sin( aInSampleCount * 2.0 * Math.PI / lSinePeriodInSamples );

                return (float) ( lSineSignal);
            }
        );
    }

    private static double getSweepFreqHz( long aInSampleCount, float aInSamplingFreqHz )
    {
        // will cycle on 30 seconds from 0Hz to 1KHz
        final double lSweepingCycleInSecs = 600;
        final double lMinSweepingFreqInHz = 20;
        final double lMaxSweepingFreqInHz = 80;

        final long lSweepingCycleInSamples = (long) ( lSweepingCycleInSecs * aInSamplingFreqHz );
        final long lSampleCountModular = aInSampleCount % lSweepingCycleInSamples;
        final double lTimeForThisSampleInSecsModular = lSampleCountModular / aInSamplingFreqHz;

        return lMinSweepingFreqInHz + ( lMaxSweepingFreqInHz - lMinSweepingFreqInHz ) * ( lTimeForThisSampleInSecsModular / lSweepingCycleInSecs );
    }

    @Override
    public String getDescription ( )
    {
        return "0Hz to 1Khz sweeping sine wave simulator";
    }

    @Override
    public boolean isAvailable ( )
    {
        return true;
    }
}
