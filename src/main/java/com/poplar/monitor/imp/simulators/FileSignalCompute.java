/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.javafx.ScreenInfo;
import com.poplar.monitor.imp.util.FileUtil;
import com.poplar.monitor.imp.util.GsonUtil;
import com.poplar.monitor.imp.util.SystemUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class FileSignalCompute implements ICompute
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    private boolean initOK;
    private int signalLengthInSamples;
    private float[][] signal;
    private int nChannels;
    private long samplingFrequency;

    public static FileSignalCompute create( SignalSourceBuilder aInBuilder )
    {
        return new FileSignalCompute ( aInBuilder );
    }

    private FileSignalCompute ( SignalSourceBuilder aInBuilder )
    {
        initOK = false;

        try
        {
            if ( aInBuilder.getSignalFileOriginType ( ) == SignalSourceBuilder.SignalFileOriginType.internal_1 )
            {
                final File tmpFile = FileUtil.getFileFromBaseAndAStringSequence (
                    SystemUtil.getCurrentDirectory ( ),
                    "tmp",
                    "sample_signal.json" );

                FileUtil.deletePlainFileIfExists ( tmpFile, true );

                FileUtil.createDirIfDoesNotExist ( tmpFile.getParentFile ( ) );
                SystemUtil.extractFileFromJarFile ( this.getClass ( ), "sample_signal.json", tmpFile );

                loadFile ( tmpFile );
            }

            else
            {
                final String signalFilePathName = aInBuilder.getSignalFilePathName ( );

                if ( signalFilePathName == null )
                {
                    logger.warn ( "aInBuilder.getSignalFilePathName() is null" );
                }

                else
                {
                    final File signalFilePath = new File ( signalFilePathName );
                    loadFile ( signalFilePath );
                }
            }
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage ( ), ex );
        }
    }

    private void loadFile( File signalFilePath ) throws Exception
    {
        if ( ! signalFilePath.exists ( ) )
        {
            logger.error ( "file: " + signalFilePath + " does not exist" );
        }

        else
        {
            final ScreenInfo screenInfo = GsonUtil.readJsonFromFile ( signalFilePath, ScreenInfo.class );

            float[][] lSignal = screenInfo.getSignal ( );
            signal = lSignal;
            signalLengthInSamples = lSignal.length;

            nChannels = screenInfo.getSignalSourceReadOnlyInfo ().getNumberOfChannels ();
            samplingFrequency = screenInfo.getSignalSourceReadOnlyInfo ().getSamplingFrequencyInHz ();

            initOK = true;
        }
    }

    public int getNumberOfChannels()
    {
        Preconditions.checkState ( initOK );
        return nChannels;
    }

    public long getSamplingFrequencyInHz ( )
    {
        Preconditions.checkState ( initOK );
        return samplingFrequency;
    }

    @Override
    public float compute(
        int aInChannel,
        long aInSampleCount,
        float aInSignalSampleSpaceMin,
        float aInSignalSampleSpaceMax,
        float aInSamplingFreqHz )
    {
        Preconditions.checkState ( initOK );

        final long mod = aInSampleCount % signalLengthInSamples;
        return signal[ (int) mod ][ aInChannel ];
    }
}

