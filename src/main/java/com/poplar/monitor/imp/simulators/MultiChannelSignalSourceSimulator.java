/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.MultiChannelVessel;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.common.AbstractMultiChannelSignalSource;
import com.poplar.monitor.imp.common.ChannelVessel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public abstract class MultiChannelSignalSourceSimulator extends AbstractMultiChannelSignalSource
{
    private final Logger logger = LogManager.getLogger(getClass());
    private final ICompute compute;
    private final AtomicLong lastResetTimeInMSec;
    private final AtomicLong lastResetSampleCount;
    private volatile long currentSampleCount;
    protected final ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;

    public MultiChannelSignalSourceSimulator(
        SignalSourceBuilder aInBuilder,
        ICompute aInCompute )
    {
        super( aInBuilder );

        compute = aInCompute;
        currentSampleCount = 0;
        lastResetTimeInMSec = new AtomicLong( 0 );
        lastResetSampleCount = new AtomicLong( 0 );

        final MultiChannelVessel lSignalVessel = createSignalVessels();

        scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor ( 1 );
        scheduledThreadPoolExecutor.scheduleAtFixedRate (
            ( ) ->
            {
                logger.trace( "scheduledThreadPoolExecutor.run() -- BEGIN" );

                try
                {
                    if ( ! started.get() )
                    {
                        logger.trace("not started, skipping");
                    }

                    else
                    {
                        final long lTimeSinceLastStartInMSec = System.currentTimeMillis() - lastResetTimeInMSec.get();
                        final long lSamplesProducedSinceLastStart = currentSampleCount - lastResetSampleCount.get();
                        final long lSamplesToBeProducedSinceLastStart = (long) ( ( lTimeSinceLastStartInMSec / 1000.0 ) * signalSourceBuilder.getSamplingFrequencyEnum ( ).getValue () );
                        long lNewSamplesToBeProducedOnThisRun = lSamplesToBeProducedSinceLastStart - lSamplesProducedSinceLastStart;

                        logger.trace("lTimeSinceLastStartInMSec:" + lTimeSinceLastStartInMSec );
                        logger.trace("lSamplesToBeProducedSinceLastStart:" + lSamplesToBeProducedSinceLastStart);
                        logger.trace("currentSampleCount:" + currentSampleCount );
                        logger.trace("lSamplesSinceLastStart:" + lSamplesProducedSinceLastStart );
                        logger.trace("lNewSamplesToBeProducedOnThisRun:" + lNewSamplesToBeProducedOnThisRun );

                        if ( lNewSamplesToBeProducedOnThisRun == 0 )
                        {
                            logger.trace("no samples to be produced on this run");
                        }
                        else
                        {
                            Preconditions.checkState( lNewSamplesToBeProducedOnThisRun <= Integer.MAX_VALUE );

                            if ( lNewSamplesToBeProducedOnThisRun > bufferCapacityInSamplesPerChannel )
                            {
                                final long lSamplesToDrop = lNewSamplesToBeProducedOnThisRun - bufferCapacityInSamplesPerChannel;

                                logger.warn("samples dropped, lSamplesToDrop:" + lSamplesToDrop);

                                droppedSamples.addAndGet( lSamplesToDrop );
                                lastResetTimeInMSec.set(System.currentTimeMillis());
                                lastResetSampleCount.set(currentSampleCount);

                                lNewSamplesToBeProducedOnThisRun = bufferCapacityInSamplesPerChannel;
                            }

                            final long lNumberOfSamplesActuallyProduced = iComputeNextSamples(
                                (int) lNewSamplesToBeProducedOnThisRun,
                                lSignalVessel,
                                currentSampleCount );

                            lSignalVessel.setLastSampleTime ( System.currentTimeMillis () );

                            logger.trace( "lActuallyProduced:" + lNumberOfSamplesActuallyProduced );

                            currentSampleCount += lNumberOfSamplesActuallyProduced;

                            filterVessels( lSignalVessel );

                            synchronized ( vesselsQueue )
                            {
                                if ( vesselsQueue.isFull ( ) )
                                {
                                    logger.warn(
                                        "samples dropped, lNumberOfSamplesActuallyProduced:" + lNumberOfSamplesActuallyProduced +
                                        ", lVesselSizePre/limit:" + vesselsQueue.size() + "/" + queueLengthLimit);

                                    droppedSamples.addAndGet(lNumberOfSamplesActuallyProduced);
                                    lastResetTimeInMSec.set(System.currentTimeMillis());
                                    lastResetSampleCount.set(currentSampleCount);
                                }
                                else
                                {
                                    if ( logger.isTraceEnabled () )
                                    {
                                        logger.trace (
                                            "added new vessel, lNumberOfSamplesActuallyProduced:" + lNumberOfSamplesActuallyProduced +
                                                ", lVesselSizePre/limit:" + vesselsQueue.size ( ) + "/" + queueLengthLimit );
                                    }

                                    signalVesselSlotFiller.setVessels ( lSignalVessel );

                                    vesselsQueue.put ( signalVesselSlotFiller );
                                }

                                if ( logger.isTraceEnabled () )
                                {
                                    logger.trace (
                                        "vesselsQueue.size(2)/max:" + vesselsQueue.size ( ) + "/" + queueLengthLimit );
                                }
                            }
                        }
                    }
                }

                catch ( Throwable ex )
                {
                    logger.error( ex.getMessage(), ex );
                }

                logger.trace( "scheduledThreadPoolExecutor.run() -- END" );
            },
            0,
            PRODUCTION_PERIOD_MILLISECONDS,
            TimeUnit.MILLISECONDS
        );
    }

    @Override
    public void flush ( )
    {
        super.flush ();
        lastResetTimeInMSec.set( System.currentTimeMillis());
        lastResetSampleCount.set( currentSampleCount );
        currentSampleCount = 0;
    }

    private static final long TWO_POWER_15 = 0x8000;

    @Override
    public long getSampleSpaceUpperBound ()
    {
        return TWO_POWER_15;
    }

    @Override
    public long getSampleSpaceLowerBound ()
    {
        return -TWO_POWER_15;
    }

    private long iComputeNextSamples (
        int aInSamplesToBeProduced,
        MultiChannelVessel aInSignalVessel,
        long ainCurrentSampleCount )
    {
        for ( int index=0; index<aInSignalVessel.getNumberOfChannels (); index++ )
        {
            ChannelVessel lSignalVessel = aInSignalVessel.getChannelVessel ( index );

            Preconditions.checkArgument( aInSamplesToBeProduced <= lSignalVessel.getAllocatedLength() );

            lSignalVessel.setLength( aInSamplesToBeProduced );

            for ( int i = 0; i < lSignalVessel.getLength(); i++ )
            {
                lSignalVessel.setSample(
                    i,
                    compute.compute(
                        index,
                        ainCurrentSampleCount + i,
                        getSampleSpaceLowerBound (),
                        getSampleSpaceUpperBound (),
                        signalSourceBuilder.getSamplingFrequencyEnum ( ).getValue ()
                    )
                );
            }
        }

        return aInSamplesToBeProduced;
    }

    @Override
    public void startSampling()
    {
        logger.trace( "startSampling -- BEGIN" );

        synchronized ( vesselsQueue )
        {
            lastResetTimeInMSec.set( System.currentTimeMillis());
            lastResetSampleCount.set( currentSampleCount );

            super.startSampling();
        }

        logger.trace( "startSampling -- END" );
    }

    @Override
    public void close() throws IOException
    {
        logger.trace( "close() -- BEGIN" );

        super.close();
        scheduledThreadPoolExecutor.shutdownNow ();

        logger.trace( "close() -- END" );
    }

    public String getPhysicalSpaceUnit ()
    {
        return "mV";
    }

    @Override
    public float getPhysicalSpaceUpperBound ()
    {
        return 0.8f * 3.3f * 1000f;  // 80% of 3.3 volts in mV
    }

    @Override
    public float getPhysicalSpaceLowerBound ()
    {
        return 0.2f * 3.3f * 1000f;  // 20% of 3.3 volts in mV
    }
}
