/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.poplar.monitor.api.SignalSourceBuilder;

import java.util.Locale;

public class PureSineGenerator extends MultiChannelSignalSourceSimulator
{
    public static PureSineGenerator create( SignalSourceBuilder aInBuilder )
    {
        return new PureSineGenerator ( aInBuilder );
    }

    private PureSineGenerator ( SignalSourceBuilder aInBuilder )
    {
        super(
            aInBuilder,
            PureToneSineSignalCompute
                .create ( )
                .setFreqInHz( iGetFreqInHz () ) );
    }

    @Override
    public String getDescription ( )
    {
        return "sinusoidal " + iGetFreqInHz () + "Hz";
    }

    @Override
    public boolean isAvailable ( )
    {
        return true;
    }

    private static double iGetFreqInHz ()
    {
        return Locale.getDefault ( ).getCountry ( ).equals ( Locale.US.getCountry ( ) ) ? 60.0 : 50.0;
    }

}
