/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import java.util.Locale;

public class PowerNoiseSignalCompute implements ICompute
{
    private static final double a = 1.0;
    private static final double[] harmonics = new double[] { 1 }; //2, 3, 4, 5, 6, 7, 8, 9 };
    private static final double pulsePeriodInMSecs = 300.0;
    private static final double pulseWidthInMSecs = 10.0;
    private final double f1Hz;

    public static PowerNoiseSignalCompute create()
    {
        return new PowerNoiseSignalCompute ();
    }

    private PowerNoiseSignalCompute ( )
    {
        f1Hz =
            ( Locale.getDefault ( ).getCountry ( ).equals ( Locale.US.getCountry ( ) ) )
            ? 60.0
            : 50.0;
    }

    @Override
    public float compute(
        int aInChannel,
        long aInSampleCount,
        float aInSignalSampleSpaceMin,
        float aInSignalSampleSpaceMax,
        float aInSamplingFreqHz )
    {
        final double lSampleSpaceRange = aInSignalSampleSpaceMax - aInSignalSampleSpaceMin;
        final double pulsePeriodInSamples = 0.001 * pulsePeriodInMSecs * aInSamplingFreqHz;
        final double pulseWidthInSamples = 0.001 * pulseWidthInMSecs * aInSamplingFreqHz;
        final double sampleCountModular = aInSampleCount % pulsePeriodInSamples;

        double lNoise = 0;
        for ( double harmonic : harmonics )
        {
            lNoise += ( a / harmonic ) * ( 0.5 * lSampleSpaceRange ) * Math.sin ( 2.0 * Math.PI * f1Hz * harmonic * ( sampleCountModular / aInSamplingFreqHz ) );
        }

        final double lRetValue;

        if ( aInChannel == 0 )
        {
            final double lSignalModulation = ( sampleCountModular < pulseWidthInSamples ) ? 1 : 0;
            final double lPulse = (float) ( lSignalModulation * a * lSampleSpaceRange );
            lRetValue = lPulse + lNoise * 0.25;
        }

        else if ( aInChannel == 1 )
        {
            lRetValue = lNoise;
        }

        else
        {
            throw new IllegalStateException( );
        }

        return (float) lRetValue;
    }
}

