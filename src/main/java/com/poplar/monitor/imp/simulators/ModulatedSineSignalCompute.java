/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

public class ModulatedSineSignalCompute implements ICompute
{
    private static final double f1Hz = 100;
    private static final double f2Hz = 0.1;

    public static ModulatedSineSignalCompute create()
    {
        return new ModulatedSineSignalCompute ();
    }

    private ModulatedSineSignalCompute ( )
    {
    }

    @Override
    public float compute(
        int aInChannel,
        long aInSampleCount,
        float aInSignalSampleSpaceMin,
        float aInSignalSampleSpaceMax,
        float aInSamplingFreqHz )
    {
        final double lSampleSpaceRange = aInSignalSampleSpaceMax - aInSignalSampleSpaceMin;

        final double lMod = Math.sin ( 2.0 * Math.PI * f2Hz * ( (double) aInSampleCount / aInSamplingFreqHz ) );
        final double lCarrier = ( 0.5 * lSampleSpaceRange ) * Math.sin ( 2.0 * Math.PI * f1Hz * ( (double) aInSampleCount / aInSamplingFreqHz ) );
        final double lSignal = lMod * lCarrier;

        final double lRetValue;

        if ( aInChannel == 0 )
        {
            lRetValue = lSignal;
        }

        else if ( aInChannel == 1 )
        {
            lRetValue = lCarrier;
        }

        else
        {
            throw new IllegalStateException( );
        }

        return (float) lRetValue;
    }
}

