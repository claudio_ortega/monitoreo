/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.simulators;

import com.poplar.monitor.api.SignalSourceBuilder;

public class CalibrationSignalGenerator extends MultiChannelSignalSourceSimulator
{
    public static CalibrationSignalGenerator create( SignalSourceBuilder aInBuilder )
    {
        return new CalibrationSignalGenerator ( aInBuilder );
    }

    private CalibrationSignalGenerator ( SignalSourceBuilder aInBuilder )
    {
        super( aInBuilder, CalibrationSignalCompute.create ( ) );
    }

    @Override
    public String getDescription ( )
    {
        return "calibration signal";
    }

    @Override
    public boolean isAvailable ( )
    {
        return true;
    }
}
