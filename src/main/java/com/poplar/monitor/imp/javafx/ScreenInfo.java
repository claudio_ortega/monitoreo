/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */


package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.Expose;
import com.poplar.monitor.api.ISignalSourceReadOnly;
import com.poplar.monitor.api.SignalSourceReadOnlyInfo;
import com.poplar.monitor.imp.util.GsonUtil;
import java.util.UUID;

public class ScreenInfo
{
    @Expose
    private ApplicationConfig applicationConfig;
    @Expose
    private SignalSourceReadOnlyInfo signalSourceReadOnlyInfo;
    @Expose
    private String id;
    @Expose
    private long screenNumber;
    @Expose
    private long appStartTime;
    @Expose
    private long screenSavingSessionCounter;
    @Expose
    private long signalStartingTime;
    @Expose
    private int signalLengthInSamples;
    @Expose
    private float[][] signal;

    static ScreenInfo create ( )
    {
        return new ScreenInfo ( );
    }

    private ScreenInfo()
    {
        id = UUID.randomUUID ().toString ();
    }

    public String getId ( )
    {
        return id;
    }

    ScreenInfo setSignalSourceReadOnlyInfo ( SignalSourceReadOnlyInfo aInSignalSourceReadOnlyInfo )
    {
        signalSourceReadOnlyInfo = aInSignalSourceReadOnlyInfo;
        return this;
    }

    ScreenInfo setApplicationConfig ( ApplicationConfig value )
        throws Exception
    {
        applicationConfig = GsonUtil.clone ( value );
        return this;
    }

    ScreenInfo setSignal ( float[][] aInSignal, int aInLength )
    {
        Preconditions.checkState ( signal == null );

        signal = new float[aInLength][2];
        signalLengthInSamples = aInLength;

        for ( int i = 0; i < aInLength; i++ )
        {
            signal[i][0] = aInSignal[i][0];
            signal[i][1] = aInSignal[i][1];
        }

        return this;
    }

    ScreenInfo setSignalStartingTime ( long value )
    {
        signalStartingTime = value;
        return this;
    }

    ScreenInfo setScreenNumber ( long value )
    {
        screenNumber = value;
        return this;
    }

    ScreenInfo setScreenSavingSessionCounter ( long aInScreenSavingSessionCounter )
    {
        screenSavingSessionCounter = aInScreenSavingSessionCounter;
        return this;
    }

    ScreenInfo setAppStartTime ( long aInAppStartTime )
    {
        appStartTime = aInAppStartTime;
        return this;
    }

    public long getScreenSavingSessionCounter ( )
    {
        return screenSavingSessionCounter;
    }

    public long getAppStartTime ( )
    {
        return appStartTime;
    }

    public ISignalSourceReadOnly getSignalSourceReadOnlyInfo ( )
    {
        return signalSourceReadOnlyInfo;
    }

    public long getSignalStartingTime ( )
    {
        return signalStartingTime;
    }

    public long getScreenNumber ( )
    {
        return screenNumber;
    }

    public int getSignalLengthInSamples ( )
    {
        return signalLengthInSamples;
    }

    public float[][] getSignal ( )
    {
        return signal;
    }

    public ApplicationConfig getApplicationConfig ( )
    {
        return applicationConfig;
    }

    public double computeLengthInSeconds ( )
    {
        return ( (double) signalLengthInSamples ) / (double) signalSourceReadOnlyInfo.getSamplingFrequencyInHz ( );
    }

    @Override
    public String toString ( )
    {
        return "ScreenInfo{" +
            "applicationConfig=" + applicationConfig +
            ", signalSourceReadOnlyInfo=" + signalSourceReadOnlyInfo +
            ", screenNumber=" + screenNumber +
            ", appStartTime=" + appStartTime +
            ", screenSavingSessionCounter=" + screenSavingSessionCounter +
            ", signalStartingTime=" + signalStartingTime +
            ", signalLengthInSamples=" + signalLengthInSamples +
            '}';
    }
}
