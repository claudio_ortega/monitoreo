/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.util.CollectionsUtil;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import org.apache.commons.math3.util.Pair;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

public class SliderTextFieldBinder
{
    private final DoubleProperty valuePropA;
    private final DoubleProperty valuePropB;
    private final DoubleProperty minProperty;
    private final DoubleProperty maxProperty;
    private Slider slider;
    private TextField textField;
    private int numberOfTicks;
    private int numberOfDecimalDigits;
    private double deadZone;
    private UnaryOperator<Double> directOperator;
    private UnaryOperator<Double> reverseOperator;
    private AtomicBoolean started;
    private DecimalStringConverter decimalConverter;
    private Consumer<Slider> postStartProcessor;

    public static SliderTextFieldBinder create ()
    {
        return new SliderTextFieldBinder ();
    }

    public SliderTextFieldBinder setPostStartProcessor ( Consumer<Slider> consumer )
    {
        postStartProcessor = consumer;
        return this;
    }

    private SliderTextFieldBinder ( )
    {
        valuePropA = new SimpleDoubleProperty ();
        valuePropB = new SimpleDoubleProperty ();
        minProperty = new SimpleDoubleProperty ();
        maxProperty = new SimpleDoubleProperty ();
        decimalConverter = null;
        numberOfDecimalDigits = 1;
        numberOfTicks = 5;
        deadZone = 0;
        directOperator = UnaryOperator.identity ();
        reverseOperator = UnaryOperator.identity ();
        slider = new Slider ( );
        textField = new TextField ( );
        started = new AtomicBoolean ( false );
        postStartProcessor = slider -> {
            slider.setSnapToTicks ( false );
            slider.setShowTickLabels ( true );
            slider.setShowTickMarks ( false );
        };
    }

    public Node getSlider ( )
    {
        return slider;
    }

    public Node getTextField ( )
    {
        return textField;
    }

    public DoubleProperty valueProperty ( )
    {
        return valuePropB;
    }

    public DoubleProperty maxProperty ( )
    {
        return maxProperty;
    }

    public DoubleProperty minProperty ( )
    {
        return minProperty;
    }

    public SliderTextFieldBinder setTextField ( TextField value )
    {
        Preconditions.checkState ( ! started.get() );

        textField = value;
        return this;
    }

    public SliderTextFieldBinder setSlider ( Slider value )
    {
        Preconditions.checkState ( ! started.get() );

        slider = value;
        return this;
    }

    public SliderTextFieldBinder setOrientation( Orientation value )
    {
        Preconditions.checkState ( ! started.get() );

        slider.setOrientation ( value );
        return this;
    }

    public SliderTextFieldBinder setLinearOperation ( Double min, Double max )
    {
        Preconditions.checkState ( ! started.get() );

        minProperty.setValue ( min );
        maxProperty.setValue ( max );

        return this;
    }

    public SliderTextFieldBinder setValue( Double value )
    {
        valuePropB.setValue ( value );
        return this;
    }

    public double getValue( )
    {
        return valuePropB.getValue ();
    }

    public SliderTextFieldBinder setLogarithmicOperation ( )
    {
        Preconditions.checkState ( ! started.get() );

        directOperator = aDouble -> Math.log10 ( aDouble );

        reverseOperator = aDouble -> Math.pow ( 10.0, aDouble );

        return this;
    }

    public SliderTextFieldBinder setLinearBySegmentsOperation ( Double ... pairs )
    {
        Preconditions.checkState ( ! started.get() );

        AtomicInteger counter = new AtomicInteger ( 0 );

        final List<Pair<Double,Double>> directFunctionPoints = CollectionsUtil.transformList (
            CollectionsUtil.createList ( pairs ),
            ( Double aInS ) -> new Pair<>( aInS, (double) counter.incrementAndGet () )
        );

        final LinearlySegmentedOperator lDirect = LinearlySegmentedOperator.create ( ).setFunctionPoints ( directFunctionPoints ).start();
        final LinearlySegmentedOperator lReverse = LinearlySegmentedOperator.create ( ).setFunctionPoints ( JavaFxUtils.swapPairsInPlace ( directFunctionPoints ) ).start();

        directOperator = lDirect.getOperator ( );
        reverseOperator = lReverse.getOperator ( );

        setNumberOfTicks ( pairs.length );
        setLinearOperation ( lDirect.getMinDomain ( ), lDirect.getMaxDomain ( ) );

        return this;
    }

    public SliderTextFieldBinder setNumberOfDecimalDigits ( int value )
    {
        Preconditions.checkState ( ! started.get() );

        numberOfDecimalDigits = value;
        return this;
    }

    public SliderTextFieldBinder setNumberOfTicks ( int value )
    {
        Preconditions.checkState ( ! started.get() );

        numberOfTicks = value;
        return this;
    }

    public SliderTextFieldBinder start()
    {
        Preconditions.checkState ( ! started.get(), "attempted to start() this instance for a second time" );

        decimalConverter = new DecimalStringConverter ( String.format( "%%.%df", numberOfDecimalDigits ) );

        iInitializeState ( );

        slider.setLabelFormatter(
            new StringConverter<Double> ( )
            {
                @Override
                public Double fromString ( String string )
                {
                    throw new IllegalStateException ( "not planned to be used" );
                }

                @Override
                public String toString ( Double value )
                {
                    Preconditions.checkState ( started.get() );
                    return String.format ( decimalConverter.getFormat (), reverseOperator.apply ( value ) );
                }
            }
        );

        JavaFxUtils.addStringContentValidator (
            textField,
            s ->
            {
                Preconditions.checkState ( started.get ( ) );

                boolean ret;

                try
                {
                    final double x = Double.parseDouble ( s );

                    ret = compareLesserWithTolerance ( reverseOperator.apply ( slider.getMax ( ) ), deadZone, x ) && compareGreaterWithTolerance ( reverseOperator.apply ( slider.getMin ( ) ), deadZone, x );
                }

                catch ( NumberFormatException ex )
                {
                    ret = false;
                }

                return ret;
            }
        );

        minProperty.addListener ( ( observable, oldValue, newValue ) -> { throw new IllegalStateException( "changes in min not supported after start" ); } );

        maxProperty.addListener ( ( observable, oldValue, newValue ) -> { throw new IllegalStateException( "changes in max not supported after start" ); } );

        valuePropA.addListener ( ( observable, oldValue, newValue ) -> {
            Preconditions.checkState ( started.get() );

            final Double tmp = directOperator.apply ( ( Double ) newValue );
            slider.valueProperty ( ).set ( tmp );
        } );

        slider.valueProperty ( ).addListener ( ( observable, oldValue, newValue ) -> {
            Preconditions.checkState ( started.get() );

            final Double tmp = reverseOperator.apply ( ( Double ) newValue );
            valuePropB.set ( tmp );
        } );

        valuePropB.addListener ( ( observable, oldValue, newValue ) -> {
            Preconditions.checkState ( started.get() );

            if ( Math.abs ( ( ( Double ) newValue - valuePropA.get ( ) ) ) > deadZone )
            {
                textField.textProperty ( ).set ( decimalConverter.toString ( newValue ) );
            }
        } );

        textField.textProperty ( ).addListener ( ( observable, oldValue, newValue ) -> {
            Preconditions.checkState ( started.get() );

            if ( decimalConverter.fromString ( newValue ) != null )
            {
                valuePropA.set ( ( Double ) decimalConverter.fromString ( newValue ) );
            }
        } );

        postStartProcessor.accept ( slider );

        started.set ( true );

        return this;
    }

    private void iInitializeState ( )
    {
        Preconditions.checkState ( ! started.get() );
        Preconditions.checkState ( minProperty.get () < maxProperty().get() );

        final double min = minProperty.get ( );
        final double max = maxProperty.get ( );
        final double value = valuePropB.get ( );

        if ( value < min )
        {
            valuePropA.set( min );
            valuePropB.set( min );
        }

        else if ( value > max )
        {
            valuePropA.set( max );
            valuePropB.set( max );
        }

        else
        {
            valuePropA.set( value );
            valuePropB.set( value );
        }

        Preconditions.checkState ( minProperty.get ( ) <= valuePropA.get ( ) );
        Preconditions.checkState ( valuePropA.get () <= maxProperty.get() );
        Preconditions.checkState ( minProperty.get ( ) <= valuePropB.get ( ) );
        Preconditions.checkState ( valuePropB.get () <= maxProperty.get() );

        final double valueA = valuePropA.get();
        final double valueB = valuePropB.get();

        slider.minProperty ( ).set ( directOperator.apply( min ) );
        slider.maxProperty ( ).set ( directOperator.apply( max ) );
        slider.valueProperty ( ).set ( directOperator.apply( valueA ) );
        slider.setMajorTickUnit ( ( slider.getMax ( ) - slider.getMin ( ) ) / ( numberOfTicks - 1 ) );
        slider.setBlockIncrement ( ( slider.getMax ( ) - slider.getMin ( ) ) / ( numberOfTicks - 1 ) );

        textField.textProperty ( ).set ( decimalConverter.toString ( valueB ) );

        deadZone = ( max - min ) * 1e-6;
    }

    private boolean compareLesserWithTolerance ( double ref, double tol, double test )
    {
        return test <= ref + tol;
    }

    private boolean compareGreaterWithTolerance ( double ref, double tol, double test )
    {
        return test >= ref - tol;
    }

    private static class DecimalStringConverter extends StringConverter<Number>
    {
        private final String format;

        DecimalStringConverter (
            String aInFormat )
        {
            format = aInFormat;
        }

        public String getFormat ( )
        {
            return format;
        }

        @Override
        public String toString ( Number object )
        {
            return String.format ( format, (Double) object );
        }

        @Override
        public Number fromString ( String string )
        {
            Double lRet;

            try
            {
                lRet = Double.parseDouble ( string );
            }

            catch ( Exception ex )
            {
                lRet = null;
            }

            return lRet;
        }
    }
}
