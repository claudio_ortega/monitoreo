/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import javafx.scene.canvas.Canvas;

public class ResizableCanvas extends Canvas
{
    private IDrawEngine redrawEngine;
    private final Object redrawEngineGuard;

    public ResizableCanvas()
    {
        redrawEngineGuard = new Object();
        redrawEngine = null;

        widthProperty().addListener(it -> redraw ( ) );
        heightProperty().addListener(it -> redraw ( ) );
    }

    public void setRedrawEngine ( IDrawEngine aInDrawEngine )
    {
        synchronized ( redrawEngineGuard )
        {
            redrawEngine = aInDrawEngine;
        }
    }

    @Override
    public boolean isResizable()
    {
        return true;
    }

    public void redraw ( )
    {
        synchronized ( redrawEngineGuard )
        {
            if ( redrawEngine != null )
            {
                redrawEngine.draw ( getGraphicsContext2D ( ) );
            }
        }
    }
}
