
/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;

import java.util.Set;

public class PropertiesBinder
{
    public static PropertiesBinder create()
    {
        return new PropertiesBinder ();
    }

    /**
     * important: apparently, in case both sides have different values at startup, then the 'from side is the one that wins
     * @param aInPropertiesSet
     * @param from
     * @param to
     * @return
     */
    public PropertiesBinder bindBidirectional ( Set<PropertiesEnum> aInPropertiesSet, IPropertiesProvider from, IPropertiesProvider to )
    {
        return iBind ( true, aInPropertiesSet, from, to );
    }

    public PropertiesBinder bindUnidirectional ( Set<PropertiesEnum> aInPropertiesSet, IPropertiesProvider from, IPropertiesProvider to )
    {
        return iBind ( false, aInPropertiesSet, from, to );
    }

    private PropertiesBinder iBind ( boolean aInBidirectional, Set<PropertiesEnum> aInPropertiesSet, IPropertiesProvider from, IPropertiesProvider to )
    {
        Preconditions.checkState ( from != null );
        Preconditions.checkState ( to != null );

        for ( PropertiesEnum next : aInPropertiesSet )
        {
            Preconditions.checkState ( from.getProperty ( next ) != null, "prop name:" + next );
            Preconditions.checkState ( to.getProperty ( next ) != null, "prop name:" + next );

            if ( aInBidirectional )
            {
                to.getProperty ( next ).bindBidirectional ( from.getProperty ( next ) );
            }
            else
            {
                to.getProperty ( next ).bind ( from.getProperty ( next ) );
            }
        }

        return this;
    }
}
