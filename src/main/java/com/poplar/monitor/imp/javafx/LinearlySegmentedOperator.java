/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.util.CollectionsUtil;
import org.apache.commons.math3.util.Pair;

import java.util.List;
import java.util.function.UnaryOperator;

public class LinearlySegmentedOperator
{
    private List<Pair<Double,Double>> pairs;
    private SegmentOperator operator;

    public static LinearlySegmentedOperator create( )
    {
        return new LinearlySegmentedOperator ();
    }

    public LinearlySegmentedOperator setFunctionPoints ( List<Pair<Double, Double>> aInFunction )
    {
        Preconditions.checkArgument ( aInFunction.size () > 1 );

        JavaFxUtils.assertOnListOfPairsMonotonicUp ( aInFunction );

        pairs = CollectionsUtil.createImmutableList ( aInFunction );

        return this;
    }

    public LinearlySegmentedOperator start()
    {
        operator = new SegmentOperator( pairs );
        return this;
    }

    public UnaryOperator<Double> getOperator()
    {
        return operator;
    }

    public double getMinDomain()
    {
        return operator.minDomain;
    }
    public double getMaxDomain()
    {
        return operator.maxDomain;
    }

    private static class SegmentOperator implements UnaryOperator<Double>
    {
        private final int size;
        private final Double minDomain;
        private final Double maxDomain;
        private List<Pair<Double,Double>> pairs;

        SegmentOperator( List<Pair<Double,Double>> aInPairs )
        {
            pairs = aInPairs;
            size = pairs.size ();

            minDomain = pairs.get ( 0 ).getFirst ( );
            maxDomain = pairs.get ( size-1 ).getFirst ( );

            Preconditions.checkState ( minDomain < maxDomain );
        }

        public static boolean greaterThanOrClose( double ref, double test )
        {
            return test > ref || Math.abs( ref - test ) < 1e-6;
        }

        @Override
        public Double apply ( Double x )
        {
            final double ret;

            if ( JavaFxUtils.lesserThanOrClose ( minDomain, x ) )
            {
                ret = pairs.get ( 0 ).getSecond ( );
            }

            else if ( greaterThanOrClose ( maxDomain, x ) )
            {
                ret = pairs.get ( size-1 ).getSecond ( );
            }

            else
            {
                Preconditions.checkState ( ! ( x > maxDomain ), "aDouble: " + x + ", maxDomain: " + maxDomain );

                // which slot does the point belong to?
                int foundSlot = - 1;

                for ( int i = 0; i < size-1; i++ )
                {
                    final double pointAx = pairs.get ( i ).getFirst ( );
                    final double pointBx = pairs.get ( i + 1 ).getFirst ( );

                    if ( greaterThanOrClose ( pointAx, x ) && JavaFxUtils.lesserThanOrClose ( pointBx, x ) )
                    {
                        foundSlot = i;
                        break;
                    }

                    else if ( greaterThanOrClose ( pointBx, x ) && JavaFxUtils.lesserThanOrClose ( pointAx, x ) )
                    {
                        foundSlot = i;
                        break;
                    }
                }

                Preconditions.checkState ( ! ( foundSlot < 0 ) );
                Preconditions.checkState ( ! ( foundSlot > ( size - 2 ) ) );

                ret = iLinearMapping ( pairs.get ( foundSlot ), pairs.get ( foundSlot + 1 ), x );
            }

            return ret;
        }

        private static double iLinearMapping(
            Pair<Double,Double> pointA,
            Pair<Double,Double> pointB,
            double inValueToBeMapped )
        {
            double dx = pointB.getFirst () - pointA.getFirst ();
            Preconditions.checkState ( dx > 1e-6 );

            double dy = pointB.getSecond ( ) - pointA.getSecond ( );
            double r = dy / dx;

            return pointA.getSecond () + r * ( inValueToBeMapped - pointA.getFirst ( ) );
        }
    }
}
