/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.ISignalSource;
import com.poplar.monitor.api.IMultiChannelVesselFilter;
import com.poplar.monitor.api.MultiChannelVessel;
import com.poplar.monitor.api.SignalChannelBuilder;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.api.SignalSourceFactory;
import com.poplar.monitor.api.SignalSourceReadOnlyInfo;
import com.poplar.monitor.imp.common.CircularQueue;
import com.poplar.monitor.imp.common.MultiChannelVesselFilter;
import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.nonlinear.RectifyingFilter;
import com.poplar.monitor.imp.util.GsonUtil;
import com.poplar.monitor.imp.util.FileUtil;
import com.poplar.monitor.imp.util.StringUtil;
import com.poplar.monitor.imp.util.SystemUtil;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;
import javafx.util.Duration;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@SuppressWarnings( "SuspiciousNameCombination" )
public class SignalPane implements IPropertiesProvider
{
    private final static Logger logger = LogManager.getLogger( SignalPane.class );

    // the first dimension determines the canvas layer, the second determines the signal channel number
    private volatile ResizableCanvas[][] signalCanvases;
    private enum LAYER_ENUM { GRID, CONFIG_INFO, VU_METER, SIGNAL, TRIGGER, FAST_COUNTERS }
    private volatile Affine[] affineTransformations;
    private volatile boolean[] isChannelVisible;
    private volatile Point2D[] lastDisplayedPoint;
    private volatile MultiChannelVessel signalVessels;
    private volatile ISignalSource signalSource;
    private final IMultiChannelVesselFilter filters;
    private volatile ApplicationConfig applicationConfig;
    private volatile long sampleSweepCount;
    private volatile long holdOffLimitInSamples;
    private volatile long displayLimitInSamples;
    private volatile long monotonicSampleCount;
    private final AtomicLong screenCount;
    private volatile int sampleDecimationDisplayRate;
    private volatile Timeline animation;
    private final AtomicBoolean animationScheduled;
    private final AtomicBoolean isInitializing;
    private long[] sampleSpaceTriggerLevel;
    private final List<ChangeListener<DisplayStatus>> listeners;
    private final ScheduledThreadPoolExecutor triggerCleanupExecutor;
    private final AudioClip triggerAudioClip;
    private final int vuMeterWidth;
    private final int vuMeterInset;
    private final Color backgroundColor;
    private final LanguageManager languageManager;
    private ICausalDigitalFilter[] vuMeterFilter;
    private final int animationPeriodMs;
    private final ChangeListener<ApplicationConfig> applicationConfigChangeListener;
    private final ChangeProducer<DisplayStatus> displayStatusChangeProducer;
    private final long appStartingTime;
    private long lastDisplayTime;
    private final Deque<ScreenInfo> screenSavingQueueA;
    private final Deque<ScreenInfo> screenSavingQueueB;
    private float[][] samplesForCurrentScreen;
    private volatile int nextIndexForCurrentScreen;
    private volatile long startTimeForCurrentScreen;
    private final AtomicInteger screenSavingSessionCounter;
    private final ScheduledThreadPoolExecutor periodicScreenSavingExecutor;
    private ScheduledThreadPoolExecutor periodicSignalSourceResetExecutor;
    private final AtomicBoolean stopped;
    private final AtomicBoolean insideInvalidation;
    private final HoldoffTimeDampener holdoffTimeDampener;
    private DoubleProperty displayTimeMsPerDivProperty;
    private boolean displayTimeIsShort;
    private DoubleProperty holdOffTimeInSecondsValueProperty;
    private DoubleProperty triggerSoundVolumeValueProperty;
    private DoubleProperty[] channelSensitivityProperty;
    private DoubleProperty[] channelTriggerLevelProperty;
    private BooleanProperty triggerModeContinuousProperty;
    private BooleanProperty triggerModeTriggerOnChannelOneProperty;
    private BooleanProperty triggerModeTriggerOnChannelTwoProperty;
    private BooleanProperty useAdaptiveFilteringProperty;
    private final TimeoutFilter timeoutFilter;
    private int samplesForVUMeterRefreshCycle;
    private CircularQueue<float[]> preTriggerQueue;
    private CircularQueueFiller preTriggerQueueFiller;
    private volatile boolean isInAutoMode;
    private volatile boolean[] usePositiveSlope;
    private volatile int channelTriggeredIndex;
    private final Callable<Void> playTriggerAlarmCallable;
    private final Callable<Void> stopTriggerAlarmCallable;

    public void setApplicationConfigProvider ( IApplicationConfigProvider newValue )
    {
        applicationConfigProvider = newValue;
    }

    private IApplicationConfigProvider applicationConfigProvider;

    public SignalPane ( )
    {
        filters = MultiChannelVesselFilter.create ( );
        monotonicSampleCount = 0;
        animationPeriodMs = 50;
        vuMeterWidth = 20;
        vuMeterInset = 4;
        appStartingTime = System.currentTimeMillis ();
        lastDisplayTime = System.currentTimeMillis ();
        stopped = new AtomicBoolean ( true );
        insideInvalidation = new AtomicBoolean ( false );
        screenSavingQueueA = new ConcurrentLinkedDeque<>();
        screenSavingQueueB = new ConcurrentLinkedDeque<>();
        languageManager =  LanguageManager.getSingleton ();
        backgroundColor = Color.rgb ( 20, 20, 20 );
        listeners = new LinkedList<>();
        screenCount = new AtomicLong ( 0 );
        triggerAudioClip = new AudioClip( getClass ().getClassLoader ( ).getResource ( "com/poplar/monitor/imp/javafx/2000hz.wav" ).toExternalForm() );
        animationScheduled = new AtomicBoolean ( false );
        timeoutFilter = TimeoutFilter.create ().setWindow ( 10_000 );
        triggerCleanupExecutor = new ScheduledThreadPoolExecutor ( 1 );
        isInitializing = new AtomicBoolean ( false );
        screenSavingSessionCounter = new AtomicInteger ( 0 );

        applicationConfigChangeListener =
            ( ApplicationConfig aInNewConfig, ApplicationConfig aInOldConfig ) ->
            {
                logger.info ( "change in config, aInConfigOld: " + aInOldConfig );
                logger.info ( "change in config, aInNewConfig: " + aInNewConfig );

                if ( periodicSignalSourceResetExecutor == null )
                {
                    iInitializePeriodicSignalSourceResetExecutor ( aInNewConfig );
                }

                iNotifyListeners ( DisplayStatus.init_started );

                isInitializing.set( true );

                JavaFxUtils.executeOutsideFxThread (
                    ( ) ->
                    {
                        try
                        {
                            iInit ( aInNewConfig, aInOldConfig );

                            applicationConfig = GsonUtil.clone( aInNewConfig );

                            isInitializing.set ( false );

                            iNotifyListeners ( DisplayStatus.init_done );
                        }

                        catch ( Exception ex )
                        {
                            logger.error ( ex.getMessage ( ), ex );

                            JavaFxUtils.showNotification (
                                LanguageManager.getSingleton ( ).getValue ( "monitoring-initialized-failed" ) + "\n" + signalSource.getDescription ( ),
                                JavaFxUtils.NotificationLevel.ERROR
                            );

                            isInitializing.set ( false );

                            iNotifyListeners ( DisplayStatus.init_error );
                        }

                        finally
                        {
                            isInitializing.set ( false );
                        }

                        return null;
                    }
                );
            };

        displayStatusChangeProducer =
            ( ChangeListener<DisplayStatus> aInChangeListener ) ->
            {
                synchronized ( listeners )
                {
                    listeners.add( aInChangeListener );
                }
            };

        periodicScreenSavingExecutor = new ScheduledThreadPoolExecutor ( 1 );

        periodicScreenSavingExecutor.scheduleAtFixedRate (
            () ->
            {
                try
                {
                    if ( stopped.get () )
                    {
                        final ScreenInfo lScreenInfo = screenSavingQueueB.pollFirst ( );

                        if ( lScreenInfo != null )
                        {
                            iSaveSingleScreenToDisk ( lScreenInfo );
                        }
                    }
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            },
            0,
            500,
            TimeUnit.MILLISECONDS
        );

        holdoffTimeDampener = HoldoffTimeDampener.create ( 100 );  // in msec
        samplesForVUMeterRefreshCycle = 0;

        playTriggerAlarmCallable =
            ( ) ->
            {
                triggerAudioClip.play ( );
                SystemUtil.sleepMsec ( 200 );
                triggerAudioClip.stop ( );
                return null;
            };

        stopTriggerAlarmCallable =
            ( ) ->
            {
                triggerAudioClip.stop ( );
                return null;
            };
    }

    private void iInitializePeriodicSignalSourceResetExecutor ( ApplicationConfig aInNewConfig )
    {
        periodicSignalSourceResetExecutor = new ScheduledThreadPoolExecutor ( 1 );

        periodicSignalSourceResetExecutor.scheduleAtFixedRate (
            ( ) ->
            {
                try
                {
                    iReinitializeSignalSource ( );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage ( ), ex );
                }
            },
            aInNewConfig.getSignalSourceBuilder ().getPeriodicResetIntervalInSeconds (),
            aInNewConfig.getSignalSourceBuilder ().getPeriodicResetIntervalInSeconds (),
            TimeUnit.SECONDS
        );
    }

    public void close()
    {
        holdoffTimeDampener.close();

        try
        {
            if ( periodicSignalSourceResetExecutor != null )
            {
                periodicSignalSourceResetExecutor.shutdown ( );
                periodicSignalSourceResetExecutor.awaitTermination ( 3000, TimeUnit.MILLISECONDS );
            }

            // final variables, never null
            triggerCleanupExecutor.shutdownNow ( );
            periodicScreenSavingExecutor.shutdownNow ( );
            triggerCleanupExecutor.awaitTermination ( 3000, TimeUnit.MILLISECONDS );
            periodicScreenSavingExecutor.awaitTermination ( 3000, TimeUnit.MILLISECONDS );
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage ( ), ex );
        }
    }

    public void cleanAllScreens ( )
    {
        logger.info ( "cleanAllScreens() -- BEGIN" );

        iRefreshAllScreens ( );

        logger.info ( "cleanAllScreens() -- END" );
    }

    /**
     *    this is used to reinit the driver that goes bad after one hour or two..
     */
    private void iReinitializeSignalSource ( )
    {
        logger.info ( "iReinitializeSignalSource() -- BEGIN" );

        try
        {
            if ( signalSource == null )
            {
                logger.info ( "signalSource is currently null, skipping reinitialization" );
            }

            else
            {
                JavaFxUtils.executeOutsideFxThread (
                    ( ) ->
                    {
                        JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "monitoring-initializing" ) + "\n" + signalSource.getDescription ( ), JavaFxUtils.NotificationLevel.INFO );

                        final boolean isStarted = signalSource.getIsStarted ( );

                        signalSource.close ( );
                        signalSource = SignalSourceFactory.create ( applicationConfig.getSignalSourceBuilder ( ) );

                        filters.reset ( );
                        signalSource.attachVesselFilter ( filters );

                        logger.info ( "signalSource has been reset, new signalSource: " + signalSource );

                        if ( ! signalSource.isAvailable ( ) )
                        {
                            JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "monitoring-initialized-failed" ) + "\n" + signalSource.getDescription ( ), JavaFxUtils.NotificationLevel.ERROR );
                        }

                        else
                        {
                            iHandleVesselFiltersConfig ( );

                            if ( isStarted )
                            {
                                signalSource.startSampling ( );
                            }

                            JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "monitoring-initialized-ok" ) + "\n" + signalSource.getDescription ( ), JavaFxUtils.NotificationLevel.INFO );
                        }

                        JavaFxUtils.executeInFxThread (
                            ( ) ->
                            {
                                iRefreshAllScreens ( );
                                return null;
                            }
                        );

                        return null;
                    } );
            }
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage (), ex );
        }

        logger.info ( "iReinitializeSignalSource() -- END" );
    }

    public void stop()
    {
        logger.info ( "stop() -- BEGIN" );

        if ( isInitializing.get () )
        {
            logger.info( "still initializing, skip" );
        }

        else if ( stopped.get () )
        {
            logger.info( "already stopped, skip" );
        }

        else if ( ! iGetSignalSourceIsAvailable () )
        {
            logger.info( "the signal source is unavailable" );
        }

        else
        {
            triggerCleanupExecutor.submit ( stopTriggerAlarmCallable );

            if ( signalSource != null )
            {
                signalSource.stopSampling ( );
                signalSource.flush ( );
            }

            iNotifyListeners ( DisplayStatus.stopped );
            stopped.set ( true );
        }

        logger.info ( "stop() -- END" );
    }

    public void start()
    {
        logger.info ( "start() -- BEGIN" );

        Preconditions.checkState ( Platform.isFxApplicationThread ( ) );

        if ( isInitializing.get () )
        {
            JavaFxUtils.showNotification ( "still-initializing", JavaFxUtils.NotificationLevel.WARN );
            logger.info( "still initializing, won't start yet ..." );
        }

        else if ( ! stopped.get () )
        {
            logger.info( "already started, skip" );
        }

        else if ( ! iGetSignalSourceIsAvailable () )
        {
            JavaFxUtils.showNotification ( "signal-source-unavailable", JavaFxUtils.NotificationLevel.WARN, true );
            logger.info( "the signal source is unavailable" );
        }

        else
        {
            signalSource.flush ( );
            signalSource.startSampling ( );
            iNotifyListeners ( DisplayStatus.playing );
            stopped.set ( false );
        }

        logger.info ( "start() -- END" );
    }

    public List<Canvas> getSignalCanvasForChannel ( int aInChannelIndex )
    {
        final List<Canvas> ret = new LinkedList<>();

        for ( LAYER_ENUM layer : LAYER_ENUM.values () )
        {
            ret.add ( signalCanvases[layer.ordinal ( )][aInChannelIndex] );
        }

        return ret;
    }

    public int getNumberOfChannels()
    {
        return signalSource.getNumberOfChannels();
    }

    public ChangeListener<ApplicationConfig> getConfigChangeListener ( )
    {
        return applicationConfigChangeListener;
    }

    public ChangeProducer<DisplayStatus> getDisplayStatusChangeProducer ( )
    {
        return displayStatusChangeProducer;
    }

    private int iGetChannelTriggerIndex ( )
    {
        final int ret;

        if ( triggerModeTriggerOnChannelOneProperty.get () )
        {
            ret = 0;
        }

        else if ( triggerModeTriggerOnChannelTwoProperty.get () )
        {
            ret = 1;
        }

        else
        {
            ret = -1;
        }

        return ret;
    }

    private boolean iGetIsInAutomaticMode ( )
    {
        return
            ! triggerModeTriggerOnChannelOneProperty.get () &&
            ! triggerModeTriggerOnChannelTwoProperty.get ();
    }

    private void iComputeDecimationRate ( ApplicationConfig aInApplicationConfig )
    {
        final double numberOfSamplesPerSec = aInApplicationConfig.getSignalSourceBuilder ().getSamplingFrequencyEnum ().getValue () * aInApplicationConfig.getSignalPaneBuilder ().getHorizontalGridCount () * ( displayTimeMsPerDivProperty.get ( ) * 0.001 );
        final double maxNumberOfDisplayedPointsPerSec = aInApplicationConfig.getSignalPaneBuilder ( ).getMaxNumberOfDisplayedPointsPerSec ( );
        sampleDecimationDisplayRate = ( int ) Math.ceil ( numberOfSamplesPerSec / maxNumberOfDisplayedPointsPerSec );
    }

    private void iInvalidate ( )
    {
        logger.info ( "iInvalidate() -- BEGIN" );

        applicationConfig = applicationConfigProvider.getCopyOfLatestConfig ();

        if ( signalSource == null )
        {
            logger.error ( "signalSource is null, skipping invalidation process" );
        }

        else
        {
            nextIndexForCurrentScreen = 0;
            startTimeForCurrentScreen = - 1;
            sampleSweepCount = -1;
            displayLimitInSamples = ( long ) ( applicationConfig.getSignalPaneBuilder ().getHorizontalGridCount () * ( displayTimeMsPerDivProperty.get ( ) * 0.001 ) * signalSource.getSamplingFrequencyInHz ( ) );
            holdOffLimitInSamples = displayLimitInSamples + ( long ) ( holdOffTimeInSecondsValueProperty.get ( ) * signalSource.getSamplingFrequencyInHz ( ) );
            samplesForCurrentScreen = new float[(int)displayLimitInSamples][signalVessels.getNumberOfChannels ( )];

            for ( int lChannelIndex = 0; lChannelIndex < signalSource.getNumberOfChannels ( ); lChannelIndex++ )
            {
                usePositiveSlope[lChannelIndex] = channelTriggerLevelProperty[lChannelIndex].get() >= 0;
            }

            channelTriggeredIndex = iGetChannelTriggerIndex ();
            displayTimeIsShort = displayTimeMsPerDivProperty.get () < applicationConfig.getSignalPaneBuilder ( ).getDisplayModeThresholdMSecPerDiv ( );
            isInAutoMode = iGetIsInAutomaticMode ();

            // VU meter goes every 50 ms
            samplesForVUMeterRefreshCycle = applicationConfig.getSignalSourceBuilder ().getSamplingFrequencyEnum ( ).getValue () / 20;

            iComputeDecimationRate ( applicationConfig );
            iHandleVesselFiltersConfig ( );
            iRecomputeAffines ( );
            iRecomputeVisibility ( );
            iRecomputeTriggerLevels ( );
            iRefreshAllScreens ( );
            iResetPreviousScreenState ( );
            iResetCircularQueue ();

            triggerAudioClip.setVolume ( triggerSoundVolumeValueProperty.get ( ) / 5.0 );
        }

        logger.info ( "iInvalidate() -- END" );
    }

    private static class CircularQueueFiller implements CircularQueue.ISlotFiller<float[]>
    {
        private float data[];

        private CircularQueueFiller()
        {
            data = null;
        }

        public CircularQueueFiller setData( float[] in )
        {
            data = in;
            return this;
        }

        @Override
        public void fill ( float[] targetToBeFilled )
        {
            System.arraycopy ( data, 0, targetToBeFilled, 0, data.length );
        }
    }

    private void iResetCircularQueue()
    {
        final int ensembleSize = applicationConfig.getSignalSourceBuilder ().getNChannels ();

        final double displayTimeInSecs = ( (double) applicationConfig.getSignalPaneBuilder ().getHorizontalGridCount () ) * applicationConfig.getSignalPaneBuilder ().getDisplayTimeMsPerDivision () * 0.001;

        final double delayTimeInSecs = displayTimeInSecs * 0.5;

        final int queueSize = 2 + (int) Math.round ( delayTimeInSecs * ( double) applicationConfig.getSignalSourceBuilder ().getSamplingFrequencyEnum ().getValue () );

        preTriggerQueue = CircularQueue
            .<float[]>create ( )
            .setDebugMode ( false )
            .setAllowDataOverrun ( true )
            .setSlots ( new float[queueSize][ensembleSize] );

        preTriggerQueueFiller = new CircularQueueFiller ( );
    }

    private void iHandleVesselFiltersConfig ( )
    {
        // FIXME
        // HUGE, HUGE, HUGE WARNING !!!!
        // so why if we remove the invocation to useAdaptiveFilteringProperty.get () below,
        // then this code stops to be executed ???? ..   this is crazy..
        // set a break point and you'll see ..

        applicationConfig.getFilterBuilder ( ).setUseAdaptiveFiltering ( useAdaptiveFilteringProperty.get ( ) );

        filters.setApplicationConfig ( applicationConfig );
    }

    private static boolean iShouldResetSignalSource (
        ApplicationConfig aInNewConfig,
        ApplicationConfig aInOldConfig )
    {
        Preconditions.checkNotNull ( aInNewConfig );

        final boolean ret;

        if ( aInOldConfig == null )
        {
            ret = true;
        }

        else
        {
            if ( logger.isTraceEnabled () )
            {
                logger.trace( "point (2.1): " + aInOldConfig.getSignalSourceBuilder ().toString () );
                logger.trace( "point (2.2): " + aInNewConfig.getSignalSourceBuilder ().toString () );
            }

            ret = ! aInOldConfig.getSignalSourceBuilder ().toString ().equals ( aInNewConfig.getSignalSourceBuilder ().toString () );
        }

        return ret;
    }

    private void iInit( ApplicationConfig aInNewConfig, ApplicationConfig aInOldConfig ) throws Exception
    {
        if ( ! iShouldResetSignalSource ( aInNewConfig, aInOldConfig ) )
        {
            logger.info ( "should not reset signal source" );
        }

        else
        {
            if ( aInNewConfig.getSignalSourceBuilder ( ).getSignalSourceType () == SignalSourceBuilder.SignalSourceType.soundDriver )
            {
                JavaFxUtils.showNotification (
                    LanguageManager.getSingleton ( ).getValue ( "monitoring-initializing" ) +
                    "\n" +
                    aInNewConfig.getSignalSourceBuilder ( ).getAudioMixerDescriptionRegexp ( ),
                    JavaFxUtils.NotificationLevel.INFO
                );
            }

            signalSource = SignalSourceFactory.create ( aInNewConfig.getSignalSourceBuilder ( ) );
            signalSource.attachVesselFilter ( filters );

            logger.info ( "new signalSource: " + signalSource );

            if ( aInNewConfig.getSignalSourceBuilder ( ).getSignalSourceType () == SignalSourceBuilder.SignalSourceType.soundDriver )
            {
                if ( signalSource.isAvailable ( ) )
                {
                    JavaFxUtils.showNotification (
                        LanguageManager.getSingleton ( ).getValue ( "monitoring-initialized-ok" ) + "\n" + signalSource.getDescription ( ),
                        JavaFxUtils.NotificationLevel.INFO
                    );
                }

                else
                {
                    JavaFxUtils.showNotification (
                        LanguageManager.getSingleton ( ).getValue ( "monitoring-initialized-failed" ) + "\n" + signalSource.getDescription ( ),
                        JavaFxUtils.NotificationLevel.ERROR
                    );
                }
            }
        }

        Preconditions.checkNotNull ( signalSource );
        Preconditions.checkState ( signalSource.getNumberOfChannels ( ) == aInNewConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).size ( ) );

        useAdaptiveFilteringProperty = new SimpleBooleanProperty ( aInNewConfig.getFilterBuilder ().getUseAdaptiveFiltering ( ) );

        sampleSweepCount = -1;
        holdOffLimitInSamples = -1;
        displayLimitInSamples = -1;

        displayTimeMsPerDivProperty = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ().getHorizontalGridCount () * aInNewConfig.getSignalPaneBuilder ( ).getDisplayTimeMsPerDivision ( ) );
        holdOffTimeInSecondsValueProperty = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ( ).getTriggerFreezeTimeInSeconds ( ) );
        triggerSoundVolumeValueProperty = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ( ).getTriggerSoundVolume ( ) );

        channelSensitivityProperty = new DoubleProperty[2];
        channelSensitivityProperty[0] = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ().get ( 0 ).getSensitivityMilliVoltsPerDiv ( ) );
        channelSensitivityProperty[1] = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ().get ( 1 ).getSensitivityMilliVoltsPerDiv ( ) );

        channelTriggerLevelProperty = new DoubleProperty[2];
        channelTriggerLevelProperty[0] = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ().get ( 0 ).getTriggerLevelPercentage ( ) );
        channelTriggerLevelProperty[1] = new SimpleDoubleProperty ( aInNewConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ().get ( 1 ).getTriggerLevelPercentage () );

        triggerModeContinuousProperty = new SimpleBooleanProperty (  );
        triggerModeTriggerOnChannelOneProperty = new SimpleBooleanProperty (  );
        triggerModeTriggerOnChannelTwoProperty = new SimpleBooleanProperty (  );

        iComputeDecimationRate ( aInNewConfig );

        logger.info (
            "sampleDecimationDisplayRate: " +
                sampleDecimationDisplayRate +
            ", effective maximum number of points per screen: " +
                ( long ) ( signalSource.getSamplingFrequencyInHz ( ) * aInNewConfig.getSignalPaneBuilder ().getHorizontalGridCount () * ( displayTimeMsPerDivProperty.get ( ) * 0.001 ) / sampleDecimationDisplayRate ) );

        signalVessels = signalSource.createSignalVessels ( );
        Preconditions.checkArgument ( signalVessels.getNumberOfChannels () == 2 );

        final float[] lSampleTimeSlice = new float [ signalVessels.getNumberOfChannels ( ) ];

        affineTransformations = new Affine[signalSource.getNumberOfChannels ( )];
        isChannelVisible = new boolean[signalSource.getNumberOfChannels ( )];
        lastDisplayedPoint = new Point2D[signalSource.getNumberOfChannels ( )];
        vuMeterFilter = new RectifyingFilter[signalSource.getNumberOfChannels ( )];
        sampleSpaceTriggerLevel = new long[signalSource.getNumberOfChannels ( )];
        usePositiveSlope = new boolean[signalSource.getNumberOfChannels ( )];

        holdoffTimeDampener
            .setHoldOffDelayInMSec ( 100 )
            .setTask (
                ( ) ->
                JavaFxUtils.executeInFxThread (
                    ( ) ->
                    {
                        try
                        {
                            insideInvalidation.set ( true );

                            iInvalidate ( );
                        }

                        catch ( Exception ex )
                        {
                            logger.error ( ex.getMessage ( ), ex );
                        }

                        finally
                        {
                            insideInvalidation.set ( false );
                        }

                        return null;
                    }
                )
            );

        final InvalidationListener lInvalidationListener =
            ( Observable observable ) ->
            {
                logger.info ( "invalidated() -- BEGIN " );

                logger.info ( "observable: " + observable );

                try
                {
                    holdoffTimeDampener.doReschedule ( );
                }

                catch (Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }

                logger.info ( "invalidated() -- END " );
            };

        signalCanvases = new ResizableCanvas[LAYER_ENUM.values ().length][signalSource.getNumberOfChannels ( )];

        for ( int lChannelIndex = 0; lChannelIndex < signalSource.getNumberOfChannels ( ); lChannelIndex++ )
        {
            for ( LAYER_ENUM layer : LAYER_ENUM.values ( ) )
            {
                signalCanvases[layer.ordinal ( )][lChannelIndex] = new ResizableCanvas ( );
            }

            {
                final GraphicsContext gc = signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex].getGraphicsContext2D ( );
                gc.setStroke ( aInNewConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( lChannelIndex ).getRgbColor ( ) );
                gc.setLineDashes ( );
                gc.setLineCap ( StrokeLineCap.ROUND );
                gc.setLineWidth ( 1.5 );
            }

            {
                final GraphicsContext gc = signalCanvases[LAYER_ENUM.FAST_COUNTERS.ordinal ( )][lChannelIndex].getGraphicsContext2D ( );
                gc.setTextBaseline ( VPos.BASELINE );
                gc.setFont ( Font.font ( "Monospaced", FontWeight.BOLD, 14 ) );
                gc.setTextAlign ( TextAlignment.RIGHT );
                gc.setFill ( Color.WHITE );
            }

            signalCanvases[LAYER_ENUM.GRID.ordinal ()][lChannelIndex].widthProperty().addListener ( lInvalidationListener );
            signalCanvases[LAYER_ENUM.GRID.ordinal ()][lChannelIndex].heightProperty().addListener ( lInvalidationListener );

            vuMeterFilter[lChannelIndex] = RectifyingFilter
                .create ( )
                .setFsInHz ( signalSource.getSamplingFrequencyInHz ( ) )
                .setHoldOffInMs ( 100 )
                .setTauInMs ( 100 )
                .start ();

            lastDisplayedPoint[lChannelIndex] = null;

            channelSensitivityProperty[lChannelIndex].addListener ( lInvalidationListener );
            channelTriggerLevelProperty[lChannelIndex].addListener ( lInvalidationListener );
        }

        displayTimeMsPerDivProperty.addListener ( lInvalidationListener );
        holdOffTimeInSecondsValueProperty.addListener ( lInvalidationListener );
        triggerSoundVolumeValueProperty.addListener ( lInvalidationListener );
        triggerModeContinuousProperty.addListener ( lInvalidationListener );
        triggerModeTriggerOnChannelOneProperty.addListener ( lInvalidationListener );
        triggerModeTriggerOnChannelTwoProperty.addListener ( lInvalidationListener );
        useAdaptiveFilteringProperty.addListener ( lInvalidationListener );

        if ( ! animationScheduled.get () )
        {
            animationScheduled.set( true );

            Preconditions.checkState ( animation == null );

            animation = new Timeline ( );

            animation.setCycleCount ( Animation.INDEFINITE );

            animation.getKeyFrames ( ).add (
                new KeyFrame (
                    Duration.millis ( animationPeriodMs ),
                    ( ActionEvent actionEvent ) ->
                    {
                        try
                        {
                            if ( isInitializing.get ( ) )
                            {
                                logger.trace ( "initializing, skipping" );
                            }

                            else if ( insideInvalidation.get() )
                            {
                                logger.trace ( "invalidation is executing right at this time, skipping" );
                            }

                            else if ( signalSource == null )
                            {
                                logger.trace ( "signalSource is null, builder is: " + aInNewConfig.getSignalSourceBuilder ( ).toString ( ) );
                            }

                            else if ( ! signalSource.isAvailable ( ) )
                            {
                                logger.trace ( "signalSource is not available, builder is: " + aInNewConfig.getSignalSourceBuilder ( ).toString ( ) );
                            }

                            else
                            {
                                if ( signalSource.getIsStarted () )
                                {
                                    timeoutFilter.mark ( ! signalSource.getSamplesAreAvailable ( ) );

                                    if ( timeoutFilter.isConditionHeld ( ) )
                                    {
                                        logger.info ( "inactivity detected, closing line: " + timeoutFilter );

                                        JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "restart-application" ) + "\n" + signalSource.getDescription ( ), JavaFxUtils.NotificationLevel.ERROR, true );

                                        signalSource.close();

                                        signalSource = null;
                                    }
                                }

                                if ( signalSource != null )
                                {
                                    while ( signalSource.getSamplesAreAvailable ( ) )
                                    {
                                        signalSource.fillOutWithNextSamples ( signalVessels );

                                        for ( int count = 0; count < signalVessels.getNumberOfSamples ( ); count++ )
                                        {
                                            monotonicSampleCount++;

                                            signalVessels.getOneSampleOnTimeIndex ( count, lSampleTimeSlice );

                                            if ( preTriggerQueue != null )
                                            {
                                                Preconditions.checkNotNull ( preTriggerQueueFiller );

                                                if ( ! isInAutoMode )
                                                {
                                                    preTriggerQueue.put ( preTriggerQueueFiller.setData ( lSampleTimeSlice ) );
                                                }

                                                iPlotSignal ( lSampleTimeSlice );

                                                iPlotVUMeter ( lSampleTimeSlice );
                                            }
                                        }
                                    }
                                }
                            }

                            iDrawFastCounters ( );
                        }

                        catch ( Throwable t )
                        {
                            logger.error ( t.getMessage (), t );
                        }
                    }
                )
            );

            animation.play ( );
        }
    }

    private void iRecomputeAffines ( )
    {
        for ( int lChannelIndex = 0; lChannelIndex < signalSource.getNumberOfChannels ( ); lChannelIndex++ )
        {
            affineTransformations[lChannelIndex] = iCreateNewAffine (
                signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex],
                iComputeGainFromTheConfig ( channelSensitivityProperty[lChannelIndex].get ( ) ) );
        }
    }

    private void iRecomputeVisibility ( )
    {
        for ( int lChannelIndex = 0; lChannelIndex < signalSource.getNumberOfChannels ( ); lChannelIndex++ )
        {
            isChannelVisible[lChannelIndex] = applicationConfig.getSignalPaneBuilder ().getSignalChannelBuilders ().get ( lChannelIndex ).getVisible ();
        }
    }

    private Pair<Double, Double> iComputeSampleDisplayableBounds ( double aInChannelGain )
    {
        final double middlePointInSampleSpace = 0.5 * ( signalSource.getSampleSpaceLowerBound ( ) + signalSource.getSampleSpaceUpperBound ( ) );
        final double middleValueInSampleSpaceRange = 0.5 * ( signalSource.getSampleSpaceUpperBound ( ) - signalSource.getSampleSpaceLowerBound ( ) );

        final double sampleA1 = middlePointInSampleSpace - middleValueInSampleSpaceRange / aInChannelGain;
        final double sampleA2 = middlePointInSampleSpace + middleValueInSampleSpaceRange / aInChannelGain;

        return new Pair<> ( sampleA1 , sampleA2 );
    }

    private void iRecomputeTriggerLevels ( )
    {
        for ( int i=0; i<applicationConfig.getSignalSourceBuilder ().getNChannels ( ); i++ )
        {
            final Pair<Double, Double> lSampleSpaceDisplayableBounds = iComputeSampleDisplayableBounds ( iComputeGainFromTheConfig ( channelSensitivityProperty[i].get ( ) ) );

            final double sampleA1 = lSampleSpaceDisplayableBounds.getKey ();
            final double sampleA2 = lSampleSpaceDisplayableBounds.getValue ();

            sampleSpaceTriggerLevel[i] = (long) ( sampleA1 + ( ( ( (channelTriggerLevelProperty[i].get () / 100.0f) + 1.0 ) / 2 ) * ( sampleA2 - sampleA1 ) ) );
        }
    }

    private Affine iCreateNewAffine ( Canvas aInCanvas, double aInChannelGain )
    {
        final Affine lAffine = new Affine();

        final Pair<Double, Double> lDisplayableBounds = iComputeSampleDisplayableBounds ( aInChannelGain );

        final double sampleA1 = lDisplayableBounds.getKey ();
        final double sampleA2 = lDisplayableBounds.getValue ( );

        final double displayB1 = aInCanvas.getHeight () - 1;
        final double displayB2 = 0;

        final double k1 = ( displayB1 - displayB2 ) / ( sampleA1 - sampleA2 );
        final double k2 = ( sampleA2 * displayB1 - sampleA1 * displayB2 ) / ( sampleA2 - sampleA1 );

        lAffine.prependScale (
            ( aInCanvas.getWidth ( ) - vuMeterWidth ) / ( applicationConfig.getSignalPaneBuilder ().getHorizontalGridCount () * ( displayTimeMsPerDivProperty.get ( ) * 0.001 ) ), k1 );

        lAffine.prependTranslation (
            vuMeterWidth,
            k2 );

        return lAffine;
    }

    private void iPlotVUMeter( float[] aInNextSignalEnsemble )
    {
        for ( int lChannelIndex = 0; lChannelIndex < aInNextSignalEnsemble.length; lChannelIndex++ )
        {
            final double lNextSample = vuMeterFilter[lChannelIndex].filter ( aInNextSignalEnsemble[lChannelIndex] );

            if ( monotonicSampleCount % samplesForVUMeterRefreshCycle == 0 )
            {
                final Canvas lCanvas = signalCanvases[LAYER_ENUM.VU_METER.ordinal ()][lChannelIndex];
                final GraphicsContext gc = lCanvas.getGraphicsContext2D ( );

                if ( Double.isNaN ( lNextSample ) || Double.isInfinite ( lNextSample ) )
                {
                    gc.setFill ( Color.RED );
                    gc.fillRect ( vuMeterInset, JavaFxUtils.getUpperLeftPoint ( gc ).getY (), vuMeterWidth - 2 * vuMeterInset, JavaFxUtils.getLowerLeftPoint ( gc ).getY ( ) );
                }

                else
                {
                    gc.setFill ( applicationConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( lChannelIndex ).getRgbColor ( ) );

                    if ( null != affineTransformations[lChannelIndex] )
                    {
                        final Point2D lNewPoint = affineTransformations[lChannelIndex].transform ( 0, lNextSample );
                        final Point2D lNewZeroPoint = affineTransformations[lChannelIndex].transform ( 0, 0 );

                        gc.clearRect ( vuMeterInset, 0, vuMeterWidth - 2 * vuMeterInset, lNewPoint.getY ( ) );
                        gc.fillRect ( vuMeterInset, lNewPoint.getY ( ), vuMeterWidth - 2 * vuMeterInset, lNewZeroPoint.getY ( ) - lNewPoint.getY ( ) );
                    }
                }
            }
        }
    }

    private void iPlotSignal( float[] aInNextSignalEnsemble )
    {
        if ( displayTimeIsShort )
        {
            iPlotSignalInScreenMode ( aInNextSignalEnsemble );
        }

        else
        {
            iPlotSignalInSampleMode ( aInNextSignalEnsemble );
        }
    }

    private boolean iIsTriggerConditionMetOnSignal ()
    {
        final boolean ret;

        if ( usePositiveSlope [channelTriggeredIndex] )
        {
            ret =
                preTriggerQueue.size () > 1
                &&
                preTriggerQueue.peekFirst ( 1 ) [channelTriggeredIndex] < sampleSpaceTriggerLevel [channelTriggeredIndex]
                &&
                preTriggerQueue.peekFirst ( 0 ) [channelTriggeredIndex] > sampleSpaceTriggerLevel [channelTriggeredIndex];
        }

        else
        {
            ret =
                preTriggerQueue.size () > 1
                &&
                preTriggerQueue.peekFirst ( 1 ) [channelTriggeredIndex] > sampleSpaceTriggerLevel [channelTriggeredIndex]
                &&
                preTriggerQueue.peekFirst ( 0 ) [channelTriggeredIndex] < sampleSpaceTriggerLevel [channelTriggeredIndex];
        }

        return ret;
    }

    private void iPlotSignalInSampleMode ( float[] aInNextSignalEnsemble )
    {
        if ( sampleSweepCount == - 1 )
        {
            if ( isInAutoMode || iIsTriggerConditionMetOnSignal ( ) )
            {
                sampleSweepCount++;

                if ( ! isInAutoMode )
                {
                    if ( applicationConfig.getSignalPaneBuilder ( ).getTriggerSoundVolume () > 0.1 )
                    {
                        triggerCleanupExecutor.submit ( playTriggerAlarmCallable );
                    }
                }

                iCleanSignalCanvas ( );
                iSaveCurrentDisplayedSample ( aInNextSignalEnsemble );

                if ( applicationConfig.getSignalPaneBuilder ( ).getIsScreensSavingWindowEnabled ( ) )
                {
                    iAccumulateOneSampleForScreenSaving ( aInNextSignalEnsemble );
                }
            }
        }

        else if ( sampleSweepCount < displayLimitInSamples )
        {
            sampleSweepCount++;

            if ( ( sampleSweepCount % sampleDecimationDisplayRate ) == 0 )
            {
                iPlotSignalForCurrentSample ( aInNextSignalEnsemble );
                iSaveCurrentDisplayedSample ( aInNextSignalEnsemble );
            }

            if ( applicationConfig.getSignalPaneBuilder ( ).getIsScreensSavingWindowEnabled ( ) )
            {
                iAccumulateOneSampleForScreenSaving ( aInNextSignalEnsemble );
            }
        }

        else if ( sampleSweepCount == displayLimitInSamples )
        {
            sampleSweepCount++;
            screenCount.incrementAndGet ( );
            lastDisplayTime = System.currentTimeMillis ( );

            if ( applicationConfig.getSignalPaneBuilder ( ).getIsScreensSavingWindowEnabled ( ) )
            {
                iSaveScreenSamplesIntoQueueA ( );
            }
        }

        else if ( sampleSweepCount < holdOffLimitInSamples )
        {
            sampleSweepCount++;
        }

        else // sampleSweepCount >= holdOffLimitInSamples )
        {
            sampleSweepCount = - 1;
        }
    }

    private void iPlotSignalInScreenMode ( float[] aInNextSignalEnsemble )
    {
        if ( sampleSweepCount == -1 )
        {
            if ( isInAutoMode || iIsTriggerConditionMetOnSignal( ) )
            {
                if ( ! isInAutoMode )
                {
                    if ( applicationConfig.getSignalPaneBuilder ( ).getTriggerSoundVolume () > 0.1 )
                    {
                        triggerCleanupExecutor.submit ( playTriggerAlarmCallable );
                    }

                    while ( ! preTriggerQueue.isEmpty () )
                    {
                        final float[] preTriggerSample = preTriggerQueue.consume ( );

                        sampleSweepCount++;

                        iAccumulateSampleForCurrentScreen ( preTriggerSample );

                        if ( applicationConfig.getSignalPaneBuilder ( ).getIsScreensSavingWindowEnabled ( ) )
                        {
                            iAccumulateOneSampleForScreenSaving ( preTriggerSample );
                        }
                    }
                }

                else
                {
                    sampleSweepCount++;

                    iAccumulateSampleForCurrentScreen ( aInNextSignalEnsemble );

                    if ( applicationConfig.getSignalPaneBuilder ( ).getIsScreensSavingWindowEnabled ( ) )
                    {
                        iAccumulateOneSampleForScreenSaving ( aInNextSignalEnsemble );
                    }
                }

                iSaveCurrentDisplayedSample ( aInNextSignalEnsemble );
            }
        }

        else if ( sampleSweepCount < displayLimitInSamples )
        {
            sampleSweepCount++;

            if ( ( sampleSweepCount % sampleDecimationDisplayRate ) == 0 )
            {
                iAccumulateSampleForCurrentScreen ( aInNextSignalEnsemble );
            }

            if ( applicationConfig.getSignalPaneBuilder ().getIsScreensSavingWindowEnabled () )
            {
                iAccumulateOneSampleForScreenSaving ( aInNextSignalEnsemble );
            }

            iSaveCurrentDisplayedSample ( aInNextSignalEnsemble );
        }

        else if ( sampleSweepCount == displayLimitInSamples )
        {
            sampleSweepCount++;
            screenCount.incrementAndGet ();
            lastDisplayTime = System.currentTimeMillis ( );

            iCleanSignalCanvas();
            iPlotSignalForLatestScreen ( );
            iResetPreviousScreenState ();

            if ( applicationConfig.getSignalPaneBuilder ().getIsScreensSavingWindowEnabled () )
            {
                iSaveScreenSamplesIntoQueueA ( );
            }
        }

        else if ( sampleSweepCount < holdOffLimitInSamples )
        {
            sampleSweepCount++;
        }

        else // sampleSweepCount >= holdOffLimitInSamples )
        {
            sampleSweepCount = -1;
        }
    }

    private void iAccumulateOneSampleForScreenSaving ( float[] aInNextSignalEnsemble )
    {
        if ( nextIndexForCurrentScreen == 0 )
        {
            startTimeForCurrentScreen = System.currentTimeMillis ();
        }

        if ( nextIndexForCurrentScreen < samplesForCurrentScreen.length )
        {
            System.arraycopy (
                aInNextSignalEnsemble,
                0,
                samplesForCurrentScreen[nextIndexForCurrentScreen],
                0,
                aInNextSignalEnsemble.length );

            nextIndexForCurrentScreen++;
        }
    }

    private void iSaveScreenSamplesIntoQueueA ( )
    {
        synchronized ( screenSavingQueueA )
        {
            try
            {
                final ScreenInfo screenInfo = ScreenInfo
                    .create ( )
                    .setAppStartTime ( appStartingTime )
                    // session counter is saved later !!
                    .setSignalStartingTime ( startTimeForCurrentScreen )
                    .setScreenNumber ( screenCount.get () )
                    .setApplicationConfig ( GsonUtil.clone ( applicationConfig ) )
                    .setSignal ( samplesForCurrentScreen, nextIndexForCurrentScreen )
                    .setSignalSourceReadOnlyInfo ( SignalSourceReadOnlyInfo.create( signalSource ) );

                nextIndexForCurrentScreen = 0;

                screenSavingQueueA.addFirst ( screenInfo );

                if ( screenSavingQueueA.size ( ) > (long) applicationConfig.getSignalPaneBuilder ( ).getNumberOfSavedScreens ( ) )
                {
                    screenSavingQueueA.removeLast ( );
                }
            }

            catch ( Exception ex )
            {
                logger.error ( ex.getMessage (), ex );
            }
        }
    }

    private static double iEstimateNumberOfSecondsRecorded ( Iterable<ScreenInfo> aInList )
    {
        double ret = 0;

        for ( ScreenInfo screenInfo : aInList )
        {
            ret += screenInfo.computeLengthInSeconds ();
        }

        return ret;
    }

    public void saveScreensAToB ( )
    {
        synchronized ( screenSavingQueueA )
        {
            synchronized ( screenSavingQueueB )
            {
                if ( screenSavingQueueA.size ( ) > 0 )
                {
                    final int lCount = screenSavingSessionCounter.incrementAndGet ();

                    final double lMaxNumberOfSecondsAllowed = 300;  // 5 minutes

                    if ( ( iEstimateNumberOfSecondsRecorded ( screenSavingQueueA ) + iEstimateNumberOfSecondsRecorded ( screenSavingQueueB ) ) > ( lMaxNumberOfSecondsAllowed + 0.001 ) )
                    {
                        JavaFxUtils.showNotification ( String.format ( LanguageManager.getSingleton ( ).getValue ( "too-many-pending-screens" ), lMaxNumberOfSecondsAllowed ), JavaFxUtils.NotificationLevel.WARN );
                    }

                    else
                    {
                        ScreenInfo screenInfo;
                        while ( ( screenInfo = screenSavingQueueA.pollLast ( ) ) != null )
                        {
                            screenInfo.setScreenSavingSessionCounter ( lCount );
                            screenSavingQueueB.addFirst ( screenInfo );
                        }
                    }
                }
            }
        }
    }

    private String iFormatPatientName()
    {
        final StringBuilder sb = new StringBuilder ( );

        {
            final String lastName = applicationConfig.getSurgeryInfo ( ).getPatientLastName ( );
            sb.append ( lastName.isEmpty ( ) ? "last-name-unknown" : lastName );
        }

        sb.append ( "-" );

        {
            final String firstName = applicationConfig.getSurgeryInfo ( ).getPatientFirstName ( );
            sb.append ( firstName.isEmpty ( ) ? "first-name-unknown" : firstName );
        }

        return sb.toString ();
    }

    private void iSaveSingleScreenToDisk ( final ScreenInfo screenInfo ) throws Exception
    {
        Preconditions.checkState ( ! Platform.isFxApplicationThread ( ) );

        final File lScreenSavingSessionDirectory = FileUtil.getFileFromBaseAndAStringSequence (
            new File ( applicationConfig.getSignalPaneBuilder ( ).getScreensSavingDirectory ( ) ),
            iFormatPatientName (),
            new SimpleDateFormat ( "yyyy-MM-dd.HH-mm-ss" ).format ( appStartingTime ),
            Long.toString ( screenInfo.getScreenSavingSessionCounter () )
        );

        // one json file per screen
        {
            final File lJSonFile = FileUtil.getFileFromBaseAndAStringSequence ( lScreenSavingSessionDirectory,
                "json", new SimpleDateFormat ( "HH-mm-ss-SSS" ).format ( screenInfo.getSignalStartingTime ( ) ) + String.format ( "-%08d.json",
                    screenInfo.getScreenNumber ( ) ) );

            FileUtil.createDirIfDoesNotExist ( lJSonFile.getParentFile ( ) );

            GsonUtil.writeJsonToFile ( lJSonFile, screenInfo );
        }

        {
            // one csv file per screen x channel
            final File lCsvFileCh1 = FileUtil.getFileFromBaseAndAStringSequence (
                lScreenSavingSessionDirectory,
                "csv",
                new SimpleDateFormat ( "HH-mm-ss-SSS" ).format ( screenInfo.getSignalStartingTime ( ) ) + String.format ( "-%08d.1.csv", screenInfo.getScreenNumber ( ) ) );

            final File lCsvFileCh2 = FileUtil.getFileFromBaseAndAStringSequence (
                lScreenSavingSessionDirectory,
                "csv",
                new SimpleDateFormat ( "HH-mm-ss-SSS" ).format ( screenInfo.getSignalStartingTime ( ) ) + String.format ( "-%08d.2.csv", screenInfo.getScreenNumber ( ) ) );

            FileUtil.createDirIfDoesNotExist ( lCsvFileCh1.getParentFile ( ) );
            FileUtil.createDirIfDoesNotExist ( lCsvFileCh2.getParentFile ( ) );

            try ( PrintStream ps1 = FileUtil.createPrintStreamFromFile ( lCsvFileCh1, false );
                  PrintStream ps2 = FileUtil.createPrintStreamFromFile ( lCsvFileCh2, false ) )
            {
                iPrintSignalHeaderToCsv ( ps1, screenInfo, 0 );
                iPrintSignalHeaderToCsv ( ps2, screenInfo, 1 );

                final double periodInMicroSec = ( 1_000_000 / (double) screenInfo.getApplicationConfig ( ).getSignalSourceBuilder ( ).getSamplingFrequencyEnum ( ).getValue () );

                for ( int sampleCount = 0; sampleCount < screenInfo.getSignalLengthInSamples ( ); sampleCount++ )
                {
                    ps1.println ( String.format ( "%d, %.3f, %.3f", sampleCount, sampleCount * periodInMicroSec, screenInfo.getSignal ( )[sampleCount][0] ) );
                    ps2.println ( String.format ( "%d, %.3f, %.3f", sampleCount, sampleCount * periodInMicroSec, screenInfo.getSignal ( )[sampleCount][1] ) );
                }
            }
        }

        {
            // one temporary .png file per screen
            final File lImageFilePath = FileUtil.getFileFromBaseAndAStringSequence ( lScreenSavingSessionDirectory, "png", new SimpleDateFormat ( "HH-mm-ss-SSS" ).format ( screenInfo.getSignalStartingTime ( ) ) + String.format ( "-%08d.png", screenInfo.getScreenNumber ( ) ) );
            FileUtil.createDirIfDoesNotExist ( lImageFilePath.getParentFile ( ) );
            JavaFxUtils.generateFileFromCanvas ( iGenerateCanvasFromScreenInfo ( screenInfo ), "png", lImageFilePath );

            // one .pdf file per screen
            final File lPdfFilePath = FileUtil.getFileFromBaseAndAStringSequence ( lScreenSavingSessionDirectory, "pdf", new SimpleDateFormat ( "HH-mm-ss-SSS" ).format ( screenInfo.getSignalStartingTime ( ) ) + String.format ( "-%08d.pdf", screenInfo.getScreenNumber ( ) ) );
            FileUtil.createDirIfDoesNotExist ( lPdfFilePath.getParentFile ( ) );
            JavaFxUtils.generatePdfFile ( screenInfo, lPdfFilePath, lImageFilePath );

            // remove .png file after .pdf was created
            FileUtil.deleteDirectoryRecursively ( lImageFilePath.getParentFile ( ), true );
        }
    }

    private Canvas iGenerateCanvasFromScreenInfo ( ScreenInfo aInScreenInfo )
    {
        final double h0 = 40;
        final double h1 = 300;
        final double h2 = 390;

        final double w0 = 60;
        final double w1 = 450;
        final double w2 = 550;


        final Canvas canvas = new Canvas ( w2, 2 * h2 );
        final GraphicsContext gc = canvas.getGraphicsContext2D ( );

        int reportIndex=0;

        for ( int lChannelIndex=0; lChannelIndex<applicationConfig.getSignalSourceBuilder ().getNChannels ( ); lChannelIndex++ )
        {
            if ( aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( lChannelIndex ).getVisible ( ) )
            {
                iFillOutCanvasChannelOneFromScreenInfo (
                    gc,
                    lChannelIndex,
                    reportIndex++,
                    aInScreenInfo,
                    h0,
                    h1,
                    h2,
                    w0,
                    w1 );
            }
        }

        return canvas;
    }

    private void iFillOutCanvasChannelOneFromScreenInfo (
        GraphicsContext gc,
        int aInChannelIndex,
        int aInReportIndex,
        ScreenInfo aInScreenInfo,
        double h0,
        double h1,
        double h2,
        double w0,
        double w1 )
    {
        final Point2D lUpperLeftPointSignal = new Point2D ( w0, h0 + aInReportIndex * h2 );
        final Point2D lUpperRightPointSignal = lUpperLeftPointSignal.add ( w1, 0 );
        final Point2D lLowerRightPointSignal = lUpperLeftPointSignal.add ( w1, h1 );
        final Point2D lLowerLeftPointSignal = lUpperLeftPointSignal.add ( 0, h1 );

        final double yCenterb =
            ( aInScreenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceUpperBound () +
              aInScreenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceLowerBound () ) * 0.5;

        final double gain = iComputeGainFromTheConfig (
            aInScreenInfo.getApplicationConfig ().getSignalPaneBuilder ().getSignalChannelBuilders ().get ( aInChannelIndex ).getSensitivityMilliVoltsPerDiv ( ) );

        final double invertedGain = 1.0 / gain;

        final double yMaxb =
            yCenterb +
                ( aInScreenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceUpperBound () -
                  aInScreenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceLowerBound ( ) ) * 0.5 * invertedGain;

        final double yMinb =
            yCenterb -
                ( aInScreenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceUpperBound () -
                  aInScreenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceLowerBound ( ) ) * 0.5 * invertedGain;

        final JavaFxUtils.LinearTx yLinearTx = new JavaFxUtils.LinearTx (
            yMaxb,
            lUpperLeftPointSignal.getY (),
            yMinb,
            lLowerRightPointSignal.getY () );

        final JavaFxUtils.LinearTx xLinearTx = new JavaFxUtils.LinearTx (
            0,
            lUpperLeftPointSignal.getX ( ),
            (double) aInScreenInfo.getSignalLengthInSamples ( ) / (double) aInScreenInfo.getSignalSourceReadOnlyInfo ().getSamplingFrequencyInHz ( ),
            lLowerRightPointSignal.getX () );

        for ( int i=0; i<aInScreenInfo.getSignalLengthInSamples ( ); i++ )
        {
            double px = xLinearTx.tx ( i / ( double) aInScreenInfo.getSignalSourceReadOnlyInfo ().getSamplingFrequencyInHz ( ) );
            px = JavaFxUtils.capValueInBetweenMinAndMax ( px, lUpperLeftPointSignal.getX ( ), lLowerRightPointSignal.getX ( ) );

            double py = yLinearTx.tx ( (double) aInScreenInfo.getSignal()[i][aInChannelIndex] );
            py = JavaFxUtils.capValueInBetweenMinAndMax ( py, lUpperLeftPointSignal.getY ( ), lLowerRightPointSignal.getY ( ) );

            if ( i == 0 )
            {
                gc.moveTo (px, py );
            }
            else
            {
                gc.lineTo ( px, py );
            }
        }

        gc.setStroke ( Color.DARKBLUE );
        gc.setLineWidth ( 1 );
        gc.stroke ();

        gc.setStroke ( Color.BLACK );
        gc.setLineWidth ( 1 );
        JavaFxUtils.drawRect ( gc, lUpperLeftPointSignal, lLowerRightPointSignal );

        final double maxTick =
              aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getVerticalGridCount () *
              aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( aInChannelIndex ).getSensitivityMilliVoltsPerDiv ( ) / 2;

        final double minTick = - maxTick;

        final String markFormat = "%.3f";

        iGenerateTickNumbers (
            true,
            gc,
            lUpperLeftPointSignal,
            lLowerLeftPointSignal,
            11,
            minTick,
            maxTick,
            markFormat );

        iGenerateTickLines (
            true,
            gc,
            lUpperLeftPointSignal,
            lLowerLeftPointSignal,
            11 );

        iGenerateTickLines (
            false,
            gc,
            lLowerRightPointSignal,
            lLowerLeftPointSignal,
            11 );

        iGenerateTickNumbers (
            false,
            gc,
            lLowerRightPointSignal,
            lLowerLeftPointSignal,
            11,
            0,
            aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getHorizontalGridCount () *
            aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getDisplayTimeMsPerDivision ( ),
            markFormat );

        {
            // header
            gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 14 ) );
            gc.setTextAlign ( TextAlignment.CENTER );
            final String headerString = aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( aInChannelIndex ).getSignalName ( );
            final Point2D titlePoint = lUpperLeftPointSignal.midpoint ( lUpperRightPointSignal ).add ( 0, -10 );
            gc.fillText ( headerString, titlePoint.getX ( ), titlePoint.getY ( ) );
        }

        {
            // footer
            gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 10 ) );
            gc.setTextAlign ( TextAlignment.CENTER );
            final String footerString =
                String.format (
                    markFormat + " mV/div" + "   " + markFormat + " mSec/div",
                    aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( aInChannelIndex ).getSensitivityMilliVoltsPerDiv (),
                    aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getDisplayTimeMsPerDivision () );
            final Point2D footerPoint = lLowerLeftPointSignal.midpoint ( lLowerRightPointSignal ).add ( 0, 30 );
            gc.fillText ( footerString, footerPoint.getX ( ), footerPoint.getY ( ) );
        }
    }

    private static void iGenerateTickNumbers (
        boolean aInVertical,
        GraphicsContext gc,
        Point2D pointMin,
        Point2D pointMax,
        int numberOfTicks,
        double minTick,
        double maxTick,
        String mask )
    {
        final double delta = ( maxTick - minTick ) / ( numberOfTicks - 1 );
        double tick = minTick;

        gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 8 ) );
        gc.setTextAlign ( aInVertical ? TextAlignment.RIGHT : TextAlignment.CENTER );

        for ( Point2D next : JavaFxUtils.getPointsInBetween ( pointMin, pointMax, numberOfTicks ) )
        {
            final Point2D point;

            if ( aInVertical )
            {
                point = next.add ( -5, 0 );
            }

            else
            {
                point = next.add ( 0, 10 );
            }

            gc.fillText ( String.format ( mask, tick ), point.getX (), point.getY () );

            tick = tick + delta;
        }
    }

    private static void iGenerateTickLines (
        boolean aInVertical,
        GraphicsContext gc,
        Point2D pointMin,
        Point2D pointMax,
        int numberOfTicks )
    {
        for ( Point2D pointA : JavaFxUtils.getPointsInBetween ( pointMin, pointMax, numberOfTicks ) )
        {
            final Point2D pointB;

            if ( aInVertical )
            {
                pointB = pointA.add ( 5, 0 );
            }

            else
            {
                pointB = pointA.add ( 0, -5 );
            }

            JavaFxUtils.strokeLine ( gc, pointA, pointB, true );
        }
    }

    private void iPrintSignalHeaderToCsv ( PrintStream ps, ScreenInfo aInScreenInfo, int aInChannelIndex )
    {
        ps.println (
            "appStartingTime" +
            ", screenNumber" +
            ", signalLength" +
            ", signalStartingTime" +
            ", samplingFrequency" +
            ", displayTimeMilliSecPerDiv" +
            ", verticalGainMilliVoltPerDiv" +
            ", signalName" );

        ps.println (
            String.format(
                "%d, %d, %d, %d, %d, %.1f, %.1f, %s",
                appStartingTime,
                aInScreenInfo.getScreenNumber ( ),
                aInScreenInfo.getSignalLengthInSamples ( ),
                aInScreenInfo.getSignalStartingTime ( ),
                aInScreenInfo.getApplicationConfig ( ).getSignalSourceBuilder ( ).getSamplingFrequencyEnum ( ).getValue (),
                displayTimeMsPerDivProperty.get ( ),
                channelSensitivityProperty[aInChannelIndex].get ( ),
                aInScreenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( aInChannelIndex ).getSignalName ( )
            )
        );

        ps.println ();

        ps.println (
            "count" +
            ", timeMicroSec" +
            ", value"
        );
    }

    private void iDrawTrigger ()
    {
        for ( int lChannelIndex=0; lChannelIndex<applicationConfig.getSignalSourceBuilder ().getNChannels ( ); lChannelIndex++ )
        {
            final Canvas canvas = signalCanvases[LAYER_ENUM.TRIGGER.ordinal ()][lChannelIndex];
            final GraphicsContext gc = canvas.getGraphicsContext2D ( );

            gc.clearRect ( 0, 0, gc.getCanvas ( ).getWidth ( ), gc.getCanvas ( ).getHeight ( ) );

            gc.setLineCap ( StrokeLineCap.ROUND );

            final Point2D lPointForLevelLeft = affineTransformations[lChannelIndex].transform (
                0,
                sampleSpaceTriggerLevel[lChannelIndex] );

            final Point2D lPointForLevelRight = affineTransformations[lChannelIndex].transform (
                applicationConfig.getSignalPaneBuilder ().getHorizontalGridCount () * ( displayTimeMsPerDivProperty.get ( ) * 0.001 ),
                sampleSpaceTriggerLevel[lChannelIndex] );

            final Color enabledColor = Color.RED.darker();
            final Color disabledColor = Color.GREEN.darker().darker();

            final Color triggerColor;

            if ( isInAutoMode )
            {
                triggerColor = disabledColor;
            }

            else
            {
                triggerColor = ( lChannelIndex == channelTriggeredIndex ) ? enabledColor : disabledColor;
            }

            gc.setLineWidth ( 1 );
            gc.setStroke ( triggerColor );
            gc.setFill ( triggerColor );
            JavaFxUtils.strokeLine ( gc, lPointForLevelLeft, lPointForLevelRight, false );

            if ( ! isInAutoMode )
            {
                if ( displayTimeIsShort )
                {
                    JavaFxUtils.strokeCenteredCircle ( gc, lPointForLevelLeft.midpoint ( lPointForLevelRight ), 4 );
                }
                else
                {
                    JavaFxUtils.strokeCenteredCircle ( gc, lPointForLevelLeft, 4 );
                }
            }
        }
    }

    private int iGetHighestIndexChannel ( )
    {
        return iGetIndexChannel ( false );
    }

    private int iGetLowestIndexChannel ( )
    {
        return iGetIndexChannel ( true );
    }

    private int iGetIndexChannel ( boolean aInBreakOnFirstMatch )
    {
        int channelIndex = -1;

        for ( int lChannelIndex=0; lChannelIndex < applicationConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).size ( ); lChannelIndex++ )
        {
            if ( applicationConfig.getSignalPaneBuilder ().getSignalChannelBuilders ().get ( lChannelIndex ).getVisible () )
            {
                channelIndex = lChannelIndex;

                if ( aInBreakOnFirstMatch )
                {
                    break;
                }
            }
        }

        Preconditions.checkState ( channelIndex == 0 || channelIndex == 1 );

        return channelIndex;
    }

    private void iDrawCanvasWithConfigurationInfo ( )
    {
        logger.info( "iDrawCanvasWithConfigurationInfo() -- BEGIN" );

        for ( int lChannelIndex=0; lChannelIndex<applicationConfig.getSignalSourceBuilder ().getNChannels ( ); lChannelIndex++ )
        {
            final Canvas canvas = signalCanvases[LAYER_ENUM.CONFIG_INFO.ordinal ()][lChannelIndex];
            final GraphicsContext gc = canvas.getGraphicsContext2D ( );

            gc.clearRect ( 0, 0, gc.getCanvas ( ).getWidth ( ), gc.getCanvas ( ).getHeight ( ) );

            if ( signalSource != null )
            {
                final SignalChannelBuilder lSignalChannelBuilder = applicationConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( lChannelIndex );

                // both channels
                {
                    final Point2D lNearUpperLeftPoint = JavaFxUtils.getUpperLeftPoint ( gc ).add ( vuMeterWidth, 0 ).add ( 10, 10 );

                    gc.setTextBaseline ( VPos.TOP );
                    gc.setFill ( lSignalChannelBuilder.getRgbColor ( ) );
                    gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 28 ) );
                    gc.setTextAlign ( TextAlignment.LEFT );
                    gc.fillText (
                        languageManager.getValue ( "Channel" ) + " " + ( lChannelIndex + 1 ) + "  " + lSignalChannelBuilder.getSignalName ( ),
                        lNearUpperLeftPoint.getX ( ),
                        lNearUpperLeftPoint.getY ( ) );
                }

                // both channels
                {
                    final double lVerticalSensitivity = channelSensitivityProperty[lChannelIndex].get ( );
                    final Point2D lNearLowerLeftPoint = JavaFxUtils.getLowerLeftPoint ( gc ).add ( vuMeterWidth, 0 ).add ( 5, - 10 );

                    gc.setTextBaseline ( VPos.BASELINE );
                    gc.setFill ( Color.YELLOW );
                    gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 14 ) );
                    gc.setTextAlign ( TextAlignment.LEFT );
                    gc.fillText (
                        String.format (
                            "%.03f %s/div   %.0f ms/div",
                            lVerticalSensitivity,
                            signalSource.getPhysicalSpaceUnit ( ),
                            displayTimeMsPerDivProperty.get ( )
                        ),
                        lNearLowerLeftPoint.getX ( ),
                        lNearLowerLeftPoint.getY ( )
                    );
                }

                // only top channel
                if ( lChannelIndex == iGetLowestIndexChannel ( ) )
                {
                    gc.setTextBaseline ( VPos.TOP );
                    gc.setFill ( Color.LIGHTGRAY );
                    gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 28 ) );
                    gc.setTextAlign ( TextAlignment.RIGHT );

                    final Point2D lNearUpperRightPoint = JavaFxUtils.getUpperRightPoint ( gc ).add ( - 20, 10 );
                    gc.fillText (
                        StringUtil.concatenate (
                            false, false, " ",
                            applicationConfig.getSurgeryInfo ( ).getPatientFirstName ( ).trim ( ),
                            applicationConfig.getSurgeryInfo ( ).getPatientMiddleName ( ).trim ( ),
                            applicationConfig.getSurgeryInfo ( ).getPatientLastName ( ).trim ( ),
                            applicationConfig.getSurgeryInfo ( ).getPatientAge ( ).trim ( ).isEmpty ( ) ? "" : "(" + applicationConfig.getSurgeryInfo ( ).getPatientAge ( ).trim ( ) + ")" ),
                        lNearUpperRightPoint.getX ( ),
                        lNearUpperRightPoint.getY ( ) );
                }

                // only bottom channel
                if ( lChannelIndex == iGetHighestIndexChannel ( ) )
                {
                    gc.setTextBaseline ( VPos.BASELINE );
                    gc.setFill ( Color.GRAY );
                    gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 14 ) );
                    gc.setTextAlign ( TextAlignment.CENTER );

                    final double fsInKHz = applicationConfig.getSignalSourceBuilder ( ).getSamplingFrequencyEnum ( ).getValue ( ) / 1000.0;
                    final int screenWidthInPixels = (int) ( canvas.getWidth ( ) - vuMeterWidth );
                    final double samplesPerScreen = signalSource.getSamplingFrequencyInHz ( ) * applicationConfig.getSignalPaneBuilder ( ).getHorizontalGridCount ( ) * ( displayTimeMsPerDivProperty.get ( ) * 0.001 );
                    final double displayedSamplesPerScreen = samplesPerScreen / sampleDecimationDisplayRate;

                    final Point2D lNearLowerCenterPoint = JavaFxUtils.getLowerLeftPoint ( gc ).midpoint ( JavaFxUtils.getLowerRightPoint ( gc ) ).add ( 0, - 10 );
                    gc.fillText (
                        String.format (
                            "%s @ %.3f Khz  [width:%d/samples:%.0f/displayed:%.0f/rate:%d] mode:%s filter:%s saving:%d",
                            signalSource.getDescription ( ),
                            fsInKHz,
                            screenWidthInPixels,
                            samplesPerScreen,
                            displayedSamplesPerScreen,
                            sampleDecimationDisplayRate,
                            displayTimeIsShort ? "short" : "long",
                            applicationConfig.getFilterBuilder ( ).getUseAdaptiveFiltering ( ) ? "on" : "off",
                            applicationConfig.getSignalPaneBuilder ( ).getNumberOfSavedScreens ( ) ),
                        lNearLowerCenterPoint.getX ( ),
                        lNearLowerCenterPoint.getY ( ) );
                }
            }
        }

        logger.info( "iDrawCanvasWithConfigurationInfo() -- END" );
    }

    private double iComputeGainFromTheConfig ( double aInSensitivityInVoltPerDiv )
    {
        return
            ( signalSource.getPhysicalSpaceUpperBound () - signalSource.getPhysicalSpaceLowerBound () )
            /
            ( applicationConfig.getSignalSourceBuilder ().getPhysicalGain () * aInSensitivityInVoltPerDiv * applicationConfig.getSignalPaneBuilder ().getVerticalGridCount () );
    }

    private void iDrawFastCounters ( )
    {
        final int lChannelIndex = iGetHighestIndexChannel ( );

        final Canvas canvas = signalCanvases[LAYER_ENUM.FAST_COUNTERS.ordinal ()][lChannelIndex];
        final GraphicsContext gc = canvas.getGraphicsContext2D ( );
        final Point2D lRightBottom = JavaFxUtils.getLowerRightPoint ( gc );

        gc.clearRect ( 0, 0, lRightBottom.getX (), lRightBottom.getY() );

        {
            gc.setFill ( Color.GRAY );
            final Point2D lPoint = JavaFxUtils.getLowerRightPoint ( gc ).add ( -5, -82 );

            synchronized ( screenSavingQueueB )
            {
                gc.fillText (
                    String.format ( "%d/%d/%d", screenCount.get ( ), screenSavingQueueA.size (), screenSavingQueueB.size () ),
                    lPoint.getX ( ),
                    lPoint.getY ( ) );
            }
        }

        {
            gc.setFill ( Color.GRAY );
            final Point2D lPoint = JavaFxUtils.getLowerRightPoint ( gc ).add ( -5, -67 );

            gc.fillText (
                iFormatOnElapsedTime ( lastDisplayTime - appStartingTime ),
                lPoint.getX ( ),
                lPoint.getY ( )
            );
        }

        {
            gc.setFill ( Color.GRAY );
            final Point2D lPoint = JavaFxUtils.getLowerRightPoint ( gc ).add ( -5, -52 );

            gc.fillText (
                iFormatOnElapsedTime ( System.currentTimeMillis ( ) - appStartingTime ),
                lPoint.getX ( ),
                lPoint.getY ( ) );
        }

        {
            gc.setFill ( getCountOnPendingScreens() > 0 ? Color.RED : Color.GREEN );
            final Point2D lPoint = JavaFxUtils.getLowerRightPoint ( gc ).add ( -5, -37 );

            gc.fillText (
                getCountOnPendingScreens() > 0 ? "screens are pending" : "all screens saved",
                lPoint.getX ( ),
                lPoint.getY ( ) );
        }

        {
            gc.setFill ( Color.GRAY );
            final Point2D lPoint = JavaFxUtils.getLowerRightPoint ( gc ).add ( -5, -22 );

            gc.fillText (
                iGetSignalSourceStarted ( ) ? "running" : "stopped",
                lPoint.getX ( ),
                lPoint.getY ( )
            );
        }

        {
            gc.setFill ( Color.GRAY );
            final Point2D lPoint = JavaFxUtils.getLowerRightPoint ( gc ).add ( -5, -7 );

            gc.fillText (
                iGetSignalSourceIsAvailable ( ) ? "source-ok" : "source-n/a",
                lPoint.getX ( ),
                lPoint.getY ( )
            );
        }
    }

    private boolean iGetSignalSourceIsAvailable()
    {
        return ( signalSource != null ) && signalSource.isAvailable ();
    }

    private boolean iGetSignalSourceStarted()
    {
        return ( signalSource != null ) && signalSource.getIsStarted ( );
    }

    public int getCountOnPendingScreens()
    {
        synchronized ( screenSavingQueueB )
        {
            return screenSavingQueueB.size ( );
        }
    }

    private static String iFormatOnElapsedTime ( long msec )
    {
        final long sec = msec / 1000;
        final long min = sec / 60;
        final long hour = min / 60;
        return String.format ( "%02d:%02d:%02d.%03d", hour, min % 60, sec % 60, msec % 1000 );
    }

    private void iRefreshAllScreens ( )
    {
        logger.info( "iRefreshAllScreens() -- BEGIN" );

        iDrawGrid ( );
        iDrawTrigger ( );
        iDrawCanvasWithConfigurationInfo ( );
        iCleanSignalCanvas ();

        logger.info( "iRefreshAllScreens() -- END" );
    }

    private void iDrawGrid ( )
    {
        final int gridDensity = 10;

        for ( int lChannelIndex=0; lChannelIndex<applicationConfig.getSignalSourceBuilder ().getNChannels ( ); lChannelIndex++ )
        {
            final Canvas lCanvas = signalCanvases[LAYER_ENUM.GRID.ordinal ()][lChannelIndex];
            final GraphicsContext gc = lCanvas.getGraphicsContext2D ( );

            gc.setFill ( backgroundColor );
            gc.fillRect ( 0, 0, gc.getCanvas ( ).getWidth ( ), gc.getCanvas ( ).getHeight ( ) );

            {
                final double opacity = lChannelIndex == 0 ? 0.3 : 0.2;
                final int illuminationHeight = 15;

                gc.setFill (
                    new LinearGradient (
                        0, 1, 0, 0,
                        true,
                        CycleMethod.NO_CYCLE,
                        new Stop ( 0.0, Color.rgb ( 0, 0, 0, opacity ) ),
                        new Stop ( 0.9, Color.rgb ( 255, 255, 255, opacity ) ),
                        new Stop ( 1.0, Color.rgb ( 0, 0, 0, opacity ) )
                    )
                );

                gc.fillRoundRect ( 0, 0, gc.getCanvas ( ).getWidth ( ), illuminationHeight, illuminationHeight, illuminationHeight );
            }

            gc.setLineWidth ( 1 );

            // horizontal lines
            {
                final double gridStepVertical = ( lCanvas.getHeight ( ) - 1 ) / gridDensity;
                for ( int i = 0; i < gridDensity; i++ )
                {
                    final double y = i * gridStepVertical;

                    if ( i == gridDensity / 2)
                    {
                        gc.setStroke ( Color.rgb ( 255, 255, 255, 0.35 ) );
                    }
                    else
                    {
                        gc.setStroke ( Color.rgb ( 255, 255, 255, 0.15 ) );
                    }

                    JavaFxUtils.strokeLine ( gc, new Point2D ( vuMeterWidth, y ), new Point2D ( lCanvas.getWidth ( ), y ), true );
                }
            }

            // vertical lines
            {
                final double gridStepHorizontal = ( lCanvas.getWidth ( ) - 1 - vuMeterWidth ) / gridDensity;
                for ( int i = 0; i < gridDensity; i++ )
                {
                    final double x = vuMeterWidth + i * gridStepHorizontal;

                    gc.setStroke ( Color.rgb ( 255, 255, 255, 0.15 ) );

                    JavaFxUtils.strokeLine ( gc, new Point2D ( x, 0 ), new Point2D ( x, lCanvas.getHeight ( ) ), true );
                }
            }
        }
    }

    private void iCleanSignalCanvas()
    {
        for ( int lChannelIndex=0; lChannelIndex<signalVessels.getNumberOfChannels (); lChannelIndex++ )
        {
            if ( isChannelVisible[lChannelIndex] )
            {
                final GraphicsContext gc = signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex].getGraphicsContext2D ( );
                gc.clearRect ( 0, 0, gc.getCanvas ( ).getWidth ( ), gc.getCanvas ( ).getHeight ( ) );
            }
        }
    }

    private void iResetPreviousScreenState ( )
    {
        for ( int lChannelIndex=0; lChannelIndex<signalVessels.getNumberOfChannels (); lChannelIndex++ )
        {
            if ( isChannelVisible[lChannelIndex] )
            {
                JavaFxUtils.resetAccumulatedPoints ( signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex].getGraphicsContext2D ( ) );
                lastDisplayedPoint[lChannelIndex] = null;
            }
        }
    }

    private void iPlotSignalForCurrentSample ( float[] aInNextSignalEnsemble )
    {
        for ( int lChannelIndex=0; lChannelIndex<aInNextSignalEnsemble.length; lChannelIndex++ )
        {
            if ( isChannelVisible[lChannelIndex] )
            {
                final GraphicsContext gc = signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex].getGraphicsContext2D ( );

                final double timeInSec =  ( (double) sampleSweepCount ) / ( (double) signalSource.getSamplingFrequencyInHz ( ) );
                final Point2D lNewDisplayablePoint = affineTransformations[lChannelIndex].transform ( timeInSec, aInNextSignalEnsemble[lChannelIndex] );

                JavaFxUtils.strokeLine ( gc, lNewDisplayablePoint, lastDisplayedPoint[lChannelIndex], false );
            }
        }
    }

    private void iSaveCurrentDisplayedSample ( float[] aInNextSignalEnsemble )
    {
        for ( int lChannelIndex=0; lChannelIndex<aInNextSignalEnsemble.length; lChannelIndex++ )
        {
            if ( isChannelVisible[lChannelIndex] )
            {
                final double timeInSec =  ( (double) sampleSweepCount ) / ( (double) signalSource.getSamplingFrequencyInHz ( ) );
                final Point2D lNewDisplayablePoint = affineTransformations[lChannelIndex].transform ( timeInSec, aInNextSignalEnsemble[lChannelIndex] );
                lastDisplayedPoint[lChannelIndex] = lNewDisplayablePoint;
            }
        }
    }

    private void iPlotSignalForLatestScreen ( )
    {
        for ( int lChannelIndex=0; lChannelIndex<signalVessels.getNumberOfChannels (); lChannelIndex++ )
        {
            if ( isChannelVisible[lChannelIndex] )
            {
                final GraphicsContext gc = signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex].getGraphicsContext2D ( );
                gc.stroke ( );
            }
        }
    }

    private void iAccumulateSampleForCurrentScreen ( float[] aInNextSignalEnsemble )
    {
        for ( int lChannelIndex=0; lChannelIndex<aInNextSignalEnsemble.length; lChannelIndex++ )
        {
            if ( isChannelVisible[lChannelIndex] )
            {
                final double timeInSec = ( (double) sampleSweepCount / (double) signalSource.getSamplingFrequencyInHz ( ) );
                final Point2D lNewDisplayablePoint = affineTransformations[lChannelIndex].transform ( timeInSec, aInNextSignalEnsemble[lChannelIndex] );
                final GraphicsContext gc = signalCanvases[LAYER_ENUM.SIGNAL.ordinal ( )][lChannelIndex].getGraphicsContext2D ( );

                if ( sampleSweepCount == 0 )
                {
                    JavaFxUtils.moveToFirstPointForLine ( gc, lNewDisplayablePoint );
                }

                else
                {
                    JavaFxUtils.accumulatePointForLine ( gc, lNewDisplayablePoint );
                }
            }
        }
    }

    private void iNotifyListeners( DisplayStatus aInNewDisplayStatus )
    {
        synchronized ( listeners )
        {
            for ( ChangeListener<DisplayStatus> lNext : listeners )
            {
                lNext.change ( aInNewDisplayStatus, null );
            }
        }
    }

    @Override
    public Property getProperty ( PropertiesEnum aInPropertiesEnum )
    {
        Property ret = null;

        switch ( aInPropertiesEnum )
        {
            case ModeContinuousValue:
                ret = triggerModeContinuousProperty;
                break;
            case TriggerChannelOneValue:
                ret = triggerModeTriggerOnChannelOneProperty;
                break;
            case TriggerChannelTwoValue:
                ret = triggerModeTriggerOnChannelTwoProperty;
                break;
            case DisplayTimeValueMsPerDiv:
                ret = displayTimeMsPerDivProperty;
                break;
            case TriggerFreezeTimeInSecondsValue:
                ret = holdOffTimeInSecondsValueProperty;
                break;
            case TriggerSoundVolumeValue:
                ret = triggerSoundVolumeValueProperty;
                break;
            case ChannelOneSensitivityValue:
                ret = channelSensitivityProperty[0];
                break;
            case ChannelTwoSensitivityValue:
                ret = channelSensitivityProperty[1];
                break;
            case ChannelOneTriggerLevelValue:
                ret = channelTriggerLevelProperty[0];
                break;
            case ChannelTwoTriggerLevelValue:
                ret = channelTriggerLevelProperty[1];
                break;
            case UseAdaptiveFilteringValue:
                ret = useAdaptiveFilteringProperty;
                break;

            default:
                Preconditions.checkState ( false, "not defined: " + aInPropertiesEnum );
        }

        return ret;
    }
}

