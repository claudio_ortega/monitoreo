/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */
package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.FilterBuilder;
import com.poplar.monitor.api.SignalChannelBuilder;
import com.poplar.monitor.api.SignalPaneBuilder;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.audio.SignalSourceAudio;
import com.poplar.monitor.imp.util.GsonUtil;
import com.poplar.monitor.imp.util.CollectionsUtil;
import com.poplar.monitor.imp.util.FileUtil;
import com.poplar.monitor.imp.util.StringUtil;
import com.poplar.monitor.imp.util.SystemUtil;
import javafx.beans.property.Property;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.SegmentedButton;
import javafx.beans.binding.Bindings;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;

public class SettingsController
    implements
        Initializable,
        ChangeProducer<ApplicationConfig>,
        IPropertiesProvider,
        IApplicationConfigProvider
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    private static final File configFilePath = FileUtil.getFileFromBaseAndAStringSequence ( SystemUtil.getCurrentDirectory ( ), "config", "config.json" );
    private static final File defaultConfigFilePath = FileUtil.getFileFromBaseAndAStringSequence ( SystemUtil.getCurrentDirectory ( ), "config", "default-config.json" );
    private static final File configFilePathInvalidCopy = FileUtil.getFileFromBaseAndAStringSequence ( SystemUtil.getCurrentDirectory ( ), "config", "invalid.config.json" );

    private final List<ChangeListener<ApplicationConfig>> configChangeListeners;
    private ApplicationConfig currentConfig;
    private ApplicationConfig latestNotifiedConfig;
    private final AtomicBoolean controlsInitialized;
    private Stage stage;

    @FXML
    private GridPane filtering;
    @FXML
    private GridPane gridSignalSource;
    @FXML
    private GridPane gridDisplay;
    @FXML
    private GridPane gridDisplayChannelOne;
    @FXML
    private GridPane gridDisplayChannelTwo;
    @FXML
    private GridPane gridSamplingFrequency;

    @FXML
    private ComboBox<SignalSourceBuilder.SignalSourceType> signalSourceTypeCombo;
    @FXML
    private ComboBox<String> audioDriverSelectorEnumCombo;
    @FXML
    private ComboBox<SignalSourceBuilder.SimulatorType> simulatorSelectorEnumCombo;
    @FXML
    private ComboBox<SignalSourceBuilder.SignalFileOriginType> signalFileOriginTypeEnumCombo;

    @FXML
    private ComboBox<SimultaneousDisplayModeEnum> simultaneousDisplayModeEnumCombo;
    @FXML
    private ComboBox<SignalSourceBuilder.SamplingFrequencyEnum> samplingFrequencyCombo;
    @FXML
    private ComboBox<FilterBuilder.LineFrequencyEnum> filterNotchFrequencyEnumCombo;

    @FXML
    private SegmentedButton triggerModeSegmentedButton;
    @FXML
    private ToggleButton triggerModeContinuousButton;
    @FXML
    private ToggleButton triggerModeTriggerOnChannelOneButton;
    @FXML
    private ToggleButton triggerModeTriggerOnChannelTwoButton;

    @FXML
    private Button scanAudioSourcesButton;
    @FXML
    private Button saveAsDefaultButton;
    @FXML
    private Button readDefaultsButton;
    @FXML
    private Button showReportsDirectoryButton;
    @FXML
    private Button signalFileOpenButton;

    @FXML
    private Slider triggerFreezeTimeInSecondsSlider;
    @FXML
    private Slider maxNumberOfDisplayedPointsPerSecSlider;
    @FXML
    private Slider channelOneTriggerLevelSlider;
    @FXML
    private Slider channelTwoTriggerLevelSlider;
    @FXML
    private Slider channelOneSensitivitySlider;
    @FXML
    private Slider channelTwoSensitivitySlider;
    @FXML
    private Slider displayTimeMsPerDivSlider;
    @FXML
    private Slider triggerSoundVolumeSlider;
    @FXML
    private Slider screenSavingCountSlider;
    @FXML
    private Slider adaptiveCoefficientSlider;

    private SliderTextFieldBinder triggerFreezeTimeInSecondsBinder;
    private SliderTextFieldBinder maxNumberOfDisplayedPointsPerSecBinder;
    private SliderTextFieldBinder channelOneTriggerLevelBinder;
    private SliderTextFieldBinder channelTwoTriggerLevelBinder;
    private SliderTextFieldBinder channelOneSensitivityBinder;
    private SliderTextFieldBinder channelTwoSensitivityBinder;
    private SliderTextFieldBinder displayTimeMsPerDivBinder;
    private SliderTextFieldBinder triggerSoundVolumeBinder;
    private SliderTextFieldBinder screenSavingCountBinder;
    private SliderTextFieldBinder adaptiveCoefficientBinder;

    @FXML
    private TextField channelOneNameText;
    @FXML
    private TextField channelTwoNameText;
    @FXML
    private TextField displayTimeMsPerDivTextField;
    @FXML
    private TextField triggerFreezeTimeInSecondsTextField;
    @FXML
    private TextField maxNumberOfDisplayedPointsPerSecTextField;
    @FXML
    private TextField channelOneTriggerLevelTextField;
    @FXML
    private TextField channelTwoTriggerLevelTextField;
    @FXML
    private TextField channelOneSensitivityTextField;
    @FXML
    private TextField channelTwoSensitivityTextField;
    @FXML
    private TextField patientFirstNameTextField;
    @FXML
    private TextField patientMiddleNameTextField;
    @FXML
    private TextField patientLastNameTextField;
    @FXML
    private TextField patientAgeTextField;
    @FXML
    private TextField triggerSoundVolumeTextField;
    @FXML
    private TextField screenSavingCountTextField;
    @FXML
    private TextField screenSavingDirectoryTextField;
    @FXML
    private TextField signalFilePathReadOnlyTextField;
    @FXML
    private TextField adaptiveCoefficientTextField;

    @FXML
    private Label audioDriverSelectorLabel;
    @FXML
    private Label simulatorSelectorLabel;
    @FXML
    private Label signalFileOriginTypeLabel;
    @FXML
    private CheckBox useAdaptiveFilteringCheckBox;
    @FXML
    private CheckBox startMonitoringAutomaticallyCheckBox;

    private long lastTimeAudioSourcesScanned;

    @FXML
    private void iHandleShowReportsDirectory( ActionEvent event ) throws Exception
    {
        try
        {
            final File lScreenDirectoryPath = new File ( currentConfig.getSignalPaneBuilder ( ).getScreensSavingDirectory ( ) );

            if ( ! lScreenDirectoryPath.exists ( ) )
            {
                JavaFxUtils.showNotification ( "screen-dir-does-not-exist", JavaFxUtils.NotificationLevel.INFO );
            }

            else
            {
                if ( SystemUtil.getOSType ( ) == SystemUtil.OSType.Mac )
                {
                    Runtime.getRuntime ( ).exec ( "open " + lScreenDirectoryPath );
                }

                else if ( SystemUtil.getOSType ( ) == SystemUtil.OSType.Windows )
                {
                    Runtime.getRuntime ( ).exec ( "explorer " + lScreenDirectoryPath );
                }

                else
                {
                    JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "unsupported-platform" ) + ": " + SystemUtil.getOSType ( ), JavaFxUtils.NotificationLevel.INFO );
                }
            }
        }

        catch ( Throwable t )
        {
            logger.error ( t.getMessage (), t );
        }
    }

    @FXML
    private void iHandleButtonReadDefault ( ActionEvent event ) throws Exception
    {
        if ( defaultConfigFilePath.exists ( ) )
        {
            try
            {
                final ApplicationConfig lTestCurrentConfig = GsonUtil.readJsonFromFile ( defaultConfigFilePath, ApplicationConfig.class );
                lTestCurrentConfig.validate ( );

                currentConfig = GsonUtil.readJsonFromFile ( defaultConfigFilePath, ApplicationConfig.class );
                currentConfig.validate ( );

                iApplyConfigToControls ( );

                iNotifyListenersOnConfigChange ( );
                latestNotifiedConfig = GsonUtil.clone ( currentConfig );

                logger.info ( "default config was loaded" );

                JavaFxUtils.showNotification ( "load-default-config-success", JavaFxUtils.NotificationLevel.INFO );
            }

            catch ( Exception ex )
            {
                logger.info ( "default config file is invalid, ignoring and removing file: " + defaultConfigFilePath );
                FileUtil.deletePlainFileIfExists ( defaultConfigFilePath, true );
                logger.error ( ex.toString ( ), ex );
            }
        }

        else
        {
            logger.info ( "no default config was found, so no default config was loaded, missing path: " + defaultConfigFilePath );
            JavaFxUtils.showNotification ( "load-default-config-failed", JavaFxUtils.NotificationLevel.ERROR );
        }
    }

    @FXML
    private void iHandleOpenSignalFile ( ActionEvent event )
    {
        final FileChooser fileChooser = new FileChooser();

        final File screensDirectory = FileUtil.getFileFromBaseAndAStringSequence ( SystemUtil.getCurrentDirectory ( ), "screens" );

        if ( screensDirectory.exists () )
        {
            fileChooser.setInitialDirectory ( screensDirectory );
        }

        fileChooser.setTitle ( "Open Signal File" );
        fileChooser.setSelectedExtensionFilter ( new FileChooser.ExtensionFilter ( "Signal Files", "json" ) );

        final File selectedFile = fileChooser.showOpenDialog ( stage );

        if ( selectedFile != null )
        {
            signalFilePathReadOnlyTextField.setText ( selectedFile.getName () );
            currentConfig.getSignalSourceBuilder ().setSignalFilePathName ( selectedFile.getAbsolutePath () );
        }
    }

    @FXML
    private void iHandleScanAudioSources ( ActionEvent event )
    {
        audioDriverSelectorEnumCombo.setDisable ( true );

        audioDriverSelectorEnumCombo.getScene ( ).setCursor( Cursor.WAIT );

        lastTimeAudioSourcesScanned = System.currentTimeMillis ();

        JavaFxUtils.executeOutsideFxThread (
            ( ) ->
            {
                try
                {
                    final List<String> compatibleMixers = SignalSourceAudio.getAllCompatibleMixersInSystem ( currentConfig.getSignalSourceBuilder ( ), true );

                    logger.info ( "scanning button, returned compatibleMixers: [" + compatibleMixers + "]" );

                    JavaFxUtils.executeInFxThread (
                        ( ) ->
                        {
                            audioDriverSelectorEnumCombo.getItems ( ).clear ( );
                            audioDriverSelectorEnumCombo.getItems ( ).addAll ( compatibleMixers );

                            if ( compatibleMixers.size ( ) > 0 )
                            {
                                audioDriverSelectorEnumCombo.getSelectionModel ( ).select ( 0 );
                            }

                            audioDriverSelectorEnumCombo.getScene ( ).setCursor ( Cursor.DEFAULT );
                            audioDriverSelectorEnumCombo.setDisable ( false );

                            if ( compatibleMixers.size ( ) > 0 )
                            {
                                JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "scanning-success" ) + "\n" +
                                        StringUtil.concatenate ( "\n", compatibleMixers ), JavaFxUtils.NotificationLevel.INFO );
                            }

                            else
                            {
                                JavaFxUtils.showNotification ( LanguageManager.getSingleton ( ).getValue ( "scanning-success-empty" ), JavaFxUtils.NotificationLevel.INFO );
                            }

                            return null;
                        }
                    );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage ( ), ex );

                    JavaFxUtils.executeInFxThread ( ( ) -> {
                            audioDriverSelectorEnumCombo.getItems ( ).clear ( );
                            audioDriverSelectorEnumCombo.getScene ( ).setCursor ( Cursor.DEFAULT );
                            audioDriverSelectorEnumCombo.setDisable ( false );

                            JavaFxUtils.showNotification ( "scanning-failed", JavaFxUtils.NotificationLevel.ERROR );

                            return null;
                        } );
                }

                return null;
            }
        );
    }

    @FXML
    private void iHandleButtonSaveAsDefault(ActionEvent event) throws Exception
    {
        iApplyControlsToConfig ();
        GsonUtil.writeJsonToFile ( defaultConfigFilePath, currentConfig );

        logger.info( "default config was saved" );
    }

    public SettingsController ( )
    {
        configChangeListeners = new LinkedList<> ( );
        controlsInitialized = new AtomicBoolean ( false );
    }

    public void setStage( Stage value )
    {
        stage = value;
    }

    @Override
    public void readDefaultConfigFromDisk()
    {
        try
        {
            iHandleButtonReadDefault( null );
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage (), ex );
        }
    }

    public void applySettings ( )
    {
        try
        {
            if ( controlsInitialized.get() )
            {
                iApplyControlsToConfig ( );
                iNotifyListenersOnConfigChange ( );

                GsonUtil.writeJsonToFile ( configFilePath, currentConfig );

                latestNotifiedConfig = GsonUtil.clone ( currentConfig );
            }
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage ( ), ex );
        }
    }

    @Override
    public ApplicationConfig getCopyOfLatestConfig()
    {
        ApplicationConfig ret = null;

        try
        {
            if ( controlsInitialized.get() )
            {
                iApplyControlsToConfig ( );
                ret = GsonUtil.clone ( currentConfig );
            }
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage ( ), ex );
        }

        return ret;
    }

    private enum SimultaneousDisplayModeEnum
    {
        allChannels, channelOneOnly, channelTwoOnly;

        @Override
        public String toString ( )
        {
            return LanguageManager.getSingleton ( ).getValue ( name ( ) );
        }
    }

    @Override
    public void initialize ( URL location, ResourceBundle aInResourceBundle )
    {
        iInitializeControls ( );
    }

    public void init ( )
        throws Exception
    {
        iLoadConfigFromFileOrCreateNewOne ( );

        Preconditions.checkNotNull ( currentConfig );
        logger.info ( "currentConfig: " + currentConfig );

        audioDriverSelectorEnumCombo.getItems ( ).clear ( );
        audioDriverSelectorEnumCombo.getItems ( ).addAll ( SignalSourceAudio.getAllCompatibleMixersInSystem ( currentConfig.getSignalSourceBuilder ( ), true ) );

        iApplyConfigToControls ( );

        iNotifyListenersOnConfigChange ( );
        latestNotifiedConfig = GsonUtil.clone ( currentConfig );

        logger.info ( "currentConfig:" + currentConfig );
        logger.info ( "latestNotifiedConfig:" + latestNotifiedConfig );
        Preconditions.checkState ( latestNotifiedConfig.equals ( currentConfig ) );
    }

    private void iInitializeControls ( )
    {
        try
        {
            //
            filtering.setPadding (
                new Insets (
                    10, 200, 10, 200 ) );
            filtering.setVgap ( 10 );
            filtering.setHgap ( 70 );
            //
            gridSignalSource.setPadding (
                new Insets (
                    10, 200, 10, 200 ) );
            gridSignalSource.setVgap ( 10 );
            gridSignalSource.setHgap ( 70 );
            //
            gridSamplingFrequency.setPadding (
                new Insets (
                    10, 200, 10, 200 ) );
            gridSamplingFrequency.setVgap ( 10 );
            gridSamplingFrequency.setHgap ( 70 );
            //
            gridDisplay.setPadding (
                new Insets (
                    10, 200, 10, 200 ) );
            gridDisplay.setVgap ( 10 );
            gridDisplay.setHgap ( 70 );
            //
            gridDisplayChannelOne.setPadding (
                new Insets (
                    10, 200, 10, 200 ) );
            gridDisplayChannelOne.setVgap ( 10 );
            gridDisplayChannelOne.setHgap ( 70 );
            //
            gridDisplayChannelTwo.setPadding (
                new Insets (
                    10, 200, 10, 200 ) );
            gridDisplayChannelTwo.setVgap ( 10 );
            gridDisplayChannelTwo.setHgap ( 70 );

            simultaneousDisplayModeEnumCombo.getItems ( ).addAll (
                SimultaneousDisplayModeEnum.allChannels,
                SimultaneousDisplayModeEnum.channelOneOnly,
                SimultaneousDisplayModeEnum.channelTwoOnly );
            samplingFrequencyCombo.getItems ( ).addAll (
                SignalSourceBuilder.SamplingFrequencyEnum.value8000, SignalSourceBuilder.SamplingFrequencyEnum.value11025, SignalSourceBuilder.SamplingFrequencyEnum.value16000,
                SignalSourceBuilder.SamplingFrequencyEnum.value22050, SignalSourceBuilder.SamplingFrequencyEnum.value32000, SignalSourceBuilder.SamplingFrequencyEnum.value44100,
                SignalSourceBuilder.SamplingFrequencyEnum.value48000 );
            signalSourceTypeCombo.getItems ( ).addAll (
                SignalSourceBuilder.SignalSourceType.soundDriver,
                SignalSourceBuilder.SignalSourceType.signalFile,
                SignalSourceBuilder.SignalSourceType.simulator );
            simulatorSelectorEnumCombo.getItems ( ).addAll (
                SignalSourceBuilder.SimulatorType.powerLineNoise,
                SignalSourceBuilder.SimulatorType.calibrationMinMax,
                SignalSourceBuilder.SimulatorType.pureSine );
            signalFileOriginTypeEnumCombo.getItems ( ).addAll (
                SignalSourceBuilder.SignalFileOriginType.internal_1,
                SignalSourceBuilder.SignalFileOriginType.selectable );
            filterNotchFrequencyEnumCombo.getItems ( ).addAll (
                FilterBuilder.LineFrequencyEnum.value50,
                FilterBuilder.LineFrequencyEnum.value60 );

            triggerModeSegmentedButton.getStyleClass ( ).add ( SegmentedButton.STYLE_CLASS_DARK );
            JavaFxUtils.restrictOneSelectedButtonAtATime ( triggerModeSegmentedButton.getButtons ( ) );

            maxNumberOfDisplayedPointsPerSecBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( maxNumberOfDisplayedPointsPerSecSlider )
                .setTextField ( maxNumberOfDisplayedPointsPerSecTextField )
                .setNumberOfDecimalDigits ( 0 )
                .setLinearBySegmentsOperation ( 200.0, 500.0, 1000.0, 2000.0, 5000.0, 10000.0, 20000.0, 50000.0 )
                .start ();

            channelOneTriggerLevelBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( channelOneTriggerLevelSlider )
                .setTextField ( channelOneTriggerLevelTextField )
                .setNumberOfDecimalDigits ( 0 )
                .setNumberOfTicks ( 11 )
                .setLinearOperation ( - 100.0, 100.0 )
                .start ();

            channelTwoTriggerLevelBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( channelTwoTriggerLevelSlider )
                .setTextField ( channelTwoTriggerLevelTextField )
                .setNumberOfDecimalDigits ( 0 )
                .setNumberOfTicks ( 11 )
                .setLinearOperation ( - 100.0, 100.0 )
                .start ();

            channelOneSensitivityBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( channelOneSensitivitySlider )
                .setTextField ( channelOneSensitivityTextField )
                .setNumberOfDecimalDigits ( 3 )
                .setLinearBySegmentsOperation ( 0.005, 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1.0, 2.0, 5.0 )
                .start ( );

            channelTwoSensitivityBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( channelTwoSensitivitySlider )
                .setTextField ( channelTwoSensitivityTextField )
                .setNumberOfDecimalDigits ( 3 )
                .setLinearBySegmentsOperation ( 0.005, 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1.0, 2.0, 5.0 )
                .start ( );

            displayTimeMsPerDivBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( displayTimeMsPerDivSlider )
                .setTextField ( displayTimeMsPerDivTextField )
                .setNumberOfDecimalDigits ( 0 )
                .setLinearBySegmentsOperation ( 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0 )
                .start ();

            triggerFreezeTimeInSecondsBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( triggerFreezeTimeInSecondsSlider )
                .setTextField ( triggerFreezeTimeInSecondsTextField )
                .setNumberOfDecimalDigits ( 2 )
                .setLinearBySegmentsOperation ( 0.01, 0.05, 0.1, 0.5, 1.0, 5.0 )
                .start ( );

            triggerSoundVolumeBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( triggerSoundVolumeSlider )
                .setTextField ( triggerSoundVolumeTextField )
                .setNumberOfDecimalDigits ( 0 )
                .setNumberOfTicks ( 6 )
                .setLinearOperation ( 0.0, 5.0 )
                .setPostStartProcessor (
                    ( slider ) ->
                    {
                        slider.setShowTickLabels ( true );
                        slider.setShowTickMarks ( true );
                        slider.setSnapToTicks ( true );
                        slider.setMinorTickCount ( 0 );
                    }
                )
                .start ( );

            screenSavingCountBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( screenSavingCountSlider )
                .setTextField ( screenSavingCountTextField )
                .setNumberOfDecimalDigits ( 0 )
                .setNumberOfTicks ( 11 )
                .setLinearOperation ( 0.0, 10.0 )
                .setPostStartProcessor (
                    ( slider ) ->
                    {
                        slider.setShowTickLabels ( true );
                        slider.setShowTickMarks ( true );
                        slider.setSnapToTicks ( true );
                        slider.setMinorTickCount ( 0 );
                    }
                )
                .start ( );

            adaptiveCoefficientBinder = SliderTextFieldBinder
                .create ( )
                .setSlider ( adaptiveCoefficientSlider )
                .setTextField ( adaptiveCoefficientTextField )
                .setNumberOfDecimalDigits ( 1 )
                .setLinearBySegmentsOperation ( 1.0, 10.0, 100.0, 1000.0, 10000.0 )
                .setPostStartProcessor (
                    ( slider ) ->
                    {
                        slider.setShowTickLabels ( true );
                        slider.setShowTickMarks ( true );
                        slider.setSnapToTicks ( false );
                        slider.setMinorTickCount ( 0 );
                    }
                )
                .start ( );

            signalSourceTypeCombo.valueProperty ( ).addListener (
                ( ov, old_val, new_val ) ->
                {
                    audioDriverSelectorEnumCombo.setDisable ( new_val != SignalSourceBuilder.SignalSourceType.soundDriver );
                    simulatorSelectorEnumCombo.setDisable ( new_val != SignalSourceBuilder.SignalSourceType.simulator );
                    signalFileOriginTypeEnumCombo.setDisable ( new_val != SignalSourceBuilder.SignalSourceType.signalFile );
                    samplingFrequencyCombo.setDisable ( new_val != SignalSourceBuilder.SignalSourceType.soundDriver );
                }
            );

            audioDriverSelectorLabel.disableProperty ( ).bind ( audioDriverSelectorEnumCombo.disableProperty ( ) );
            scanAudioSourcesButton.disableProperty ( ).bind ( audioDriverSelectorEnumCombo.disableProperty ( ) );
            simulatorSelectorLabel.disableProperty ( ).bind ( simulatorSelectorEnumCombo.disableProperty ( ) );
            signalFileOriginTypeLabel.disableProperty ( ).bind ( signalFileOriginTypeEnumCombo.disableProperty ( ) );

            // it doesn't work as expected, does not disable the button when is in 'internal'
            signalFileOpenButton.disableProperty ( ).bind (
                Bindings.createBooleanBinding (
                    ( ) ->
                        ! (
                        signalFileOriginTypeEnumCombo.valueProperty ( ).get ( ) == SignalSourceBuilder.SignalFileOriginType.selectable &&
                        signalSourceTypeCombo.valueProperty ( ).get ( ) == SignalSourceBuilder.SignalSourceType.signalFile
                        ),
                    signalSourceTypeCombo.valueProperty ( ),
                    signalFileOriginTypeEnumCombo.valueProperty ()
                )
            );

            signalFilePathReadOnlyTextField.disableProperty ( ).bind ( signalFileOpenButton.disableProperty ( ) );

            //
            signalFilePathReadOnlyTextField.setEditable ( false );
        }

        catch ( Exception ex )
        {
            logger.error (
                ex.getMessage ( ), ex );
        }
    }

    /**
     * @return  -1 if not found
     */
    private static int findFirstMatchingItem( ComboBox<String> aInCombo, String regexp )
    {
        int matchIndex = -1;

        int i=0;
        for ( String next : aInCombo.getItems () )
        {
            if ( next.matches ( regexp ) )
            {
                matchIndex = i;
                break;
            }

            i++;
        }

        return matchIndex;
    }

    private void iApplyConfigToControls ( )
    {
        // signal source
        final String lRegexp = currentConfig.getSignalSourceBuilder ( ).getAudioMixerDescriptionRegexp ( );

        Preconditions.checkState ( lRegexp != null );

        int matchIndex = findFirstMatchingItem( audioDriverSelectorEnumCombo, lRegexp );

        if ( matchIndex == -1 )
        {
            logger.warn( "regexp: " + lRegexp + " not found, will attempt to use the first available, if any" );
            matchIndex = 0;
        }

        audioDriverSelectorEnumCombo.getSelectionModel ( ).select ( matchIndex );

        // displayed channels
        if ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).getVisible ( ) )
        {
            if ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).getVisible ( ) )
            {
                simultaneousDisplayModeEnumCombo.<String>getSelectionModel ( ).select (
                    SimultaneousDisplayModeEnum.allChannels );
            }

            else
            {
                simultaneousDisplayModeEnumCombo.getSelectionModel ( ).select (
                    SimultaneousDisplayModeEnum.channelOneOnly );
            }
        }

        else
        {
            if ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).getVisible ( ) )
            {
                simultaneousDisplayModeEnumCombo.getSelectionModel ( ).select (
                    SimultaneousDisplayModeEnum.channelTwoOnly );
            }
            else
            {
                logger.warn (
                    "invalid currentConfig, visible attributes over both channels are false, acting as both are true" );
                simultaneousDisplayModeEnumCombo.getSelectionModel ( ).select (
                    SimultaneousDisplayModeEnum.allChannels );
            }
        }

        // sampling frequency
        samplingFrequencyCombo.getSelectionModel ( ).select (
            currentConfig.getSignalSourceBuilder ( ).getSamplingFrequencyEnum ( ) );

        simulatorSelectorEnumCombo.getSelectionModel ( ).select (  currentConfig.getSignalSourceBuilder ().getSimulatorType ( ) );
        signalFileOriginTypeEnumCombo.getSelectionModel ( ).select (  currentConfig.getSignalSourceBuilder ().getSignalFileOriginType ( ) );
        // signalFilePathName is done in the callback for FileChooser

        if ( currentConfig.getSignalSourceBuilder ().getSignalFilePathName () == null )
        {
            signalFilePathReadOnlyTextField.setText ( "n/a" );
        }
        else
        {
            final File signalFilePath = new File ( currentConfig.getSignalSourceBuilder ( ).getSignalFilePathName ( ) );

            if ( signalFilePath.exists() )
            {
                signalFilePathReadOnlyTextField.setText ( signalFilePath.getName ( ) );
            }
            else
            {
                signalFilePathReadOnlyTextField.setText ( "n/a" );
                currentConfig.getSignalSourceBuilder ().setSignalFilePathName ( null );
            }
        }

        // sweep mode, trigger channel
        if ( currentConfig.getSignalPaneBuilder ( ).getSweepModeEnum ( )  == SignalPaneBuilder.SweepModeEnum.continuous )
        {
            triggerModeContinuousButton.setSelected ( true );
            triggerModeTriggerOnChannelOneButton.setSelected ( false );
            triggerModeTriggerOnChannelTwoButton.setSelected ( false );
        }

        else if ( currentConfig.getSignalPaneBuilder ( ).getTriggerChannelIndex () == 0 )
        {
            triggerModeContinuousButton.setSelected ( false );
            triggerModeTriggerOnChannelOneButton.setSelected ( true );
            triggerModeTriggerOnChannelTwoButton.setSelected ( false );
        }

        else //  ( currentConfig.getSignalPaneBuilder ( ).getTriggerChannelIndex () == 1 )
        {
            triggerModeContinuousButton.setSelected ( false );
            triggerModeTriggerOnChannelOneButton.setSelected ( false );
            triggerModeTriggerOnChannelTwoButton.setSelected ( true );
        }

        // display signal source type
        signalSourceTypeCombo.getSelectionModel ( ).select (
            currentConfig.getSignalSourceBuilder ( ).getSignalSourceType ( ) );

        samplingFrequencyCombo.setDisable (
            currentConfig.getSignalSourceBuilder ( ).getSignalSourceType ( ) != SignalSourceBuilder.SignalSourceType.soundDriver );

        // channel names
        channelOneNameText.setText (
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).getSignalName ( ) );
        channelTwoNameText.setText (
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).getSignalName ( ) );

        // display
        startMonitoringAutomaticallyCheckBox.setSelected ( currentConfig.getSignalPaneBuilder ( ).getMonitorStartAutomatically ( ) );
        displayTimeMsPerDivBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getDisplayTimeMsPerDivision ( ) );
        triggerFreezeTimeInSecondsBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getTriggerFreezeTimeInSeconds ( ) );
        maxNumberOfDisplayedPointsPerSecBinder.setValue ( ( double ) currentConfig.getSignalPaneBuilder ( ).getMaxNumberOfDisplayedPointsPerSec ( ) );

        // trigger volume
        triggerSoundVolumeBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getTriggerSoundVolume ( ) );

        // channels
        channelOneSensitivityBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).getSensitivityMilliVoltsPerDiv ( ) );
        channelOneTriggerLevelBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).getTriggerLevelPercentage ( ) );
        channelTwoSensitivityBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).getSensitivityMilliVoltsPerDiv ( ) );
        channelTwoTriggerLevelBinder.setValue ( currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).getTriggerLevelPercentage ( ) );

        // filtering
        useAdaptiveFilteringCheckBox.setSelected ( currentConfig.getFilterBuilder ( ).getUseAdaptiveFiltering ( ) );
        filterNotchFrequencyEnumCombo.getSelectionModel ( ).select ( currentConfig.getFilterBuilder ( ).getNotchFrequencyEnum ( ) );
        adaptiveCoefficientBinder.setValue ( currentConfig.getFilterBuilder ( ).getAdaptiveCoefficient ( ) );

        // surgery info
        patientFirstNameTextField.setText ( currentConfig.getSurgeryInfo ( ).getPatientFirstName ( ) );
        patientMiddleNameTextField.setText ( currentConfig.getSurgeryInfo ( ).getPatientMiddleName ( ) );
        patientLastNameTextField.setText ( currentConfig.getSurgeryInfo ( ).getPatientLastName ( ) );
        patientAgeTextField.setText ( currentConfig.getSurgeryInfo ( ).getPatientAge ( ) );

        // screen saving
        screenSavingDirectoryTextField.setText ( currentConfig.getSignalPaneBuilder ( ).getScreensSavingDirectory ( ) );
        screenSavingDirectoryTextField.setEditable ( false );
        screenSavingCountBinder.setValue ( (double) Math.round ( currentConfig.getSignalPaneBuilder ( ).getNumberOfSavedScreens ( ) ) );

        controlsInitialized.set ( true );
    }

    private void iApplyControlsToConfig ( )
    {
        // displayed channels
        final SimultaneousDisplayModeEnum lSimultaneousDisplayModeEnum = simultaneousDisplayModeEnumCombo.getSelectionModel ( ).getSelectedItem ( );

        //
        if ( lSimultaneousDisplayModeEnum.equals ( SimultaneousDisplayModeEnum.allChannels ) )
        {
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).setVisible ( true );
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).setVisible ( true );
        }
        else if ( lSimultaneousDisplayModeEnum.equals ( SimultaneousDisplayModeEnum.channelOneOnly ) )
        {
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).setVisible ( true );
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).setVisible ( false );
        }
        else if ( lSimultaneousDisplayModeEnum.equals ( SimultaneousDisplayModeEnum.channelTwoOnly ) )
        {
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).setVisible ( false );
            currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).setVisible ( true );
        }
        else
        {
            throw new IllegalStateException( "unhandled enum: " + lSimultaneousDisplayModeEnum );
        }

        // signal source type
        currentConfig.getSignalSourceBuilder ( ).setSignalSourceType (
            signalSourceTypeCombo.getSelectionModel ( ).getSelectedItem ( ) );

        // signal source mixer
        if ( audioDriverSelectorEnumCombo.getSelectionModel ( ).getSelectedItem ( ) != null )
        {
            currentConfig.getSignalSourceBuilder ( ).setAudioMixerDescriptionRegexp ( StringUtil.splitOverSpaces (
                audioDriverSelectorEnumCombo.getSelectionModel ( ).getSelectedItem ( ) ).get ( 0 ) + ".*" );
        }

        // last signal source scanning time
        currentConfig.getSignalSourceBuilder ( ).setAudioMixerLastScanningTime ( lastTimeAudioSourcesScanned );

        // sampling frequency
        currentConfig.getSignalSourceBuilder ( ).setSamplingFrequencyEnum ( samplingFrequencyCombo.getSelectionModel ( ).getSelectedItem ( ) );

        currentConfig.getSignalSourceBuilder ( ).setSimulatorType ( simulatorSelectorEnumCombo.getSelectionModel ( ).getSelectedItem ( ) );
        currentConfig.getSignalSourceBuilder ( ).setSignalFileOriginType ( signalFileOriginTypeEnumCombo.getSelectionModel ( ).getSelectedItem ( ) );
        // not done here, done in the callback for FileChooser
        // currentConfig.getSignalSourceBuilder ( ).setSignalFilePathName ()

        // sweep mode, trigger channel
        if ( triggerModeContinuousButton.isSelected () )
        {
            currentConfig.getSignalPaneBuilder ( ).setSweepModeEnum ( SignalPaneBuilder.SweepModeEnum.continuous );
            currentConfig.getSignalPaneBuilder ( ).setTriggerChannelIndex( 0 );
        }

        else if ( triggerModeTriggerOnChannelOneButton.isSelected () )
        {
            currentConfig.getSignalPaneBuilder ( ).setSweepModeEnum ( SignalPaneBuilder.SweepModeEnum.triggered );
            currentConfig.getSignalPaneBuilder ( ).setTriggerChannelIndex( 0 );
        }

        else if ( triggerModeTriggerOnChannelTwoButton.isSelected () )
        {
            currentConfig.getSignalPaneBuilder ( ).setSweepModeEnum ( SignalPaneBuilder.SweepModeEnum.triggered );
            currentConfig.getSignalPaneBuilder ( ).setTriggerChannelIndex( 1 );
        }

        // channel names
        currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).setSignalName ( channelOneNameText.getText ( ) );
        currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).setSignalName ( channelTwoNameText.getText ( ) );

        // display
        currentConfig.getSignalPaneBuilder ( ).setMonitorStartAutomatically ( startMonitoringAutomaticallyCheckBox.isSelected () );
        currentConfig.getSignalPaneBuilder ( ).setTriggerFreezeTimeInSeconds ( triggerFreezeTimeInSecondsBinder.getValue () );
        currentConfig.getSignalPaneBuilder ( ).setDisplayTimeMsPerDivision ( displayTimeMsPerDivBinder.getValue ( ) );
        currentConfig.getSignalPaneBuilder ( ).setMaxNumberOfDisplayedPointsPerSec ( ( int ) maxNumberOfDisplayedPointsPerSecBinder.getValue ( ) );

        // trigger volume
        currentConfig.getSignalPaneBuilder ( ).setTriggerSoundVolume ( triggerSoundVolumeBinder.getValue ( ) );

        // channels
        currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).setSensitivityMilliVoltsPerDiv ( channelOneSensitivityBinder.getValue ( ) );
        currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 0 ).setTriggerLevelPercentage ( channelOneTriggerLevelBinder.getValue ( ) );
        currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).setSensitivityMilliVoltsPerDiv ( channelTwoSensitivityBinder.getValue ( ) );
        currentConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( 1 ).setTriggerLevelPercentage ( channelTwoTriggerLevelBinder.getValue ( ) );

        // save screens info
        currentConfig.getSignalPaneBuilder ( ).setScreensSavingDirectory ( screenSavingDirectoryTextField.getText ( ) );
        currentConfig.getSignalPaneBuilder ( ).setNumberOfSavedScreens ( (int) screenSavingCountBinder.getValue ( ) );

        // filtering
        currentConfig.getFilterBuilder ( ).setUseAdaptiveFiltering ( useAdaptiveFilteringCheckBox.isSelected ( ) );
        currentConfig.getFilterBuilder ( ).setNotchFrequencyEnum ( filterNotchFrequencyEnumCombo.getSelectionModel ( ).getSelectedItem ( ) );
        currentConfig.getFilterBuilder ( ).setAdaptiveCoefficient ( adaptiveCoefficientBinder.getValue () );

        // surgery info
        currentConfig.getSurgeryInfo ( ).setPatientFirstName ( patientFirstNameTextField.getText ( ) );
        currentConfig.getSurgeryInfo ( ).setPatientMiddleName ( patientMiddleNameTextField.getText ( ) );
        currentConfig.getSurgeryInfo ( ).setPatientLastName ( patientLastNameTextField.getText ( ) );
        currentConfig.getSurgeryInfo ( ).setPatientAge ( patientAgeTextField.getText ( ) );
    }

    @Override
    public void addListener ( com.poplar.monitor.imp.javafx.ChangeListener<ApplicationConfig> aInConfigListener )
    {
        synchronized ( configChangeListeners )
        {
            configChangeListeners.add ( aInConfigListener );
        }
    }

    private void iNotifyListenersOnConfigChange ( )
        throws Exception
    {
        synchronized ( configChangeListeners )
        {
            for ( com.poplar.monitor.imp.javafx.ChangeListener<ApplicationConfig> lNextListener : configChangeListeners )
            {
                lNextListener.change ( GsonUtil.clone ( currentConfig ), latestNotifiedConfig );
            }
        }
    }

    private void iLoadConfigFromFileOrCreateNewOne ( )
        throws IOException
    {
        if ( configFilePath.exists ( ) )
        {
            boolean lSuccessReading;

            try
            {
                currentConfig = GsonUtil.readJsonFromFile ( configFilePath, ApplicationConfig.class );

                currentConfig.validate();

                lSuccessReading = true;
            }

            catch ( Exception ex )
            {
                logger.error (
                    "did not find a valid currentConfig file on this path: " + configFilePath.getAbsolutePath ( ) +
                    ", will leave a copy of the invalid content on this other place: " + configFilePathInvalidCopy );

                currentConfig = null;

                lSuccessReading = false;
            }

            if ( ! lSuccessReading )
            {
                try
                {
                    FileUtil.copyFile ( configFilePath, configFilePathInvalidCopy );
                    FileUtil.deletePlainFileIfExists ( configFilePath, true );
                }

                catch ( IOException ex )
                {
                    logger.error ( ex.toString ( ), ex );
                }
            }
        }

        else
        {
            logger.warn ( "did not find currentConfig file on this path: " + configFilePath.getAbsolutePath ( ) );
        }

        if ( currentConfig != null )
        {
            logger.info ( "found currentConfig file on this path: " + configFilePath.getAbsolutePath ( ) );
        }

        else
        {
            logger.warn ( "starting on a fresh default currentConfig" );

            currentConfig = new ApplicationConfig ( );

            currentConfig.setGitVersion ( SystemUtil.getGitVersion ( true ) );

            currentConfig.setSignalPaneBuilder (
                SignalPaneBuilder.init ( )
                    .setMonitorStartAutomatically ( true )
                    .setNumberOfSavedScreens( 0 )
                    .setTriggerChannelIndex ( 0 )
                    .setTriggerFreezeTimeInSeconds ( 0.050f )
                    .setDisplayTimeMsPerDivision ( 10.0f )
                    .setMaxNumberOfDisplayedPointsPerSec ( 1000 )
                    .setDisplayModeThresholdMSecPerDiv ( 50.01 )
                    .setSweepModeEnum ( SignalPaneBuilder.SweepModeEnum.continuous )
                    .setSignalChannelBuilders ( CollectionsUtil.createList (
                        SignalChannelBuilder
                            .init ( )
                            .setSignalName ( "" )
                            .setSensitivityMilliVoltsPerDiv ( 5 )
                            .setTriggerLevelPercentage ( 80.0 )
                            .setRgbAsStringColor ( "#00f0f0" )
                            .create ( ),

                        SignalChannelBuilder
                            .init ( )
                            .setSignalName ( "" )
                            .setSensitivityMilliVoltsPerDiv ( 5 )
                            .setTriggerLevelPercentage ( 80.0 )
                            .setRgbAsStringColor ( "#00f000" )
                            .create ( ) ) )
                    .create ( ) );

            currentConfig.setSignalSourceBuilder (
                SignalSourceBuilder
                    .init ( )
                    .setSignalSourceType ( SignalSourceBuilder.SignalSourceType.simulator )
                    .setSimulatorType ( SignalSourceBuilder.SimulatorType.powerLineNoise )
                    .setSignalFileOriginType ( SignalSourceBuilder.SignalFileOriginType.internal_1 )
                    .setnChannels ( 2 )
                    .setSamplingFrequencyEnum ( SignalSourceBuilder.SamplingFrequencyEnum.value44100 )
                    .setAudioMixerDescriptionRegexp ( ".*" )
                    .setPeriodicResetIntervalInSeconds ( 7_200 ).create ( ) );

            currentConfig.setFilterBuilder (
                FilterBuilder
                    .init ( )
                    .setUseAdaptiveFiltering ( false )
                    .setAdaptiveCoefficient ( 10000.0 )
                    .setNotchFrequencyEnum (
                        Locale.getDefault ( ).getCountry ( ).equals ( Locale.US.getCountry ( ) )
                            ? FilterBuilder.LineFrequencyEnum.value60
                            : FilterBuilder.LineFrequencyEnum.value50
                    )
            );

            currentConfig.setSurgeryInfo (
                SurgeryInfo
                    .init ( )
                    .create ( ) );

            GsonUtil.writeJsonToFile ( defaultConfigFilePath, currentConfig );

            GsonUtil.writeJsonToFile ( configFilePath, currentConfig );
        }
    }

    public void close ( )
        throws Exception
    {
        iApplyControlsToConfig();
        GsonUtil.writeJsonToFile ( configFilePath, currentConfig );
    }

    @Override
    public Property getProperty ( PropertiesEnum aInPropertiesEnum )
    {
        Property ret = null;

        switch ( aInPropertiesEnum )
        {
            case ModeContinuousValue:
                ret = triggerModeContinuousButton.selectedProperty ();
                break;
            case TriggerChannelOneValue:
                ret = triggerModeTriggerOnChannelOneButton.selectedProperty ( );
                break;
            case TriggerChannelTwoValue:
                ret = triggerModeTriggerOnChannelTwoButton.selectedProperty ();
                break;
            case DisplayTimeValueMsPerDiv:
                ret = displayTimeMsPerDivBinder.valueProperty ();
                break;
            case UseAdaptiveFilteringValue:
                ret = useAdaptiveFilteringCheckBox.selectedProperty ();
                break;
            case TriggerFreezeTimeInSecondsValue:
                ret = triggerFreezeTimeInSecondsBinder.valueProperty ();
                break;
            case TriggerSoundVolumeValue:
                ret = triggerSoundVolumeBinder.valueProperty ();
                break;
            case ChannelOneSensitivityValue:
                ret = channelOneSensitivityBinder.valueProperty ();
                break;
            case ChannelTwoSensitivityValue:
                ret = channelTwoSensitivityBinder.valueProperty ();
                break;
            case ChannelOneTriggerLevelValue:
                ret = channelOneTriggerLevelBinder.valueProperty ();
                break;
            case ChannelTwoTriggerLevelValue:
                ret = channelTwoTriggerLevelBinder.valueProperty ();
                break;

            default:
                Preconditions.checkState ( false, "not defined: " + aInPropertiesEnum );
        }

        return ret;
    }
}

