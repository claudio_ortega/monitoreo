/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class HoldoffTimeDampener
{
    private static final Logger logger = LogManager.getLogger ( HoldoffTimeDampener.class );

    private Runnable runnable;
    private final ScheduledThreadPoolExecutor executor;
    private final AtomicLong nextTriggerTime;
    private long holdOffDelay;

    private HoldoffTimeDampener ( int aInSamplingTimeInMSec )
    {
        holdOffDelay = 0;

        nextTriggerTime = new AtomicLong ( 0 );

        executor = new ScheduledThreadPoolExecutor ( 1 );

        executor.scheduleAtFixedRate (
            () ->
            {
                try
                {
                    synchronized ( executor )
                    {
                        if ( runnable != null &&
                            nextTriggerTime.get ( ) > 0 &&
                            System.currentTimeMillis ( ) > nextTriggerTime.get ( ) )
                        {
                            nextTriggerTime.set ( 0 );
                            runnable.run ( );
                        }
                    }
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage ( ), ex );
                }
            },
            0,
            aInSamplingTimeInMSec,
            TimeUnit.MILLISECONDS );
    }

    public static HoldoffTimeDampener create ( int aInSamplingTimeInMSec )
    {
        return new HoldoffTimeDampener ( aInSamplingTimeInMSec );
    }

    public HoldoffTimeDampener setTask ( Runnable r )
    {
        synchronized ( executor )
        {
            runnable = r;
        }

        return this;
    }

    public HoldoffTimeDampener setHoldOffDelayInMSec ( long aInDelay )
    {
        synchronized ( executor )
        {
            holdOffDelay = aInDelay;
        }

        return this;
    }

    public void doReschedule ( )
    {
        synchronized ( executor )
        {
            nextTriggerTime.set ( System.currentTimeMillis ( ) + holdOffDelay );
        }
    }

    public void close ( )
    {
        synchronized ( executor )
        {
            executor.shutdown ( );
        }
    }
}

