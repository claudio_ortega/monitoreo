/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.Expose;
import com.poplar.monitor.api.FilterBuilder;
import com.poplar.monitor.api.SignalPaneBuilder;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.util.SystemUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationConfig
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    @Expose
    private String gitVersion;

    @Expose
    private SignalSourceBuilder signalSourceBuilder;

    @Expose
    private SignalPaneBuilder signalPaneBuilder;

    @Expose
    private FilterBuilder filterBuilder;

    @Expose
    private SurgeryInfo surgeryInfo;


    public String getGitVersion ( )
    {
        return gitVersion;
    }

    public ApplicationConfig setGitVersion ( String aInGitVersion )
    {
        gitVersion = aInGitVersion;
        return this;
    }

    public SurgeryInfo getSurgeryInfo ( )
    {
        return surgeryInfo;
    }

    public ApplicationConfig setSurgeryInfo ( SurgeryInfo value )
    {
        surgeryInfo = value;
        return this;
    }

    public FilterBuilder getFilterBuilder ( )
    {
        return filterBuilder;
    }

    public ApplicationConfig setFilterBuilder ( FilterBuilder aInValue )
    {
        filterBuilder = aInValue;
        return this;
    }

    public SignalSourceBuilder getSignalSourceBuilder ()
    {
        return signalSourceBuilder;
    }

    public ApplicationConfig setSignalSourceBuilder ( SignalSourceBuilder aInSignalSourceBuilder )
    {
        signalSourceBuilder = aInSignalSourceBuilder;
        return this;
    }

    public SignalPaneBuilder getSignalPaneBuilder ()
    {
        return signalPaneBuilder;
    }

    public ApplicationConfig setSignalPaneBuilder ( SignalPaneBuilder aInSignalPaneBuilder )
    {
        signalPaneBuilder = aInSignalPaneBuilder;
        return this;
    }

    public void validate()
    {
        Preconditions.checkState ( signalSourceBuilder != null );
        Preconditions.checkState ( signalPaneBuilder != null );
        Preconditions.checkState ( filterBuilder != null );
        Preconditions.checkState ( surgeryInfo != null );
        Preconditions.checkState ( gitVersion.equals( SystemUtil.getGitVersion ( true ) ), "git version in config file is invalid: " + gitVersion );

        logger.trace( "successfully validated: " + toString () );
    }

    @Override
    public String toString ( )
    {
        return "ApplicationConfig{" +
            "gitVersion=" + gitVersion +
            ", signalSourceBuilder=" + signalSourceBuilder +
            ", signalPaneBuilder=" + signalPaneBuilder +
            ", filterBuilder=" + filterBuilder +
            ", surgeryInfo=" + surgeryInfo +
            '}';
    }

    @Override
    public boolean equals ( Object o )
    {
        return toString ().equals ( o.toString () );
    }

    @Override
    public int hashCode ( )
    {
        return toString ().hashCode ();
    }
}
