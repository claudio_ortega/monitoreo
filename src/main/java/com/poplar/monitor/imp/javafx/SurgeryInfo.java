/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.gson.annotations.Expose;

public class SurgeryInfo
{
    @Expose
    private String patientFirstName;
    @Expose
    private String patientMiddleName;
    @Expose
    private String patientLastName;
    @Expose
    private String patientAge;


    private SurgeryInfo ( )
    {
        patientFirstName = "";
        patientMiddleName = "";
        patientLastName = "";
        patientAge = "";
    }

    public static SurgeryInfo init()
    {
        return new SurgeryInfo ();
    }

    public SurgeryInfo create ()
    {
        return this;
    }

    public String getPatientFirstName ( )
    {
        return patientFirstName;
    }

    public SurgeryInfo setPatientFirstName ( String value )
    {
        patientFirstName = value;
        return this;
    }

    public String getPatientMiddleName ( )
    {
        return patientMiddleName;
    }

    public SurgeryInfo setPatientMiddleName ( String value )
    {
        patientMiddleName = value;
        return this;
    }

    public String getPatientLastName ( )
    {
        return patientLastName;
    }

    public SurgeryInfo setPatientLastName ( String value )
    {
        patientLastName = value;
        return this;
    }

    public String getPatientAge ( )
    {
        return patientAge;
    }

    public SurgeryInfo setPatientAge ( String value )
    {
        patientAge = value;
        return this;
    }

    @Override
    public boolean equals ( Object o )
    {
        return toString ().equals ( o.toString () );
    }

    @Override
    public int hashCode ( )
    {
        return toString ().hashCode ();
    }

    @Override
    public String toString ( )
    {
        return "SurgeryInfo{" +
            "patientFirstName='" + patientFirstName + '\'' +
            ", patientMiddleName='" + patientMiddleName + '\'' +
            ", patientLastName='" + patientLastName + '\'' +
            ", patientAge='" + patientAge + '\'' +
            '}';
    }
}
