/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;

import java.util.Locale;
import java.util.ResourceBundle;

public class LanguageManager
{
    private static LanguageManager SINGLETON;

    private final ResourceBundle resourceBundle;

    private LanguageManager ()
    {
        resourceBundle = ResourceBundle.getBundle ( "com.poplar.monitor.imp.javafx.strings", Locale.getDefault () );
        Preconditions.checkState ( resourceBundle != null );
    }

    public synchronized static void createSingleton ()
    {
        Preconditions.checkState ( SINGLETON == null, "singleton should have been still null by now" );
        SINGLETON = new LanguageManager ( );
    }

    public synchronized static LanguageManager getSingleton()
    {
        Preconditions.checkState ( SINGLETON != null, "singleton should have been created before this call" );
        return SINGLETON;
    }

    public ResourceBundle getResourceBundle()
    {
        return resourceBundle;
    }

    public String getValue( String key )
    {
        return resourceBundle.getString ( key );
    }

    public boolean containsKey( String key )
    {
        return resourceBundle.containsKey ( key );
    }
}
