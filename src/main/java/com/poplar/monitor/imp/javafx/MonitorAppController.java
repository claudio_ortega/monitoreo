/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.SegmentedButton;
import org.controlsfx.control.ToggleSwitch;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class MonitorAppController
    implements IPropertiesProvider
{
    private static final Logger logger = LogManager.getLogger ( MonitorAppController.class );

    private volatile SignalPane signalPane;
    private volatile ApplicationConfig applicationConfig;
    private Pane generalControlPane;
    private Pane monitoringBox;

    private final ToggleButton buttonStart;
    private final ToggleButton buttonPause;

    private final ToggleButton triggerModeContinuousButton;
    private final ToggleButton triggerModeTriggerOnChannelOneButton;
    private final ToggleButton triggerModeTriggerOnChannelTwoButton;

    private final SegmentedButton triggerModeSegmentedButton;

    private final Button buttonSaveSignal;
    private final Button readFromDefaultsButton;
    private SliderTextFieldBinder displayTimeMsPerDivSlider;
    private SliderTextFieldBinder triggerSoundVolumeBinder;
    private SliderTextFieldBinder triggerFreezeTimeInSecondsBinder;

    private CheckBox useAdaptiveFilteringCheckBox;
    private IApplicationConfigProvider applicationConfigProvider;

    private final com.poplar.monitor.imp.javafx.ChangeListener<ApplicationConfig> configChangeListener;

    private final ChannelControlPane[] channelControlPanes;
    private Callable<Void> startHandler;
    private Callable<Void> stopHandler;

    private static final String BACKGROUND_TRANSPARENT = "-fx-background-color: transparent";
    private static final String BACKGROUND_GRAY = "-fx-background-color: hsba(0, 0%, 100%, 1.0)";
    //private static final String BACKGROUND_DARK_GRAY = "-fx-background-color: hsba(0, 0%, 70%, 1.0)";
    //private static final String BACKGROUND_IMAGE_OR_COLOR = "-fx-background-image: url( file://" + MonitorApp.class.getResource ( "background.png" ).getPath () + "); -fx-background-repeat: repeat";

    public static MonitorAppController create() throws Exception
    {
        return new MonitorAppController ();
    }

    public MonitorAppController init() throws Exception
    {
        // we want to reuse all the code that reacts to the pause button selection
        // to work as that button was just clicked on start
        buttonPause.setSelected ( true );
        return this;
    }

    private MonitorAppController ( ) throws Exception
    {
        final LanguageManager languageManager = LanguageManager.getSingleton ();

        channelControlPanes = new ChannelControlPane[2];

        for ( int channelIx=0; channelIx<channelControlPanes.length; channelIx++ )
        {
            channelControlPanes[channelIx] = new ChannelControlPane ();
        }

        configChangeListener = new com.poplar.monitor.imp.javafx.ChangeListener<ApplicationConfig>()
        {
            @Override
            public void change ( ApplicationConfig aInNewValue, ApplicationConfig aInOldValue )
            {
                applicationConfig = aInNewValue;

                JavaFxUtils.executeInFxThread (
                    ( ) ->
                    {
                        buttonSaveSignal.setDisable ( ! aInNewValue.getSignalPaneBuilder ( ).getIsScreensSavingWindowEnabled ( ) );
                        return null;
                    }
                );
            }
        };

        startHandler = new Callable<Void> ( )
        {
            @Override
            public Void call ( )
                throws Exception
            {
                if ( signalPane != null )
                {
                    signalPane.start ( );
                }

                buttonStart.setDisable ( true );
                buttonPause.setDisable ( false );
                buttonPause.requestFocus ();

                return null;
            }
        };

        stopHandler = new Callable<Void> ( )
        {
            @Override
            public Void call ( )
                throws Exception
            {
                if ( signalPane != null )
                {
                    signalPane.stop ( );
                }

                buttonStart.setDisable ( false );
                buttonPause.setDisable ( true );
                buttonStart.requestFocus ();

                return null;
            }
        };

        final AtomicBoolean autoStartHandled = new AtomicBoolean ( false );

        final com.poplar.monitor.imp.javafx.ChangeListener<DisplayStatus> displayStatusChangeListener = new com.poplar.monitor.imp.javafx.ChangeListener<DisplayStatus>()
        {
            @Override
            public void change ( DisplayStatus aInNewValue, DisplayStatus aInOldValue )
            {
                try
                {
                    JavaFxUtils.executeInFxThread (
                        () ->
                        {
                            switch ( aInNewValue )
                            {
                                case init_started:
                                    JavaFxUtils.executeInFxThread (
                                        ( ) ->
                                        {
                                            buttonStart.setDisable ( true );
                                            buttonPause.setDisable ( false );
                                            return null;
                                        }
                                    );

                                    break;

                                case init_done:
                                    JavaFxUtils.executeInFxThread (
                                        ( ) ->
                                        {
                                            buttonStart.setDisable ( false );
                                            buttonPause.setDisable ( true );
                                            buttonStart.requestFocus ();

                                            iPopulateMonitoringBox ( );

                                            if ( applicationConfig.getSignalPaneBuilder ().getMonitorStartAutomatically () && ! autoStartHandled.get() )
                                            {
                                                autoStartHandled.set ( true );

                                                JavaFxUtils.executeInFxThreadAfterDelay (
                                                    startHandler,
                                                    3_000
                                                );
                                            }

                                            return null;
                                        }
                                    );
                                    break;

                                case init_error:
                                case stopped:
                                case playing:
                                    break;

                                default:
                                    Preconditions.checkState ( false, "unhandled enum" );
                            }

                            return null;
                        }
                    );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            }
        };

        monitoringBox = new VBox (  );

        buttonStart = new ToggleButton ( languageManager.getValue ( "Start" ), new Glyph ( "FontAwesome", FontAwesome.Glyph.PLAY ).color( Color.BLACK ).size ( 12 ) );
        buttonPause = new ToggleButton ( languageManager.getValue ( "Pause" ), new Glyph ( "FontAwesome", FontAwesome.Glyph.PAUSE ).color( Color.BLACK ).size ( 12 ) );

        buttonStart.setOnAction (
            ( event ) ->
            {
                try
                {
                    startHandler.call ( );
                }
                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            }
        );

        buttonPause.setOnAction (
            ( event ) ->
            {
                try
                {
                    stopHandler.call ( );
                }
                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            }
        );

        buttonSaveSignal = new Button ( languageManager.getValue ( "save-latest-screens" ) );
        buttonSaveSignal.setOnAction (
            ( event ) -> signalPane.saveScreensAToB ( )
        );

        readFromDefaultsButton = new Button( languageManager.getValue ( "read-default" ) );

        readFromDefaultsButton.setStyle (
            "-fx-background-color: " +
            "        linear-gradient(#ffd65b, #e68400)," +
            "        linear-gradient(#ffef84, #f2ba44)," +
            "        linear-gradient(#ffea6a, #efaa22)," +
            "        linear-gradient(#ffe657 0%, #f8c202 50%, #eea10b 100%)," +
            "        linear-gradient(from 0% 0% to 15% 50%, rgba(255,255,255,0.9), rgba(255,255,255,0));" +
            "    -fx-background-radius: 5;" +
            "    -fx-background-insets: 0,1,2,3,0;" +
            "    -fx-text-fill: #654b00;" +
            "    -fx-font-weight: bold;" +
            "    -fx-font-size: 14px;" +
            "    -fx-padding: 8 20 8 20;"
        );

        triggerModeContinuousButton = new ToggleButton ( languageManager.getValue ( "Continuous" ) );
        triggerModeTriggerOnChannelOneButton = new ToggleButton ( languageManager.getValue ( "Trigger-Channel-1" ) );
        triggerModeTriggerOnChannelTwoButton = new ToggleButton ( languageManager.getValue ( "Trigger-Channel-2" ) );

        triggerModeSegmentedButton = new SegmentedButton (
            triggerModeContinuousButton,
            triggerModeTriggerOnChannelOneButton,
            triggerModeTriggerOnChannelTwoButton );

        JavaFxUtils.restrictOneSelectedButtonAtATime ( triggerModeSegmentedButton.getButtons ( ) );

        useAdaptiveFilteringCheckBox = new CheckBox ();

        triggerSoundVolumeBinder = SliderTextFieldBinder
            .create ( )
            .setNumberOfDecimalDigits ( 0 )
            .setOrientation ( Orientation.HORIZONTAL )
            .setNumberOfTicks ( 6 )
            .setLinearOperation ( 0.0, 5.0 )
            .setPostStartProcessor ( slider -> {
                slider.setShowTickLabels ( true );
                slider.setShowTickMarks ( true );
                slider.setSnapToTicks ( true );
                slider.setMinorTickCount ( 0 );
            } )
            .start ( );

        displayTimeMsPerDivSlider = SliderTextFieldBinder
            .create ( )
            .setNumberOfDecimalDigits ( 0 )
            .setOrientation ( Orientation.HORIZONTAL )
            .setLinearBySegmentsOperation ( 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0 )
            .start ();

        triggerFreezeTimeInSecondsBinder = SliderTextFieldBinder
            .create ( )
            .setNumberOfDecimalDigits ( 2 )
            .setOrientation ( Orientation.HORIZONTAL )
            .setLinearBySegmentsOperation ( 0.01, 0.05, 0.1, 0.5, 1.0, 5.0 )
            .start ( );

        generalControlPane = new GeneralControlPane ( ).getPane ( );

        signalPane = new SignalPane ();
        signalPane.getDisplayStatusChangeProducer ( ).addListener ( displayStatusChangeListener );
    }

    public void setApplicationConfigProvider ( IApplicationConfigProvider newValue )
    {
        applicationConfigProvider = newValue;
    }

    public SignalPane getSignalPane()
    {
        return signalPane;
    }

    public Pane getPane()
    {
        return monitoringBox;
    }

    public void stop()
    {
        try
        {
            stopHandler.call ( );
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage (), ex );
        }
    }

    public void start()
    {
        try
        {
            startHandler.call ( );
        }

        catch ( Exception ex )
        {
            logger.error ( ex.getMessage (), ex );
        }
    }

    public ChangeListener<ApplicationConfig> getConfigChangeListener ( )
    {
        return configChangeListener;
    }

    private static class ChannelControlPane
    {
        private final VBox vBox;
        private final GridPane gridPane;
        private final SliderTextFieldBinder sensitivityBinder;
        private final SliderTextFieldBinder triggerLevelBinder;

        private Pane getPane ( )
        {
            return vBox;
        }

        ChannelControlPane ( )
        {
            final LanguageManager languageManager = LanguageManager.getSingleton ( );

            sensitivityBinder = SliderTextFieldBinder
                .create ( )
                .setNumberOfDecimalDigits ( 3 )
                .setOrientation ( Orientation.VERTICAL )
                .setLinearBySegmentsOperation ( 0.005, 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1.0, 2.0, 5.0 )
                .start ( );

            triggerLevelBinder = SliderTextFieldBinder
                .create ( )
                .setNumberOfDecimalDigits ( 0 )
                .setOrientation ( Orientation.VERTICAL )
                .setNumberOfTicks ( 11 )
                .setLinearOperation ( - 100.0, 100.0 )
                .start ( );

            gridPane = new GridPane ( );
            gridPane.setVgap ( 0 );

            gridPane.add ( sensitivityBinder.getSlider (), 0, 0 );
            gridPane.add ( new Label ( languageManager.getValue ( "Sensitivity-short" ) ), 0, 1 );
            gridPane.add ( sensitivityBinder.getTextField (), 0, 2 );

            gridPane.add ( triggerLevelBinder.getSlider (), 1, 0 );
            gridPane.add ( new Label ( languageManager.getValue ( "Trigger-short" ) ), 1, 1 );
            gridPane.add ( triggerLevelBinder.getTextField (), 1, 2 );

            VBox.setVgrow ( gridPane, Priority.ALWAYS );

            vBox = new VBox ( );
            vBox.setPadding ( new Insets ( 15, 1, 20, 4 ) );
            vBox.setSpacing ( 25 );
            vBox.setAlignment ( Pos.CENTER );
            vBox.getChildren ( ).add ( gridPane );

            //gridPane.setGridLinesVisible ( true );

            deepVisitNode ( gridPane, node -> {
                if ( node instanceof Label )
                {
                    ( ( Label ) node ).setAlignment ( Pos.CENTER );
                    ( ( Label ) node ).setMinWidth ( 30 );
                    GridPane.setValignment ( node, VPos.CENTER );
                    GridPane.setHalignment ( node, HPos.CENTER );
                    GridPane.setHgrow ( node, Priority.ALWAYS );
                }

                else if ( node instanceof Slider )
                {
                    ( ( Slider ) node ).setOrientation ( Orientation.VERTICAL );
                    GridPane.setMargin ( node, new Insets ( 5, 0, 5, 0 ) );
                    GridPane.setValignment ( node, VPos.CENTER );
                    GridPane.setHalignment ( node, HPos.CENTER );
                    GridPane.setVgrow ( node, Priority.ALWAYS );
                }

                else if ( node instanceof TextField )
                {
                    ( ( TextField ) node ).setAlignment ( Pos.CENTER );
                    GridPane.setValignment ( node, VPos.CENTER );
                    GridPane.setHalignment ( node, HPos.CENTER );
                    ( ( TextField ) node ).setMaxWidth ( 45 );
                }
            } );
        }
    }

    private class GeneralControlPane
    {
        private final HBox hBox;

        private Pane getPane ( )
        {
            return hBox;
        }

        GeneralControlPane ( )
        {
            final LanguageManager languageManager = LanguageManager.getSingleton ( );

            final SegmentedButton buttonStartPause = new SegmentedButton ( buttonStart, buttonPause );
            JavaFxUtils.restrictOneSelectedButtonAtATime ( buttonStartPause.getButtons ( ) );

            final HBox saveOrPlayOrPauseBox = new HBox ( buttonSaveSignal, createExpandableRegion ( 10, 50 ), buttonStartPause );
            final HBox displayTimeBox = new HBox ( displayTimeMsPerDivSlider.getSlider (), displayTimeMsPerDivSlider.getTextField ( ), new Label ( languageManager.getValue ( "Display-Time-Short" ) ) );
            final HBox triggerModeBox = new HBox ( triggerModeSegmentedButton );
            final HBox triggerVolumeBox = new HBox ( triggerSoundVolumeBinder.getSlider (), triggerSoundVolumeBinder.getTextField (), new Label ( languageManager.getValue ( "Trigger-Volume" ) ) );
            final HBox triggerTimeBox = new HBox ( triggerFreezeTimeInSecondsBinder.getSlider (), triggerFreezeTimeInSecondsBinder.getTextField (), new Label ( languageManager.getValue ( "Trigger-Freeze-Time-Short" ) ) );
            final HBox powerLineFilterTimeBox = new HBox (
                createExpandableRegion( 10, 50 ), useAdaptiveFilteringCheckBox,
                new Label ( languageManager.getValue ( "power-line-filter" ) ),
                createExpandableRegion( 10, 50 )
            );

            final GridPane gridPane = new GridPane ( );
            gridPane.add ( saveOrPlayOrPauseBox, 0, 0 );
            gridPane.add ( createExpandableRegion ( 10, 50 ), 1, 0 );
            gridPane.add ( triggerModeBox, 2, 0 );
            gridPane.add ( createExpandableRegion ( 10, 50 ), 3, 0 );
            gridPane.add ( triggerVolumeBox, 4, 0 );
            gridPane.add ( displayTimeBox, 0, 1 );
            gridPane.add ( powerLineFilterTimeBox, 2, 1 );
            gridPane.add ( triggerTimeBox, 4, 1 );

            //gridPane.setGridLinesVisible ( true );

            gridPane.setVgap ( 0 );
            gridPane.setHgap ( 0 );
            gridPane.setPadding ( new Insets ( 0, 0, 0, 0 ) );

            readFromDefaultsButton.setOnAction( e -> {
                try
                {
                    closeLineInSignalPane ( );
                    applicationConfigProvider.readDefaultConfigFromDisk ();
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            } );

            final HBox defaultsRegion = new HBox ( readFromDefaultsButton );
            readFromDefaultsButton.setAlignment ( Pos.CENTER_LEFT );
            defaultsRegion.setAlignment ( Pos.CENTER_LEFT );

            hBox = new HBox ( defaultsRegion, gridPane, createExpandableRegion ( 10, 50 ) );
            hBox.setSpacing ( 40 );
            hBox.setAlignment ( Pos.CENTER );

            deepVisitNode (
                gridPane, node -> {
                    if ( node instanceof Label )
                    {
                        ( ( Label ) node ).setAlignment ( Pos.CENTER );
                    }

                    else if ( node instanceof TextField )
                    {
                        ( ( TextField ) node ).setAlignment ( Pos.CENTER );
                        ( ( TextField ) node ).setMaxWidth ( 60 );
                    }

                    else if ( node instanceof ToggleSwitch )
                    {
                        ( ( ToggleSwitch ) node ).setPadding ( new Insets ( 10, 0, 0, 0 ) );
                    }

                    else if ( node instanceof HBox )
                    {
                        ( ( HBox ) node ).setAlignment ( Pos.CENTER_LEFT );
                        ( ( HBox ) node ).setSpacing ( 10 );
                    }

                    else if ( node instanceof Slider )
                    {
                        HBox.setHgrow ( node, Priority.ALWAYS );
                        ( ( Slider ) node ).setOrientation ( Orientation.HORIZONTAL );
                        ( ( Slider ) node ).setPrefWidth ( 200 );
                        ( ( Slider ) node ).setMaxWidth ( 200 );
                        ( ( Slider ) node ).setMinWidth ( 200 );
                        ( ( Slider ) node ).setPadding ( new Insets ( 3, 3, 3, 3 ) );
                    }
                } );
        }
    }

    private static Region createExpandableRegion ( int min, int max )
    {
        final Region region = new Region ();
        region.setMinWidth ( min );
        region.setPrefWidth ( max );
        return region;
    }

    private static void deepVisitNode ( Node node, Consumer<Node> consumer )
    {
        consumer.accept ( node );

        if ( node instanceof Pane )
        {
            for ( Node next : ( (Pane) node ).getChildren () )
            {
                deepVisitNode ( next, consumer );
            }
        }
    }

    @SuppressWarnings( "StatementWithEmptyBody" )
    private void iPopulateMonitoringBox ( )
    {
        logger.info( "iPopulateMonitoringBox -- begin" );

        Preconditions.checkState ( Platform.isFxApplicationThread ( ) );

        logger.info( "signalPane:" + signalPane );
        logger.info( "applicationConfig:" + applicationConfig );

        monitoringBox.getChildren ( ).clear ( );

        boolean signalBoxHasSignal = false;

        if ( signalPane == null || applicationConfig == null )
        {
            logger.info( "skipping, no signalPane or applicationConfig.. " );
        }

        else
        {
            for ( int i = 0; i < signalPane.getNumberOfChannels ( ); i++ )
            {
                if ( applicationConfig.getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( i ).getVisible ( ) )
                {
                    final List<Canvas> lCanvases = signalPane.getSignalCanvasForChannel ( i );
                    final GridPane lGridPane = new GridPane ( );
                    final Pane canvasContainer = new Pane();

                    for ( final Canvas lCanvas : lCanvases )
                    {
                        canvasContainer.getChildren ( ).add ( lCanvas );
                        lCanvas.widthProperty ( ).bind ( canvasContainer.widthProperty ( ) );
                        lCanvas.heightProperty ( ).bind ( canvasContainer.heightProperty ( ) );
                    }

                    lGridPane.add ( channelControlPanes[i].getPane ( ), 0, 0 );
                    lGridPane.add ( canvasContainer, 1, 0 );

                    lGridPane.setPrefSize ( 10000, 5000 );
                    lGridPane.setMaxSize ( 10000, 5000 );
                    lGridPane.setMinSize ( 500, 10 );

                    GridPane.setVgrow ( canvasContainer, Priority.ALWAYS );
                    GridPane.setHgrow ( canvasContainer, Priority.ALWAYS );

                    VBox.setVgrow ( lGridPane, Priority.ALWAYS );
                    monitoringBox.getChildren ( ).add ( lGridPane );

                    signalBoxHasSignal = true;

                    final Separator lSeparator = new Separator (  );
                    lSeparator.setMaxHeight ( 0 );
                    lSeparator.setMinHeight ( 0 );

                    monitoringBox.getChildren ( ).add ( lSeparator );
                }
            }
        }

        if ( signalBoxHasSignal )
        {
            monitoringBox.getChildren ( ).add ( generalControlPane );
        }

        deepVisitNode (
            monitoringBox, node -> {
                if ( node instanceof Button )
                {
                    // nada
                }

                else if ( node instanceof TextField )
                {
                    // nada
                }

                else
                {
                    node.setStyle ( BACKGROUND_TRANSPARENT );
                }
            } );

        monitoringBox.setStyle ( BACKGROUND_GRAY );

        logger.info( "iPopulateMonitoringBox -- end" );
    }

    public void closeLineInSignalPane ( ) throws Exception
    {
        if ( signalPane != null )
        {
            signalPane.stop ( );
            signalPane.cleanAllScreens ( );
        }
    }

    public void close ( )
    {
        signalPane.stop ( );
        signalPane.close ( );
    }

    @Override
    public Property getProperty ( PropertiesEnum aInPropertiesEnum )
    {
        Property ret = null;

        switch ( aInPropertiesEnum )
        {
            case ModeContinuousValue:
                ret = triggerModeContinuousButton.selectedProperty ( );
                break;
            case TriggerChannelOneValue:
                ret = triggerModeTriggerOnChannelOneButton.selectedProperty ( );
                break;
            case TriggerChannelTwoValue:
                ret = triggerModeTriggerOnChannelTwoButton.selectedProperty ();
                break;
            case UseAdaptiveFilteringValue:
                ret = useAdaptiveFilteringCheckBox.selectedProperty ();
                break;
            case DisplayTimeValueMsPerDiv:
                ret = displayTimeMsPerDivSlider.valueProperty ();
                break;
            case TriggerFreezeTimeInSecondsValue:
                ret = triggerFreezeTimeInSecondsBinder.valueProperty ();
                break;
            case TriggerSoundVolumeValue:
                ret = triggerSoundVolumeBinder.valueProperty ();
                break;
            case ChannelOneSensitivityValue:
                ret = channelControlPanes[0].sensitivityBinder.valueProperty ();
                break;
            case ChannelTwoSensitivityValue:
                ret = channelControlPanes[1].sensitivityBinder.valueProperty ();
                break;
            case ChannelOneTriggerLevelValue:
                ret = channelControlPanes[0].triggerLevelBinder.valueProperty ();
                break;
            case ChannelTwoTriggerLevelValue:
                ret = channelControlPanes[1].triggerLevelBinder.valueProperty ();
                break;

            default:
                Preconditions.checkState ( false, "not defined: " + aInPropertiesEnum );
        }

        return ret;
    }
}
