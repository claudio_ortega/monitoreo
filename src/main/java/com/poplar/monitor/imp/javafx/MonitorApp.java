/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.util.CollectionsUtil;
import com.poplar.monitor.imp.util.SystemUtil;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import org.controlsfx.control.SegmentedButton;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFontRegistry;


public class MonitorApp extends Application
{
    private  final Logger logger = LogManager.getLogger ( getClass () );

    private SettingsController settingsController;
    private MonitorAppController monitorAppController;
    private Stage stage;

    private LanguageManager languageManager;

    public static void main ( String[] args ) throws Exception
    {
        if ( Arrays.asList ( args ).contains ( "--version" ) )
        {
            System.out.println ( SystemUtil.getGitVersion ( true ) );
        }

        else
        {
            launch ( args );
        }

        System.exit ( 0 );
    }

    @Override
    public void start ( final Stage aInStage ) throws Exception
    {
        stage = aInStage;

        logger.info ( "app version: " + SystemUtil.getGitVersion ( true ) );
        logger.info ( "process id: " + SystemUtil.getPIDForThisProcess ( "n/a" ) );
        logger.info ( "cmd line args named: " + getParameters ().getNamed ( ) );
        logger.info ( "cmd line args unnamed: " + getParameters ().getUnnamed ( ) );
        logger.info ( "java args: " + SystemUtil.getJavaExtraArguments ( ) );
        logger.info ( "java home: " + SystemUtil.getEnvironmentOrSysProperty ( "java.home" ) );
        logger.info ( "java runtime version: " + SystemUtil.getEnvironmentOrSysProperty ( "java.runtime.version" ) );
        logger.info ( "java vm name: " + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.name" ) );
        logger.info ( "java vm info: " + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.info" ) );
        logger.info ( "java vm specification version: " + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.version" ) );
        logger.info ( "java vm specification vendor: " + SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.vendor" ) );
        logger.info ( "current directory: " + SystemUtil.getCurrentDirectory () );
        logger.info ( "user name: " + SystemUtil.Constants.USER_NAME );
        logger.info ( "user home dir: " + SystemUtil.Constants.USER_HOME_DIR );
        logger.info ( "default locale: " + Locale.getDefault () );
        logger.info ( "os name: " + SystemUtil.getEnvironmentOrSysProperty ( "os.name" ) );
        logger.info ( "os version: " + SystemUtil.getEnvironmentOrSysProperty ( "os.version" ) );
        logger.info ( "os arch: " + SystemUtil.getEnvironmentOrSysProperty ( "os.arch" ) );

        final String lJavaVMSpecVersion = SystemUtil.getEnvironmentOrSysProperty ( "java.vm.specification.version" );
        Preconditions.checkState ( "1.8".equals ( lJavaVMSpecVersion ), "this application needs java 1.8, but this version was detected: [" + lJavaVMSpecVersion + "], aborting" );

        Preconditions.checkArgument ( getParameters ( ).getUnnamed ( ).isEmpty ( ), "unnamed command line args are not supported" );

        if ( getParameters ().getNamed ( ).containsKey ( "locale" ) )
        {
            final Locale lNewLocale = new Locale( getParameters ().getNamed ( ).get( "locale" ) );
            Locale.setDefault ( lNewLocale );
        }

        logger.info ( "effective default locale: [" + Locale.getDefault ()  + "]" );
        logger.info ( "effective default locale' country: [" + Locale.getDefault ().getCountry () + "]" );
        logger.info ( "effective default locale' display country: [" + Locale.getDefault ().getDisplayCountry ( ) + "]" );
        logger.info ( "effective default locale' language: [" + Locale.getDefault ().getLanguage () + "]"  );
        logger.info ( "effective default locale' displayed language: [" + Locale.getDefault ().getDisplayLanguage ( ) + "]"  );

        // number formatting should still be locale US ..
        Locale.setDefault ( Locale.Category.FORMAT, Locale.US );

        LanguageManager.createSingleton ();
        languageManager = LanguageManager.getSingleton ();

        aInStage.getIcons ( ).add ( new Image ( getClass ( ).getResourceAsStream ( "app_icon.png" ) ) );
        aInStage.setTitle ( languageManager.getValue ( "The-Facial-Nerve-Monitoring-System" ) + " - " + SystemUtil.getGitVersion ( false ) );

        // make all the background stuff to start going right away..
        final Parent lRoot = iInitializeMainPanel ( );

        // register listeners
        settingsController.addListener ( monitorAppController.getConfigChangeListener () );
        settingsController.addListener ( monitorAppController.getSignalPane ().getConfigChangeListener ( ) );

        monitorAppController.getSignalPane ().getDisplayStatusChangeProducer ().addListener (
            ( aInNewValue,  aInOldValue ) ->
            {
                if ( DisplayStatus.init_done == aInNewValue )
                {
                    final Set<PropertiesEnum> values = CollectionsUtil.createSet (
                        PropertiesEnum.ModeContinuousValue,
                        PropertiesEnum.TriggerChannelOneValue,
                        PropertiesEnum.TriggerChannelTwoValue,
                        PropertiesEnum.DisplayTimeValueMsPerDiv,
                        PropertiesEnum.TriggerFreezeTimeInSecondsValue,
                        PropertiesEnum.TriggerSoundVolumeValue,
                        PropertiesEnum.ChannelOneSensitivityValue,
                        PropertiesEnum.ChannelTwoSensitivityValue,
                        PropertiesEnum.ChannelOneTriggerLevelValue,
                        PropertiesEnum.ChannelTwoTriggerLevelValue,
                        PropertiesEnum.UseAdaptiveFilteringValue
                    );

                    PropertiesBinder.create ( ).bindBidirectional ( values, settingsController, monitorAppController );
                    PropertiesBinder.create ( ).bindUnidirectional ( values, monitorAppController, monitorAppController.getSignalPane ( ) );
                }
            }
        );

        settingsController.init();

        final Scene lScene = new Scene ( lRoot );
        aInStage.setScene ( lScene );
        JavaFxUtils.centerOnScreen ( aInStage, 1, true );
        aInStage.show ( );

        lScene.getWindow().setOnCloseRequest (
            ( ev ) ->
            {
                try
                {
                    Preconditions.checkState ( Platform.isFxApplicationThread ( ) );

                    monitorAppController.stop ();

                    final int lPendingScreens = monitorAppController.getSignalPane ().getCountOnPendingScreens ( );

                    logger.info( "on exit, pending screens: " + lPendingScreens );

                    if ( lPendingScreens > 0 )
                    {
                        final Alert alert = new Alert( Alert.AlertType.WARNING );
                        alert.setTitle ( "" );
                        alert.setHeaderText ( languageManager.getValue ( "pending-screens" ) );
                        alert.setContentText ( "" );

                        final ButtonType buttonTypeOne = new ButtonType ( languageManager.getValue ( "quit" ) );
                        final ButtonType buttonTypeCancel = new ButtonType ( languageManager.getValue ( "go-back" ) );

                        alert.getButtonTypes().setAll ( buttonTypeOne, buttonTypeCancel );

                        final Optional<ButtonType> result = alert.showAndWait();

                        if ( result.isPresent () && result.get() == buttonTypeCancel )
                        {
                            logger.info( "going back to monitoring from the modal dialog" );
                            monitorAppController.start ();
                            ev.consume ();
                        }

                        else
                        {
                            logger.info( "quitting the app from the modal dialog" );
                        }
                    }
                }

                catch (Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            }
        );
    }

    private Parent iInitializeMainPanel ( ) throws Exception
    {
        monitorAppController = MonitorAppController.create ( );
        monitorAppController.init();

        final VBox lRoot = new VBox ();
        lRoot.setAlignment ( Pos.CENTER );

        final FXMLLoader lSettingsLoader = new FXMLLoader ( getClass ( ).getResource ( "Settings.fxml" ) );
        lSettingsLoader.setResources ( languageManager.getResourceBundle ( ) );
        lSettingsLoader.load ( );
        settingsController = lSettingsLoader.getController ( );

        settingsController.setStage( stage );

        monitorAppController.setApplicationConfigProvider( settingsController );
        monitorAppController.getSignalPane ().setApplicationConfigProvider ( settingsController );

        final Parent lSettingPanel = lSettingsLoader.getRoot ( );

        GlyphFontRegistry.register ( "icomoon", getClass ( ).getResourceAsStream ( "icomoon.ttf" ), 16 );

        final ToggleButton lMonitor = new ToggleButton ( languageManager.getValue ( "Monitoring" ), new Glyph( "FontAwesome", FontAwesome.Glyph.EYE ).color( Color.BLACK ) );
        final ToggleButton lSettings = new ToggleButton ( languageManager.getValue ( "Settings" ), new Glyph( "FontAwesome", FontAwesome.Glyph.GEAR ).color( Color.BLACK ) );

        final SegmentedButton lMonitorOrSettingsButtons = new SegmentedButton( lMonitor, lSettings );
        lMonitorOrSettingsButtons.setPadding ( new Insets ( 5, 0, 5, 0 ) );
        JavaFxUtils.restrictOneSelectedButtonAtATime ( lMonitorOrSettingsButtons.getButtons ( ) );

        lSettings.setOnAction (
            ( event ) ->
            {
                try
                {
                    monitorAppController.closeLineInSignalPane ( );

                    final ScrollPane scrollPane = new ScrollPane ( lSettingPanel );
                    scrollPane.setFitToWidth ( true );
                    scrollPane.setHbarPolicy ( ScrollPane.ScrollBarPolicy.AS_NEEDED );

                    lRoot.getChildren ( ).clear ( );
                    lRoot.getChildren ( ).add ( scrollPane );
                    lRoot.getChildren ( ).add ( lMonitorOrSettingsButtons );

                    lSettings.setDisable ( true );
                    lMonitor.setDisable ( false );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            }
        );

        lMonitor.setOnAction (
            ( event ) ->
            {
                try
                {
                    lRoot.getChildren ( ).clear ( );
                    lRoot.getChildren ( ).add ( monitorAppController.getPane ( ) );
                    lRoot.getChildren ( ).add ( lMonitorOrSettingsButtons );

                    settingsController.applySettings ( );
                    monitorAppController.init ( );

                    lMonitor.setDisable ( true );
                    lSettings.setDisable ( false );
                }

                catch ( Exception ex )
                {
                    logger.error ( ex.getMessage (), ex );
                }
            }
        );

        lMonitor.setSelected ( true );
        lMonitor.setDisable ( true );

        // splash
        {
            final StackPane stackPane = new StackPane ( );
            stackPane.getChildren ( ).add ( new ImageView ( new Image ( getClass ( ).getResourceAsStream ( "surgery2.jpg" ) ) ) );
            stackPane.setAlignment ( Pos.CENTER );

            lRoot.getChildren ( ).clear ( );
            lRoot.getChildren ( ).add ( stackPane );
        }

        monitorAppController.getSignalPane ().getDisplayStatusChangeProducer ().addListener (
            ( aInNewValue, aInOldValue ) ->
            {
                if ( DisplayStatus.init_done == aInNewValue )
                {
                    JavaFxUtils.executeInFxThread (
                        () ->
                        {
                            lRoot.getChildren ( ).clear ( );
                            lRoot.getChildren ( ).add ( monitorAppController.getPane ( ) );
                            lRoot.getChildren ( ).add ( lMonitorOrSettingsButtons );
                            return null;
                        }
                    );
                }
            }
        );

        return lRoot;
    }

    @Override
    public void stop () throws Exception
    {
        logger.info ( "app stop -- BEGIN" );

        super.stop ( );

        monitorAppController.close ( );
        settingsController.close ( );

        logger.info ( "app stop -- END" );
    }
}
