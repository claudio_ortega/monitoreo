/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;

public class TimeoutFilter
{
    private volatile long lastTimeConditionHeld;
    private volatile long windowInMSec;

    public static TimeoutFilter create ( )
    {
        return new TimeoutFilter ( );
    }

    private TimeoutFilter ( )
    {
        lastTimeConditionHeld = - 1;
        windowInMSec = - 1;
    }

    public TimeoutFilter setWindow ( long aInWindowInMSec )
    {
        windowInMSec = aInWindowInMSec;
        return this;
    }

    public TimeoutFilter mark ( boolean aInCondition )
    {
        if ( aInCondition )
        {
            if ( lastTimeConditionHeld == - 1 )
            {
                lastTimeConditionHeld = System.currentTimeMillis ( );
            }
        }

        else
        {
            lastTimeConditionHeld = - 1;
        }

        return this;
    }

    public boolean isConditionHeld ( )
    {
        final boolean ret;

        if ( lastTimeConditionHeld == - 1 )
        {
            ret = false;
        }

        else
        {
            Preconditions.checkState ( windowInMSec > 0 );
            ret = ( System.currentTimeMillis ( ) - lastTimeConditionHeld ) > windowInMSec;
        }

        return ret;
    }

    @Override
    public String toString ( )
    {
        return "TimeoutFilter{" +
            "lastTimeConditionHeld=" + lastTimeConditionHeld +
            ", windowInMSec=" + windowInMSec +
            '}';
    }
}
