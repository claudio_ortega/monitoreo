/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.javafx;

import com.google.common.base.Preconditions;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.poplar.monitor.imp.util.CollectionsUtil;
import com.poplar.monitor.imp.util.StringUtil;
import com.poplar.monitor.imp.util.SystemUtil;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.math3.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.Notifications;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.function.Predicate;

public class JavaFxUtils
{
    private static final Logger logger = LogManager.getLogger ( JavaFxUtils.class );

    private JavaFxUtils ( )
    {
    }

    public static IDrawEngine DEBUGGING_DRAW_ENGINE = gc -> {
        logger.trace( "draw -- BEGIN" );

        final double lCanvasWidth = gc.getCanvas ().getWidth ( );
        final double lCanvasHeight = gc.getCanvas ().getHeight ( );

        if ( lCanvasWidth < 1 || lCanvasHeight < 1 )
        {
            logger.warn ( "empty canvas, skip on drawing" );
        }

        else
        {
            gc.clearRect ( 0, 0, lCanvasWidth, lCanvasHeight );

            final double physicalMinX = 0.0;
            final double physicalMaxX = 1.0;

            final double physicalMinY = 0.0;
            final double physicalMaxY = 1.0;

            final double SCALE_X = lCanvasWidth / ( physicalMaxX - physicalMinX );
            final double SCALE_Y = lCanvasHeight / ( physicalMaxY - physicalMinY );

            final double lPhysicalDeltaX = lCanvasWidth / SCALE_X;
            final double lPhysicalDeltaY = lCanvasHeight / SCALE_Y;

            final Affine affine = new Affine ( );
            affine.prependTranslation ( 0.0, - ( physicalMaxY - physicalMinY ) );
            affine.prependScale ( SCALE_X, - SCALE_Y );

            gc.setLineWidth ( 3 );

            // down under the 'normal' physical convention
            final Point2D dwnLeft = affine.transform ( new Point2D ( 0, 0 ) );
            final Point2D dwnRight = affine.transform ( new Point2D ( lPhysicalDeltaX, 0 ) );
            final Point2D upLeft = affine.transform ( new Point2D ( 0, lPhysicalDeltaY ) );
            final Point2D upRight = affine.transform ( new Point2D ( lPhysicalDeltaX, lPhysicalDeltaY ) );
            final Point2D center = affine.transform ( new Point2D ( lPhysicalDeltaX, lPhysicalDeltaY ).midpoint ( new Point2D ( 0, 0 ) ) );

            final Point2D zero = affine.transform ( new Point2D ( 0, 0 ) );

            gc.setStroke ( Color.DARKRED );
            gc.strokeOval ( zero.getX ( ) - 25, zero.getY ( ) - 25, 50.0, 50.0 );

            gc.setStroke ( Color.RED );
            JavaFxUtils.strokeLine ( gc, dwnLeft, upRight, true );
            gc.setStroke ( Color.GREEN );
            JavaFxUtils.strokeLine ( gc, dwnRight, upLeft, true );
            gc.setStroke ( Color.BLUE );
            JavaFxUtils.drawRect ( gc, dwnLeft, upRight );

            gc.setFill ( Color.BLACK );
            gc.setFont ( Font.font ( Font.getDefault ( ).getFamily ( ), 15 ) );
            gc.setTextAlign ( TextAlignment.CENTER );
            gc.setTextBaseline ( VPos.CENTER );

            gc.fillText (
                "canvas width: " + lCanvasWidth + ", canvas height: " + lCanvasHeight,
                center.getX ( ),
                center.getY ( ) - 10 );
            gc.fillText (
                "phys width: " + lPhysicalDeltaX + ", phys height: " + lPhysicalDeltaY,
                center.getX ( ),
                center.getY ( ) + 10 );
            gc.fillText (
                "time: " + new Date ().getTime (),
                center.getX ( ),
                center.getY ( ) + 30 );
        }

        logger.trace( "draw -- END" );
    };

    public static double capValueInBetweenMinAndMax ( double aInWatch, double aInMin, double aInMax )
    {
        final double lResult;

        if ( aInWatch > aInMax  )
        {
            lResult = aInMax;
        }
        else if ( aInWatch < aInMin  )
        {
            lResult = aInMin;
        }
        else
        {
            lResult = aInWatch;
        }

        return lResult;
    }

    public static void strokeCenteredCircle ( GraphicsContext gc, Point2D p, double radius )
    {
        gc.fillOval ( p.getX () - radius, p.getY () - radius, 2*radius, 2*radius );
    }

    public static void strokeLine ( GraphicsContext gc, Point2D a, Point2D b, boolean aInCapOnBoundaries )
    {
        if ( aInCapOnBoundaries )
        {
            final double lMaxX = gc.getCanvas ().getWidth ( );
            final double lMaxY = gc.getCanvas ().getHeight ();

            gc.strokeLine (
                snapToGrid ( capValueInBetweenMinAndMax ( a.getX ( ), 0, lMaxX - 1 ) ),
                snapToGrid ( capValueInBetweenMinAndMax ( a.getY ( ), 0, lMaxY - 1 ) ),
                snapToGrid ( capValueInBetweenMinAndMax ( b.getX ( ), 0, lMaxX - 1 ) ),
                snapToGrid ( capValueInBetweenMinAndMax ( b.getY ( ), 0, lMaxY - 1 ) ) );
        }
        else
        {
            gc.strokeLine (
                snapToGrid ( a.getX ( ) ), snapToGrid ( a.getY ( ) ),
                snapToGrid ( b.getX ( ) ), snapToGrid ( b.getY ( ) ) );
        }
    }

    public static void resetAccumulatedPoints ( GraphicsContext gc )
    {
        gc.closePath();
        gc.beginPath ();
    }

    public static void accumulatePointForLine ( GraphicsContext gc, Point2D a )
    {
        gc.lineTo ( snapToGrid ( a.getX ( ) ), snapToGrid ( a.getY ( ) ) );
    }

    public static void moveToFirstPointForLine ( GraphicsContext gc, Point2D a )
    {
        gc.closePath ( );
        gc.beginPath ( );

        gc.moveTo ( snapToGrid ( a.getX ( ) ), snapToGrid ( a.getY ( ) ) );
    }

    private static double snapToGrid ( double in )
    {
        return Math.floor ( in ) + 0.5;
    }

    public static void drawRect ( GraphicsContext g, Point2D a, Point2D b )
    {
        g.strokeLine ( a.getX ( ), a.getY ( ), a.getX ( ), b.getY ( ) );
        g.strokeLine ( a.getX ( ), b.getY ( ), b.getX ( ), b.getY ( ) );
        g.strokeLine ( b.getX ( ), b.getY ( ), b.getX ( ), a.getY ( ) );
        g.strokeLine ( b.getX ( ), a.getY ( ), a.getX ( ), a.getY ( ) );
    }

    public static Point2D getLowerRightPoint ( GraphicsContext g )
    {
        return new Point2D ( g.getCanvas ().getWidth (), g.getCanvas ().getHeight () );
    }

    public static Point2D getLowerLeftPoint ( GraphicsContext g )
    {
        return new Point2D ( 0, g.getCanvas ().getHeight () );
    }

    public static Point2D getUpperRightPoint ( GraphicsContext g )
    {
        return new Point2D ( g.getCanvas ().getWidth (), 0 );
    }

    public static Point2D getUpperLeftPoint ( GraphicsContext g )
    {
        return Point2D.ZERO;
    }


    public static Point2D getCenterPoint ( GraphicsContext g )
    {
        return Point2D.ZERO.midpoint ( getLowerRightPoint ( g ) );
    }

    // why this behaves differently (and better than) primaryStage.centerOnScreen ();
    public static void centerOnScreen ( Stage aInStage, double aInFactor, boolean aInFixMinimumSize )
    {
        final Rectangle2D primScreenBounds = Screen.getPrimary ( ).getVisualBounds();

        final double newWidth = primScreenBounds.getWidth () * aInFactor;
        final double newHeight = primScreenBounds.getHeight () * aInFactor;

        aInStage.setWidth ( 0 );
        aInStage.setHeight ( 0 );

        aInStage.setX ( ( primScreenBounds.getWidth() - newWidth) / 2 );
        aInStage.setY ( ( primScreenBounds.getHeight ( ) - newHeight ) / 2 );

        aInStage.setWidth ( newWidth );
        aInStage.setHeight ( newHeight );

        if ( aInFixMinimumSize )
        {
            aInStage.setMinWidth ( newWidth );
            aInStage.setMinHeight ( newHeight );
        }
    }

    public static void generateFileFromCanvas( Canvas canvas, String format, File outFile ) throws Exception
    {
        final WritableImage wim = new WritableImage ( (int) canvas.getWidth (), (int) canvas.getHeight () );

        final CountDownLatch latch = new CountDownLatch ( 1 );

        JavaFxUtils.executeInFxThread ( ( ) -> {
            try
            {
                canvas.snapshot ( null, wim );
            }

            catch ( Exception ex )
            {
                logger.error ( ex.getMessage ( ), ex );
            }

            finally
            {
                latch.countDown ( );
            }

            return null;
        } );

        latch.await ();

        ImageIO.write (
            SwingFXUtils.fromFXImage (
                wim,
                null ),
            format,
            outFile );
    }

    public static void executeInFxThread ( Callable<Void> callable )
    {
        executeInFxThreadAfterDelay ( callable, 0 );
    }

    public static void executeInFxThreadAfterDelay (
        Callable<Void> callable,
        long aInInitialDelayInMSec )
    {
        if ( Platform.isFxApplicationThread ( ) && aInInitialDelayInMSec == 0 )
        {
            try
            {
                callable.call ( );
            }

            catch ( Exception ex )
            {
                logger.error ( ex.getMessage ( ), ex );
            }
        }

        else
        {
            new Thread (
                () ->
                {
                    try
                    {
                        if ( aInInitialDelayInMSec > 0 )
                        {
                            SystemUtil.sleepMsec ( aInInitialDelayInMSec );
                        }

                        Platform.runLater (
                            () ->
                            {
                                try
                                {
                                    callable.call ( );
                                }

                                catch ( Exception ex )
                                {
                                    logger.error ( ex.getMessage ( ), ex );
                                }
                            }
                        );
                    }

                    catch ( Exception ex )
                    {
                        logger.error ( ex.getMessage ( ), ex );
                    }
                }
            ).start ( );
        }
    }

    public static void executeOutsideFxThread ( Callable<Void> callable )
    {
        executeOutsideFxThreadWithDelay( callable, 0 );
    }

    public static void executeOutsideFxThreadWithDelay ( Callable<Void> callable, final long delayMSec )
    {
        new Thread ( ( ) -> {
            try
            {
                if ( delayMSec > 0 )
                {
                    SystemUtil.sleepMsec ( delayMSec );
                }

                callable.call ( );
            }

            catch ( Exception ex )
            {
                logger.error ( ex.getMessage ( ), ex );
            }
        } ).start ();
    }

    public static void restrictOneSelectedButtonAtATime ( final List<ToggleButton> aInButtons )
    {
        for ( ToggleButton nextButton : aInButtons )
        {
            nextButton.pressedProperty ( ).addListener (
                ( observable, oldValue, newValue ) ->
                {
                    if ( newValue )
                    {
                        for ( ToggleButton next : aInButtons )
                        {
                            next.setSelected ( false );
                        }
                    }
                }
            );
        }
    }

    public static void addStringContentValidator ( TextField aInTextField, Predicate<String> contentValidator )
    {
        aInTextField.textProperty ( ).addListener ( ( observableValue, oldValue, newValue ) -> aInTextField.setStyle (
            contentValidator.test ( newValue )
                ? "-fx-control-inner-background: white"
                : "-fx-control-inner-background: #fdf498"
        ) );
    }

    public static void addIntegerContentValidator ( TextField aInTextField, final Predicate<Integer> contentValidator )
    {
        aInTextField.textProperty ( ).addListener ( ( observableValue, oldValue, newValue ) -> {
            boolean test;
            try
            {
                test = contentValidator.test ( Integer.valueOf ( newValue ) );
            }
            catch ( Exception ex )
            {
                test = false;
            }

            aInTextField.setStyle (
                test
                    ? "-fx-control-inner-background: white"
                    : "-fx-control-inner-background: #fdf498"
            );
        } );
    }

    public static List<Point2D> getPointsInBetween ( Point2D a, Point2D b, int ticks )
    {
        final List<Point2D> lRet = new LinkedList<> (  );

        final double deltaX = ( a.getX () - b.getX () ) / ( ticks - 1 );
        final double deltaY = ( a.getY () - b.getY () ) / ( ticks - 1 );

        for ( int i=0; i<ticks; i ++ )
        {
            lRet.add ( new Point2D ( b.getX () + i * deltaX, b.getY () + i * deltaY ) );
        }

        return lRet;
    }

    public static void generatePdfFile ( ScreenInfo screenInfo, File aInPdfFilePath, File aInImageFilePath ) throws Exception
    {
        try ( final FileOutputStream fp = new FileOutputStream ( aInPdfFilePath ) )
        {
            final SimpleDateFormat upToMsSimpleDateFormat = new SimpleDateFormat ( "yyyy-MM-dd.HH:mm:ss.SSS" );

            final Document document = new Document ( );

            PdfWriter.getInstance ( document, fp );
            document.open ( );

            final ParagraphBuilder firstTitle = ParagraphBuilder
                .create ( )
                .setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.HELVETICA, 25 ) )
                .addChunk ( "\n" )
                .addChunk ( "\n" )
                .addChunk ( "\n" )
                .addChunk ( "Facial Nerve Monitor - Surgery Report\n" )
                .addChunk ( "\n" )
                .addChunk ( "\n" );

            document.add ( firstTitle.paragraph );

            final ParagraphBuilder versionTitle = ParagraphBuilder.create ( ).setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.HELVETICA, 16 ) );
            versionTitle
                .addChunk ( "\n" )
                .addChunk ( "\n" )
                .addChunk ( "Application Version\n" );

            document.add ( versionTitle.paragraph );

            final ParagraphBuilder pb1 = ParagraphBuilder
                .create ( )
                .setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.COURIER, 12 ) )
                .addChunk ( "\n" )
                .addChunk ( screenInfo.getApplicationConfig ( ).getGitVersion ( ) + "\n" );

            document.add ( pb1.paragraph );

            final ParagraphBuilder generalTitle = ParagraphBuilder.create ( ).setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.HELVETICA, 16 ) );
            generalTitle
                .addChunk ( "\n" )
                .addChunk ( "\n" )
                .addChunk ( "General Information\n" );

            document.add ( generalTitle.paragraph );

            final ParagraphBuilder pb2 = ParagraphBuilder.create ( ).setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.COURIER, 12 ) )
                .addChunk ( "\n" )
                .addChunk ( "Surgery starting time: " + upToMsSimpleDateFormat.format ( screenInfo.getAppStartTime ( ) ) + "\n" )
                .addChunk ( "Screen starting time: " + upToMsSimpleDateFormat.format ( screenInfo.getSignalStartingTime ( ) ) + "\n" )
                .addChunk ( "Screen session number: " + screenInfo.getScreenSavingSessionCounter ( ) + "\n" )
                .addChunk ( "Screen number: " + screenInfo.getScreenNumber ( ) + "\n" )
                .addChunk ( "Screen duration: " + String.format ( "%.3f", screenInfo.computeLengthInSeconds ( ) ) + " secs.\n" )
                .addChunk ( "Screen id: " + screenInfo.getId ( ) + "\n" )
                .addChunk ( "Patient's first name: " + screenInfo.getApplicationConfig ( ).getSurgeryInfo ( ).getPatientFirstName ( ) + "\n" )
                .addChunk ( "Patient's middle name: " + screenInfo.getApplicationConfig ( ).getSurgeryInfo ( ).getPatientMiddleName ( ) + "\n" )
                .addChunk ( "Patient's last name: " + screenInfo.getApplicationConfig ( ).getSurgeryInfo ( ).getPatientLastName ( ) + "\n" )
                .addChunk ( "Patient's age: " + screenInfo.getApplicationConfig ( ).getSurgeryInfo ( ).getPatientAge ( ) + "\n" );

            if ( screenInfo.getApplicationConfig ().getFilterBuilder ().getUseAdaptiveFiltering ( ) )
            {
                pb2
                    .addChunk ( "Line filter: on\n" )
                    .addChunk ( "Line filter center freq: " + screenInfo.getApplicationConfig ().getFilterBuilder ().getNotchFrequencyEnum ( ).getValue () + " Hz.\n" );
            }
            else
            {
                pb2
                    .addChunk ( "Line filter: off\n" );
            }

            pb2.addChunk ( "\n" );

            document.add ( pb2.paragraph );

            for ( int channelIndex=0; channelIndex<screenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).size(); channelIndex++ )
            {
                if ( screenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get( channelIndex).getVisible () )
                {
                    final ParagraphBuilder channelTitle = ParagraphBuilder.create ( ).setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.HELVETICA, 16 ) );
                    channelTitle
                        .addChunk ( "\n" )
                        .addChunk ( "\n" )
                        .addChunk ( "Channel " + ( channelIndex + 1 ) + "   " + screenInfo.getApplicationConfig ( ).getSignalPaneBuilder ( ).getSignalChannelBuilders ( ).get ( channelIndex ).getSignalName ( ) + "\n" );

                    document.add ( channelTitle.paragraph );

                    final StatsInfo channelStats = computeStats ( screenInfo, channelIndex );

                    final ParagraphBuilder channelStatsInfo = ParagraphBuilder.create ( ).setFont ( new com.itextpdf.text.Font ( com.itextpdf.text.Font.FontFamily.COURIER, 12 ) );

                    channelStatsInfo
                        .addChunk ( "\n" )
                        .addChunk ( String.format ( "Signal max: %.3f mVolt\n", channelStats.max ) )
                        .addChunk ( String.format ( "Signal min: %.3f mVolt\n", channelStats.min ) )
                        .addChunk ( String.format ( "Signal diff max-min: %.3f mVolt\n", channelStats.max - channelStats.min ) )
                        .addChunk ( String.format ( "Signal DC: %.3f mVolt\n", channelStats.dc ) )
                        .addChunk ( String.format ( "Signal RMS DC: %.3f mVolt rms\n", channelStats.rmsDC ) )
                        .addChunk ( String.format ( "Signal RMS AC: %.3f mVolt rms\n", channelStats.rmsAC ) );

                    document.add ( channelStatsInfo.paragraph );
                }
            }

            final Image image = Image.getInstance ( aInImageFilePath.getAbsolutePath ( ) );
            image.setAlignment ( Image.RIGHT | Image.TEXTWRAP );

            document.add ( image );

            document.close ( );
        }
    }

    public static void showNotification ( String aInString, NotificationLevel aInLevel )
    {
        showNotification( aInString, aInLevel, false );
    }

    public static void showNotification ( String aInString, NotificationLevel aInLevel, boolean aInHeldIndefinite )
    {
        executeInFxThread ( ( ) -> {
            final String lTextMessage = LanguageManager.getSingleton ( ).containsKey ( aInString ) ? LanguageManager.getSingleton ( ).getValue ( aInString ) : aInString;

            final Notifications lNotifications = Notifications
                .create ( )
                .darkStyle ()
                .text ( lTextMessage.trim ( ) )
                .position ( Pos.BASELINE_RIGHT );

            final String lTimeStamp = StringUtil.chopRightPortionOfString ( " - " + new SimpleDateFormat ( "HH:mm:ss.SSS" ).format ( new Date ( ) ), 2 );
            final Duration lDuration = aInHeldIndefinite ? Duration.INDEFINITE : Duration.seconds ( 5 );

            if ( aInLevel == NotificationLevel.ERROR )
            {
                lNotifications.title ( LanguageManager.getSingleton ( ).getValue ( "error" ) + lTimeStamp ).hideAfter ( lDuration ).showError ( );
            }

            else if ( aInLevel == NotificationLevel.WARN )
            {
                lNotifications.title ( LanguageManager.getSingleton ( ).getValue ( "warning" ) + lTimeStamp ).hideAfter ( lDuration ).showWarning ( );
            }

            else if ( aInLevel == NotificationLevel.INFO )
            {
                lNotifications.title ( LanguageManager.getSingleton ( ).getValue ( "info" ) + lTimeStamp ).hideAfter ( lDuration ).showInformation ( );
            }

            return null;
        } );
    }

    public static boolean lesserThanOrClose( double ref, double test )
    {
        return test < ref || Math.abs( ref - test ) < 1e-6;
    }

    enum NotificationLevel { WARN, ERROR, INFO }

    private static class ParagraphBuilder
    {
        final Paragraph paragraph;
        com.itextpdf.text.Font font;

        private ParagraphBuilder()
        {
            paragraph = new Paragraph (  );
        }

        ParagraphBuilder setFont( com.itextpdf.text.Font aInFont )
        {
            font = aInFont;
            return this;
        }

        ParagraphBuilder addChunk( String string )
        {
            paragraph.add( new Chunk ( string, font ) );
            return this;
        }

        static ParagraphBuilder create( )
        {
            return new ParagraphBuilder ();
        }
    }

    private static class StatsInfo
    {
        double min;
        double max;
        double dc;
        double rmsDC;
        double rmsAC;

        static StatsInfo computeOnArray ( float[] data, int len )
        {
            StatsInfo statsInfo = new StatsInfo ();

            double sum = 0;
            double pwr = 0;
            for ( int i=0; i<len; i++ )
            {
                if ( i == 0 )
                {
                    statsInfo.min = data[i];
                    statsInfo.max = data[i];
                }
                else
                {
                    if ( data[i] > statsInfo.max )
                    {
                        statsInfo.max = data[i];
                    }

                    if ( data[i] < statsInfo.min )
                    {
                        statsInfo.min = data[i];
                    }
                }

                sum += data[i];
                pwr += ( data[i] * data[i] );
            }

            statsInfo.dc = sum / len;

            double acPwr = 0;
            for ( int i=0; i<len; i++ )
            {
                double ac = data[i] - statsInfo.dc;
                acPwr += ( ac * ac );
            }

            statsInfo.rmsDC = Math.sqrt ( pwr / len );
            statsInfo.rmsAC = Math.sqrt ( acPwr / len );

            return statsInfo;
        }
    }

    private static StatsInfo computeStats ( ScreenInfo screenInfo, int channelIndex )
    {
        return StatsInfo.computeOnArray (
            iExtractSignalFormOneChannel ( screenInfo, channelIndex ),
            screenInfo.getSignalLengthInSamples ( ) );
    }

    private static float[] iExtractSignalFormOneChannel( ScreenInfo screenInfo, int channelIndex )
    {
        final float[] ret = new float[screenInfo.getSignalLengthInSamples ( )];

        // we take the center of the range off the samples, yes.. we do..
        final double physMax = screenInfo.getSignalSourceReadOnlyInfo ().getPhysicalSpaceUpperBound ( );
        final double physMin = screenInfo.getSignalSourceReadOnlyInfo ().getPhysicalSpaceLowerBound ( );
        final double physSemiRange = ( physMax - physMin ) / 2.0;

        final double physMaxScaled = physSemiRange / (float) screenInfo.getApplicationConfig ().getSignalSourceBuilder ().getPhysicalGain ();
        final double physMinScaled = - physSemiRange / (float) screenInfo.getApplicationConfig ().getSignalSourceBuilder ().getPhysicalGain ();

        final LinearTx linearTx = new LinearTx (
            screenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceLowerBound (),
            physMinScaled,
            screenInfo.getSignalSourceReadOnlyInfo ().getSampleSpaceUpperBound (),
            physMaxScaled
        );

        for ( int i=0; i<screenInfo.getSignalLengthInSamples ( ); i++ )
        {
            ret[i] = (float) linearTx.tx ( screenInfo.getSignal ()[i][channelIndex] );
        }

        return ret;
    }

    public static class LinearTx
    {
        // a is the transformation output coordinate
        // b is the transformation input coordinate
        // x0,x1 are the two points

        private final double x0b;
        private final double x0a;
        private final double rate;

        public LinearTx(
            double aInx0b,
            double aInx0a,
            double aInx1b,
            double aInx1a
        )
        {
            x0b = aInx0b;
            x0a = aInx0a;

            rate = ( aInx0a - aInx1a ) / ( aInx0b - aInx1b );
        }

        public double tx ( double in )
        {
            return x0a + ( in - x0b ) * rate;
        }
    }

    public static <T> List<Pair<T,T>> swapPairsInPlace ( List<Pair<T,T>> in )
    {
        final List<Pair<T,T>> swappedList = new LinkedList<> ( );

        for ( Pair<T,T> next : in )
        {
            swappedList.add ( new Pair<> ( next.getSecond ( ), next.getFirst ( ) ) );
        }

        return swappedList;
    }

    private static void assertOnListOfDoublesIsMonotonicUp ( List<Double> in )
    {
        Preconditions.checkArgument ( in.size ( ) > 1 );

        double next = in.get(0);

        for ( int i=1; i<in.size (); i++ )
        {
            Preconditions.checkArgument ( in.get ( i ) > next );
            next = in.get( i );
        }
    }

    public static void assertOnListOfPairsMonotonicUp ( List<Pair<Double, Double>> in )
    {
        Preconditions.checkArgument ( in.size ( ) > 1 );

        final List<Double> first = CollectionsUtil.transformList (
            in, aInS -> aInS.getFirst () );

        assertOnListOfDoublesIsMonotonicUp ( first );

        final List<Double> second = CollectionsUtil.transformList (
            in, aInS -> aInS.getSecond () );

        assertOnListOfDoublesIsMonotonicUp ( second );
    }
}


