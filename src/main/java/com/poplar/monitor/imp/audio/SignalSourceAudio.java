/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.audio;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.MultiChannelVessel;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.common.AbstractMultiChannelSignalSource;
import com.poplar.monitor.imp.common.ProcessThreadFactory;
import com.poplar.monitor.imp.util.CollectionsUtil;
import com.poplar.monitor.imp.util.StringUtil;
import com.poplar.monitor.imp.util.SystemUtil;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class SignalSourceAudio extends AbstractMultiChannelSignalSource
{
    private final static Logger logger = LogManager.getLogger ( SignalSourceAudio.class );

    private static final int SAMPLE_SIZE_IN_BYTES = 2;
    private static final int FRACTIONAL_BUFFER_RATIO = 16;

    private static final int MAX_16bits = 256*256;
    private static final int MAX_POSITIVE_16bits = (256*256)/2 - 1;

    private final byte[] buffer;
    private final ThreadPoolExecutor serialExecutor;
    private final AtomicBoolean keepSerialExecutorProcessing;

    private volatile TargetDataLine line;
    private Mixer.Info mixerInfo;
    private final Object lineGuard;
    private long dropIncidentsCount;

    private static final long TWO_POWER_15 = 0x8000;

    @Override
    public long getSampleSpaceUpperBound ()
    {
        return TWO_POWER_15;
    }

    @Override
    public long getSampleSpaceLowerBound ()
    {
        return -TWO_POWER_15;
    }

    private static byte[] iGetBuffer( SignalSourceBuilder aInSignalSourceBuilder )
    {
        final int lSize = iGetMaxDivisible (
            65535,
            FRACTIONAL_BUFFER_RATIO * SAMPLE_SIZE_IN_BYTES * aInSignalSourceBuilder.getNChannels ( ) );

        logger.info( "allocating audio buffer, size: " + lSize );

        return new byte[lSize];
    }

    private static int iGetBufferSizeForDriver( SignalSourceBuilder aInSignalSourceBuilder )
    {
        return iGetMaxDivisible (
            65535,
            SAMPLE_SIZE_IN_BYTES * aInSignalSourceBuilder.getNChannels ( ) );
    }

    public static SignalSourceAudio create( SignalSourceBuilder aInSignalSourceBuilder )
    {
        return new SignalSourceAudio ( aInSignalSourceBuilder );
    }

    private SignalSourceAudio (
        SignalSourceBuilder aInSignalSourceBuilder )
    {
        super ( aInSignalSourceBuilder );

        dropIncidentsCount = 0;

        buffer = iGetBuffer( aInSignalSourceBuilder );

        lineGuard = new Object();

        mixerInfo = null;

        line = null;

        serialExecutor = new ThreadPoolExecutor (
            1,
            1,
            0,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<> ( ),
            new ProcessThreadFactory ( getClass ( ) ) );

        keepSerialExecutorProcessing = new AtomicBoolean ( true );

        final MultiChannelVessel lSignalVessel = MultiChannelVessel.create (
            signalSourceBuilder.getNChannels ( ),
            bufferCapacityInSamplesPerChannel
        );

        final int lSampleSizeAllChannelsInBytes = SAMPLE_SIZE_IN_BYTES * signalSourceBuilder.getNChannels ( );

        serialExecutor.submit (
            ( ) ->
            {
                logger.debug ( "serialExecutor.run() -- BEGIN" );

                while ( keepSerialExecutorProcessing.get ( ) )
                {
                    try
                    {
                        if ( ! started.get ( ) )
                        {
                            logger.debug ( "not started, skipping" );
                            SystemUtil.sleepMsec ( 500 );
                        }

                        else
                        {
                            if ( line == null )
                            {
                                logger.error ( "line is not initialized" );
                                SystemUtil.sleepMsec ( 10_000 );
                            }

                            else
                            {
                                final int lNumberOfReadBytes;

                                final int lShouldReadWithoutBlocking = Math.min ( buffer.length / FRACTIONAL_BUFFER_RATIO, line.available () );

                                if ( lShouldReadWithoutBlocking == 0 )
                                {
                                    lNumberOfReadBytes = 0;
                                }

                                else
                                {
                                    lNumberOfReadBytes = line.read (
                                        buffer,
                                        0,
                                        lShouldReadWithoutBlocking );
                                }

                                final int lNumberOfReadFrames = lNumberOfReadBytes / lSampleSizeAllChannelsInBytes;
                                lSignalVessel.setNumberOfSamples ( lNumberOfReadFrames );
                                lSignalVessel.setLastSampleTime ( System.currentTimeMillis () );

                                if ( lNumberOfReadBytes == 0 )
                                {
                                    logger.trace ( "no bytes are coming out from the driver" );
                                }

                                else
                                {
                                    for ( int lFrameCount = 0; lFrameCount < lNumberOfReadFrames; lFrameCount++ )
                                    {
                                        final int lFistByteInFrameIndex = lFrameCount * lSampleSizeAllChannelsInBytes;

                                        lSignalVessel.getChannelVessel ( 0 ).setSample (
                                            lFrameCount,
                                            ( float ) iGetSampleFromBytes (
                                                buffer[lFistByteInFrameIndex],
                                                buffer[lFistByteInFrameIndex + 1]
                                            )
                                        );

                                        lSignalVessel.getChannelVessel ( 1 ).setSample (
                                            lFrameCount,
                                            ( float ) iGetSampleFromBytes (
                                                buffer[lFistByteInFrameIndex + 2],
                                                buffer[lFistByteInFrameIndex + 3]
                                            )
                                        );
                                    }

                                    filterVessels( lSignalVessel );

                                    synchronized ( vesselsQueue )
                                    {
                                        if ( vesselsQueue.isFull ( ) )
                                        {
                                            final int lDropped = lSignalVessel.getNumberOfSamples ();
                                            final long lAcumDropped = droppedSamples.addAndGet ( lDropped );

                                            if ( dropIncidentsCount++ % 100 == 0 )
                                            {
                                                logger.info (
                                                    "samples dropped now/total/dropIncidentCount: " + lDropped + "/" + lAcumDropped + "/" + dropIncidentsCount +
                                                    ", queue length now/limit:" + vesselsQueue.size ( ) + "/" + queueLengthLimit );
                                            }
                                        }

                                        else
                                        {
                                            signalVesselSlotFiller.setVessels ( lSignalVessel );
                                            vesselsQueue.put ( signalVesselSlotFiller );
                                        }
                                    }
                                }
                            }
                        }
                    }

                    catch ( Throwable ex )
                    {
                        logger.error (
                            ex.getMessage ( ),
                            ex );
                    }
                }

                logger.debug ( "serialExecutor.run() -- END" );
            }
        );
    }

    public SignalSourceAudio start ( )
        throws Exception
    {
        Preconditions.checkState ( ! Platform.isFxApplicationThread ( ) );
        iInit ( );
        return this;
    }

    @Override
    public String getDescription ( )
    {
        return iGetMixerLoggingLineWithHash( mixerInfo, true );
    }

    private static String iGetMixerLoggingLine ( Mixer.Info aInInfo )
    {
        return aInInfo.getDescription ( ) +
            ", " + aInInfo.getName ( ) +
            ", " + aInInfo.getVendor ( ) +
            ", " + aInInfo.getVersion ( ) +
            ", " + aInInfo.toString ( );
    }

    private static String iGetMixerLoggingLineWithHash ( Mixer.Info aInInfo, boolean aInGetShortDescription )
    {
        String ret;

        try
        {
            final String lMixerDescription = ( aInInfo == null ) ? "undetermined" : iGetMixerLoggingLine( aInInfo );

            final MessageDigest digestAlgorithm = MessageDigest.getInstance ( "SHA1" );
            digestAlgorithm.reset();
            final byte[] lNextAsBytes = lMixerDescription.getBytes();
            digestAlgorithm.update( lNextAsBytes, 0, lNextAsBytes.length );
            final String lStringDigest = StringUtil.byteArrayToString ( digestAlgorithm.digest ( ), "", true );

            ret = String.format (
                "%s:%s - %s",
                lStringDigest.substring ( 0, 4 ),
                lStringDigest.substring ( 4, 8 ),
                aInGetShortDescription ? iGetShortDescriptionFromLongOne ( lMixerDescription, "undetermined" ) : lMixerDescription );
        }

        catch ( Exception ex )
        {
            ret = "undetermined";
            logger.error ( ex.getMessage (), ex );
        }

        return ret;
    }

    private static String iGetShortDescriptionFromLongOne ( String aInLongDescription, String aInUndetermined )
    {
        final List<String> lTmp = StringUtil.splitOverString ( aInLongDescription, "," );
        return lTmp.size() > 1 ? lTmp.get ( 1 ) : aInUndetermined;
    }

    private static void iLogMixerList ( String aInTag, List<Mixer.Info> aInMixerInfoList )
    {
        logger.info( aInTag + " -- (" + aInMixerInfoList.size() + " elements) -- list - BEGIN" );

        aInMixerInfoList.sort ( Comparator.comparing ( SignalSourceAudio::iGetMixerLoggingLine ) );

        for ( Mixer.Info lMixerInfo : aInMixerInfoList )
        {
            logger.info ( "    " + iGetMixerLoggingLineWithHash ( lMixerInfo, false ) );
        }

        logger.info( aInTag + " -- (" + aInMixerInfoList.size() + " elements) -- list - END" );
    }

    public static List<String> getAllCompatibleMixersInSystem (
        SignalSourceBuilder aInSignalSourceBuilder,
        final boolean aInGetShortDescription )
            throws Exception
    {
        return CollectionsUtil.transformList (
            iGetCompatibleMixers (
                false,   // do not use the regexp in the config, just give me all possible
                aInSignalSourceBuilder ), aInS -> iGetMixerLoggingLineWithHash ( aInS, aInGetShortDescription ) );
    }

    private static List<Mixer.Info> iGetCompatibleMixers (
        boolean aInFilterOnRegexp,
        SignalSourceBuilder aInSignalSourceBuilder )
            throws Exception
    {
        final List<Mixer.Info> lAllMixers = CollectionsUtil.createList ( AudioSystem.getMixerInfo ( ) );

        iLogMixerList (
            "all audio mixers in the system, unfiltered",
            lAllMixers );

        final List<Mixer.Info> lLineCompatibleMixers;

        logger.info ( "aInFilterOnRegexp: " + aInFilterOnRegexp );

        if ( aInFilterOnRegexp )
        {
            final List<Mixer.Info> lRegexpCompatibleMixers = iFilterRegexpCompatibleMixers (
                lAllMixers,
                aInSignalSourceBuilder.getAudioMixerDescriptionRegexp ( ) );

            iLogMixerList (
                "audio mixers in the system, filtered on regexp: [" + aInSignalSourceBuilder.getAudioMixerDescriptionRegexp () + "]",
                lRegexpCompatibleMixers );

            lLineCompatibleMixers = iFilterLineCompatibleMixers (
                lRegexpCompatibleMixers,
                aInSignalSourceBuilder );

            iLogMixerList (
                "audio mixers in the system, both functional and filtered on regexp: [" + aInSignalSourceBuilder.getAudioMixerDescriptionRegexp () + "]: ",
                lLineCompatibleMixers );
        }

        else
        {
            lLineCompatibleMixers = iFilterLineCompatibleMixers (
                lAllMixers,
                aInSignalSourceBuilder );

            iLogMixerList (
                "audio mixers in the system, functional and unfiltered: ",
                lLineCompatibleMixers );
        }

        return lLineCompatibleMixers;
    }

    private void iInit ( )
        throws Exception
    {
        Preconditions.checkState ( line == null );

        final List<Mixer.Info> lCompatibleMixers = iGetCompatibleMixers(
            true,     // filter on the driver regexp that is inside signalSourceBuilder
            signalSourceBuilder );

        synchronized ( lineGuard )
        {
            if ( lCompatibleMixers.size ( ) == 0 )
            {
                mixerInfo = null;

                line = null;

                logger.error ( "the application will not work under this configuration, as there is no compatible mixers in the system" );
            }

            else
            {
                mixerInfo = lCompatibleMixers.get ( 0 );

                if ( lCompatibleMixers.size ( ) > 1 )
                {
                    logger.warn ( "the application has more than one applicable device, so it will use the first available (below)" );
                }

                logger.info ( "the system will now attempt to use this compatible mixer: [" + iGetMixerLoggingLineWithHash ( mixerInfo, true ) + "]" );

                final CreationResult lCr = iAttemptLineCreationFromMixerInfo (
                    signalSourceBuilder,
                    mixerInfo,
                    true );

                if ( ! lCr.success )
                {
                    line = null;
                    logger.error ( "the attempt to open the line was unsuccessful, error msg: " + lCr.errorMsg );
                }

                else
                {
                    logger.info ( "the attempt to open the line was successful" );

                    lCr.line.addLineListener ( event -> logger.info( "line event: " + event ) );

                    line = lCr.line;
                }
            }
        }

        // line might be still null in here... that indicates that line is useless..
        if ( line == null )
        {
            logger.error( "the attempt to open the line was unsuccessful, mixer info: " + iGetMixerLoggingLineWithHash ( mixerInfo, true ) );
        }
    }

    private static List<Mixer.Info> iFilterLineCompatibleMixers (
        List<Mixer.Info> aInMixerList,
        SignalSourceBuilder aInSignalSourceBuilder )
    {
        return CollectionsUtil.filterList (
            aInMixerList,
            ( aInTUnderTest ) ->
            {
                final CreationResult lCr = iAttemptLineCreationFromMixerInfo (
                    aInSignalSourceBuilder,
                    aInTUnderTest,
                    false );

                if ( lCr.line != null )
                {
                    lCr.line.close();
                }

                if ( lCr.success )
                {
                    logger.info ( "initialization succeeded, mixer info: " + iGetMixerLoggingLineWithHash ( aInTUnderTest, true ) );
                }

                else
                {
                    logger.info ( "initialization failed, mixer info: " + iGetMixerLoggingLineWithHash ( aInTUnderTest, true ) );
                }

                return lCr.success;
            }
        );
    }

    private static List<Mixer.Info> iFilterRegexpCompatibleMixers (
        List<Mixer.Info> aInMixerList,
        String aInDriverNameRegexp
    )
    {
        return CollectionsUtil.filterList (
            aInMixerList, aInTUnderTest -> iCheckMatchOnRegexp (
                aInDriverNameRegexp,
                iGetMixerLoggingLineWithHash ( aInTUnderTest, false ) ) );
    }

    private static boolean iCheckMatchOnRegexp ( String aInRegexp, String aInTarget )
    {
        String lTmpRegexp = aInRegexp.trim ( );
        final boolean aInNegateRegexp = lTmpRegexp.startsWith ( "!" );

        if ( aInNegateRegexp )
        {
            lTmpRegexp = lTmpRegexp.substring ( 1 );
        }

        boolean lRet;

        if ( aInRegexp .contains ( ".*" ) )
        {
            lRet = aInNegateRegexp != aInTarget.matches ( lTmpRegexp );
        }
        else
        {
            lRet = aInRegexp.equals ( aInTarget );
        }

        return lRet;
    }

    private static class CreationResult
    {
        private final boolean success;
        private final String errorMsg;
        private final TargetDataLine line;

        private CreationResult ( boolean aInSuccess, TargetDataLine aInLine, String aInErrorMsg )
        {
            success = aInSuccess;
            line = aInLine;
            errorMsg = aInErrorMsg;
        }
    }

    private static CreationResult iAttemptLineCreationFromMixerInfo (
        SignalSourceBuilder aInSignalSourceBuilder,
        Mixer.Info aInMixerInfo,
        boolean aInLogAnErrorIfAttemptFails )
    {
        logger.info ( "iAttemptLineCreationFromMixerInfo -- BEGIN");

        TargetDataLine lLine = null;
        boolean success;
        String errMsg;

        try
        {
            final AudioFormat lAudioFormat = new AudioFormat (
                AudioFormat.Encoding.PCM_SIGNED,
                aInSignalSourceBuilder.getSamplingFrequencyEnum ( ).getValue (),
                SAMPLE_SIZE_IN_BYTES * 8,
                aInSignalSourceBuilder.getNChannels ( ),
                SAMPLE_SIZE_IN_BYTES * aInSignalSourceBuilder.getNChannels ( ),
                aInSignalSourceBuilder.getSamplingFrequencyEnum ( ).getValue (),
                true );

            final DataLine.Info lLineInfo = new DataLine.Info (
                TargetDataLine.class,
                lAudioFormat );

            if ( ! AudioSystem.isLineSupported ( lLineInfo ) )
            {
                errMsg = "format is not supported: " + lAudioFormat;
                success = false;
            }

            else
            {
                final Mixer lMixer = AudioSystem.getMixer ( aInMixerInfo );

                logger.info( "about to invoke getLine() with aInMixerInfo: [" + iGetMixerLoggingLineWithHash( aInMixerInfo, true ) + "]" );
                logger.info( "about to invoke getLine() with lLineInfo: [" + lLineInfo.toString (  ) + "]" );
                logger.info( "existing as opened lines: " + Arrays.asList ( lMixer.getTargetLines () ) );

                lLine = ( TargetDataLine ) lMixer.getLine ( lLineInfo );

                lLine.open( lAudioFormat, iGetBufferSizeForDriver ( aInSignalSourceBuilder ) );

                lLine.start ( );

                Thread.sleep ( 500 );

                if ( lLine.available ( ) == 0 )
                {
                    success = false;
                    errMsg = "the line produced no samples";
                }
                else
                {
                    success = true;
                    errMsg = "line looks ok";
                }
            }
        }

        catch ( Exception ex )
        {
            success = false;

            errMsg = "exception received while initialize driver: " + ex.getMessage ();

            if ( aInLogAnErrorIfAttemptFails )
            {
                logger.error ( errMsg, ex );
            }
            else
            {
                logger.info ( errMsg );
            }
        }

        finally
        {
            if ( lLine != null && lLine.isOpen () )
            {
                lLine.stop();
                drainLine ( lLine );
            }
        }

        logger.info ( "iAttemptLineCreationFromMixerInfo -- END");

        return new CreationResult ( success, lLine, errMsg );
    }

    /**
     *
     * @param aInValue
     * @param aInModulus
     * @return returns maximum integer less than or equal to aInValue, but divisible by aInModulus
     */
    private static int iGetMaxDivisible ( int aInValue, int aInModulus )
    {
        return ( aInValue / aInModulus ) * aInModulus;
    }

    @Override
    public boolean isAvailable ( )
    {
        synchronized ( lineGuard )
        {
            return line != null;
        }
    }

    @Override
    public void close ( )
        throws IOException
    {
        logger.info ( "close() -- BEGIN" );

        super.close ( );
        keepSerialExecutorProcessing.set ( false );

        synchronized ( lineGuard )
        {
            if ( line != null )
            {
                line.stop();
                drainLine ( line );
                line.close ( );
                logger.info ( "line is closed now." );
            }
        }

        serialExecutor.shutdownNow ( );

        try
        {
            serialExecutor.awaitTermination (
                3000,
                TimeUnit.MILLISECONDS );
        }

        catch ( Exception ex )
        {
            logger.error (
                ex.getMessage ( ), ex );
        }

        logger.info ( "close() -- END" );
    }

    private static int iGetModule256 ( int x )
    {
        return ( x < 0 ) ? x + 256 : x;
    }

    private static int iConvertToSignedNumber ( int x )
    {
        return x > MAX_POSITIVE_16bits ? x - MAX_16bits : x;
    }

    public static int testGetSampleFromBytes ( byte msb, byte lsb )
    {
        return iGetSampleFromBytes ( msb, lsb );
    }

    private static int iGetSampleFromBytes ( byte msb, byte lsb )
    {
        final int lLsb = iGetModule256 ( ( int ) lsb );
        final int lMsb = iGetModule256 ( ( int ) msb );
        return iConvertToSignedNumber ( 256 * lMsb + lLsb );
    }

    @Override
    public void startSampling ( )
    {
        logger.info ( "startSampling() -- BEGIN" );

        super.startSampling ( );

        synchronized ( lineGuard )
        {
            if ( line != null && ! line.isActive () && ! line.isRunning () )
            {
                logger.info ( "line.isActive(): " + line.isActive () );
                logger.info ( "line.isRunning(): " + line.isRunning () );

                line.start ( );
            }
        }

        logger.info ( "startSampling() -- END" );
    }

    @Override
    public void stopSampling ( )
    {
        logger.info ( "stopSampling() -- BEGIN" );

        synchronized ( lineGuard )
        {
            if ( line != null && line.isActive () && line.isRunning () )
            {
                logger.info ( "line.isActive(): " + line.isActive () );
                logger.info ( "line.isRunning(): " + line.isRunning () );

                line.stop();
                drainLine ( line );
            }
        }

        super.stopSampling ( );

        logger.info ( "stopSampling() -- END" );
    }

    private static void drainLine( TargetDataLine line )
    {
        int count;

        final byte[] buffer = new byte[10000];

        do
        {
            count = line.read (
                buffer,
                0,
                buffer.length );
        }

        while ( count > 0 );
    }

    @Override
    public String getPhysicalSpaceUnit ( )
    {
        return "mV";
    }

    @Override
    public String toString ( )
    {
        return "SignalSourceAudio{" +
            "mixerInfo=" + iGetMixerLoggingLineWithHash( mixerInfo, false )+
            ", line=" + line +
            '}';
    }

    @Override
    public float getPhysicalSpaceUpperBound ()
    {
        return 0.8f * 3.3f * 1000f;  // 80% of 3.3 volts in mV
    }

    @Override
    public float getPhysicalSpaceLowerBound ()
    {
        return 0.2f * 3.3f * 1000f;  // 20% of 3.3 volts in mV
    }
}
