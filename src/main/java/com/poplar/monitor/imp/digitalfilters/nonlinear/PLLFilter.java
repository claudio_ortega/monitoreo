/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.nonlinear;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalFilters;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import org.apache.commons.math3.complex.Complex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PLLFilter implements ICausalDigitalFilter
{
    private final Logger logger = LogManager.getLogger ( getClass() );

    private ICausalDigitalFilter lowPass;
    private ICausalDigitalFilter integrator;
    private double fsInHz;
    private long sampleCount;
    private double integratorOut;

    public static PLLFilter create ()
    {
        return new PLLFilter ();
    }

    private PLLFilter ( )
    {
    }

    public PLLFilter setFsInHz ( double value )
    {
        fsInHz = value;
        return this;
    }

    @Override
    public void reset ( )
    {
        integratorOut = 0;
        sampleCount = 0;
        lowPass.reset ( );
        integrator.reset ( );
    }

    public PLLFilter start ()
    {
        {
            final double tauInSecs = 5.0;
            final double z0 = 1 - ( 1.0 / ( fsInHz * tauInSecs ) );

            final PolynomialFraction lowPassPolynomialFraction = PolynomialFraction
                .create ( )
                .addPole ( z0 );

            final double lowPassDCGain = lowPassPolynomialFraction.evalaluateOnASinglePoint ( Complex.ONE ).abs ( );

            lowPass = CausalFilters.createFromPolynomialFraction ( 1.0 / lowPassDCGain, lowPassPolynomialFraction );
        }

        {
            final PolynomialFraction integratorPolynomialFraction = PolynomialFraction
                .create ( )
                .addPole ( Complex.ONE );

            integrator = CausalFilters.createFromPolynomialFraction ( 1.0 / fsInHz, integratorPolynomialFraction );
        }

        reset();

        return this;
    }

    @Override
    public double filter ( double in )
    {
        sampleCount++;

        if ( sampleCount % (long) fsInHz == 0 )
        {
            logger.info( "integratorOut: " + integratorOut );
        }

        final double vcoOut = iGetVCOOut ( integratorOut );

        final double phaseDetectorOut = vcoOut * in;

        final double lowPassOut = lowPass.filter ( phaseDetectorOut );

        integratorOut = integrator.filter ( lowPassOut );

        return 1000 * vcoOut;
    }

    private double iGetVCOOut( double in )
    {
        final double centerFreq = 50;
        return Math.cos ( 2.0 * Math.PI * ( sampleCount / fsInHz ) * ( centerFreq + in ) );
    }

    @Override
    public String toString ( )
    {
        return "PLLFilter{" +
            "fsInHz=" + fsInHz +
            ", logger=" + logger +
            ", lowPass=" + lowPass +
            ", sampleCount=" + sampleCount +
            ", integratorOut=" + integratorOut +
            '}';
    }
}
