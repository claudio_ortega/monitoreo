/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.google.common.base.Preconditions;
import java.util.Arrays;

public class FixedDelayFilter
{
    private final double memory[];

    // the place for the next sample
    private int indexForNextSample;

    public static FixedDelayFilter create ( int length )
    {
        return new FixedDelayFilter ( length );
    }

    private FixedDelayFilter ( int length )
    {
        Preconditions.checkArgument ( length >= 0 );

        memory = new double[ length+1 ];

        reset();
    }

    public void reset ()
    {
        indexForNextSample = 0;
        Arrays.fill ( memory, 0 );
    }

    public double put ( double input )
    {
        memory [ indexForNextSample ] = input;

        indexForNextSample = ( indexForNextSample + 1 ) % memory.length;

        return memory [ indexForNextSample ];
    }

    @Override
    public String toString ( )
    {
        return "FixedDelayFilter{" +
            "memory.length=" + memory.length +
            ", indexForNextSample=" + indexForNextSample +
            '}';
    }
}
