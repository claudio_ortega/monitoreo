/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;

public class ZerosOnlyCausalFilter implements ICausalDigitalFilter
{
    private final CausalConvolutionFilter filter;

    /**
     *        example #1 : for an delta or identity filter: new double[] { 1.0 }
     *        example #2 : for a filter with z transform: f(z) = (1 - zˆ-1) : new double[] { 1.0, -1.0 }
     *        example #3 : for a filter with z transform: f(z) = (1 - a * zˆ-1) : new double[] { 1, -a }
     *        example #4 : for a filter with z transform: f(z) = (1 + b * zˆ-1 + c * zˆ-2 ) : new double[] { 1, b, c }
     *
     * @param aInFilterCoefs  coefs when the filter is expresses as H(z) = ( 1 + a1 * z ^ -1 + a2 * z ^ -2 ... )
     *                            aInFilterCoefs = { 1, a1, a2 .. }
     * @return
     */

    public static ZerosOnlyCausalFilter create ( double aInFilterCoefs [] )
    {
        return new ZerosOnlyCausalFilter ( aInFilterCoefs );
    }

    private ZerosOnlyCausalFilter ( double aInFilterCoefs[] )
    {
        filter = CausalConvolutionFilter.create ( aInFilterCoefs );
    }

    public double filter ( double in )
    {
        return filter.putAndGet ( in );
    }

    @Override
    public void reset ( )
    {
        filter.reset();
    }

    @Override
    public String toString ( )
    {
        return "ZerosOnlyCausalFilter{" +
            "filter=" + filter +
            '}';
    }
}
