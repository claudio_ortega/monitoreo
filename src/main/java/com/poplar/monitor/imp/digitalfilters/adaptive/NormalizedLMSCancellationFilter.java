/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.adaptive;

import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.BandpassBiQuadFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.DCBlockFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.FixedDelayFilter;

/**
 * see
 * main sources:
 * http://www.cs.cmu.edu/~aarti/pubs/ANC.pdf
 * http://www.ee.cityu.edu.hk/~hcso/it6303_4.pdf
 */
public class NormalizedLMSCancellationFilter implements ICancellationFilter
{
    private AdaptiveLinearCombiner adaptiveLinearCombiner;
    private FixedDelayFilter delayFilter;
    private PowerIntegrator powerIntegrator;
    private DCBlockFilter dcBlockFilter;
    private BandpassBiQuadFilter bandpassFilter;

    private double samplingFreqInHz;
    private double cancellationFreqInHz;
    private double mu;
    private boolean bypass;
    private boolean needsRestart;
    private long sampleCounter;

    public static NormalizedLMSCancellationFilter create ( )
    {
        return new NormalizedLMSCancellationFilter ( );
    }

    private NormalizedLMSCancellationFilter ( )
    {
        sampleCounter = 0;
    }

    @Override
    public void reset ( )
    {
        needsRestart = true;
    }

    @Override
    public ICancellationFilter setCancellationFreqInHz ( double value )
    {
        if ( ! FilterUtil.areCloseEnough ( cancellationFreqInHz, value, 1E-6 ) )
        {
            cancellationFreqInHz = value;
            needsRestart = true;
        }

        return this;
    }

    @Override
    public ICancellationFilter setSamplingFreqInHz ( double value )
    {
        if ( ! FilterUtil.areCloseEnough ( samplingFreqInHz, value, 0.01 ) )
        {
            samplingFreqInHz = value;
            needsRestart = true;
        }

        return this;
    }

    @Override
    public ICancellationFilter setMu ( double value )
    {
        final double newMu = value * 1e-7;

        if ( ! FilterUtil.areCloseEnough ( mu, newMu, 1E-10 ) )
        {
            mu = newMu;
            needsRestart = true;
        }

        return this;
    }

    @Override
    public ICancellationFilter setBypass ( boolean value )
    {
        if ( value != bypass )
        {
            needsRestart = true;
            bypass = value;
        }

        return this;
    }

    @Override
    public ICancellationFilter start ( )
    {
        if ( needsRestart )
        {
            final double delayInMilliSecs = 200;
            final int delayLengthInSamples = ( int ) ( 0.001 * delayInMilliSecs * samplingFreqInHz );
            delayFilter = FixedDelayFilter.create ( delayLengthInSamples );

            final double adaptiveLengthInSecs = 1.1 / cancellationFreqInHz;
            final int adaptiveLengthInSamples = ( int ) ( adaptiveLengthInSecs * samplingFreqInHz );

            adaptiveLinearCombiner = AdaptiveLinearCombiner.create ( adaptiveLengthInSamples );

            powerIntegrator = PowerIntegrator.create ( adaptiveLengthInSamples );

            dcBlockFilter = DCBlockFilter
                .create ()
                .setFsInHz ( samplingFreqInHz )
                .setPoleFreq ( 1.0 )
                .start ();

            bandpassFilter = BandpassBiQuadFilter
                .create ( )
                .setFsInHz ( samplingFreqInHz )
                .setCenterFrequencyInHz ( cancellationFreqInHz )
                .setBandwidthInHz ( 3.0 )   // should be able to catch all variation in line frequency
                .start ( );

            needsRestart = false;
        }

        return this;
    }

    @Override
    public double filter ( double in )
    {
        final long AUTO_RESET_TIME_IN_SECS = 5 * 60;

        if ( needsRestart || ( ++sampleCounter % ( AUTO_RESET_TIME_IN_SECS * samplingFreqInHz ) ) == 0 )
        {
            reset ();
            start ();
        }

        final double ret;

        if ( bypass )
        {
            ret = in;
        }

        else
        {
            final double inAC = dcBlockFilter.filter ( in );
            final double inputACEnergy = powerIntegrator.add ( inAC * inAC );

            if ( Double.isNaN ( inputACEnergy ) || inputACEnergy < 1e-2 )
            {
                delayFilter.reset();
                bandpassFilter.reset ();
                adaptiveLinearCombiner.reset ();

                ret = in;
            }

            else
            {
                final double delayedReference = delayFilter.put ( inAC );
                final double bandPass = bandpassFilter.filter ( delayedReference );
                final double y = iBoundMax( adaptiveLinearCombiner.putAndGet ( bandPass ), 1e6 );
                final double error = inAC - y;

                ret = error;

                // adapt coefficients with normalization
                adaptiveLinearCombiner.combineInputIntoTaps ( 2 * mu * error / inputACEnergy );
            }
        }

        return ret;
    }

    @SuppressWarnings( "SuspiciousNameCombination" )
    private static double iBoundMax ( double x, double boundary )
    {
        final double y;

        if ( Double.isNaN ( x ) )
        {
            y = 0;
        }

        else if ( Math.abs( x ) > boundary )
        {
            y = ( x > 0 ) ? boundary : -boundary;
        }

        else
        {
            y = x;
        }

        return y;
    }

    @Override
    public String toString ( )
    {
        return
            "NormalizedLMSCancellationFilter{"
            + "adaptiveLinearCombiner="
            + adaptiveLinearCombiner
            + ", delayFilter="
            + delayFilter
            + ", powerIntegrator="
            + powerIntegrator
            + ", dcBlockFilter="
            + dcBlockFilter
            + ", bandpassFilter="
            + bandpassFilter
            + ", samplingFreqInHz="
            + samplingFreqInHz
            + ", cancellationFreqInHz="
            + cancellationFreqInHz
            + ", mu="
            + mu
            + ", bypass="
            + bypass
            + ", needsRestart="
            + needsRestart
            + '}';
    }
}
