/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.nonlinear;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.ILockedFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalFilters;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import org.apache.commons.math3.complex.Complex;


public class AGCFilter
    implements ICausalDigitalFilter, ILockedFilter
{
    private double holdOffInMs;
    private double tauInMs;
    private double fsInHz;
    private double outMaximumLevel;
    private double inputLinearThreshold;

    private RectifyingFilter rectifyingFilter;
    private ICausalDigitalFilter lowPass;

    private boolean insideLockRange;
    private long count;

    public static AGCFilter create ( )
    {
        return new AGCFilter ( );
    }

    private AGCFilter ( )
    {
    }

    public AGCFilter setOutMaximumLevel ( double aInLevel )
    {
        outMaximumLevel = aInLevel;
        return this;
    }

    public AGCFilter setInputLinearThreshold ( double aInLevel )
    {
        inputLinearThreshold = aInLevel;
        return this;
    }

    public AGCFilter setFsInHz ( double aInFsInHz )
    {
        fsInHz = aInFsInHz;
        return this;
    }

    public AGCFilter setHoldOffInMs ( double aInHoldoffInMs )
    {
        holdOffInMs = aInHoldoffInMs;
        return this;
    }

    public AGCFilter setTauInMs ( double aInTauInMs )
    {
        tauInMs = aInTauInMs;
        return this;
    }

    @Override
    public boolean insideLockRange ( )
    {
        return insideLockRange;
    }

    public AGCFilter start ()
    {
        final double s0real = 1.0 / ( tauInMs / 1000.0 );

        final double z0real = 1.0 - FilterUtil.scaleFsIntoTwoPi ( s0real, fsInHz );

        final PolynomialFraction polynomialFraction = PolynomialFraction.create ( ).addPole ( z0real );

        final Complex dcGain = polynomialFraction.evalaluateOnASinglePoint ( Complex.ONE );

        lowPass = CausalFilters.createFromPolynomialFraction ( 1.0 / dcGain.abs (), polynomialFraction );

        rectifyingFilter = RectifyingFilter
            .create ()
            .setFsInHz ( fsInHz )
            .setHoldOffInMs ( holdOffInMs )
            .setTauInMs ( tauInMs )
            .setDebug ( true )
            .start ( );

        reset();

        return this;
    }

    @Override
    public double filter ( double in )
    {
        final double inputPeak = rectifyingFilter.filter ( in );
        final double lowPassOnInputPeak = lowPass.filter ( inputPeak );

        final double gain;

        if ( lowPassOnInputPeak < inputLinearThreshold )
        {
            gain = outMaximumLevel / inputLinearThreshold;
            insideLockRange = false;
        }

        else
        {
            gain = outMaximumLevel / lowPassOnInputPeak;
            insideLockRange = true;
        }

        return gain * in;
    }

    @Override
    public void reset ( )
    {
        rectifyingFilter.reset ();
        lowPass.reset();
        insideLockRange = false;
    }
}
