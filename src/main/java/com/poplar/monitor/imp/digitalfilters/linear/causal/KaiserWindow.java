/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.google.common.base.Preconditions;

/**
 *     see Kaiser window discussion from here: 'Digital CausalFilters: Analysis and Design', Andreas Antoniou, Chapter 9, pag 237
 *     see the impulse response before windowing from here: http://www.labbookpages.co.uk/audio/firWindowing.html
 */
public class KaiserWindow
{
    public enum PassTypeEnum { lowPass, highPass, bandPass, bandStop }

    public static double[] computeKaiserWindow(
        PassTypeEnum aInPassType,
        double fSamplingHz,
        double attenuationDb,
        double maxRippleDb,
        double fDeltaPassToStopAbsolute,
        double fTransitionOne,
        Double fTransitionTwoOrNull )
    {
        if ( fTransitionTwoOrNull != null )
        {
            Preconditions.checkArgument ( fTransitionTwoOrNull < fSamplingHz / 2 );
            Preconditions.checkArgument ( fTransitionOne < fTransitionTwoOrNull );
            Preconditions.checkArgument ( ( fTransitionTwoOrNull - fTransitionOne ) > fDeltaPassToStopAbsolute );
        }

        final double[] w = computeHalfKaiserWindow(
            fSamplingHz,
            fDeltaPassToStopAbsolute,
            attenuationDb,
            maxRippleDb );

        final double[] h = computeRectangularWindowOnRightSide (
            aInPassType,
            w.length,
            fSamplingHz,
            fTransitionOne,
            fTransitionTwoOrNull );

        return iConvertToCausal ( iComputeScalarProduct ( w, h ) );
    }

    private static double[] computeRectangularWindowOnRightSide (
        PassTypeEnum aInPassType,
        int aInHalfLength,
        double fSamplingHz,
        double fTransitionOne,
        Double fTransitionTwoOrNull )
    {
        Preconditions.checkArgument ( fTransitionOne < fSamplingHz / 2 );

        final double TWO_PI = 2 * Math.PI;

        final double h[] = new double[ aInHalfLength ];

        if ( aInPassType == PassTypeEnum.lowPass )
        {
            Preconditions.checkArgument ( fTransitionTwoOrNull == null );

            final double fNormalizedOne = fTransitionOne / fSamplingHz;

            h[0] = 2 * fNormalizedOne;

            for ( int i = 1; i < aInHalfLength; i++ )
            {
                h[i] = Math.sin ( TWO_PI * fNormalizedOne * i ) / ( Math.PI * i );
            }
        }

        else if ( aInPassType == PassTypeEnum.highPass )
        {
            Preconditions.checkArgument ( fTransitionTwoOrNull == null );

            final double fNormalizedOne = fTransitionOne / fSamplingHz;

            h[0] = 1 - 2 * fNormalizedOne;

            for ( int i = 1; i < aInHalfLength; i++ )
            {
                h[i] = - Math.sin ( TWO_PI * fNormalizedOne * i ) / ( Math.PI * i );
            }
        }

        else if ( aInPassType == PassTypeEnum.bandPass )
        {
            Preconditions.checkArgument ( fTransitionTwoOrNull != null );
            Preconditions.checkArgument ( fTransitionOne < fTransitionTwoOrNull );
            Preconditions.checkArgument ( fTransitionTwoOrNull < fSamplingHz / 2 );

            final double fNormalizedOne = fTransitionOne / fSamplingHz;
            final double fNormalizedTwo = fTransitionTwoOrNull / fSamplingHz;

            h[0] = 2 * ( fNormalizedTwo - fNormalizedOne );

            for ( int i = 1; i < aInHalfLength; i++ )
            {
                h[i] = ( Math.sin ( TWO_PI * fNormalizedTwo * i ) - Math.sin ( TWO_PI * fNormalizedOne * i ) ) / ( Math.PI * i );
            }
        }

        else if ( aInPassType == PassTypeEnum.bandStop )
        {
            Preconditions.checkArgument ( fTransitionTwoOrNull != null );
            Preconditions.checkArgument ( fTransitionOne < fTransitionTwoOrNull );
            Preconditions.checkArgument ( fTransitionTwoOrNull < fSamplingHz / 2 );

            final double fNormalizedOne = fTransitionOne / fSamplingHz;
            final double fNormalizedTwo = fTransitionTwoOrNull / fSamplingHz;

            h[0] = 1 - 2 * ( fNormalizedTwo - fNormalizedOne );

            for ( int i = 1; i < aInHalfLength; i++ )
            {
                h[i] = - ( Math.sin ( TWO_PI * fNormalizedTwo * i ) - Math.sin ( TWO_PI * fNormalizedOne * i ) ) / ( Math.PI * i );
            }
        }

        return h;
    }

    private static double[] computeHalfKaiserWindow(
        double fSamplingHz,
        double fDeltaPassToStopAbsolute,
        double attenuationDb,
        double maxRippleDb )
    {
        Preconditions.checkArgument ( fDeltaPassToStopAbsolute > 0 );

        // step #2
        final double delta1 = Math.pow ( 10, - attenuationDb / 20 );

        final double k1 = Math.pow ( 10, maxRippleDb / 20 );
        final double delta2 = ( k1 - 1 ) / ( 1 + k1 );

        final double deltaMin = Math.min ( delta1, delta2 );

        // step #3
        attenuationDb = - 20.0 * Math.log10 ( deltaMin );

        // step #4
        final double alfa;

        if ( attenuationDb <= 21.0 )
        {
            alfa = 0;
        }

        else if ( attenuationDb <= 50.0 )
        {
            alfa = 0.5842 * Math.pow( attenuationDb - 21.0, .4 ) + 0.07886 * ( attenuationDb - 21.0 );
        }

        else
        {
            alfa = 0.1102 * ( attenuationDb - 8.7 );
        }

        // step #5
        final double d;

        if ( attenuationDb <= 21.0 )
        {
            d = 0.9222;
        }

        else
        {
            d = ( attenuationDb - 7.95 ) / 14.36;
        }

        final double nD = ( fSamplingHz * d ) / ( fDeltaPassToStopAbsolute ) + 1;

        int n = (int) nD + 1;

        if ( ( n % 2 ) == 0 )
        {
            n++;
        }

        // should be even
        Preconditions.checkState ( n % 2 == 1 );

        // step #6
        final int halfLength = (n+1) / 2;

        final double w[] = new double[ halfLength ];
        for ( int i=0; i<halfLength; i++ )
        {
            final double beta = alfa * Math.sqrt ( 1 - Math.pow ( ( 2.0 * i / ( n - 1 ) ), 2 ) );
            w[ i ] = ( iGetComputeBesselFirstKind ( beta, 10 ) / iGetComputeBesselFirstKind ( alfa, 10 ) );
        }

        return w;
    }

    private static double iGetComputeBesselFirstKind ( double x, int precision )
    {
        double ret = 1;

        for ( int k=1; k<precision; k++ )
        {
            ret += Math.pow ( ( Math.pow ( ( x / 2 ), k ) / iGetFactorial ( k ) ), 2 );
        }

        return ret;
    }

    private static double iGetFactorial ( int n )
    {
        double ret = 1;

        if ( n > 1 )
        {
            for ( int i=2; i<=n; i++ )
            {
                ret *= i ;
            }
        }

        return ret;
    }

    private static double[] iComputeScalarProduct ( double[] a, double[] b )
    {
        Preconditions.checkArgument ( a.length == b.length );

        final double[] p = new double[a.length];

        for ( int i=0; i<a.length; i++ )
        {
            p[i] = a[i] * b[i];
        }

        return p;
    }

    private static double[] iConvertToCausal ( double[] x )
    {
        final int yLength = 2 * x.length - 1;
        final double[] y = new double[ yLength ];

        for ( int i=0; i<x.length; i++ )
        {
            y[ i + x.length - 1 ] = x[ i ];
            y[ i ] = x[ x.length - 1 - i ];
        }

        return y;
    }
}