/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import org.apache.commons.math3.complex.Complex;

public class AllPassQuadratureFilter implements ICausalDigitalFilter
{
    private double fsInHz;
    private double quadratureFrequencyInHz;
    private ICausalDigitalFilter filter;

    public static AllPassQuadratureFilter create()
    {
        return new AllPassQuadratureFilter ();
    }

    private AllPassQuadratureFilter ( )
    {
    }

    public AllPassQuadratureFilter setFsInHz ( double value )
    {
        fsInHz = value;
        return this;
    }

    public AllPassQuadratureFilter setQuadratureFrequencyInHz ( double value )
    {
        quadratureFrequencyInHz = value;
        return this;
    }

    public AllPassQuadratureFilter start ( )
    {
        /*
         - to have mod( G(s) ) = 1 for all omega then the pole and the zero should be one inverse of the other
         - the pole should be inside the unit circle for the filter to be stable
         - the angle for the pole should be 0.25 * pi, for the zero 0.75 * pi, so arg( G(s) ) results in 0.5 * pi
         */
        final double wn = FilterUtil.scaleFsIntoTwoPi ( quadratureFrequencyInHz, fsInHz );
        final double realPole = 1.0 - wn;
        final double realZero = 1.0 / realPole;

        final PolynomialFraction polynomialFraction = PolynomialFraction
            .create ( )
            .addZero ( realZero )
            .addPole ( realPole );

        final double absoluteValueAtWn = polynomialFraction.evalaluateOnASinglePoint ( new Complex ( 0, wn ).exp () ).abs ();

        filter = CausalFilters.createFromPolynomialFraction ( 1.0 / absoluteValueAtWn, polynomialFraction );

        return this;
    }

    @Override
    public double filter ( double in )
    {
        return filter.filter ( in );
    }

    @Override
    public void reset ( )
    {
        filter.reset ();
    }
}
