/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.adaptive;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.BandpassBiQuadFilter;
import com.poplar.monitor.imp.digitalfilters.nonlinear.AGCFilter;
import com.poplar.monitor.imp.digitalfilters.nonlinear.FrequencyEstimationFilter;

/**
 * see
 * main sources:
 * http://www.cs.cmu.edu/~aarti/pubs/ANC.pdf
 * http://www.ee.cityu.edu.hk/~hcso/it6303_4.pdf
 * secondary:
 * https://www.researchgate.net/publication/261054312_Least-Mean-Square_algorithm_based_adaptive_filters_for_removing_power_line_interference_from_ECG_signal?ev=pubRec
 * https://en.wikipedia.org/wiki/Adaptive_filter
 */

public class ToneCancellationFilter implements ICancellationFilter
{
    private double cancellationFreqInHz;
    private double samplingFreqInHz;
    private double sineCoefficient;
    private double cosineCoefficient;
    private double mu;

    private double theta;
    private boolean bypass;
    private BandpassBiQuadFilter bandpassFilter;
    private AGCFilter agcFilter;
    private FrequencyEstimationFilter frequencyEstimationFilter;
    private boolean needsRestart;

    public static ToneCancellationFilter create ( )
    {
        return new ToneCancellationFilter ( );
    }

    private ToneCancellationFilter ( )
    {
    }

    @Override
    public void reset ( )
    {
        needsRestart = true;
    }

    @Override
    public ToneCancellationFilter setCancellationFreqInHz ( double value )
    {
        if ( ! FilterUtil.areCloseEnough ( cancellationFreqInHz, value, 1E-6 ) )
        {
            cancellationFreqInHz = value;
            needsRestart = true;
        }

        return this;
    }

    @Override
    public ToneCancellationFilter setSamplingFreqInHz ( double value )
    {
        if ( ! FilterUtil.areCloseEnough ( samplingFreqInHz, value, 0.01 ) )
        {
            samplingFreqInHz = value;
            needsRestart = true;
        }

        return this;
    }

    @Override
    public ToneCancellationFilter setMu ( double value )
    {
        final double newMu = value * 1e-4;

        if ( ! FilterUtil.areCloseEnough ( mu, newMu, 1E-10 ) )
        {
            mu = newMu;
            needsRestart = true;
        }

        return this;
    }

    @Override
    public ToneCancellationFilter setBypass ( boolean value )
    {
        bypass = value;
        return this;
    }

    @Override
    public ToneCancellationFilter start ( )
    {
        if ( needsRestart )
        {
            bandpassFilter = BandpassBiQuadFilter
                .create ( )
                .setFsInHz ( samplingFreqInHz )
                .setCenterFrequencyInHz ( cancellationFreqInHz )
                .setBandwidthInHz ( 3.0 )   // should be able to catch all variation in line frequency
                .start ( );

            agcFilter = AGCFilter
                .create ( )
                .setFsInHz ( samplingFreqInHz )
                .setTauInMs ( 1000 * 10.0 * ( 1.0 / cancellationFreqInHz ) )    // 10 cycles of the carrier frequency
                .setHoldOffInMs ( 1000 * 2.0 * ( 1.0 / cancellationFreqInHz ) ) // 2 cycles of the carrier frequency
                .setOutMaximumLevel ( 1.0 ).setInputLinearThreshold ( 1.0 )
                .start ( );

            frequencyEstimationFilter = FrequencyEstimationFilter
                .create ()
                .setSamplingFreqInHz ( samplingFreqInHz )
                .setExpectedFreqInHz ( cancellationFreqInHz )
                .setLockFreqTolerancePercent ( 5.0 )
                .start();

            needsRestart = false;
        }

        return this;
    }

    @Override
    public double filter ( double in )
    {
        Preconditions.checkState ( ! needsRestart );

        final double ret;

        if ( bypass )
        {
            ret = in;
        }

        else
        {
            // pseudo PLL to get fake external reference
            // by estimating the freq for the line noise
            final double bandPassOut = bandpassFilter.filter ( in );
            final double bandPassWithAgcOut = agcFilter.filter ( bandPassOut );
            final double estimatedFrequency = frequencyEstimationFilter.filter ( bandPassWithAgcOut );

            theta = theta + ( 1.0 / samplingFreqInHz ) * estimatedFrequency * 2 * Math.PI;

            final double sine = Math.sin ( theta );
            final double cosine = Math.cos ( theta );

            // LMS
            final double error = in - ( sineCoefficient * sine + cosineCoefficient * cosine );
            sineCoefficient = sineCoefficient + mu * error * sine;
            cosineCoefficient = cosineCoefficient + mu * error * cosine;

            ret = error;
        }

        return ret;
    }
}
