/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.polynomialfraction;

import org.apache.commons.math3.complex.Complex;

import java.util.LinkedList;
import java.util.List;

public class PolynomialFraction
{
    private final List<Complex> poleList;
    private final List<Complex> zeroList;
    private double gain;

    public static PolynomialFraction create()
    {
        return new PolynomialFraction ();
    }

    private PolynomialFraction ( )
    {
        zeroList = new LinkedList<> ();
        poleList = new LinkedList<> ();
        gain = 1.0;
    }

    public PolynomialFraction addZero( double x )
    {
        addZero ( Complex.valueOf ( x ) );
        return this;
    }

    public PolynomialFraction addPole( double x )
    {
        addPole ( Complex.valueOf ( x ) );
        return this;
    }

    public PolynomialFraction addZero( Complex c )
    {
        zeroList.add( c );
        return this;
    }

    public PolynomialFraction addPole( Complex c )
    {
        poleList.add( c );
        return this;
    }

    public PolynomialFraction setGain ( double value )
    {
        gain = value;
        return this;
    }

    public Complex evalaluateOnASinglePoint ( Complex c )
    {
        Complex tmp = Complex.ONE;

        for ( Complex next : zeroList )
        {
            tmp = tmp.multiply ( c.subtract ( next ) );
        }

        for ( Complex next : poleList )
        {
            tmp = tmp.divide ( c.subtract ( next ) );
        }

        tmp = tmp.multiply ( Complex.valueOf ( gain ) );

        return tmp;
    }

    public List<Complex> getPoleList ( )
    {
        return poleList;
    }

    public List<Complex> getZeroList ( )
    {
        return zeroList;
    }

    private static String printRootsAsEquation ( List<Complex> roots )
    {
        final StringBuilder sb = new StringBuilder (  );

        for( Complex next : roots )
        {
            if ( sb.length () > 0 )
            {
                sb.append ( "*" );
            }

            sb.append ( "(x-" ).append ( next.toString () ).append ( ")" );
        }

        return sb.toString ();
    }

    public String printEquation ( )
    {
        return gain + " * " + printRootsAsEquation ( zeroList ) + "/" + printRootsAsEquation ( poleList );
    }

    @Override
    public String toString ( )
    {
        return "PolynomialFraction{" +
            "poleList=" + poleList +
            ", zeroList=" + zeroList +
            ", gain=" + gain +
            '}';
    }
}

