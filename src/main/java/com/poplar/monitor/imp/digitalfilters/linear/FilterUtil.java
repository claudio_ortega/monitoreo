/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear;

import com.google.common.base.Preconditions;
import org.apache.commons.math3.complex.Complex;

public class FilterUtil
{
    private FilterUtil ( )
    {
    }

    public static double[] multiplyArray( double[] in, double factor )
    {
        final double[] out = new double[ in.length ];

        for ( int i=0; i<in.length; i++ )
        {
            out[i] = in[i] * factor;
        }

        return out;
    }

    public static int computeMod ( int x, int modulus )
    {
        if ( x < 0 )
        {
            x += modulus;
        }

        return x % modulus;
    }

    public static double getUnitDeltaFilterOutput ( int i )
    {
        return i == 0 ? 1 : 0;
    }

    public static double getUnitStepFilterOutput ( int i )
    {
        return i > 0 ? 1 : 0;
    }

    public static double getUnitRampFilterOutput ( int i )
    {
        return i;
    }

    public static Complex[] clone ( Complex[] x )
    {
        final Complex[] ret = new Complex[x.length];

        for ( int i = 0; i < x.length; i++ )
        {
            ret[i] = new Complex ( x[i].getReal (), x[i].getImaginary () );
        }

        return ret;
    }

    public static void multiplyInPlace ( Complex[] x, Complex multiplier )
    {
        for ( int i = 0; i < x.length; i++ )
        {
            x[i] = x[i].multiply ( multiplier );
        }
    }

    public static void addAtoB ( Complex[] a, Complex[] b )
    {
        Preconditions.checkArgument ( a.length == b.length );

        for ( int i = 0; i < a.length; i++ )
        {
            b[i] = b[i].add ( a[i] );
        }
    }

    public static void checkAllAreSmall ( double[] doubles )
    {
        for ( double d : doubles )
        {
            Preconditions.checkState ( Math.abs ( d ) < 1e-10, "d:" + d );
        }
    }

    public static double[] getRealComponents ( Complex[] c )
    {
        final double[] ret = new double[c.length];

        for( int i=0; i<c.length; i++ )
        {
            ret[i] = c[i].getReal ();
        }

        return ret;
    }

    public static double[] getImaginaryComponents ( Complex[] c )
    {
        final double[] ret = new double[c.length];

        for( int i=0; i<c.length; i++ )
        {
            ret[i] = c[i].getImaginary ( );
        }

        return ret;
    }

    public static double[] multiplyAndClone ( double[] x, double gain )
    {
        final double[] ret = new double[x.length];

        for ( int i = 0; i < x.length; i++ )
        {
            ret[i] = gain * x[i];
        }

        return ret;
    }

    public static double[] shiftRightUpToNewLength ( double[] c, int newLength )
    {
        Preconditions.checkArgument ( newLength >= c.length );

        final double[] ret = new double[newLength];

        int offset = newLength - c.length;

        System.arraycopy ( c, 0, ret, offset, ret.length - offset );

        for( int i=0; i<offset; i++ )
        {
            ret[i] = 0;
        }

        return ret;
    }

    public static void shiftRightOne ( Complex[] x )
    {
        System.arraycopy ( x, 0, x, 1, x.length - 1 );

        x[0] = Complex.ZERO;
    }

    public static double scaleFsIntoTwoPi ( double inHz, double aInFs )
    {
        return 2 * Math.PI * inHz / aInFs;
    }

    public static boolean areCloseEnough ( double a, double b, double howMuchClose )
    {
        return Math.abs ( a - b ) < howMuchClose;
    }
}
