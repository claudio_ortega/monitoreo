/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.adaptive;

import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalConvolutionFilter;

import java.util.Arrays;

public class AdaptiveLinearCombiner extends CausalConvolutionFilter
{
    public static AdaptiveLinearCombiner create ( int tapLength )
    {
        return new AdaptiveLinearCombiner ( tapLength );
    }

    private AdaptiveLinearCombiner ( int tapLength )
    {
        super ( new double[tapLength] );
    }

    // used for LMS adaptive filters,
    // see Adaptive Signal Processing, Widrow $ Sterns,
    // formula 6.3, Wk+1 = Wk + multiplier * Xk
    // Wk := taps[k]
    // Xk := memory[k]
    public void combineInputIntoTaps ( double multiplier )
    {
        for ( int i = 0; i < taps.length; i++ )
        {
            final int iShifted = ( memory.length + indexForNextSample - i - 1 ) % memory.length;
            taps[i] = taps[i] + multiplier * memory[iShifted];
        }
    }

    @Override
    public void reset ( )
    {
        super.reset ();
        Arrays.fill ( taps, 0 );
    }
}
