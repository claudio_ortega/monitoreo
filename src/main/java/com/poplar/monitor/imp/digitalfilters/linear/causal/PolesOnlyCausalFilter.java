/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;

public class PolesOnlyCausalFilter implements ICausalDigitalFilter
{
    private final CausalConvolutionFilter filter;
    private final double gain;

    /**
     *        example #1 : for a filter with z transform: f(z) = 1 / (1 - a * zˆ-1) : new double[] { 1, -a }
     *        example #2 : for a filter with z transform: f(z) = 1 / (1 + b * zˆ-1 + c * zˆ-2 ) : new double[] { 1, b, c }
     *
     * @param aInFilterCoefs coefs when the filter is expresses as H(z) = 1 / ( 1 + a1 * z ^ -1 + a2 * z ^ -2 ... )
     *                       aInFilterCoefs = { 1, a1, a2 .. }
     * @return
     */
    public static PolesOnlyCausalFilter create ( double aInFilterCoefs[] )
    {
        return new PolesOnlyCausalFilter ( aInFilterCoefs );
    }

    private PolesOnlyCausalFilter ( double aInFilterCoefs[] )
    {
        gain = aInFilterCoefs[0];

        // for example #2, we should get this
        // y[n] = x[n] - b* y[n-1] - c * y[n-2]
        final double[] firOnOutputCoefs = FilterUtil.multiplyArray ( aInFilterCoefs, ( - 1 / gain ) );
        firOnOutputCoefs[0] = 0;

        filter = CausalConvolutionFilter.create ( firOnOutputCoefs );
    }

    @Override
    public double filter ( double in )
    {
        // for example #2, we should get this
        // y[n] = x[n] - b * y[n-1] - c * y[n-2];
        double tmp = filter.putAndGet ( in );
        tmp = tmp + in / gain;

        // we put y[n] in place for next computation
        filter.putWithOffset ( tmp, - 1 );

        return tmp;
    }

    @Override
    public void reset ( )
    {
        filter.reset ();
    }

    @Override
    public String toString ( )
    {
        return "PolesOnlyCausalFilter{" +
            ", gain=" + gain +
            ", filter=" + filter +
            '}';
    }
}
