/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.nonlinear;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.common.CircularQueue;
import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalFilters;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import org.apache.commons.math3.complex.Complex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicLong;

/*
    IMPORTANT: the input to this filter should be quasi sinusoidal
 */
public class FrequencyEstimationFilter implements ICausalDigitalFilter
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    private double lastSample;
    private boolean lastSampleIsValid;
    private boolean lastCountOnCrossIsValid;
    private long sampleCount;
    private long lastCountOnCross;
    private long detectionCount;
    private int detectionCountCycle;
    private double samplingFreqInHz;
    private double expectedFreqInHz;
    private double lockFreqTolerance;
    private double resultFreqInHz;
    private boolean resultFreqWasMeasured;
    private CircularQueue<AtomicLong> queue;
    private final LongSlotFiller filler;
    private long minimumPeriodInSamples;
    private long maximumPeriodInSamples;
    private ICausalDigitalFilter lowPass;

    private static class LongSlotFiller implements CircularQueue.ISlotFiller<AtomicLong>
    {
        private Long data;

        private LongSlotFiller()
        {
        }

        public LongSlotFiller setData( long in )
        {
            data = in;
            return this;
        }

        @Override
        public void fill ( AtomicLong input )
        {
            input.set ( data );
        }
    }

    public static FrequencyEstimationFilter create ()
    {
        return new FrequencyEstimationFilter ();
    }

    private FrequencyEstimationFilter ( )
    {
        filler = new LongSlotFiller();
    }

    public FrequencyEstimationFilter setSamplingFreqInHz ( double value )
    {
        samplingFreqInHz = value;
        return this;
    }

    public FrequencyEstimationFilter setExpectedFreqInHz ( double value )
    {
        expectedFreqInHz = value;
        return this;
    }

    public FrequencyEstimationFilter setLockFreqTolerancePercent ( double value )
    {
        lockFreqTolerance = value * 0.01;
        return this;
    }

    @Override
    public void reset ( )
    {
        sampleCount = 0;
        detectionCount = 0;

        lastCountOnCross = 0;
        lastCountOnCrossIsValid = false;

        lastSample = 0;
        lastSampleIsValid = false;

        resultFreqInHz = expectedFreqInHz;
        resultFreqWasMeasured = false;
    }

    public FrequencyEstimationFilter start ()
    {
        minimumPeriodInSamples = ( long ) ( samplingFreqInHz / ( expectedFreqInHz * ( 1 + lockFreqTolerance  ) ) );
        maximumPeriodInSamples = ( long ) ( samplingFreqInHz / ( expectedFreqInHz * ( 1 - lockFreqTolerance ) ) );

        final double tauInSecs = 1;
        final double z0RealPole = 1.0 - FilterUtil.scaleFsIntoTwoPi ( 1.0 / tauInSecs, samplingFreqInHz );
        final PolynomialFraction polynomialFraction = PolynomialFraction.create ( ).addPole ( z0RealPole );
        final Complex dcGain = polynomialFraction.evalaluateOnASinglePoint ( Complex.ONE );
        lowPass = CausalFilters.createFromPolynomialFraction ( 1.0 / dcGain.abs ( ), polynomialFraction );

        detectionCountCycle = 10 * (int) expectedFreqInHz; // 30 secs

        queue = CircularQueue
            .<AtomicLong>create ( )
            .setSlots ( new AtomicLong[ (int) expectedFreqInHz] )   // compute over last second
            .setAllowDataOverrun ( true )
            .initializeSlots ( ( ) -> new AtomicLong ( 0 ) );

        reset();

        return this;
    }

    @Override
    public double filter ( double in )
    {
        sampleCount++;

        final boolean detectedInThisCycle;

        if ( lastSampleIsValid && lastSample < 0 && in > 0 )
        {
            detectionCount++;

            if ( lastCountOnCrossIsValid )
            {
                final long period = sampleCount - lastCountOnCross;

                if ( iIsPeriodWithinAcceptableRange ( period ) )
                {
                    queue.put ( filler.setData ( period ) );
                }
            }

            lastCountOnCross = sampleCount;
            lastCountOnCrossIsValid = true;

            detectedInThisCycle = true;
        }

        else
        {
            detectedInThisCycle = false;
        }

        if ( detectedInThisCycle && ( detectionCount % detectionCountCycle == 0 ) )
        {
            if ( queue.size() == 0 )
            {
                resultFreqWasMeasured = false;
                resultFreqInHz = expectedFreqInHz;
            }

            else
            {
                long sum = 0;

                for ( int i = 0; i < queue.size ( ); i++ )
                {
                    sum += queue.peekFirst ( i ).get ( );
                }

                final long mean = sum / queue.size();

                Preconditions.checkState ( iIsPeriodWithinAcceptableRange ( mean ) );

                resultFreqWasMeasured = true;
                resultFreqInHz = samplingFreqInHz / mean;
            }
        }

        lastSample = in;
        lastSampleIsValid = true;

        return lowPass.filter ( resultFreqInHz );
    }

    private boolean iIsPeriodWithinAcceptableRange ( long in )
    {
        return in > minimumPeriodInSamples && in < maximumPeriodInSamples;
    }

    /**
     *
     * @return true only if it was possible to compute a value close enough to the expected frequency
     */
    public boolean isFilterLocked()
    {
        return resultFreqWasMeasured;
    }
}
