/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.polynomialfraction;

import org.apache.commons.math3.complex.Complex;
import java.util.LinkedList;
import java.util.List;

public class PolynomialFractionEvaluator
{
    public static List<Complex> evaluateDFT ( PolynomialFraction holder, int nPoints )
    {
        return evaluatePolynomialOnSomePoints ( holder, sampleUnitCircleUniformly ( nPoints ) );
    }

    // the point (1,0) gets always sampled on the unit circle
    private static List<Complex> sampleUnitCircleUniformly ( int nPoints )
    {
        final double pieceOfPi = 2.0 * Math.PI / nPoints;  // pieceOfPi.. funny

        final List<Complex> ret = new LinkedList<> (  );

        for ( int i=0; i<nPoints; i++ )
        {
            ret.add( new Complex( 0, i * pieceOfPi ).exp() );
        }

        return ret;
    }

    private static List<Complex> evaluatePolynomialOnSomePoints ( PolynomialFraction holder, List<Complex> points )
    {
        final List<Complex> ret = new LinkedList<> (  );

        for ( Complex next : points )
        {
            ret.add( holder.evalaluateOnASinglePoint ( next ) );
        }

        return ret;
    }
}
