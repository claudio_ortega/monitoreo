/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import org.apache.commons.math3.complex.Complex;

public class BandpassBiQuadFilter implements ICausalDigitalFilter
{
    private double bandwidthInHz;
    private double fsInHz;
    private double centerFrequencyInHz;
    private ICausalDigitalFilter filter;

    public static BandpassBiQuadFilter create()
    {
        return new BandpassBiQuadFilter ();
    }

    private BandpassBiQuadFilter ( )
    {
    }

    public BandpassBiQuadFilter setFsInHz ( double value )
    {
        fsInHz = value;
        return this;
    }

    public BandpassBiQuadFilter setCenterFrequencyInHz ( double value )
    {
        centerFrequencyInHz = value;
        return this;
    }

    public BandpassBiQuadFilter setBandwidthInHz ( double value )
    {
        bandwidthInHz = value;
        return this;
    }

    public BandpassBiQuadFilter start ( )
    {
        final double wCenter = FilterUtil.scaleFsIntoTwoPi ( centerFrequencyInHz, fsInHz );
        final double semiBWInHz = bandwidthInHz / 2;
        final double r = 1.0 - FilterUtil.scaleFsIntoTwoPi ( semiBWInHz, fsInHz );
        final Complex p0 = new Complex ( 0, wCenter ).exp ( ).multiply ( r );

        final PolynomialFraction polynomialFraction = PolynomialFraction
            .create ( )
            .addZero ( 1.0 )
            .addZero ( -1.0 )
            .addPole ( p0 )
            .addPole ( p0.conjugate ( ) );

        final double dftModulusAtWn = polynomialFraction.evalaluateOnASinglePoint ( new Complex ( 0, wCenter ).exp () ).abs ();

        filter = CausalFilters.createFromPolynomialFraction ( 1.0 / dftModulusAtWn, polynomialFraction );

        return this;
    }

    @Override
    public double filter ( double in )
    {
        return filter.filter ( in );
    }

    @Override
    public void reset ( )
    {
        filter.reset ();
    }

    @Override
    public String toString ( )
    {
        return "BandpassBiQuadFilter{"
            + "bandwidthInHz="
            + bandwidthInHz
            + ", fsInHz="
            + fsInHz
            + ", centerFrequencyInHz="
            + centerFrequencyInHz
            + ", filter="
            + filter
            + '}';
    }
}
