/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;

import java.util.Arrays;

public class CausalConvolutionFilter
{
    protected final double memory[];
    protected final double taps[];

    // the place for the next sample
    protected int indexForNextSample;

    /**
     * @param aInFilterTaps for an identity filter: new double[] { 1.0, 0, 0, 0 }
     *
     */
    public static CausalConvolutionFilter create ( double aInFilterTaps [] )
    {
        return new CausalConvolutionFilter ( aInFilterTaps );
    }

    protected CausalConvolutionFilter ( double aInFilterTaps[] )
    {
        Preconditions.checkArgument ( aInFilterTaps.length > 0 );

        taps = Arrays.copyOf ( aInFilterTaps, aInFilterTaps.length );

        memory = new double[ taps.length ];
        Arrays.fill ( memory, 0 );

        indexForNextSample = 0;
    }

    public void putWithOffset ( double input, int offset )
    {
        memory[ FilterUtil.computeMod ( indexForNextSample + offset, memory.length ) ] = input;
    }

    public double putAndGet ( double input )
    {
        memory[indexForNextSample] = input;

        double lAccumulator = 0;

        for ( int i = 0; i < taps.length; ++i )
        {
            final int iShifted = FilterUtil.computeMod ( indexForNextSample - i, memory.length );
            lAccumulator += ( taps[i] * memory[iShifted] );
        }

        indexForNextSample = FilterUtil.computeMod( indexForNextSample + 1 , memory.length );

        return lAccumulator;
    }

    public void reset()
    {
        Arrays.fill ( memory, 0 );

        indexForNextSample = 0;
    }

    @Override
    public String toString ( )
    {
        return "CausalConvolutionFilter{" +
            "memory.length=" + memory.length +
            ", taps.length=" + taps.length +
            ", indexForNextSample=" + indexForNextSample +
            '}';
    }
}
