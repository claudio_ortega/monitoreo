/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;

public class DCBlockFilter implements ICausalDigitalFilter
{
    private double fsInHz;
    private double poleFreqInHz;
    private ICausalDigitalFilter filter;

    public static DCBlockFilter create()
    {
        return new DCBlockFilter ();
    }

    private DCBlockFilter ( )
    {
    }

    public DCBlockFilter setFsInHz ( double value )
    {
        fsInHz = value;
        return this;
    }

    public DCBlockFilter setPoleFreq ( double value )
    {
        poleFreqInHz = value;
        return this;
    }

    public DCBlockFilter start ( )
    {
        final double wp = FilterUtil.scaleFsIntoTwoPi ( poleFreqInHz, fsInHz );

        final PolynomialFraction polynomialFraction = PolynomialFraction
            .create ( )
            .addZero ( 1.0 )
            .addPole ( 1.0 - wp );

        filter = CausalFilters.createFromPolynomialFraction ( 1.0, polynomialFraction );

        return this;
    }

    @Override
    public double filter ( double in )
    {
        return filter.filter ( in );
    }

    @Override
    public void reset ( )
    {
        filter.reset ();
    }

    @Override
    public String toString ( )
    {
        return
            "DCBlockFilter{" +
            "fsInHz=" + fsInHz +
            ", poleFreqInHz=" + poleFreqInHz +
            ", filter=" + filter +
            '}';
    }
}
