/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.adaptive;

import com.poplar.monitor.imp.digitalfilters.linear.causal.FixedDelayFilter;

public class PowerIntegrator
{
    private final FixedDelayFilter delayFilter;
    private double cumulativeEnergy;

    public static PowerIntegrator create ( int length )
    {
        return new PowerIntegrator ( length );
    }

    private PowerIntegrator ( int length )
    {
        delayFilter = FixedDelayFilter.create ( length );
        cumulativeEnergy = 0;
    }

    public double add ( double inputEnergy )
    {
        cumulativeEnergy = cumulativeEnergy + inputEnergy - delayFilter.put ( inputEnergy );
        return cumulativeEnergy;
    }

    @Override
    public String toString ( )
    {
        return "PowerIntegrator{" +
            "delayFilter=" + delayFilter +
            ", cumulativeEnergy=" + cumulativeEnergy +
            '}';
    }
}
