/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.nonlinear;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RectifyingFilter implements ICausalDigitalFilter
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    private double lambda;
    private double holdOffInMs;
    private double lastOutput;
    private double fsInHz;
    private double tauInMs;

    private long holdOffInSamples;
    private long holdSampleCount;
    private boolean firstTime;
    private boolean debugOn;

    public static RectifyingFilter create ( )
    {
        return new RectifyingFilter ( );
    }

    private RectifyingFilter ( )
    {
        debugOn = false;
    }

    public RectifyingFilter setFsInHz ( double aInFsInHz )
    {
        fsInHz = aInFsInHz;
        return this;
    }

    public RectifyingFilter setHoldOffInMs ( double aInHoldOffInMs )
    {
        holdOffInMs = aInHoldOffInMs;
        return this;
    }

    public RectifyingFilter setTauInMs ( double aInTauInMs )
    {
        tauInMs = aInTauInMs;
        return this;
    }

    public RectifyingFilter setDebug ( boolean value )
    {
        debugOn = value;
        return this;
    }

    public RectifyingFilter start ()
    {
        holdOffInSamples = (long) ( ( holdOffInMs / 1000.0 ) * fsInHz );

        final double normalizedTau = FilterUtil.scaleFsIntoTwoPi ( 1.0 / ( tauInMs / 1000.0 ), fsInHz );

        lambda = 1.0 - normalizedTau;

        reset();

        return this;
    }

    @Override
    public double filter ( double in )
    {
        if ( firstTime )
        {
            firstTime = false;
            lastOutput = in;
            holdSampleCount = 0;
        }

        else if ( in > lastOutput )
        {
            lastOutput = in;
            holdSampleCount = 0;
        }

        else
        {
            holdSampleCount++;

            if ( holdSampleCount > holdOffInSamples )
            {
                lastOutput = lambda * lastOutput;
            }
            // else .. if we are inside the hold off period, keep the output clamped
        }

        return lastOutput;
    }

    @Override
    public void reset ( )
    {
        firstTime = true;
    }
}
