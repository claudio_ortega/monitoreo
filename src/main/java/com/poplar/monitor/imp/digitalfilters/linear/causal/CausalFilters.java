/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.digitalfilters.linear.causal;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import org.apache.commons.math3.complex.Complex;

import java.util.Arrays;
import java.util.List;

public class CausalFilters
{
    public static ICausalDigitalFilter createCascade ( ICausalDigitalFilter... digitalFilters )
    {
        return new FilterCascadeCausal ( digitalFilters );
    }

    private static class FilterCascadeCausal implements ICausalDigitalFilter
    {
        private ICausalDigitalFilter[] filters;

        private FilterCascadeCausal ( ICausalDigitalFilter[] digitalFilters )
        {
            filters = digitalFilters;
        }

        @Override
        public double filter ( double in )
        {
            double tmp = in;

            for ( ICausalDigitalFilter next : filters )
            {
                tmp = next.filter ( tmp );
            }

            return tmp;
        }

        @Override
        public void reset ( )
        {
            for ( ICausalDigitalFilter next : filters )
            {
                next.reset ( );
            }
        }

        @Override
        public String toString ( )
        {
            return "FilterCascadeCausal{" +
                "filters=" + Arrays.asList ( filters ) +
                '}';
        }
    }

    public static ICausalDigitalFilter createFromPolynomialFraction ( double gain, PolynomialFraction polynomialFraction )
    {
        // test causality
        Preconditions.checkArgument (
            polynomialFraction.getPoleList ( ).size ( ) >= polynomialFraction.getZeroList ( ).size ( ),
            "polynomial ratio should not have more zeros than poles, you should add " +
            ( polynomialFraction.getZeroList ( ).size ( ) - polynomialFraction.getPoleList ( ).size ( ) ) +
            " poles in zero for polynomialFraction: " +
                polynomialFraction );

        // transform the product of Pi (z-zi) into summation of a(i) * Zˆi,
        // with i positive from highest order down and including zero
        final Complex[] numeratorCoefficients = createCoefficients ( polynomialFraction.getZeroList ( ) );
        FilterUtil.checkAllAreSmall ( FilterUtil.getImaginaryComponents ( numeratorCoefficients ) );

        final Complex[] denominatorCoefficients = createCoefficients ( polynomialFraction.getPoleList ( ) );
        FilterUtil.checkAllAreSmall ( FilterUtil.getImaginaryComponents ( denominatorCoefficients ) );

        return CausalFilters.createCascade (
            ZerosOnlyCausalFilter.create ( FilterUtil.multiplyAndClone ( FilterUtil.shiftRightUpToNewLength ( FilterUtil.getRealComponents ( numeratorCoefficients ), denominatorCoefficients.length ), gain ) ),
            PolesOnlyCausalFilter.create ( FilterUtil.getRealComponents ( denominatorCoefficients ) )
        );
    }

    private static Complex[] createCoefficients ( List<Complex> roots )
    {
        // transform the product of Pi (z-zi) into summation of a(i) * Zˆi,
        // with i positive from highest order down and including zero
        final Complex[] a;

        if ( roots.size ( ) == 0 )
        {
            a = new Complex[]{ Complex.ONE };
        }
        else
        {
            a = new Complex[roots.size ( ) + 1];  // ie: (+1) say there is two roots, then the polinomial is orden 2, so it needs 3 coefs a': a0, a1, a2

            // start with [1, 0, 0..]
            a[0] = Complex.ONE;
            for ( int i = 1; i < a.length; i++ )
            {
                a[i] = Complex.ZERO;
            }

            // per each root: shift, multiplyInPlace and add
            for ( Complex next : roots )
            {
                final Complex[] tmp = FilterUtil.clone ( a );

                FilterUtil.shiftRightOne ( tmp );
                FilterUtil.multiplyInPlace ( tmp, next.negate ( ) );  // if the root is say 5, then the coef is -5, as in (x-5)
                FilterUtil.addAtoB ( tmp, a );
            }
        }

        return a;
    }
}
