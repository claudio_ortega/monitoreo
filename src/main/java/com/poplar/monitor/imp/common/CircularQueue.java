/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.common;

import com.google.common.base.Preconditions;
import java.util.Arrays;

public class CircularQueue<T>
{
    public interface ISlotFiller<T>
    {
        void fill ( T targetToBeFilled );
    }

    public interface ISlotInitializer<T>
    {
        T init ();
    }

    private T[] elements;
    private int head;
    private int tail;
    private int maxLength;
    private boolean allowDataOverrun;
    private boolean debugMode;

    public static <T> CircularQueue<T> create (  )
    {
        return new CircularQueue<>();
    }

    public CircularQueue<T> setSlots ( T[] aInElements )
    {
        elements = aInElements;
        maxLength = aInElements.length;
        return this;
    }

    public CircularQueue<T> setDebugMode ( boolean value )
    {
        debugMode = value;
        return this;
    }

    public CircularQueue<T> setAllowDataOverrun ( boolean value )
    {
        allowDataOverrun = value;
        return this;
    }

    private CircularQueue ()
    {
        maxLength = 0;
        elements = null;
        head = 0;
        tail = 0;
        debugMode = true;
        allowDataOverrun = false;
    }

    public CircularQueue reset()
    {
        head = 0;
        tail = 0;

        return this;
    }

    public int capacity ( )
    {
        return maxLength;
    }

    public int size ( )
    {
        if ( debugMode )
        {
            validateState ( );
        }

        final int size;

        if ( tail > head )
        {
            size = head - tail + maxLength;
        }

        else
        {
            size = head - tail;
        }

        if ( debugMode )
        {
            Preconditions.checkState ( ! ( size > maxLength ) );
        }

        return size;
    }

    public boolean isFull ( )
    {
        return ! ( size() < ( maxLength - 1) );
    }

    public boolean isEmpty ( )
    {
        return size() == 0;
    }

    public CircularQueue<T> initializeSlots( ISlotInitializer<T> ISlotInitializer )
    {
        for ( int i=0; i<maxLength; i++ )
        {
            elements[i] = ISlotInitializer.init ();
        }

        return this;
    }

    public CircularQueue<T> put ( ISlotFiller<T> filler )
    {
        if ( debugMode )
        {
            validateState ( );
        }

        if ( allowDataOverrun )
        {
            filler.fill ( elements[head] );

            if ( isFull () )
            {
                tail = ( tail + 1 ) % maxLength;
            }

            head = ( head + 1 ) % maxLength;
        }

        else
        {
            if ( debugMode )
            {
                Preconditions.checkState ( ! isFull ( ), "this:" + toString ( ) );
            }

            filler.fill ( elements[head] );

            head = ( head + 1 ) % maxLength;
        }

        return this;
    }

    public T consume ( )
    {
        if ( debugMode )
        {
            validateState ( );
            Preconditions.checkState ( ! isEmpty (), "this:" + toString () );
        }

        final T lRet = elements[tail];

        tail = ( tail + 1 ) % maxLength;

        return lRet;
    }

    public T peekLast ( int offset )
    {
        if ( debugMode )
        {
            Preconditions.checkState ( offset >= 0 );
            Preconditions.checkState ( offset < size ( ) );
        }

        return elements[ ( maxLength + tail + offset ) % maxLength ];
    }

    public T peekFirst ( int offset )
    {
        if ( debugMode )
        {
            Preconditions.checkState ( offset >= 0 );
            Preconditions.checkState ( offset < size ( ) );
        }

        return elements[ ( maxLength + head - offset - 1 ) % maxLength ];
    }

    private void validateState ( )
    {
        Preconditions.checkArgument ( head >= 0, "this:" + toString () );
        Preconditions.checkArgument ( tail >= 0, "this:" + toString () );
        Preconditions.checkArgument ( head < maxLength, "this:" + toString () );
        Preconditions.checkArgument ( tail < maxLength, "this:" + toString () );
    }

    @Override
    public String toString ( )
    {
        return "CircularQueue{" +
            "head=" + head +
            ", tail=" + tail +
            ", maxLength=" + maxLength +
            ", elements=" + Arrays.toString ( elements ) +
            '}';
    }
}
