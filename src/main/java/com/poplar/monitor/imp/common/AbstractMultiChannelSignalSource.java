/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.common;

import com.poplar.monitor.api.ISignalSource;
import com.poplar.monitor.api.IMultiChannelVesselFilter;
import com.poplar.monitor.api.MultiChannelVessel;
import com.poplar.monitor.api.SignalSourceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractMultiChannelSignalSource implements ISignalSource
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    // how often we intent to invoke the driver for new samples
    protected static final int PRODUCTION_PERIOD_MILLISECONDS = 30;

    // how long we can generate without consumption before we fill up all buffers
    private static final int MAX_QUEUE_CAPACITY_MILLISECONDS = 1_000;

    // how long could be the separation in between consecutive calls to the driver, before losing samples from it
    private static final int MAX_BUFFER_CAPACITY_MILLISECONDS = 500;

    protected final SignalSourceBuilder signalSourceBuilder;

    protected final AtomicBoolean started;
    protected final AtomicLong droppedSamples;

    protected final CircularQueue<MultiChannelVesselSlot> vesselsQueue;
    protected final int queueLengthLimit;
    protected final int bufferCapacityInSamplesPerChannel;

    protected final SignalVesselSlotFiller signalVesselSlotFiller;

    private IMultiChannelVesselFilter vesselFilter;

    @Override
    public boolean getIsStarted( )
    {
        return started.get();
    }

    @Override
    public void flush ( )
    {
        vesselsQueue.reset();
    }

    @Override
    public void attachVesselFilter ( IMultiChannelVesselFilter aInVesselFilter )
    {
        vesselFilter = aInVesselFilter;
    }

    protected void filterVessels ( MultiChannelVessel vessel )
    {
        if ( vesselFilter != null )
        {
            vesselFilter.compute ( vessel );
        }
    }

    protected static class SignalVesselSlotFiller implements CircularQueue.ISlotFiller<MultiChannelVesselSlot>
    {
        private MultiChannelVessel multiChannelVessel;

        public void setVessels( MultiChannelVessel aInVessels )
        {
            multiChannelVessel = aInVessels;
        }

        @Override
        public void fill ( MultiChannelVesselSlot targetToBeFilled )
        {
            targetToBeFilled.fillMultiChannelVesselIntoVesselSlot ( multiChannelVessel );
        }
    }

    protected AbstractMultiChannelSignalSource( SignalSourceBuilder aInSignalSourceBuilder  )
    {
        signalSourceBuilder = aInSignalSourceBuilder;

        vesselFilter = null;

        signalVesselSlotFiller = new SignalVesselSlotFiller ();

        bufferCapacityInSamplesPerChannel = (int) ( MAX_BUFFER_CAPACITY_MILLISECONDS * signalSourceBuilder.getSamplingFrequencyEnum ( ).getValue () / 1_000.0 );
        logger.info( "bufferCapacityInSamplesPerChannel:" + bufferCapacityInSamplesPerChannel );

        queueLengthLimit = MAX_QUEUE_CAPACITY_MILLISECONDS / PRODUCTION_PERIOD_MILLISECONDS;
        logger.info( "queueLengthLimit:" + queueLengthLimit);

        started = new AtomicBoolean( false );
        droppedSamples = new AtomicLong( 0 );

        vesselsQueue = CircularQueue.<MultiChannelVesselSlot>create ()
            .setSlots ( new MultiChannelVesselSlot[ queueLengthLimit ] )
            .setAllowDataOverrun ( true )
            .setDebugMode ( false )
            .initializeSlots ( ( ) -> new MultiChannelVesselSlot ( signalSourceBuilder.getNChannels ( ), bufferCapacityInSamplesPerChannel ) );
    }

    @Override
    public boolean getSamplesAreAvailable ( )
    {
        synchronized ( vesselsQueue )
        {
            return ! vesselsQueue.isEmpty ( );
        }
    }

    @Override
    public void fillOutWithNextSamples ( MultiChannelVessel aInOutSignalVessel )
    {
        logger.trace( "fillOutWithNextSamples -- BEGIN" );

        synchronized ( vesselsQueue )
        {
            aInOutSignalVessel.setNumberOfSamples ( 0 );

            while ( ! vesselsQueue.isEmpty ( ) )
            {
                final MultiChannelVessel lOldest = vesselsQueue.consume ( ).getMultiChannelVessel ( );

                final boolean lDataFitted = MultiChannelVessel.appendFromFirstIntoSecond ( lOldest, aInOutSignalVessel );

                if ( ! lDataFitted )
                {
                    logger.info( "samples dropped, lOldest.getNumberOfSamples:" + lOldest.getNumberOfSamples (  ) );
                }
            }

            if ( logger.isTraceEnabled () )
            {
                logger.trace ( "vesselsQueue.size(2): " + vesselsQueue.size ( ) );
            }
        }
    }

    @Override
    public long getDroppedSamplesCount()
    {
        return droppedSamples.get();
    }

    @Override
    public int getNumberOfChannels()
    {
        return signalSourceBuilder.getNChannels ( );
    }

    @Override
    public long getSamplingFrequencyInHz ( )
    {
        return signalSourceBuilder.getSamplingFrequencyEnum ( ).getValue ();
    }

    @Override
    public void stopSampling()
    {
        logger.debug( "stopSampling -- BEGIN" );

        synchronized ( vesselsQueue )
        {
            started.set(false);
        }

        logger.debug( "stopSampling -- END" );
    }

    @Override
    public void startSampling()
    {
        logger.debug( "startSampling -- BEGIN" );

        synchronized ( vesselsQueue )
        {
            started.set(true);
        }

        logger.debug( "startSampling -- END" );
    }

    @Override
    public void close() throws IOException
    {
        logger.debug( "close() -- BEGIN" );
        logger.debug( "close() -- END" );
    }

    @Override
    public MultiChannelVessel createSignalVessels()
    {
        return MultiChannelVessel.create (
            signalSourceBuilder.getNChannels ( ),
            bufferCapacityInSamplesPerChannel );
    }
}
