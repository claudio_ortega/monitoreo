/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.common;

import com.google.common.base.Preconditions;

public class ChannelVessel
{
    private final int allocatedLength;
    private final float[] values;
    private int length;

    public ChannelVessel ( int aInMaxLength )
    {
        allocatedLength = aInMaxLength;
        values = new float[allocatedLength];
        length = 0;
    }

    public void setSample ( int aInIndex, float aInValue )
    {
        Preconditions.checkArgument ( aInIndex < allocatedLength, "aInIndex:" + aInIndex + ", allocatedLength" + allocatedLength );
        values[aInIndex] = aInValue;
    }

    public void setLength ( int aInNewLength )
    {
        Preconditions.checkArgument ( aInNewLength <= allocatedLength, "aInNewLength:" + aInNewLength + ", allocatedLength:" + allocatedLength);
        length = aInNewLength;
    }

    public float[] getSamples ()
    {
        return values;
    }

    public int getLength ()
    {
        return length;
    }

    public int getAllocatedLength()
    {
        return allocatedLength;
    }

    public static boolean appendFromFirstIntoSecond ( ChannelVessel aInVesselFrom, ChannelVessel aInVesselTo )
    {
        Preconditions.checkNotNull( aInVesselFrom );
        Preconditions.checkNotNull( aInVesselTo );

        final boolean lSamplesFit;
        final int lCountToAppend;

        if ( ( aInVesselFrom.getLength() + aInVesselTo.getLength() ) > aInVesselTo.getAllocatedLength() )
        {
            lCountToAppend = aInVesselTo.getAllocatedLength() - aInVesselTo.getLength();
            lSamplesFit = false;
        }
        else
        {
            lCountToAppend = aInVesselFrom.getLength();
            lSamplesFit = true;
        }

        if ( lCountToAppend > 0)
        {
            final float[] lSamplesFrom = aInVesselFrom.getSamples();
            final float[] lSamplesTo = aInVesselTo.getSamples();

            Preconditions.checkNotNull(lSamplesFrom);
            Preconditions.checkNotNull(lSamplesTo);

            final int lOffset = aInVesselTo.getLength();
            for (int j = 0; j < lCountToAppend; j++)
            {
                aInVesselTo.setSample(lOffset + j, lSamplesFrom[j]);
            }

            aInVesselTo.setLength(lOffset + lCountToAppend);
        }

        return lSamplesFit;
    }
}
