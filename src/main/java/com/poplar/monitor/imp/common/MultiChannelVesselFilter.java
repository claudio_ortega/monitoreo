/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.common;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.IMultiChannelVesselFilter;
import com.poplar.monitor.api.MultiChannelVessel;
import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.adaptive.ICancellationFilter;
import com.poplar.monitor.imp.digitalfilters.adaptive.NormalizedLMSCancellationFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.KaiserWindow;
import com.poplar.monitor.imp.digitalfilters.linear.causal.ZerosOnlyCausalFilter;
import com.poplar.monitor.imp.javafx.ApplicationConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicBoolean;

public class MultiChannelVesselFilter implements IMultiChannelVesselFilter
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    private ICancellationFilter[] adaptiveFilters;
    private ZerosOnlyCausalFilter[] lowPassFilters;
    private boolean useLowPassFiltering;
    private final AtomicBoolean started;
    private final Object guard;

    public static MultiChannelVesselFilter create ()
    {
        return new MultiChannelVesselFilter ();
    }

    private MultiChannelVesselFilter ( )
    {
        started = new AtomicBoolean ( false );
        useLowPassFiltering = false;
        lowPassFilters = null;
        adaptiveFilters = null;
        guard = new Object();
    }

    @Override
    public int compute ( MultiChannelVessel aInOutSignalVessel )
    {
        synchronized ( guard )
        {
            final int nSamples = aInOutSignalVessel.getNumberOfSamples ( );

            for ( int chIx = 0; chIx < aInOutSignalVessel.getNumberOfChannels ( ); chIx++ )
            {
                final float[] samples = aInOutSignalVessel.getChannelVessel ( chIx ).getSamples ( );

                if ( started.get ( ) )
                {
                    for ( int i = 0; i < nSamples; i++ )
                    {
                        double tmp = ( double ) samples[i];

                        tmp = adaptiveFilters[chIx].filter ( tmp );

                        if ( useLowPassFiltering )
                        {
                            tmp = lowPassFilters[chIx].filter ( tmp );
                        }

                        aInOutSignalVessel.getChannelVessel ( chIx ).setSample ( i, ( float ) tmp );
                    }
                }
            }

            return nSamples;
        }
    }

    @Override
    public void reset ( )
    {
        synchronized ( guard )
        {
            Preconditions.checkState ( started.get ( ) );

            for ( ICancellationFilter next : adaptiveFilters )
            {
                next.reset ( );
            }

            if ( lowPassFilters != null )
            {
                for ( ICausalDigitalFilter next : lowPassFilters )
                {
                    next.reset ( );
                }
            }
        }
    }

    @Override
    public IMultiChannelVesselFilter setApplicationConfig ( ApplicationConfig newValue )
    {
        synchronized ( guard )
        {
            logger.info ( "setApplicationConfig() -- BEGIN" );

            Preconditions.checkNotNull ( newValue );

            if ( adaptiveFilters == null || adaptiveFilters.length != newValue.getSignalSourceBuilder ( ).getNChannels ( ) )
            {
                adaptiveFilters = new ICancellationFilter[newValue.getSignalSourceBuilder ( ).getNChannels ( )];

                for ( int channelIndex = 0; channelIndex < adaptiveFilters.length; channelIndex++ )
                {
                    //adaptiveFilters[channelIndex] = ToneCancellationFilter.create ( );
                    adaptiveFilters[channelIndex] = NormalizedLMSCancellationFilter.create ( );
                    adaptiveFilters[channelIndex].reset ();
                }
            }

            for ( int channelIndex = 0; channelIndex < adaptiveFilters.length; channelIndex++ )
            {
                adaptiveFilters[channelIndex]
                    .setCancellationFreqInHz ( newValue.getFilterBuilder ( ).getNotchFrequencyEnum ( ).getValue ( ) )
                    .setSamplingFreqInHz ( newValue.getSignalSourceBuilder ( ).getSamplingFrequencyEnum ( ).getValue ( ) )
                    .setMu ( newValue.getFilterBuilder ( ).getAdaptiveCoefficient ( ) )
                    .setBypass ( ! newValue.getFilterBuilder ( ).getUseAdaptiveFiltering ( ) )
                    .start ( );

                logger.info ( "channelIndex: " + channelIndex + ", adaptiveFilters[.]: " + adaptiveFilters[channelIndex] );
            }

            useLowPassFiltering = newValue.getFilterBuilder ( ).getUseLowPassFiltering ( );

            if ( useLowPassFiltering )
            {
                if ( lowPassFilters == null || lowPassFilters.length != newValue.getSignalSourceBuilder ( ).getNChannels ( ) )
                {
                    lowPassFilters = new ZerosOnlyCausalFilter[newValue.getSignalSourceBuilder ( ).getNChannels ( )];

                    for ( int channelIndex = 0; channelIndex < lowPassFilters.length; channelIndex++ )
                    {
                        lowPassFilters[channelIndex] = ZerosOnlyCausalFilter.create (
                            KaiserWindow.computeKaiserWindow (
                                KaiserWindow.PassTypeEnum.lowPass,
                                newValue.getSignalSourceBuilder ( ).getSamplingFrequencyEnum ( ).getValue ( ),
                                40.0,       // att at stop
                                1,          // ripple at pass
                                900.0,      // pass Hz
                                990.0,      // stop Hz
                                null
                            )
                        );

                        logger.info ( "channelIndex: " + channelIndex + ", lowPassFilters[.]: " + lowPassFilters[channelIndex] );
                    }
                }
            }

            started.set ( true );

            logger.info ( "setApplicationConfig() -- END" );

            return this;
        }
    }
}
