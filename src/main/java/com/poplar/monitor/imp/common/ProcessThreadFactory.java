/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.common;

import com.google.common.base.Preconditions;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class ProcessThreadFactory implements ThreadFactory
{
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String prefix;

    public ProcessThreadFactory( Class aInClassForPrefixing )
    {
        this ( aInClassForPrefixing.getSimpleName() );
    }

    private ProcessThreadFactory( String aInStringForPrefixing )
    {
        prefix = aInStringForPrefixing;
    }

    /**
     * Creates a new thread.
     * @param aInRunnable command to be executed.
     * @return Thread instance.
     */
    @SuppressWarnings( "NullableProblems" )
    @Override
    public Thread newThread( Runnable aInRunnable )
    {
        Preconditions.checkNotNull(aInRunnable != null);
        return new Thread( aInRunnable, prefix + "-" + threadNumber.getAndIncrement() );
    }
}
