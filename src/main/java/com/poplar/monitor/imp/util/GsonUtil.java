/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.util;

import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class GsonUtil
{

    private final static GsonBuilder GSON_BUILDER = new GsonBuilder ()
        .setPrettyPrinting ()
        .serializeNulls ()
        .excludeFieldsWithoutExposeAnnotation ()
        .registerTypeAdapter ( File.class, new FileAdapter () );

    private GsonUtil()
    {
    }

    /**
     *
     * @param aInFilePath
     * @param aInObject
     * @param <T>
     * @throws java.io.IOException
     */
    public static <T> void writeJsonToFile(
        File aInFilePath,
        T aInObject )  throws IOException
    {
        final File lTmpFile = File.createTempFile ( "tmp-config-", ".json" );

        FileUtil.writeLinesIntoFile (
                CollectionsUtil.createImmutableList ( GSON_BUILDER.create().toJson ( aInObject ) ),
                lTmpFile );

        FileUtil.createDirIfDoesNotExist( aInFilePath.getParentFile() );

        FileUtil.copyTransformedFileContent (
                lTmpFile,
                aInFilePath, ( FileUtil.OneLineTransformer ) ( aInLine, aInLineNumber ) -> aInLine.replace( "\\u003c", "<" ).replace ( "\\u003e", ">" ) );

        FileUtil.deletePlainFileIfExists ( lTmpFile, true );
    }

    public static <T> T readJsonFromFile( File aInFile, Class<T> aInClass ) throws Exception
    {
        try(
            final FileInputStream lFileInputStream = new FileInputStream ( aInFile );
            final InputStreamReader lInputStreamReader = new InputStreamReader ( lFileInputStream, "UTF-8" );
            final BufferedReader lBufferedReader = new BufferedReader ( lInputStreamReader ) )
        {
            final StringBuilder lSb = new StringBuilder (  );

            for ( String lNextLine: FileUtil.getMatchingLinesFromReader ( lBufferedReader, null ) )
            {
                if ( ! lNextLine.trim().startsWith ( "#" ) )
                {
                    lSb.append ( lNextLine );
                }
            }

            return GSON_BUILDER.create ().fromJson ( lSb.toString (), aInClass );
        }
    }

    public static <T> T clone ( T aInObject )
    {
        return (T) GSON_BUILDER
            .create ()
            .fromJson ( GSON_BUILDER.create ().toJson ( aInObject ),
                aInObject.getClass () );
    }

    // this adapter fixes a known bug in GSon library, see https://code.google.com/p/google-gson/issues/detail?id=414
    public static class FileAdapter extends TypeAdapter<File>
    {
        @Override
        public File read ( JsonReader reader ) throws IOException
        {
            final File lRet;

            if ( reader.peek () == JsonToken.NULL )
            {
                reader.nextNull ();
                lRet = null;
            }

            else
            {
                reader.beginObject ();
                reader.nextName ();
                lRet = new File ( reader.nextString () );
                reader.endObject ();
            }

            return lRet;
        }

        @Override
        public void write ( JsonWriter writer, File value ) throws IOException
        {
            throw new IllegalStateException ( "only implements read() for fixing a known bug: https://code.google.com/p/google-gson/issues/detail?id=414" );
        }
    }
}
