/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.imp.util;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.javafx.DummyForResourceLoad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.net.URL;

public class SystemUtil
{
    private final static Logger logger = LogManager.getLogger ( SystemUtil.class );

    // you shall not instantiate this class
    private SystemUtil ()
    {
    }

    public static class Constants
    {
        public static final String LINE_SEPARATOR = System.getProperty( "line.separator" );
        public static final String PATH_SEPARATOR = System.getProperty( "path.separator" );
        public static final String FILE_SEPARATOR = System.getProperty( "file.separator" );
        public static final String USER_HOME_DIR = System.getProperty( "user.home" );
        public static final String USER_NAME = System.getProperty( "user.name" );
    }

    public static List<String> getJavaExtraArguments ()
    {
        return ManagementFactory.getRuntimeMXBean().getInputArguments ();
    }

    public static File getCurrentDirectory ()
    {
        return FileUtil.getFileWithAbsolutePath ( new File ( "." ) );
    }

    public static void sleepMsec( long aInMsec )
    {
        try
        {
            Thread.sleep ( aInMsec );
        }
        catch ( Exception ex )
        {
            // do nothing
        }
    }

    public static String getPIDForThisProcess ( String aInDefaultIfNotFound )
    {
        final String lThisProcessName = ManagementFactory.getRuntimeMXBean ().getName ();
        final int lIndexOfAt = lThisProcessName.indexOf ( "@" );

        final String lReturn;

        if ( lIndexOfAt >= 0 )
        {
            lReturn = lThisProcessName.substring ( 0, lIndexOfAt );
        }
        else
        {
            lReturn = aInDefaultIfNotFound;
        }

        return lReturn;
    }

    public static String getEnvironmentOrSysProperty ( String name )
    {
        String ret = System.getenv ( name );

        if ( ret == null )
        {
            ret = System.getProperty ( name );
        }

        return ret;
    }

    public static SimpleDateFormat getUTCHighPrecisionDateFormat()
    {
        final SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat ( "yyyy-MM-dd.HH:mm:ss.SSSZZZ" );
        lSimpleDateFormat.setTimeZone ( TimeZone.getTimeZone ( "UTC" ) );
        return lSimpleDateFormat;
    }

    public static SimpleDateFormat getHighPrecisionDateFormat()
    {
        return new SimpleDateFormat ( "yyyy-MM-dd.HH:mm:ss.SSSZZZ" );
    }

    public enum OSType { Windows, Mac, Linux }

    public static OSType getOSType()
    {
        final String lOSName = System.getProperty ( "os.name" ).toLowerCase ( );

        final OSType lRet;

        if ( lOSName.contains ( "win" ) )
        {
            lRet = OSType.Windows;
        }

        else if ( lOSName.contains ( "mac" ) )
        {
            lRet = OSType.Mac;

        }

        else if ( lOSName.contains ( "inux" ) )
        {
            lRet = OSType.Linux;
        }

        else
        {
            lRet = null;
        }

        Preconditions.checkState ( lRet != null, "lOSName:" + lOSName );

        return lRet;
    }

    /**
     * @param aInBootstrapClass         this class must exist in the same jar file from where we are going to extract the file that goes by aInSimpleFileName
     * @param aInResourceNameToExtract  the name of the file to be extract from the jar
     * @param aInOutputPathToCreate     the place in the FS to put the extracted file contents
     * @throws IOException
     */
    public static void extractFileFromJarFile (
        Class aInBootstrapClass,
        String aInResourceNameToExtract,
        File aInOutputPathToCreate )
        throws IOException
    {
        Preconditions.checkArgument ( ! aInOutputPathToCreate.exists ( ), "file :[" + aInOutputPathToCreate + "] should not exist" );

        final Set<URL> lFoundURLSet = listResourceFilesFromAllSources(
            aInBootstrapClass,
            aInResourceNameToExtract );

        logger.debug( "file [" + aInResourceNameToExtract + "] was found in all these URLs: " + lFoundURLSet );

        Preconditions.checkState ( lFoundURLSet.size ( ) > 0, "file [" + aInResourceNameToExtract + "] should be included in the jars loadable from the class loader, but it was not found" );

        if ( lFoundURLSet.size() > 1 )
        {
            logger.warn(
                "file [" + aInResourceNameToExtract +
                    "] should be included in only one of the jars loadable from the class loader, " +
                    "but it was found in all these URLs: " + lFoundURLSet );
        }

        final InputStream lIS = lFoundURLSet.iterator().next().openStream();

        Preconditions.checkState ( lIS != null, "input stream is null" );

        final BufferedInputStream lBIS = new BufferedInputStream( lIS );

        final FileOutputStream lFOS = new FileOutputStream( aInOutputPathToCreate, false );
        final BufferedOutputStream lBOS = new BufferedOutputStream( lFOS );

        FileUtil.copyStream( lBIS, lBOS );

        lFOS.flush ();
        lFOS.close ();

        lIS.close ();

        Preconditions.checkState ( aInOutputPathToCreate.exists ( ), "file :[" + aInOutputPathToCreate + "] should exist" );
    }

    public static Set<URL> listResourceFilesFromAllSources (
        Class aInClass,
        String aInSimpleFileName )
        throws IOException
    {
        return CollectionsUtil.concatenateSets (
            getFileURLFromSystemClassLoader( aInSimpleFileName ),
            getFileURLFromClassLoader( aInClass.getClassLoader(), aInSimpleFileName ),
            getFileURLFromClassLoader( Thread.currentThread().getContextClassLoader(), aInSimpleFileName ) );
    }

    private static Set<URL> getFileURLFromSystemClassLoader( String aInSimpleFileName )
        throws IOException
    {
        final Set<URL> lFindings = new HashSet<>();

        final Enumeration<URL> lEnumeration = ClassLoader.getSystemResources ( aInSimpleFileName );

        while ( lEnumeration.hasMoreElements() )
        {
            final URL lNextURL = lEnumeration.nextElement();

            logger.debug( "using static getSystemResources(), found file [" + aInSimpleFileName + "] inside [" + lNextURL.getFile() + "]");

            lFindings.add ( lNextURL );
        }

        return lFindings;
    }

    private static Set<URL> getFileURLFromClassLoader(
        ClassLoader aInClassLoader,
        String aInSimpleFileName )
        throws IOException
    {
        final Set<URL> lFindings = new HashSet<> ();

        final Enumeration<URL> lEnumeration = aInClassLoader.getResources ( aInSimpleFileName );

        while ( lEnumeration.hasMoreElements() )
        {
            final URL lNextURL = lEnumeration.nextElement ();

            logger.debug( "using classloader: [" + aInClassLoader.getClass () + ", found file [" + aInSimpleFileName + "] inside [" + lNextURL.getFile() + "]");

            lFindings.add ( lNextURL );
        }

        return lFindings;
    }

    public static String getGitVersion ( boolean aInIncludeHash )
    {
        String lReturn;

        try
        {
            final String lGitDescribe = FileUtil.getContentFromResourceFile ( DummyForResourceLoad.class, "git-describe.txt",  100 )
                .replace ( "\"", "" )
                .replace ( "\n", "" )
                .trim ( );

            if ( aInIncludeHash )
            {
                lReturn = lGitDescribe;
            }

            else
            {
                final int hyphenIndex = lGitDescribe.lastIndexOf ( "-" );

                if ( hyphenIndex == -1 )
                {
                    lReturn = lGitDescribe;
                }

                else
                {
                    lReturn = lGitDescribe.substring ( 0, lGitDescribe.lastIndexOf ( "-" ) );
                }
            }
        }

        catch ( Exception ex )
        {
            lReturn = "unable to determine git version";
            logger.error ( ex.getMessage (), ex );
        }

        return lReturn;
    }
}
