/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.poplar.monitor.imp.audio.SignalSourceAudio;
import com.poplar.monitor.imp.simulators.CalibrationSignalGenerator;
import com.poplar.monitor.imp.simulators.FileSignalGenerator;
import com.poplar.monitor.imp.simulators.PowerNoiseGenerator;
import com.poplar.monitor.imp.simulators.PureSineGenerator;

public class SignalSourceFactory
{
    public static ISignalSource create ( SignalSourceBuilder aInBuilder )
            throws Exception
    {
        final ISignalSource lSource;

        if ( aInBuilder.getSignalSourceType () == SignalSourceBuilder.SignalSourceType.simulator )
        {
            if ( aInBuilder.getSimulatorType ( ) == SignalSourceBuilder.SimulatorType.powerLineNoise )
            {
                lSource = PowerNoiseGenerator.create ( aInBuilder );
            }

            else if ( aInBuilder.getSimulatorType () == SignalSourceBuilder.SimulatorType.calibrationMinMax )
            {
                lSource = CalibrationSignalGenerator.create ( aInBuilder );
            }

            else if ( aInBuilder.getSimulatorType () == SignalSourceBuilder.SimulatorType.pureSine )
            {
                lSource = PureSineGenerator.create ( aInBuilder );
            }

            else
            {
                throw new IllegalArgumentException ( "wrong simulator type" );
            }
        }

        else if ( aInBuilder.getSignalSourceType () == SignalSourceBuilder.SignalSourceType.signalFile )
        {
            lSource = FileSignalGenerator.create ( aInBuilder );
        }

        else if ( aInBuilder.getSignalSourceType () == SignalSourceBuilder.SignalSourceType.soundDriver )
        {
            lSource = SignalSourceAudio.create( aInBuilder ).start();
        }

        else
        {
            throw new IllegalArgumentException( "wrong source type" );
        }

        return lSource;
    }
}
