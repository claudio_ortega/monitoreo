/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.google.gson.annotations.Expose;

public class SignalSourceReadOnlyInfo implements ISignalSourceReadOnly
{
    @Expose
    private String description;
    @Expose
    private long sampleSpaceUpperBound;
    @Expose
    private long sampleSpaceLowerBound;
    @Expose
    private float physicalSpaceUpperBound;
    @Expose
    private float physicalSpaceLowerBound;
    @Expose
    private String physicalSpaceUnit;
    @Expose
    private long samplingFrequency;
    @Expose
    private int numberOfChannels;

    private SignalSourceReadOnlyInfo()
    {
    }

    private static SignalSourceReadOnlyInfo create ( )
    {
        return new SignalSourceReadOnlyInfo ();
    }

    public static SignalSourceReadOnlyInfo create( ISignalSource source )
    {
        return SignalSourceReadOnlyInfo
            .create ()
            .setDescription ( source.getDescription ( ) )
            .setNumberOfChannels ( source.getNumberOfChannels ( ) )
            .setPhysicalSpaceLowerBound ( source.getPhysicalSpaceLowerBound ( ) )
            .setPhysicalSpaceUpperBound ( source.getPhysicalSpaceUpperBound ( ) )
            .setSampleSpaceLowerBound ( source.getSampleSpaceLowerBound ( ) )
            .setSampleSpaceUpperBound ( source.getSampleSpaceUpperBound ( ) )
            .setSamplingFrequency ( source.getSamplingFrequencyInHz ( ) )
            .setPhysicalSpaceUnit ( source.getPhysicalSpaceUnit () );
    }

    @Override
    public String getDescription ( )
    {
        return description;
    }

    public SignalSourceReadOnlyInfo setDescription ( String description )
    {
        this.description = description;
        return this;
    }

    @Override
    public int getNumberOfChannels ( )
    {
        return numberOfChannels;
    }

    public SignalSourceReadOnlyInfo setNumberOfChannels ( int numberOfChannels )
    {
        this.numberOfChannels = numberOfChannels;
        return this;
    }

    @Override
    public float getPhysicalSpaceLowerBound ( )
    {
        return physicalSpaceLowerBound;
    }

    public SignalSourceReadOnlyInfo setPhysicalSpaceLowerBound ( float physicalSpaceLowerBound )
    {
        this.physicalSpaceLowerBound = physicalSpaceLowerBound;
        return this;
    }

    @Override
    public String getPhysicalSpaceUnit ( )
    {
        return physicalSpaceUnit;
    }

    public SignalSourceReadOnlyInfo setPhysicalSpaceUnit ( String physicalSpaceUnit )
    {
        this.physicalSpaceUnit = physicalSpaceUnit;
        return this;
    }

    @Override
    public float getPhysicalSpaceUpperBound ( )
    {
        return physicalSpaceUpperBound;
    }

    public SignalSourceReadOnlyInfo setPhysicalSpaceUpperBound ( float physicalSpaceUpperBound )
    {
        this.physicalSpaceUpperBound = physicalSpaceUpperBound;
        return this;
    }

    @Override
    public long getSampleSpaceLowerBound ( )
    {
        return sampleSpaceLowerBound;
    }

    public SignalSourceReadOnlyInfo setSampleSpaceLowerBound ( long sampleSpaceLowerBound )
    {
        this.sampleSpaceLowerBound = sampleSpaceLowerBound;
        return this;
    }

    @Override
    public long getSampleSpaceUpperBound ( )
    {
        return sampleSpaceUpperBound;
    }

    public SignalSourceReadOnlyInfo setSampleSpaceUpperBound ( long sampleSpaceUpperBound )
    {
        this.sampleSpaceUpperBound = sampleSpaceUpperBound;
        return this;
    }

    @Override
    public long getSamplingFrequencyInHz ( )
    {
        return samplingFrequency;
    }

    public SignalSourceReadOnlyInfo setSamplingFrequency ( long samplingFrequency )
    {
        this.samplingFrequency = samplingFrequency;
        return this;
    }
}
