/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.common.ChannelVessel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class MultiChannelVessel
{
    private static AtomicLong sequence = new AtomicLong ( 0 );

    private final List<ChannelVessel> channelVessels;
    private final int numberOfChannels;
    private long lastSampleTime;
    private long tracer;

    private MultiChannelVessel(
        int aInChannels,
        int aInBufferCapacityInSamplesPerChannel )
    {
        tracer = sequence.incrementAndGet ();
        lastSampleTime = 0;
        numberOfChannels = aInChannels;
        channelVessels = new ArrayList<> ();

        for ( int i=0; i<aInChannels; i++ )
        {
            channelVessels.add ( new ChannelVessel ( aInBufferCapacityInSamplesPerChannel ) );
        }
    }

    public static MultiChannelVessel create(
        int aInChannels,
        int aInBufferCapacityInSamplesPerChannel )
    {
        return new MultiChannelVessel ( aInChannels, aInBufferCapacityInSamplesPerChannel );
    }

    public ChannelVessel getChannelVessel( int channelIndex )
    {
        Preconditions.checkArgument ( channelIndex >= 0 && channelIndex < numberOfChannels );

        return channelVessels.get( channelIndex );
    }

    public MultiChannelVessel setNumberOfSamples( int aInNewLength )
    {
        for ( ChannelVessel next : channelVessels )
        {
            next.setLength ( aInNewLength );
        }

        return this;
    }

    public MultiChannelVessel setLastSampleTime ( long aInReadingTime )
    {
        lastSampleTime = aInReadingTime;
        return this;
    }

    public MultiChannelVessel setTracer ( long aInMarker )
    {
        tracer = aInMarker;
        return this;
    }

    public long getLastSampleTime ( )
    {
        return lastSampleTime;
    }

    public int getNumberOfSamples()
    {
        return channelVessels.get( 0 ).getLength ();
    }

    public int getNumberOfChannels ( )
    {
        return numberOfChannels;
    }

    public void getOneSampleOnTimeIndex( int count, float[] lEnsemble )
    {
        for ( int i=0; i<numberOfChannels; i++ )
        {
            lEnsemble[i] = channelVessels.get ( i ).getSamples ( )[count];
        }
    }

    public long getTracer ( )
    {
        return tracer;
    }

    public static boolean appendFromFirstIntoSecond ( MultiChannelVessel aInVesselsFrom, MultiChannelVessel aInVesselsTo )
    {
        boolean lReturn = true;

        for ( int i=0; i<aInVesselsFrom.numberOfChannels; i++ )
        {
            lReturn &= ChannelVessel.appendFromFirstIntoSecond (
                aInVesselsFrom.channelVessels.get ( i ),
                aInVesselsTo.channelVessels.get ( i )
            );
        }

        aInVesselsTo.setLastSampleTime ( aInVesselsFrom.getLastSampleTime () );
        aInVesselsTo.setTracer ( aInVesselsFrom.getTracer ( ) );

        return lReturn;
    }
}
