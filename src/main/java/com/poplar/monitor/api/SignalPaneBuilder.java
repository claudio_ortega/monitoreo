/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.google.gson.annotations.Expose;
import com.poplar.monitor.imp.javafx.LanguageManager;
import com.poplar.monitor.imp.util.SystemUtil;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class SignalPaneBuilder
{
    public enum SweepModeEnum
    {
        continuous, triggered;

        @Override
        public String toString ( )
        {
            return LanguageManager.getSingleton ( ).getValue ( name () );
        }
    }

    @Expose
    private List<SignalChannelBuilder> signalChannelBuilders;
    @Expose
    private int triggerChannelIndex;
    @Expose
    private int maxNumberOfDisplayedPointsPerSec;
    @Expose
    private String description;
    @Expose
    private double displayTimeMsPerDivision;
    @Expose
    private SweepModeEnum sweepModeEnum;
    @Expose
    private double triggerFreezeTimeInSeconds;
    @Expose
    private double displayModeThresholdMSecPerDiv;
    @Expose
    private double triggerSoundVolume;
    @Expose
    private int numberOfSavedScreens;
    @Expose
    private String screensSavingDirectory;
    @Expose
    private boolean screensSavingWindowEnabled;
    @Expose
    private boolean monitorStartAutomatically;

    private final int verticalGridCount;
    private final int horizontalGridCount;


    private SignalPaneBuilder()
    {
        signalChannelBuilders = new LinkedList<>();
        description = "n/a";
        displayTimeMsPerDivision = 0f;
        triggerFreezeTimeInSeconds = 0f;
        triggerChannelIndex = 0;
        sweepModeEnum = SweepModeEnum.continuous;
        displayModeThresholdMSecPerDiv = 0;
        maxNumberOfDisplayedPointsPerSec = 0;
        triggerSoundVolume = 0;
        numberOfSavedScreens = 0;
        screensSavingWindowEnabled = false;
        monitorStartAutomatically = false;
        screensSavingDirectory = new File ( SystemUtil.getCurrentDirectory ( ), "screens" ).getAbsolutePath ();
        verticalGridCount = 10;
        horizontalGridCount = 10;
    }

    public int getHorizontalGridCount ( )
    {
        return horizontalGridCount;
    }

    public int getVerticalGridCount ( )
    {
        return verticalGridCount;
    }

    public int getNumberOfSavedScreens ( )
    {
        return numberOfSavedScreens;
    }

    public boolean getMonitorStartAutomatically ( )
    {
        return monitorStartAutomatically;
    }

    public SignalPaneBuilder setMonitorStartAutomatically ( boolean value )
    {
        monitorStartAutomatically = value;
        return this;
    }

    public SignalPaneBuilder setNumberOfSavedScreens ( int value )
    {
        numberOfSavedScreens = value;
        screensSavingWindowEnabled = numberOfSavedScreens > 0;
        return this;
    }

    public boolean getIsScreensSavingWindowEnabled ( )
    {
        screensSavingWindowEnabled = numberOfSavedScreens > 0;
        return screensSavingWindowEnabled;
    }

    public String getScreensSavingDirectory ( )
    {
        return screensSavingDirectory;
    }

    public SignalPaneBuilder setScreensSavingDirectory ( String value )
    {
        screensSavingDirectory = value;
        return this;
    }

    public double getTriggerSoundVolume ( )
    {
        return triggerSoundVolume;
    }

    public SignalPaneBuilder setTriggerSoundVolume ( double value )
    {
        triggerSoundVolume = value;
        return this;
    }

    public double getDisplayModeThresholdMSecPerDiv ( )
    {
        return displayModeThresholdMSecPerDiv;
    }

    public SignalPaneBuilder setDisplayModeThresholdMSecPerDiv ( double value )
    {
        displayModeThresholdMSecPerDiv = value;
        return this;
    }

    public double getTriggerFreezeTimeInSeconds ( )
    {
        return triggerFreezeTimeInSeconds;
    }

    public SignalPaneBuilder setTriggerFreezeTimeInSeconds ( double aInValue )
    {
        triggerFreezeTimeInSeconds = aInValue;
        return this;
    }

    public int getMaxNumberOfDisplayedPointsPerSec ( )
    {
        return maxNumberOfDisplayedPointsPerSec;
    }

    public SignalPaneBuilder setMaxNumberOfDisplayedPointsPerSec ( int value )
    {
        maxNumberOfDisplayedPointsPerSec = value;
        return this;
    }

    public static SignalPaneBuilder init()
    {
        return new SignalPaneBuilder ();
    }

    public SignalPaneBuilder create ()
    {
        return this;
    }

    public int getTriggerChannelIndex()
    {
        return triggerChannelIndex;
    }

    public SignalPaneBuilder setTriggerChannelIndex( int aInTriggerChannelIndex )
    {
        triggerChannelIndex = aInTriggerChannelIndex;
        return this;
    }

    public List<SignalChannelBuilder> getSignalChannelBuilders ()
    {
        return signalChannelBuilders;
    }

    public SignalPaneBuilder setSignalChannelBuilders ( List<SignalChannelBuilder> aInValues )
    {
        signalChannelBuilders.clear();
        signalChannelBuilders.addAll(aInValues);
        return this;
    }

    public String getDescription ()
    {
        return description;
    }

    public SignalPaneBuilder setDescription ( String aInDescription )
    {
        description = aInDescription;
        return this;
    }

    public SweepModeEnum getSweepModeEnum ()
    {
        return sweepModeEnum;
    }

    public SignalPaneBuilder setSweepModeEnum ( SweepModeEnum aInSweepModeEnum )
    {
        sweepModeEnum = aInSweepModeEnum;
        return this;
    }

    public double getDisplayTimeMsPerDivision ( )
    {
        return displayTimeMsPerDivision;
    }

    public SignalPaneBuilder setDisplayTimeMsPerDivision ( double aInDisplayTimeInSeconds )
    {
        displayTimeMsPerDivision = aInDisplayTimeInSeconds;
        return this;
    }

    @Override
    public String toString ( )
    {
        return "SignalPaneBuilder{" +
            "signalChannelBuilders=" + signalChannelBuilders +
            ", triggerChannelIndex=" + triggerChannelIndex +
            ", maxNumberOfDisplayedPointsPerSec=" + maxNumberOfDisplayedPointsPerSec +
            ", description='" + description + '\'' +
            ", displayTimeMsPerDivision=" + displayTimeMsPerDivision +
            ", sweepModeEnum=" + sweepModeEnum +
            ", triggerFreezeTimeInSeconds=" + triggerFreezeTimeInSeconds +
            ", displayModeThresholdMSecPerDiv=" + displayModeThresholdMSecPerDiv +
            ", triggerSoundVolume=" + triggerSoundVolume +
            ", numberOfSavedScreens=" + numberOfSavedScreens +
            ", screensSavingWindowEnabled=" + screensSavingWindowEnabled +
            ", monitorStartAutomatically=" + monitorStartAutomatically +
            ", screensSavingDirectory=" + screensSavingDirectory +
            '}';
    }
}
