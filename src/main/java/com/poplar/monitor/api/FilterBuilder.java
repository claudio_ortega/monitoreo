/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.google.gson.annotations.Expose;

public class FilterBuilder
{
    public enum LineFrequencyEnum
    {
        value50 ( 50 ),
        value60 ( 60 );

        private final int value;

        LineFrequencyEnum ( int aInNumber )
        {
            value = aInNumber;
        }

        public int getValue ( )
        {
            return value;
        }

        @Override
        public String toString ( )
        {
            return Integer.toString ( value ) + " Hz";
        }
    }

    @Expose
    private boolean useAdaptiveFiltering;

    @Expose
    private boolean useLowPassFiltering;

    @Expose
    private LineFrequencyEnum notchFrequencyEnum;

    @Expose
    private double adaptiveCoefficient;

    public static FilterBuilder init()
    {
        return new FilterBuilder ();
    }

    public FilterBuilder create ()
    {
        useAdaptiveFiltering = false;
        notchFrequencyEnum = LineFrequencyEnum.value50;
        adaptiveCoefficient = 0;
        return this;
    }

    public boolean getUseLowPassFiltering ( )
    {
        return useLowPassFiltering;
    }

    public boolean getUseAdaptiveFiltering ( )
    {
        return useAdaptiveFiltering;
    }

    public FilterBuilder setUseAdaptiveFiltering ( boolean aInValue )
    {
        useAdaptiveFiltering = aInValue;
        return this;
    }

    public LineFrequencyEnum getNotchFrequencyEnum ( )
    {
        return notchFrequencyEnum;
    }

    public FilterBuilder setNotchFrequencyEnum ( LineFrequencyEnum aInValue )
    {
        notchFrequencyEnum = aInValue;
        return this;
    }

    public double getAdaptiveCoefficient ( )
    {
        return adaptiveCoefficient;
    }

    public FilterBuilder setAdaptiveCoefficient ( double aInValue )
    {
        adaptiveCoefficient = aInValue;
        return this;
    }

    @Override
    public String toString ( )
    {
        return "FilterBuilder{" +
            "useAdaptiveFiltering=" + useAdaptiveFiltering +
            ", notchFrequencyEnum=" + notchFrequencyEnum +
            ", adaptiveCoefficient=" + adaptiveCoefficient +
            '}';
    }
}
