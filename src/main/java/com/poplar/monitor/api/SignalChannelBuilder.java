/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.Expose;
import javafx.scene.paint.Color;

public class SignalChannelBuilder
{
    @Expose
    private String signalName;
    @Expose
    private boolean visible;
    @Expose
    private double sensitivityMilliVoltsPerDiv;
    @Expose
    private String rgbAsStringColor;
    @Expose
    private double triggerLevelPercentage;

    // DO NOT EXPOSE !!!
    private Color color;

    private SignalChannelBuilder()
    {
        signalName = "n/a";
        visible = true;
        sensitivityMilliVoltsPerDiv = 5.0;
        rgbAsStringColor = "n/a";
        color = null;
        triggerLevelPercentage = 0;
    }

    public static SignalChannelBuilder init()
    {
        return new SignalChannelBuilder();
    }

    public SignalChannelBuilder create ()
    {
        color = iGetColorFromConfigString ( rgbAsStringColor );
        return this;
    }

    public double getTriggerLevelPercentage()
    {
        return triggerLevelPercentage;
    }

    public SignalChannelBuilder setTriggerLevelPercentage( double ainValue)
    {
        triggerLevelPercentage = ainValue;
        return this;
    }

    public boolean getVisible ()
    {
        return visible;
    }

    public SignalChannelBuilder setVisible ( boolean aInVisible )
    {
        visible = aInVisible;
        return this;
    }

    public String getSignalName()
    {
        return signalName;
    }

    public SignalChannelBuilder setSignalName(String aInSignalName)
    {
        signalName = aInSignalName;
        return this;
    }

    public double getSensitivityMilliVoltsPerDiv ( )
    {
        return sensitivityMilliVoltsPerDiv;
    }

    public SignalChannelBuilder setSensitivityMilliVoltsPerDiv ( double aInValue )
    {
        sensitivityMilliVoltsPerDiv = aInValue;
        return this;
    }

    public Color getRgbColor ( )
    {
        // because Gson will not initialize this
        if ( color == null )
        {
            color = iGetColorFromConfigString ( rgbAsStringColor );
        }

        return color;
    }

    public SignalChannelBuilder setRgbAsStringColor ( String aInRgbColor )
    {
        rgbAsStringColor = aInRgbColor;
        return this;
    }

    @Override
    public String toString ( )
    {
        return "SignalChannelBuilder{" +
            //"@" + System.identityHashCode ( this ) + ", " +
            "signalName='" + signalName + '\'' +
            ", visible=" + visible +
            ", sensitivityMilliVoltsPerDiv=" + sensitivityMilliVoltsPerDiv +
            ", triggerLevelPercentage=" + triggerLevelPercentage +
            ", rgbAsStringColor='" + rgbAsStringColor + '\'' +
            '}';
    }

    /**
     *
     * @param aInRGBEncodedColor    "#ff0000"
     * @return                      Color.RED
     */
    private static Color iGetColorFromConfigString( String aInRGBEncodedColor )
    {
        Preconditions.checkArgument ( aInRGBEncodedColor.length ( ) == 7, "aInRGBEncodedColor: " + aInRGBEncodedColor );
        Preconditions.checkArgument ( "#".equals ( aInRGBEncodedColor.substring ( 0, 1 ) ), "aInRGBEncodedColor: " + aInRGBEncodedColor );

        final int red = Integer.parseInt ( aInRGBEncodedColor.substring ( 1,3 ), 16 );
        final int green = Integer.parseInt ( aInRGBEncodedColor.substring ( 3, 5 ), 16 );
        final int blue = Integer.parseInt ( aInRGBEncodedColor.substring ( 5,7 ), 16 );

        return Color.rgb( red, green, blue );
    }
}
