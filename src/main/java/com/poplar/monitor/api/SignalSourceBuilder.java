/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.monitor.api;

import com.google.gson.annotations.Expose;
import com.poplar.monitor.imp.javafx.LanguageManager;

public class SignalSourceBuilder
{
    public enum SignalSourceType
    {
        simulator, soundDriver, signalFile;

        @Override
        public String toString ( )
        {
            return LanguageManager.getSingleton().getValue ( name () );
        }
    }

    public enum SimulatorType
    {
        powerLineNoise, calibrationMinMax, pureSine;

        @Override
        public String toString ( )
        {
            return LanguageManager.getSingleton().getValue ( name () );
        }
    }

    public enum SignalFileOriginType
    {
        internal_1, selectable;

        @Override
        public String toString ( )
        {
            return LanguageManager.getSingleton().getValue ( name () );
        }
    }

    public enum SamplingFrequencyEnum
    {
        value8000 ( 8000 ),
        value16000 ( 16000 ),
        value32000 ( 32000 ),
        value48000 ( 48000 ),
        value11025 ( 11025 ),
        value22050 ( 22050 ),
        value44100 ( 44100 );

        private final int value;

        SamplingFrequencyEnum ( int aInNumber )
        {
            value = aInNumber;
        }

        @Override
        public String toString ( )
        {
            return Integer.toString ( value ) + " Hz";
        }

        public int getValue ( )
        {
            return value;
        }
    }

    @Expose
    private SamplingFrequencyEnum samplingFrequencyEnum;

    @Expose
    private SignalSourceType signalSourceType;

    @Expose
    private int nChannels;

    @Expose
    private String audioMixerDescriptionRegexp;

    @Expose
    private SimulatorType simulatorType;

    @Expose
    private SignalFileOriginType signalFileOriginType;

    @Expose
    private String signalFilePathName;

    @Expose
    private long audioMixerLastScanningTime;

    @Expose
    private int periodicResetIntervalInSeconds;

    @Expose
    private double physicalGain;

    public int getPeriodicResetIntervalInSeconds ( )
    {
        return periodicResetIntervalInSeconds;
    }

    public SignalSourceBuilder setPeriodicResetIntervalInSeconds ( int aInPeriodicResetIntervalInSeconds )
    {
        periodicResetIntervalInSeconds = aInPeriodicResetIntervalInSeconds;
        return this;
    }

    public double getPhysicalGain ( )
    {
        return physicalGain;
    }

    private SignalSourceBuilder()
    {
        // there are no good defaults
        nChannels = 0;
        samplingFrequencyEnum = null;
        signalSourceType = null;
        simulatorType = null;
        signalFileOriginType = null;
        signalFilePathName = null;
        audioMixerDescriptionRegexp = "";
        audioMixerLastScanningTime = 0;
        periodicResetIntervalInSeconds = 0;
        physicalGain = 440.0;
    }

    public static SignalSourceBuilder init()
    {
        return new SignalSourceBuilder();
    }

    public SignalSourceBuilder create ()
    {
        return this;
    }

    public String getSignalFilePathName ( )
    {
        return signalFilePathName;
    }

    public SignalSourceBuilder setSignalFilePathName ( String value )
    {
        signalFilePathName = value;
        return this;
    }

    public SignalFileOriginType getSignalFileOriginType ( )
    {
        return signalFileOriginType;
    }

    public SignalSourceBuilder setSignalFileOriginType ( SignalFileOriginType value )
    {
        signalFileOriginType = value;
        return this;
    }

    public SimulatorType getSimulatorType ( )
    {
        return simulatorType;
    }

    public SignalSourceBuilder setSimulatorType ( SimulatorType value )
    {
        simulatorType = value;
        return this;
    }

    public long getAudioMixerLastScanningTime ( )
    {
        return audioMixerLastScanningTime;
    }

    public SignalSourceBuilder setAudioMixerLastScanningTime ( long aInAudioMixerLastSettingTime )
    {
        audioMixerLastScanningTime = aInAudioMixerLastSettingTime;
        return this;
    }

    public String getAudioMixerDescriptionRegexp()
    {
        return audioMixerDescriptionRegexp;
    }

    public SignalSourceBuilder setAudioMixerDescriptionRegexp(String aInAudioMixerDescriptionRegexp)
    {
        audioMixerDescriptionRegexp = aInAudioMixerDescriptionRegexp;
        return this;
    }

    public SamplingFrequencyEnum getSamplingFrequencyEnum ( )
    {
        return samplingFrequencyEnum;
    }

    public SignalSourceBuilder setSamplingFrequencyEnum ( SamplingFrequencyEnum aInSamplingFrequency )
    {
        samplingFrequencyEnum = aInSamplingFrequency;
        return this;
    }

    public int getNChannels ( )
    {
        return nChannels;
    }

    public SignalSourceBuilder setnChannels(int aInnChannels)
    {
        nChannels = aInnChannels;
        return this;
    }

    public SignalSourceType getSignalSourceType ()
    {
        return signalSourceType;
    }

    public SignalSourceBuilder setSignalSourceType ( SignalSourceType aInSignalSourceType )
    {
        signalSourceType = aInSignalSourceType;
        return this;
    }

    @Override
    public String toString ( )
    {
        return "SignalSourceBuilder{" +
            "audioMixerDescriptionRegexp='" + audioMixerDescriptionRegexp + '\'' +
            ", samplingFrequencyEnum=" + samplingFrequencyEnum +
            ", signalSourceType=" + signalSourceType +
            ", nChannels=" + nChannels +
            ", simulatorType=" + simulatorType +
            ", signalFileOriginType=" + signalFileOriginType +
            ", signalFilePathName='" + signalFilePathName + '\'' +
            ", audioMixerLastScanningTime=" + audioMixerLastScanningTime +
            ", periodicResetIntervalInSeconds=" + periodicResetIntervalInSeconds +
            ", physicalGain=" + physicalGain +
            '}';
    }
}
