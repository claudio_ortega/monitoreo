/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.ISignalSource;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.api.SignalSourceFactory;

public class TestSignalSourceSimulator
    extends AbstractTestSignalSource
{
    public TestSignalSourceSimulator(String testName)
    {
        super ( testName );
    }

    public void test00 () throws Exception
    {
        final ISignalSource lSignalSource = SignalSourceFactory
            .create (
            SignalSourceBuilder
                 .init ( )
                    .setSignalSourceType ( SignalSourceBuilder.SignalSourceType.simulator )
                    .setSimulatorType ( SignalSourceBuilder.SimulatorType.powerLineNoise )
                    .setSignalFileOriginType ( SignalSourceBuilder.SignalFileOriginType.internal_1 )
                    .setnChannels ( 2 )
                    .setSamplingFrequencyEnum ( SignalSourceBuilder.SamplingFrequencyEnum.value44100 )
                    .setAudioMixerDescriptionRegexp ( ".*" )
                    .setPeriodicResetIntervalInSeconds ( 7_200 )
                    .create ( ) );

        Preconditions.checkState ( lSignalSource.isAvailable () );

        testCommon ( lSignalSource, 50, 100, null );
    }
}
