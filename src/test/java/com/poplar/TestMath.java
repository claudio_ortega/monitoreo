/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestMath extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass() );

    public TestMath ( String testName )
    {
        super ( testName );
    }

    public void test00 ()
    {
        final int N = 10_000_000;

        double x = 0;

        final long time1 = System.currentTimeMillis ( );

        for ( int i=0; i<N; i++ )
        {
            x = x + Math.sin( i );
        }
        final long time2 = System.currentTimeMillis ( );

        for ( int i=0; i<N; i++ )
        {
            x = x + Math.cos ( i );
        }
        final long time3 = System.currentTimeMillis ( );

        for ( int i=0; i<N; i++ )
        {
            x = x + Math.sqrt ( i );
        }
        final long time4 = System.currentTimeMillis ( );

        logger.info( "time in sin():" + ( time2 - time1 ) + " ms" );
        logger.info( "time in cos():" + ( time3 - time2 ) + " ms" );
        logger.info( "time in sqrt():" + ( time4 - time3 ) + " ms" );
    }
}
