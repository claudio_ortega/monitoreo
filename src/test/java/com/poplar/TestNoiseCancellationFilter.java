/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.digitalfilters.adaptive.AdaptiveLinearCombiner;
import com.poplar.monitor.imp.digitalfilters.adaptive.PowerIntegrator;
import com.poplar.monitor.imp.digitalfilters.linear.causal.FixedDelayFilter;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;

public class TestNoiseCancellationFilter extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    public TestNoiseCancellationFilter ( String testName )
    {
        super ( testName );
    }

    public void test01 ( ) throws Exception
    {
        final FixedDelayFilter filter = FixedDelayFilter.create ( 3 );

        final double[] in = new double[] { 1, 2, -1, -2, 0, 0, 0, 0, 0, 0 };
        final double[] out = new double[in.length];

        for ( int i = 0; i < in.length; i++ )
        {
            out[i] = filter.put ( in[i] );
            logger.info ( "i: " + i + ", in:" + in [ i ] + ", out:" + out[i] );
        }

        Preconditions.checkState ( Arrays.equals ( out, new double[]{ 0, 0, 0, 1, 2, -1, -2, 0, 0, 0 } ) );
    }

    public void test02 ( ) throws Exception
    {
        final AdaptiveLinearCombiner filter = AdaptiveLinearCombiner.create ( 3 );

        final double[] in = new double[] { 1, 2, -1, -2, 0, 0, 0, 0, 0, 0 };
        final double[] out = new double[in.length];

        for ( int i = 0; i < in.length; i++ )
        {
            out[i] = filter.putAndGet ( in[i] );
            filter.combineInputIntoTaps ( 1.0 );

            logger.info ( "i: " + i + ", in:" + in [ i ] + ", out:" + out[i] );
            logger.info ( "filter:" + filter );
        }

        Preconditions.checkState ( Arrays.equals ( out, new double[]{ 0, 2, -1, -5, -7, -4, 0, 0, 0, 0 } ) );
    }

    public void test03 ( ) throws Exception
    {
        final PowerIntegrator ec = PowerIntegrator.create ( 5 );

        final double[] in = new double[] { 2, 2, 2, 2, 0, 0, 0, 0, 0, 0 };
        final double[] out = new double[in.length];

        for ( int i = 0; i < in.length; i++ )
        {
            out[i] = ec.add ( in[i] );
            logger.info ( "i: " + i + ", in:" + in [ i ] + ", out:" + out[i] );
            logger.info ( "ec:" + ec );
        }

        Preconditions.checkState ( Arrays.equals ( out, new double[]{ 2, 4, 6, 8, 8, 6, 4, 2, 0, 0 } ) );
    }

    public void test04 ( ) throws Exception
    {
        final PowerIntegrator ec = PowerIntegrator.create ( 5 );

        final long ITER = 100_000_000L;

        for ( long i = 0; i < ITER; i++ )
        {
            final double y = ec.add ( 3.0E-6 );

            if ( i % ( ITER / 1000 ) == 0 )
            {
                logger.info ( "i: " + i + ", y:" + y );
            }
        }
    }
}
