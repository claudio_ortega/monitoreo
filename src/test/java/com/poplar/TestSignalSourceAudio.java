/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.audio.SignalSourceAudio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public class TestSignalSourceAudio extends AbstractTestSignalSource
{
    private final Logger logger = LogManager.getLogger ( getClass () );

    public TestSignalSourceAudio ( String testName )
    {
        super ( testName );
    }

    public void testListCompatibleDevices() throws Exception
    {
        for ( int i=0; i<5; i++ )
        {
            final List<String> lDeviceList = SignalSourceAudio.getAllCompatibleMixersInSystem (
                SignalSourceBuilder
                    .init ( )
                    .setSignalSourceType ( SignalSourceBuilder.SignalSourceType.soundDriver )
                    .setnChannels ( 2 )
                    .setSamplingFrequencyEnum ( SignalSourceBuilder.SamplingFrequencyEnum.value44100 )
                    .setAudioMixerDescriptionRegexp ( ".*" )
                    .create ( ),
                false
            );

            logger.info ( "lDeviceList: " + lDeviceList );
        }
    }

    public void testConversion () throws Exception
    {
        Preconditions.checkState ( 0 == SignalSourceAudio.testGetSampleFromBytes ( ( byte ) 0, ( byte ) 0 ) );
        Preconditions.checkState ( 1 == SignalSourceAudio.testGetSampleFromBytes ( ( byte ) 0, ( byte ) 1 ) );
        Preconditions.checkState ( 256 == SignalSourceAudio.testGetSampleFromBytes ( ( byte ) 1, ( byte ) 0 ) );
        Preconditions.checkState ( 257 == SignalSourceAudio.testGetSampleFromBytes ( ( byte ) 1, ( byte ) 1 ) );
        Preconditions.checkState ( -1 == SignalSourceAudio.testGetSampleFromBytes ( ( byte ) 0xff, ( byte ) 0xff ) );
        Preconditions.checkState ( -2 == SignalSourceAudio.testGetSampleFromBytes ( ( byte ) 0xff, ( byte ) 0xfe ) );
    }
}

