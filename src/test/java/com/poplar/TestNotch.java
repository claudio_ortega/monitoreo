/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalFilters;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.PolesOnlyCausalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.ZerosOnlyCausalFilter;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestNotch extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass() );

    public TestNotch ( String testName )
    {
        super ( testName );
    }

    public void test00 () throws Exception
    {
        final ICausalDigitalFilter digitalFilter = createNotchFilter ( 4000, 10, 16000 );

        for ( int i=0; i<20; i++ )
        {
            logger.info ( "***, " + i + ", output:" + digitalFilter.filter ( FilterUtil.getUnitDeltaFilterOutput ( i ) ) );
        }
    }

    private static ICausalDigitalFilter createNotchFilter ( double w0Hz, double bwHz, double fsHz )
    {
        final double W0 = 2 * Math.PI * w0Hz / fsHz;
        final double BW = 2 * Math.PI * bwHz / fsHz;
        final double r = 1 - BW;

        return CausalFilters.createCascade (
            ZerosOnlyCausalFilter.create ( new double[]{ 1, ( - 2 * Math.cos ( W0 ) ), 1 } ),
            PolesOnlyCausalFilter.create ( new double[]{ 1, ( - 2 * r * Math.cos ( W0 ) ), ( r * r ) } )
        );
    }
}
