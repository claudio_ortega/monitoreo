/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalFilters;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.PolesOnlyCausalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.ZerosOnlyCausalFilter;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestGenericIirFilter extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    public TestGenericIirFilter ( String testName )
    {
        super ( testName );
    }

    public void test00 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 1, 0, 0 }, new double[]{ 1, 0, 0 } );

        for ( int i = 0; i < 10; i++ )
        {
            logger.info ( i + ", output:" + filter.filter ( ( i == 0 ) ? 1 : 0 ) );
        }
    }

    public void test01 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = ZerosOnlyCausalFilter.create ( createImpulseResponseWithNoise ( 5, 1.0, 1e-6 ) );

        for ( int i = 0; i < 10; i++ )
        {
            logger.info ( i + ", output:" + filter.filter ( ( i < 4 ) ? 1 : 0 ) );
        }
    }

    public void test02 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 0 }, new double[]{ 1, - 1, 0 } );

        for ( int i = 0; i < 10; i++ )
        {
            logger.info ( "***, " + i + ", output:" + filter.filter ( ( i == 0 ) ? 1 : 0 ) );
        }
    }

    public void test03 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 1 }, new double[]{ 1, - 1 } );

        for ( int i = 0; i < 5; i++ )
        {
            logger.info ( "***, " + i + ", output:" + filter.filter ( ( i == 0 ) ? 1 : 0 ) );
        }
    }

    public void test04 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 1 }, new double[]{ 1, 1 } );

        for ( int i = 0; i < 5; i++ )
        {
            logger.info ( "***, " + i + ", output:" + filter.filter ( ( i == 0 ) ? 1 : 0 ) );
        }
    }

    public void test05 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 1 }, new double[]{ 1, - 0.9f } );

        for ( int i = 0; i < 15; i++ )
        {
            logger.info ( "***, " + i + ", output:" + filter.filter ( ( i == 0 ) ? 1 : 0 ) );
        }
    }

    public void test06 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 1, 0, 1 }, new double[]{ 1 } );

        for ( int i = 0; i < 15; i++ )
        {
            final double x = Math.sin ( i * 0.5 * Math.PI );
            final double y = filter.filter ( x );

            logger.info ( String.format ( "***, i:%2d, x:%.3f, y:%.3f", i, x, y ) );
        }
    }

    public void test07 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = createCascadedPolesAndZerosFilter ( new double[]{ 1, 0, 1 }, new double[]{ 1, 0, 0.999f } );

        for ( int i = 0; i < 15000; i++ )
        {
            final double x = Math.sin ( i * 0.5 * Math.PI );
            final double y = filter.filter ( x );

            logger.info ( String.format ( "***, i:%2d, x:%.3f, y:%.3f", i, x, y ) );
        }
    }

    public void test08 ( )
        throws Exception
    {
        final double W0 = 0; //Math.PI / 8;
        final double r = 0.25;

        final double a = - 2 * r * Math.cos( W0 );
        final double b = r * r;

        final ICausalDigitalFilter filter = PolesOnlyCausalFilter.create ( new double[]{ 1, a, b } );

        for ( int i = 0; i < 10; i++ )
        {
            final double x = FilterUtil.getUnitDeltaFilterOutput ( i );
            final double y = filter.filter ( x );

            logger.info ( String.format ( "***, i:%2d, x:%.3f, y:%.5f", i, x, y ) );
        }
    }

    public void test09 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = PolesOnlyCausalFilter.create ( new double[]{ 1, 2, 3 } );

        for ( int i = 0; i < 10; i++ )
        {
            final double x = FilterUtil.getUnitDeltaFilterOutput ( i );

            final double y = filter.filter ( x );

            logger.info ( String.format ( "***, i:%2d, x:%.3f, y:%.5f", i, x, y ) );
        }
    }

    private static ICausalDigitalFilter createCascadedPolesAndZerosFilter ( double firCoef[], double iirCoef[] )
    {
        return CausalFilters.createCascade (
            ZerosOnlyCausalFilter.create ( firCoef ),
            PolesOnlyCausalFilter.create ( iirCoef )
        );
    }

    private static double[] createImpulseResponseWithNoise(
        int length,
        double firstSample,
        double othersSamples )
    {
        final double[] resp = new double[length];

        resp[0] = firstSample;

        for ( int i=1; i<length; i++ )
        {
            resp[i] = othersSamples * ( Math.random () - 0.5 );
        }

        return resp;
    }
}

