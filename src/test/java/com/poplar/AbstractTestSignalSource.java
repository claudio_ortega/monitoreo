/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.ISignalSource;
import com.poplar.monitor.api.MultiChannelVessel;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;

public class AbstractTestSignalSource extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    protected AbstractTestSignalSource ( String testName )
    {
        super ( testName );
    }

    protected void testCommon (
        final ISignalSource aInSignalSource,
        final int aInIterations,
        final int aInProductionPeriodMSec,
        final PrintStream aInPrintStream )
            throws Exception
    {
        logger.info ( "test00 -- BEGIN" );

        final MultiChannelVessel lSignalVessels = aInSignalSource.createSignalVessels ( );

        aInSignalSource.startSampling ( );

        long lTotalReadSamples = 0;

        for ( int i = 0; i < aInIterations; i++ )
        {
            Thread.sleep ( aInProductionPeriodMSec );

            int loopCount = 0;
            while ( aInSignalSource.getSamplesAreAvailable ( ) )
            {
                Thread.sleep ( 1 );

                aInSignalSource.fillOutWithNextSamples ( lSignalVessels );

                final int lReadSamples = lSignalVessels.getNumberOfSamples ();

                if ( aInPrintStream != null )
                {
                    for ( int j = 0; j < lSignalVessels.getNumberOfChannels ( ); j++ )
                    {
                        aInPrintStream.println (
                            lSignalVessels.getChannelVessel ( 0 ).getSamples ( )[j] +
                            ", " +
                            lSignalVessels.getChannelVessel ( 1 ).getSamples ( )[j] );
                    }
                }

                final long lDroppedSamples = aInSignalSource.getDroppedSamplesCount ( );

                logger.debug ( "lReadSamples:" + lReadSamples );
                logger.debug ( "lDroppedSamples:" + lDroppedSamples );
                logger.debug ( "lSignalVessel[0].getLength():" + lSignalVessels.getChannelVessel ( 0 ).getLength ( ) );
                logger.debug ( "lSignalVessel[1].getLength():" + lSignalVessels.getChannelVessel ( 1 ).getLength ( ) );

                lTotalReadSamples += lReadSamples;
                loopCount++;
            }

            if ( loopCount > 1 )
            {
                logger.debug ( "multiple loops.." );
            }
        }

        logger.info ( "lTotalReadSamples(final):" + lTotalReadSamples );
        logger.info ( "lDroppedSamples(final):" + aInSignalSource.getDroppedSamplesCount ( ) );

        final long lTestReadSamplesTotal = ( ( aInIterations - 1 ) * aInProductionPeriodMSec / 1_000 ) * aInSignalSource.getSamplingFrequencyInHz ( );

        Preconditions.checkState (
            lTotalReadSamples >= lTestReadSamplesTotal,
            "mismatch in number of samples, lTotalReadSamples: " + lTotalReadSamples + ", aInTestReadSamplesTotal: " + lTestReadSamplesTotal );

        Preconditions.checkState (
            aInSignalSource.getDroppedSamplesCount ( ) == 0,
            "dropped count should zero, but is: " + aInSignalSource.getDroppedSamplesCount ( ) );

        logger.info ( "before stopSampling()" );
        aInSignalSource.stopSampling ( );
        logger.info ( "after stopSampling()" );

        aInSignalSource.close ( );

        if ( aInPrintStream != null )
        {
            aInPrintStream.close ( );
        }

        logger.info ( "test00 -- END" );
    }
}
