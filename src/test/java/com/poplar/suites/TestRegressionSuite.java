/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar.suites;

import com.poplar.TestCausalFilter;
import com.poplar.TestCircularQueue;
import com.poplar.TestFir;
import com.poplar.TestFloatVsDouble;
import com.poplar.TestGenericIirFilter;
import com.poplar.TestGson;
import com.poplar.TestKaiser;
import com.poplar.TestMath;
import com.poplar.TestNoiseCancellationFilter;
import com.poplar.TestNotch;
import com.poplar.TestSignalSourceAudio;
import com.poplar.TestSignalSourceSimulator;
import com.poplar.TestSignalVesselUtil;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith (Suite.class)
@Suite.SuiteClasses(
    {
        TestCausalFilter.class,
        TestCircularQueue.class,
        TestFir.class,
        TestFloatVsDouble.class,
        TestGenericIirFilter.class,
        TestGson.class,
        TestKaiser.class,
        TestMath.class,
        TestNoiseCancellationFilter.class,
        TestNotch.class,
        TestSignalSourceAudio.class,
        TestSignalSourceSimulator.class,
        TestSignalVesselUtil.class,
    } )

public class TestRegressionSuite
{
}
