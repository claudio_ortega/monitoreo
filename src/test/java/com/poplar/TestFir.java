/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.FilterUtil;
import com.poplar.monitor.imp.digitalfilters.linear.causal.ZerosOnlyCausalFilter;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestFir extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass() );

    public TestFir ( String testName )
    {
        super ( testName );
    }

    public void test00 () throws Exception
    {
        final ZerosOnlyCausalFilter fir = ZerosOnlyCausalFilter.create ( new double[]{ 0, 1 } );

        for ( int i=0; i<10; i++ )
        {
            logger.info ( i + ", output:" + fir.filter ( FilterUtil.getUnitDeltaFilterOutput ( i ) ) );
        }
    }

    public void test01 () throws Exception
    {
        final ZerosOnlyCausalFilter f = ZerosOnlyCausalFilter.create ( new double[]{ 0, 1 } );

        for ( int i=0; i<10; i++ )
        {
            logger.info ( i + ", output:" + f.filter ( FilterUtil.getUnitStepFilterOutput ( i ) ) );
        }
    }

    public void test02 () throws Exception
    {
        final ZerosOnlyCausalFilter fir = ZerosOnlyCausalFilter.create ( new double[]{ 1 } );

        for ( int i=0; i<10; i++ )
        {
            logger.info ( i + ", output:" + fir.filter ( FilterUtil.getUnitDeltaFilterOutput ( i ) ) );
        }
    }

    public void test03 () throws Exception
    {
        final ZerosOnlyCausalFilter fir = ZerosOnlyCausalFilter.create ( new double[]{ 1 } );

        for ( int i=0; i<10; i++ )
        {
            logger.info ( i + ", output:" + fir.filter ( FilterUtil.getUnitDeltaFilterOutput ( i ) ) );
        }
    }

    public void test04 ( )
        throws Exception
    {
        final ICausalDigitalFilter filter = ZerosOnlyCausalFilter.create ( new double[]{ 1, 2, 3, 4 } );

        for ( int i = 0; i < 10; i++ )
        {
            final double x = 10 + FilterUtil.getUnitRampFilterOutput ( i );
            final double y = filter.filter ( x );

            logger.info ( String.format ( "***, i:%2d, x:%.3f, y:%.3f", i, x, y ) );
        }
    }
}
