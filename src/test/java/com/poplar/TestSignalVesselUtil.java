/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.poplar.monitor.api.ISignalSource;
import com.poplar.monitor.api.MultiChannelVessel;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.api.SignalSourceFactory;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestSignalVesselUtil
    extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass() );

    public TestSignalVesselUtil(String testName)
    {
        super ( testName );
    }

    public void test00 () throws Exception
    {
        logger.info("test00 -- BEGIN");

        final ISignalSource lSignalSource = SignalSourceFactory.create (
            SignalSourceBuilder
                 .init ( )
                    .setSignalSourceType ( SignalSourceBuilder.SignalSourceType.simulator )
                    .setSimulatorType ( SignalSourceBuilder.SimulatorType.powerLineNoise )
                    .setSignalFileOriginType ( SignalSourceBuilder.SignalFileOriginType.internal_1 )
                    .setnChannels ( 2 )
                    .setSamplingFrequencyEnum ( SignalSourceBuilder.SamplingFrequencyEnum.value44100 )
                    .setAudioMixerDescriptionRegexp ( ".*" )
                    .setPeriodicResetIntervalInSeconds ( 7_200 )
                    .create ( ) );

        final MultiChannelVessel lSignalVesselsA = lSignalSource.createSignalVessels();
        final MultiChannelVessel lSignalVesselsB = lSignalSource.createSignalVessels();

        lSignalVesselsA.setNumberOfSamples ( 1_000 );
        lSignalVesselsB.setNumberOfSamples ( 0 );

        for (int i = 0; i < 10; i++)
        {
            final boolean lSamplesFitted = MultiChannelVessel.appendFromFirstIntoSecond ( lSignalVesselsA, lSignalVesselsB );
            logger.info("lSamplesFitted: " + lSamplesFitted);
        }

        lSignalSource.close();

        logger.info("test00 -- END");
    }
}
