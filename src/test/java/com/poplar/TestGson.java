/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.api.SignalChannelBuilder;
import com.poplar.monitor.api.SignalPaneBuilder;
import com.poplar.monitor.api.SignalSourceBuilder;
import com.poplar.monitor.imp.javafx.LanguageManager;
import com.poplar.monitor.imp.util.GsonUtil;
import com.poplar.monitor.imp.javafx.ApplicationConfig;
import com.poplar.monitor.imp.util.CollectionsUtil;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestGson
    extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass() );

    public TestGson ( String testName )
    {
        super ( testName );
    }

    public void test00 () throws Exception
    {
        LanguageManager.createSingleton ();

        final ApplicationConfig lConfig = new ApplicationConfig ( );

        lConfig.setSignalPaneBuilder (
            SignalPaneBuilder.init ( )
                .setTriggerChannelIndex ( 0 )
                .setMonitorStartAutomatically ( false )
                .setDisplayTimeMsPerDivision ( 0.3f )
                .setTriggerFreezeTimeInSeconds ( 1.0f )
                .setSweepModeEnum ( SignalPaneBuilder.SweepModeEnum.continuous )
                .setSignalChannelBuilders (
                    CollectionsUtil.createList (
                        SignalChannelBuilder
                            .init ( )
                            .setSignalName ( "Orbicularis Oculi" )
                            .setSensitivityMilliVoltsPerDiv ( 5 )
                            .setTriggerLevelPercentage ( 50.0 )
                            .setRgbAsStringColor ( "#ff0000" )
                            .create ( ),
                        SignalChannelBuilder.init ()
                            .setSignalName ( "Orbicularis Oris" )
                            .setSensitivityMilliVoltsPerDiv ( 5 )
                            .setTriggerLevelPercentage ( 50.0 )
                            .setRgbAsStringColor ( "#00ff00" )
                            .create () ) )
                .create () );

        lConfig.setSignalSourceBuilder (
            SignalSourceBuilder
                .init ()
                .setSignalSourceType ( SignalSourceBuilder.SignalSourceType.simulator )
                .setnChannels ( 2 )
                .setSamplingFrequencyEnum ( SignalSourceBuilder.SamplingFrequencyEnum.value8000 )
                .setAudioMixerDescriptionRegexp ( "" )
                .create () );

        for ( int i=0; i<10; i++ )
        {
            logger.info ( "" );
            logger.info ( "lConfig: " + lConfig );
            logger.info ( "lConfig: " + System.identityHashCode ( lConfig ) );

            final ApplicationConfig lCloned = GsonUtil.clone ( lConfig );
            logger.info ( "lCloned: " + lCloned );
            logger.info ( "lCloned: " + System.identityHashCode ( lCloned ) );

            Preconditions.checkState ( lConfig.toString ().equals ( lCloned.toString () ) );
            Preconditions.checkState ( System.identityHashCode ( lConfig ) != System.identityHashCode ( lCloned ) );
        }
    }
}
