/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.common.CircularQueue;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.concurrent.atomic.AtomicLong;

public class TestCircularQueue
    extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    public TestCircularQueue ( String testName )
    {
        super ( testName );
    }

    public void test00 () throws Exception
    {
        final int CAPACITY = 3;

        final CircularQueue<String> circularQueue = CircularQueue.<String>create ().setSlots ( new String[CAPACITY] );

        Preconditions.checkState ( circularQueue.isEmpty ( ) );
        Preconditions.checkState ( ! circularQueue.isFull ( ) );
        Preconditions.checkState ( circularQueue.size () == 0 );
        Preconditions.checkState ( circularQueue.capacity ( ) == CAPACITY );
    }

    private static class StringSlotFiller implements CircularQueue.ISlotFiller<StringBuilder>
    {
        private String data;

        private StringSlotFiller ()
        {
        }

        public StringSlotFiller set ( String aInData )
        {
            data = aInData;
            return this;
        }

        @Override
        public void fill ( StringBuilder input )
        {
            input.setLength ( 0 );
            input.append ( data );
        }
    }

    public void test01 () throws Exception
    {
        final int CAPACITY = 10;

        final CircularQueue<StringBuilder> circularQueue =  CircularQueue.<StringBuilder>create ().setSlots ( new StringBuilder[CAPACITY] );

        final CircularQueue.ISlotInitializer<StringBuilder> lISlotInitializer = ( ) -> new StringBuilder ( "-" );

        logger.info( "circularQueue(1): " + circularQueue );

        circularQueue.initializeSlots ( lISlotInitializer );
        logger.info( "circularQueue(2): " + circularQueue );

        final StringSlotFiller filler = new StringSlotFiller ();

        circularQueue.put ( filler );
        logger.info ( "circularQueue(3): " + circularQueue );

        Preconditions.checkState ( ! circularQueue.isEmpty ( ) );
        Preconditions.checkState ( ! circularQueue.isFull ( ) );
        Preconditions.checkState ( circularQueue.size () == 1 );
        Preconditions.checkState ( circularQueue.capacity ( ) == CAPACITY );

        final StringBuilder x = circularQueue.consume ( );
        logger.info( "x: [" + x + "]" );
        logger.info( "circularQueue(4): " + circularQueue );

        Preconditions.checkState ( circularQueue.isEmpty ( ) );
        Preconditions.checkState ( ! circularQueue.isFull ( ) );
        Preconditions.checkState ( circularQueue.size () == 0 );
        Preconditions.checkState ( circularQueue.capacity ( ) == CAPACITY );
    }

    private static class AtomicLongSlotFiller implements CircularQueue.ISlotFiller<AtomicLong>
    {
        private AtomicLong data;

        private AtomicLongSlotFiller ()
        {
        }

        public AtomicLongSlotFiller set ( long aInData )
        {
            data = new AtomicLong ( aInData );
            return this;
        }

        @Override
        public void fill ( AtomicLong input )
        {
            input.set( data.longValue () );
        }
    }

    public void test02 () throws Exception
    {
        final int CAPACITY = 10;

        final CircularQueue<AtomicLong> circularQueue =  CircularQueue.<AtomicLong>create ().setSlots ( new AtomicLong[CAPACITY] );

        final CircularQueue.ISlotInitializer<AtomicLong> lISlotInitializer = ( ) -> new AtomicLong ( -1 );

        logger.info( "circularQueue(1): " + circularQueue );

        circularQueue.initializeSlots ( lISlotInitializer );
        logger.info( "circularQueue(2): " + circularQueue );

        final AtomicLongSlotFiller filler = new AtomicLongSlotFiller ( );

        for ( int k=0; k<100; k++ )
        {
            for ( int i = 0; i < 20; i++ )
            {
                if ( circularQueue.isFull ( ) )
                {
                    logger.info ( "queue is full" );
                }

                else
                {
                    logger.info ( "queue is not full" );
                    circularQueue.put ( filler.set( i ) );
                }
            }

            logger.info ( "circularQueue(3): " + circularQueue );

            Preconditions.checkState ( ! circularQueue.isEmpty ( ) );
            Preconditions.checkState ( circularQueue.isFull ( ) );
            Preconditions.checkState ( circularQueue.size ( ) == circularQueue.capacity ( ) - 1 );
            Preconditions.checkState ( circularQueue.capacity ( ) == CAPACITY );

            while ( ! circularQueue.isEmpty () )
            {
                final AtomicLong out = circularQueue.consume ( );
                logger.info ( "out: " + out );
            }

            logger.info ( "circularQueue(4): " + circularQueue );
            Preconditions.checkState ( circularQueue.isEmpty ( ) );
            Preconditions.checkState ( ! circularQueue.isFull ( ) );
            Preconditions.checkState ( circularQueue.size ( ) == 0 );
            Preconditions.checkState ( circularQueue.capacity ( ) == CAPACITY );
        }
    }

    public void test03 () throws Exception
    {
        final int CAPACITY = 5;

        final CircularQueue<StringBuilder> circularQueue = CircularQueue
            .<StringBuilder>create ()
            .setSlots ( new StringBuilder[CAPACITY] )
            .setAllowDataOverrun ( true )
            .initializeSlots ( ( ) -> new StringBuilder ( "-" ) );

        Preconditions.checkState ( circularQueue.isEmpty ( ) );
        Preconditions.checkState ( ! circularQueue.isFull ( ) );
        Preconditions.checkState ( circularQueue.size () == 0 );
        Preconditions.checkState ( circularQueue.capacity ( ) == CAPACITY );

        final StringSlotFiller filler = new StringSlotFiller();
        circularQueue.put( filler.set ( "1" ) );
        circularQueue.put( filler.set ( "2" ) );
        circularQueue.put( filler.set ( "3" ) );
        circularQueue.put( filler.set ( "4" ) );
        circularQueue.put( filler.set ( "5" ) );
        circularQueue.put( filler.set ( "6" ) );
        circularQueue.put( filler.set ( "7" ) );
        circularQueue.put( filler.set ( "8" ) );
        logger.info( "size: " + circularQueue.size() );
        logger.info( "capacity: " + circularQueue.capacity () );

        final StringBuilder a = circularQueue.peekFirst ( 0 );
        final StringBuilder b = circularQueue.peekFirst ( 1 );
        logger.info( "a: " + a );
        logger.info( "b: " + b );
        Preconditions.checkState ( a.toString ( ).equals ( "8" ) );
        Preconditions.checkState ( b.toString ( ).equals ( "7" ) );

        final StringBuilder c = circularQueue.peekLast ( 0 );
        final StringBuilder d = circularQueue.peekLast ( 1 );
        logger.info( "c: " + c );
        logger.info( "d: " + d );
        Preconditions.checkState ( c.toString ().equals ( "5" ) );
        Preconditions.checkState ( d.toString ( ).equals ( "6" ) );
    }
}
