/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.google.common.base.Preconditions;
import com.poplar.monitor.imp.util.CollectionsUtil;
import org.apache.commons.math3.complex.Complex;
import com.poplar.monitor.imp.digitalfilters.linear.causal.KaiserWindow;
import com.poplar.monitor.imp.util.FileUtil;
import junit.framework.TestCase;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;
import org.apache.commons.math3.util.ArithmeticUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.FileUtils;

import java.io.File;
import java.io.PrintStream;

public class TestKaiser extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    public TestKaiser ( String testName )
    {
        super ( testName );
    }

    public void test00 ( )
        throws Exception
    {
        final double[] impulseResponse = KaiserWindow.computeKaiserWindow (
            KaiserWindow.PassTypeEnum.lowPass,
            10.0,
            40,
            0.1,
            1.5,
            2.5,
            null
        );

        iPlot ( "f[00]", impulseResponse );
    }

    public void test02 ( )
        throws Exception
    {
        final double[] impulseResponse = KaiserWindow.computeKaiserWindow (
            KaiserWindow.PassTypeEnum.bandStop,
            44100.0,
            25.0,
            2.0,
            40.0,
            20.0,
            80.0
        );

        iPlot ( "f[02]", impulseResponse );
    }

    public void test03 ( )
        throws Exception
    {
        final double[] impulseResponse = KaiserWindow.computeKaiserWindow (
            KaiserWindow.PassTypeEnum.lowPass,
            44100.0,
            50.0,
            0.1,
            980.0,
            990.0,
            null
        );

        iPlot ( "f[03]", impulseResponse );
    }

    private void iPlot ( String tag, double[] aInImpulseResponse ) throws Exception
    {
        logger.info( tag + " - BEGIN" );

        logger.info( String.format( "length:%03d", aInImpulseResponse.length ) );

        final double lImpulseResponse[] = padWithZeros ( aInImpulseResponse, 2048 );

        final FastFourierTransformer fft = new FastFourierTransformer ( DftNormalization.STANDARD );
        final Complex lSpectrumFFT[] = fft.transform ( lImpulseResponse, TransformType.FORWARD );
        final Complex lSpectrumDFT[] = dft ( fromRealPart ( lImpulseResponse ) );

        //final double lSpectrumDFTMod[] = module ( lSpectrumDFT );
        final double lSpectrumFFTMod[] = module ( lSpectrumFFT );

        FileUtils.mkdir ( new File("./tmp"), true );

        try (
            final PrintStream lP1 = FileUtil.createPrintStreamFromFile ( new File ( "./tmp/impulse.csv" ) );
            final PrintStream lP2 = FileUtil.createPrintStreamFromFile ( new File ( "./tmp/spectrum.csv" ) )
        )
        {
            logger.info ( "lImpulseResponse.length: "+ lImpulseResponse.length );

            for ( int i = 0; i < lImpulseResponse.length; i++ )
            {
                logger.info ( String.format ( "lImpulseResponse: %03d - %sf", i, lImpulseResponse[i] ) );
                lP1.println ( lImpulseResponse[i] );
            }

            for ( int i = 0; i < lSpectrumFFTMod.length; i++ )
            {
                logger.info ( String.format ( "lSpectrumFFTMod: %03d - %sf", i, lSpectrumFFTMod[i] ) );
                lP2.println ( lSpectrumFFTMod[i] );
            }
        }

        logger.info( tag + " - END" );
    }

    public void test04()
    {
        final double[] a = { 1, 2, 3, 4, 5 };
        final double[] b = padWithZerosToNextPowerOfTwo( a );

        logger.info( "a:" + CollectionsUtil.createList ( a ).toString () );
        logger.info( "b:" + CollectionsUtil.createList ( b ) );
        logger.info( "b:" + CollectionsUtil.createList ( b ) );
    }

    public void test05()
    {
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 1 ) == 1 );
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 2 ) == 2 );
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 3 ) == 4 );
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 4 ) == 4 );
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 5 ) == 8 );
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 127 ) == 128 );
        Preconditions.checkState ( getNextGreaterPowerOfTwo( 128 ) == 128 );
    }

    private static int getNextGreaterPowerOfTwo( int value )
    {
        Preconditions.checkArgument ( value > 0 );
        Preconditions.checkArgument ( value < 0x800_0001L );

        int ret;

        if ( ArithmeticUtils.isPowerOfTwo ( value ) )
        {
            ret = value;
        }

        else
        {
            ret = 1;

            final int max = 0x800_0000;

            for ( int i = 0; i < 30; i++ )
            {
                final int mask = max >> i;

                if ( ( mask & value ) != 0 )
                {
                    ret = 2 * mask;
                    break;
                }
            }
        }

        return ret;
    }

    private static double[] padWithZeros( double[] x, int padLength )
    {
        Preconditions.checkArgument ( ArithmeticUtils.isPowerOfTwo ( padLength ) );
        Preconditions.checkArgument ( padLength > x.length );

        final double[] ret = new double[ padLength ];    // the jvm set **all** elements to zero

        System.arraycopy ( x, 0, ret, 0, x.length );

        return ret;
    }

    private static double[] padWithZerosToNextPowerOfTwo( double[] x )
    {
        return padWithZeros( x, getNextGreaterPowerOfTwo ( x.length ) );
    }

    private static Complex[] fromRealPart ( double[] in )
    {
        final Complex ret[] = new Complex[in.length];

        for ( int i=0; i<in.length; i++ )
        {
            ret[i] = new Complex ( in[i], 0 );
        }

        return ret;
    }
    private static double[] module ( Complex[] in )
    {
        final double ret[] = new double[in.length];

        for ( int i=0; i<in.length; i++ )
        {
            ret[i] = in[i].abs ( );
        }

        return ret;
    }

    private static Complex[] dft ( Complex[] in )
    {
        final Complex dft[] = new Complex[in.length];

        final double pieceOfPi = 2.0 * Math.PI / in.length;

        for ( int i=0; i<in.length; i++ )
        {
            dft[i] = Complex.ZERO;

            for ( int k=0; k<in.length; k++ )
            {
               dft[i] = dft[i].add ( new Complex( 0, i * k * pieceOfPi ).exp ( ).multiply ( in[i] ) );
            }
        }

        return dft;
    }
}
