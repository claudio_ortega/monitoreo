/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import com.poplar.monitor.imp.digitalfilters.ICausalDigitalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.CausalFilters;
import com.poplar.monitor.imp.digitalfilters.linear.causal.PolesOnlyCausalFilter;
import com.poplar.monitor.imp.digitalfilters.linear.causal.ZerosOnlyCausalFilter;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFraction;
import com.poplar.monitor.imp.digitalfilters.polynomialfraction.PolynomialFractionEvaluator;
import junit.framework.TestCase;
import org.apache.commons.math3.complex.Complex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TestCausalFilter extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    public TestCausalFilter ( String testName )
    {
        super ( testName );
    }

    public void test00 ( )
    {
        final double s0 = 1;
        final double fs = 44_100;

        final PolynomialFraction polynomialFraction = PolynomialFraction.create ( ).addZero ( 0 ).addPole ( 1.0 - ( s0 / fs ) );

        logger.info ( "polynomialFraction: " + polynomialFraction );
        logger.info ( "equation: " + polynomialFraction.printEquation ( ) );

        final Complex z1 = polynomialFraction.evalaluateOnASinglePoint ( Complex.ZERO );
        logger.info ( "z1: " + z1 );

        final Complex z2 = polynomialFraction.evalaluateOnASinglePoint ( Complex.ONE );
        logger.info ( "z2: " + z2 );

        final Complex z3 = polynomialFraction.evalaluateOnASinglePoint ( Complex.I );
        logger.info ( "z3: " + z3 );

        final Complex z4 = polynomialFraction.evalaluateOnASinglePoint ( Complex.I.conjugate ( ) );
        logger.info ( "z4: " + z4 );

        final Complex z5 = polynomialFraction.evalaluateOnASinglePoint ( Complex.ONE.negate ( ) );
        logger.info ( "z5: " + z5 );

        final List<Complex> dft = PolynomialFractionEvaluator.evaluateDFT ( polynomialFraction, 8 );
        logger.info ( "dft: " + dft );

        final ICausalDigitalFilter filter = CausalFilters.createFromPolynomialFraction ( 1.0, polynomialFraction );
        logger.info ( "filter: " + filter );
    }

    public void test01 ( )
    {
        final ICausalDigitalFilter filter = CausalFilters.createCascade (
            ZerosOnlyCausalFilter.create ( new double[]{ 1.0, 0.0, 0.0, - 0.125 } ),
            PolesOnlyCausalFilter.create ( new double[]{ 1.0, - 0.5 } )
        );

        logger.info ( "filter: " + filter );

        for ( int i = 0; i < 10; i++ )
        {
            final double y = filter.filter ( i == 0 ? 1 : 0 );
            logger.info ( "i: " + i + ", y:" + y );
        }
    }
}
