/*
 *     Facha - The Facial Nerve Monitoring System
 *     Copyright (C) 2017-8 by PERA Labs
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     A copy of the GPLv3 License is in the file LICENSE.txt
 */

package com.poplar;

import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestFloatVsDouble extends TestCase
{
    private final Logger logger = LogManager.getLogger ( getClass ( ) );

    public TestFloatVsDouble ( String testName )
    {
        super ( testName );
    }

    public void test01 ( ) throws Exception
    {
        testDoubles ( 2_000_000_000L );
    }

    public void test02 ( ) throws Exception
    {
        testFloats ( 2_000_000_000L );
    }

    private void testDoubles ( long iter ) throws Exception
    {
        logger.info ( "DOUBLE BEGIN" );

        final double q = 0.99;

        double a = 1.0;

        for ( long i=0; i<iter; i++ )
        {
            a = a * q + ( a * q * 0.001 );

            if ( i % 1_000 == 0 )
            {
                a = 1.0;
            }
        }

        logger.info ( "DOUBLE END" );
    }

    private void testFloats ( long iter ) throws Exception
    {
        logger.info ( "FLOAT BEGIN" );

        final float q = 0.99f;

        float a = 1.0f;

        for ( long i=0; i<iter; i++ )
        {
            a = a * q + ( a * q * 0.001f );

            if ( i % 1_000 == 0 )
            {
                a = 1.0f;
            }
        }

        logger.info ( "FLOAT END" );
    }
}
