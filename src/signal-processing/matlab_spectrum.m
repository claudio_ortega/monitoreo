%
%      Facha - The Facial Nerve Monitoring System
%      Copyright (C) 2017-8 by PERA Labs
% 
%      This program is free software; you can redistribute it and/or modify
%      it under the terms of the GNU General Public License as published by
%      the Free Software Foundation; either version 3 of the License, or
%      (at your option) any later version.
% 
%      This program is distributed in the hope that it will be useful,
%      but WITHOUT ANY WARRANTY; without even the implied warranty of
%      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%      GNU General Public License for more details.
% 
%      A copy of the GPLv3 License is in the file LICENSE.txt
%
%
%     This file computes the power spectrum from samples in a file
%
%     Reference:
%     [1] Discrete Time Signal Processing 2nd Edition - Oppenheim & Schaffer - Chapter 10

% we got 1 out of 10 samples from the surgery, too bad ..
fs = 44100.0 / 10.0;

% we want a resolution of 1 Hz, and a leakage less than 80 db between consecutive spectrum sampled bins
AttSideLobeDb = 80.0;
DeltaMainLobeInHz = 1.0;

% normalize to 2*pi
DeltaMainLobe = DeltaMainLobeInHz * ( 2.0 * pi ) / fs;

% formula 10.14 in [1]
kaiser_length = ceil ( ( 24 * pi * ( AttSideLobeDb + 12 ) / ( 155 * DeltaMainLobe ) ) + 1 );

% formula 10.13 in [1]
beta = 0.12438 * ( AttSideLobeDb + 6.3 );

% taking the second/third next power of two so to get an oversampling of
% about four times, so to not to miss peaks
padded_length = 1024 * 128;

% formula 10.12 in [1], kaiser(.) makes it already causal (shifted, no negative indices)
kaiser_window = kaiser ( kaiser_length, beta )';  % the last ' means trasposed
kaiser_size = size( kaiser_window );

assert ( kaiser_size (1) == 1 );
assert ( numel( kaiser_window ) <= padded_length );

% zero pad the window
padded_kaiser_window = [kaiser_window, zeros( 1, padded_length - numel( kaiser_window ) ) ];
assert ( numel( padded_kaiser_window ) == padded_length );

% read signal
file_data = load ( '/Users/clortega/git/poplar/monitoreo/src/signal-processing/signal33.dat' );

% ignore time and sample count columns, get only the third ..
signal = file_data(:,3)';
assert ( numel( signal ) <= padded_length );

% zero pad the signal as well
padded_signal = [signal, zeros( 1, padded_length - numel( signal ) ) ];

% do a sample by sample multiplication of both: padded signal and window
windowed_padded_signal = padded_signal .* padded_kaiser_window;

% get the spectrum' modulus
assert ( numel( windowed_padded_signal ) == padded_length );
spectrum = fft ( windowed_padded_signal );
mod_spectrum = abs( spectrum );

% take only the first half for 0-pi
mod_spectrum = mod_spectrum ( 1: numel (mod_spectrum)/2 );

% time dimension in samples
n = 0:padded_length-1;

% freq dimension in samples
k = 0:padded_length/2-1;

% scaling in time and omega
tInSecs = n / fs;
omegaNorm = k * ( 2.0 * pi / padded_length );
freqInHz = ( omegaNorm * fs ) / ( 2.0 * pi );

% integrate and scale to 100%  (99.99 makes it look nicer)
cumulative_spectrum = cumsum ( mod_spectrum );
max_cumulative = cumulative_spectrum( numel( cumulative_spectrum ) );
cumulative_spectrum_scaled = ( 99.99 / max_cumulative ) * cumulative_spectrum;

% plot signal 
subplot (3,1,1);
plot( tInSecs, padded_signal );
grid;
xlabel ( 'time in seconds'  );
ylabel ( 'signal in sample space' );

% plot spectrum
subplot (3,1,2);
plot( freqInHz, mod_spectrum );
grid;
xlabel ( 'Hz' );
ylabel ( 'spectrum' );

% plot cumulative energy 
subplot (3,1,3);
plot( freqInHz, cumulative_spectrum_scaled, 'LineWidth', 2 );
grid;
xlabel ( 'Hz' );
ylabel ( 'cumulative energy' );


