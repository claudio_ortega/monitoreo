%
%      Facha - The Facial Nerve Monitoring System
%      Copyright (C) 2017-8 by PERA Labs
%
%      This program is free software; you can redistribute it and/or modify
%      it under the terms of the GNU General Public License as published by
%      the Free Software Foundation; either version 3 of the License, or
%      (at your option) any later version.
%
%      This program is distributed in the hope that it will be useful,
%      but WITHOUT ANY WARRANTY; without even the implied warranty of
%      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%      GNU General Public License for more details.
%
%      A copy of the GPLv3 License is in the file LICENSE.txt
%
%
%     This file computes the power spectrum from samples in a file
% 
%     Reference:
%     [1] Discrete Time Signal Processing 2nd Edition - Oppenheim & Schaffer - Chapter 10

% pkg load io
% pkg load control
% pkg load signal
% pkg load image

% we got 1 out of 10 samples on the initial test
fs = 44100.0 / 10.0;

% we want resolution of 1 Hz, and leakage less than 80 db between consecutive fft bins
AttSideLobeDb = 80.0;
DeltaMainLobeInHz = 1.0;

% 
time_norm_factor = ( 2.0 * pi ) / fs;

% normalize to 2*pi
DeltaMainLobe = DeltaMainLobeInHz * time_norm_factor;

% [1] formula 10.14
kaiser_length = ceil ( ( 24 * pi * ( AttSideLobeDb + 12 ) / ( 155 * DeltaMainLobe ) ) + 1 );

% [1] formula 10.13
beta = 0.12438 * ( AttSideLobeDb + 6.3 );

% taking the second/third next power of two so to get an oversampling of about four times
% so to not to miss the peaks
padded_length = 1024 * 128;

% [1] formula 10.12, already causal
kaiser_window = kaiser ( kaiser_length, beta )';
assert ( size(kaiser_window)(1)  == 1 );
assert ( numel( kaiser_window ) <= padded_length );

% zero pad the window
padded_kaiser_window = [kaiser_window, zeros( 1, padded_length - numel( kaiser_window ) ) ];
assert ( numel( padded_kaiser_window ) == padded_length );

% read signal
file_data = load ( "/Users/clortega/git/poplar/monitoreo/src/signal-processing/signal33.dat" );

% get the third column
signal = file_data(:,3)';
assert ( numel( signal ) <= padded_length );

% zero pad the signal as well
padded_signal = [signal, zeros( 1, padded_length - numel( signal ) ) ];

% sample by sample multiplication of both padded signal and window
windowed_padded_signal = padded_signal .* padded_kaiser_window;

% getting the spectrum
assert ( numel( windowed_padded_signal ) == padded_length );
spectrum = fft ( windowed_padded_signal );
mod_spectrum = abs( spectrum );
mod_spectrum_db = 20.0 * log10 ( mod_spectrum );

% time dimension in samples
n = 0:1:padded_length-1;
k = 0:1:padded_length-1;

tInSecs = n / fs;
omegaNorm = k * ( 2.0 * pi / padded_length );
fInHz = ( omegaNorm * fs ) / ( 2.0 * pi );

% plotting signal 1
subplot (2,1,1);
plot( tInSecs, padded_signal );
grid;
xlabel ( "time in seconds" );
ylabel ( "signal in sample space" );

% plotting denormalized spectrum
subplot (2,1,2);
plot( fInHz, mod_spectrum );
grid;
xlabel ( "Hz" );
ylabel ( "spectrum" );


