;
;      Facha - The Facial Nerve Monitoring System
;      Copyright (C) 2017-8 by PERA Labs
; 
;      This program is free software; you can redistribute it and/or modify
;      it under the terms of the GNU General Public License as published by
;      the Free Software Foundation; either version 3 of the License, or
;      (at your option) any later version.
; 
;      This program is distributed in the hope that it will be useful,
;      but WITHOUT ANY WARRANTY; without even the implied warranty of
;      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;      GNU General Public License for more details.
; 
;      A copy of the GPLv3 License is in the file LICENSE.txt
; 

#define MyAppName "Monitor Facial"
#define MyAppVersion "1.0"
#define MyAppPublisher "PERA Labs"
#define MyAppURL "http://www.peralabs.com/"
#define MyAppExeName "monitor-app.bat"

[Setup]
AppId={{0DBA291A-14EC-4606-A231-48688253EC15}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={userpf}\Monitor Facial
DefaultGroupName={#MyAppName}
OutputDir=..\..\target
OutputBaseFilename=setup
Compression=lzma
SolidCompression=yes
ShowLanguageDialog=no

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}";
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; OnlyBelowVersion: 0,6.1

[Files]
Source: "..\..\build\install\bin\monitor-app.bat"; DestDir: "{app}\bin"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "..\..\build\install\lib\*"; DestDir: "{app}\lib"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "..\..\build\install\resources\*"; DestDir: "{app}\resources"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\bin\{#MyAppExeName}"; IconFilename: "{app}\resources\app_icon.ico"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\bin\{#MyAppExeName}"; Tasks: desktopicon; IconFilename: "{app}\resources\app_icon.ico"
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\bin\{#MyAppExeName}"; IconFilename: "{app}\resources\app_icon.ico"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\bin\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: shellexec postinstall skipifsilent

[UninstallDelete]
Type: filesandordirs; Name: "{app}\bin"
Type: dirifempty; Name: "{app}"

[CustomMessages]
english.key1=VERY IMPORTANT
spanish.key1=MUY IMPORTANTE      
english.key2=This application needs a java runtime environment (JRE) installed in this computer.
spanish.key2=Esta aplicacion necesita de una instalacion de java runtime environment (JRE) en esta computadora.
english.key3=To verify the requirement, please execute ''java -version'' in a command window. If java is not installed or the version is prior to 1.8.0_60, then please download and install the latest java 8 JRE image from oracle.com before continuing with this installation. For that purpose, you may google for ''java 8 jre download'' to get the proper link to download from.
spanish.key3=Para verificar este requerimiento, por favor ejecute ''java -version'' en una ventana de comandos. Si el interprete java no estuviera instalado o si la version fuera previa a la 1.8.0_60 entonces por favor descargue e instale la ultima version java 8 JRE desde oracle.com antes de continuar con esta instalacion. Para obtener el link desde el cual realizar la descarga puede consultar en google: ''java 8 jre download''.
english.key4=I do not understand quite well what to do.
spanish.key4=No entiendo bien lo que debo hacer.
english.key5=I understand what to do and everything looks correct.
spanish.key5=Entiendo bien lo que debo hacer y todo parece estar en orden.

[Code]
var
  JavaMsgPage: TInputOptionWizardPage;
  
procedure InitializeWizard;
begin

  JavaMsgPage := CreateInputOptionPage(
    2,
    CustomMessage( 'key1' ),
    CustomMessage( 'key2' ),
    CustomMessage( 'key3' ),
    True,
    False );

    JavaMsgPage.Add( CustomMessage( 'key4' ) );
    JavaMsgPage.Add( CustomMessage( 'key5' ) );
   
end;


function NextButtonClick(CurPageID: Integer): Boolean;

begin

  { 100 is the java page }
  if CurPageId = 100 then
    if JavaMsgPage.SelectedValueIndex = 1 then
      Result := True
    else
      Result := False
    
  else 
    Result := True

end;






