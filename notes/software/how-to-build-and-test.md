# Build with no tests and no installers #

    gradle clean build installApp -x test

then, you can run the app locally from withinn the same build workspace:

    ./build/install/bin/monitor-app.sh [--locale=es]

# To execute test cases #

a single test:

    gradle test -Dtest.single=TestBlah

or for regression test:

    gradle clean test -Dtest.single=TestRegressionSuite


# To do a complete build #

To do a complete build you need to also create the windows installer.
The windows installer could be only built using a windows executable.
Is then possible to do that inside a linux running wine,
and that is the way we do it.
You can build the application in any operating system that supports Docker.
For example, you may build and test entirely inside MAC OS X Sierra 10.12.6,
or inside a linux box, as long as it has Docker Server 18.06.1-ce installed.
The app is built in the bitbucket/pipelines CI service,
which uses the same idea, that is:
all build happen inside a Docker instance.

# Build, tag and push the Docker image #

The build should happen inside a Docker container.
We need a Docker image with the needed components
already installed. We build such image in this section.

(1) build the build Docker image from the specification
in the Dockerfile

    docker build \
        -f ci/Docker/Dockerfile \
        -t ubuntu:18.04.facha \
        .

(2) tag the new image and push it up to the Docker hub account,
so it could be later used from bitbucket/pipelines for each build,

    docker login --username=claudioortega
    (ingress password)
    docker tag ubuntu:18.04.facha claudioortega/ubuntu:18.04.develop
    docker push claudioortega/ubuntu:18.04.develop

note that the name:tag should match with those in

    ./bitbucket-pipelines.yml

# Build the application #

(1) start the Docker container

    docker run \
        --rm -it -w $(pwd) \
        -v $HOME:$HOME \
        claudioortega/ubuntu:18.04.develop

(2) execute inside the container:

    ./ci/build_all.bash

ignore error messages coming out from wine.



