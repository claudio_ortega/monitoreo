#!/usr/bin/env bash

#
#      Facha - The Facial Nerve Monitoring System
#      Copyright (C) 2017-8 by PERA Labs
# 
#      This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
# 
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
# 
#      A copy of the GPLv3 License is in the file LICENSE.txt

ci/build_application.bash ${*}
ci/build_installers.bash

if [[ ($# -eq 0) ]]; then
    echo "detected a local build, skipping to upload artifacts"
else
    echo "detected a ci server build, proceeding to upload artifacts"
    ci/upload_installers.bash
    echo "artifacts uploaded"
fi

