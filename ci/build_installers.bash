#!/usr/bin/env bash

#
#      Facha - The Facial Nerve Monitoring System
#      Copyright (C) 2017-8 by PERA Labs
#
#      This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      A copy of the GPLv3 License is in the file LICENSE.txt
#


#      A precondition is that wine should be installed
#      To install wine use apt-get, with this command:
#           apt-get install wine
#           apt-get install zip
#
#


echo "build_installers -- START"

GIT_DESCRIBE=$(cat src/main/resources/com/poplar/monitor/imp/javafx/git-describe.txt)

# seems innocuous, but this echo allows some jenkins plugin we are using
# to be able label git descriptions onto the job results
echo "GIT_DESCRIBE:"${GIT_DESCRIBE}
echo "current directory:"$(pwd)

# we create a installer only for windows
# for that we use the app Inno Setup App on its command line mode
# this will uncompress Inno into the directory ./ci/inno/isolated-inno
unzip -q -o ./ci/inno/isolated-inno.zip -d ./ci/inno

# this will produce the windows executable into the path ./target/setup.exe
rm -rf ./target
wine ./ci/inno/isolated-inno/ISCC.exe src/inno-setup/inno-setup.iss /Q
mv ./target/setup.exe ./target/install-facha-windows-${GIT_DESCRIBE}.exe

# for all platforms: windows, linux, mac
zip -q -r ./target/install-facha-unix-and-mac-${GIT_DESCRIBE}.zip ./build/install

#
# we create a descriptor file for the jenkins job output
RELEASE_DESCRIPTOR_FILE=./target/release-content-${GIT_DESCRIBE}.txt
echo 'release content ---------------------------------------------------' >> ${RELEASE_DESCRIPTOR_FILE}
echo ${GIT_DESCRIBE} >> ${RELEASE_DESCRIPTOR_FILE}
echo 'sizes ------------------------------------------------------' >> ${RELEASE_DESCRIPTOR_FILE}
du -kb ./target/install-*.* >> ${RELEASE_DESCRIPTOR_FILE}
echo 'md5 --------------------------------------------------------' >> ${RELEASE_DESCRIPTOR_FILE}
md5sum ./target/install-* >> ${RELEASE_DESCRIPTOR_FILE}
cat -n ${RELEASE_DESCRIPTOR_FILE}

echo "build_installers -- END"
